﻿using Cmms.Business.ComponentServices;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Business.ComponentServices.NotificationStrategies;
using Cmms.Business.ComponentServices.SearchDeviceReportStrategies;
using Cmms.Business.DataContracts;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;

namespace Cmms.Business
{
    public class Bootstrapper
    {
        public static void RegisterServiceComponents(IUnityContainer container)
        {
            Data.Bootstrapper.RegisterDependencies(container);
            RegisterServices(container);
            var serviceLocator = new UnityServiceLocator(container);
            ServiceLocator.SetLocatorProvider(() => serviceLocator);
        }

        private static void RegisterServices(IUnityContainer container)
        {
            container.RegisterType<ILogService, LogService>()
                .RegisterType<IUserService, UserService>()
                .RegisterType<IRequisitionService, RequisitionService>()
                .RegisterType<IOrganizationService, OrganizationService>()
                .RegisterType<IRequisitionCommandService, RequisitionCommandService>()
                .RegisterType<IOrderService, OrderService>()
                .RegisterType<IOrderCommandService, OrderCommandService>()
                .RegisterType<IPicklistService, PicklistService>()
                .RegisterType<IPicklistCommandService, PicklistCommandService>()
                .RegisterType<IDeviceService, DeviceService>()
                .RegisterType<IImportExportHistoryService, ImportExportHistoryService>()
                .RegisterType<IBranchService, BranchService>()
                .RegisterType<IProviderService, ProviderService>()
                .RegisterType<ICompanyService, CompanyService>()
                .RegisterType<IDeviceStatisticProviderService, DeviceStatisticProviderService>()
                .RegisterType<IDeliveryDeviceReportService, DeliveryDeviceReportService>()
                .RegisterType<IInventoryDeviceReportService, InventoryDeviceReportService>()
                .RegisterType<IDeliveryGoodDeviceReportService, DeliveryGoodDeviceReportService>()
                .RegisterType<IDeliveryNffDeviceReportService, DeliveryNffDeviceReportService>()
                .RegisterType<IDeliveryFailureDeviceReportService, DeliveryFailureDeviceReportService>()
                .RegisterType<INotDeliveryDeviceReportService, NotDeliveryDeviceReportService>()
                .RegisterType<IRefusedHTKTDeviceReportService, RefusedHTKTDeviceReportService>()
                .RegisterType<IDeliverySLADeviceReportService, DeliverySLADeviceReportService>()
                .RegisterType<IRerepairDeviceReportService, RerepairDeviceReportService>()
                .RegisterType<IOverRepairTimeDeviceReportService, OverRepairTimeDeviceReportService>()
                .RegisterType<INotificationService, NotificationService>()
                .RegisterType<IAnalyzeReportService, AnalyzeReportService>()
                .RegisterType<IEntityStatusService, EntityStatusService>()
                .RegisterType<IImportExportDeviceReportService, ImportExportDeviceReportService>()
                .RegisterType<IAwaitingItemNotificationService, AwaitingItemNotificationService>()
                .RegisterType<INotificationStrategyService, OrderNotificationStrategyService>("R5ORDERS")
                .RegisterType<INotificationStrategyService, PicklistNotificationStrategyService>("R5PICKLISTS")
                .RegisterType<INotificationStrategyService, RequisitionNotificationStrategyService>("R5REQUISITIONS")
                .RegisterType<ISearchDeviceForReportingService, BaoCaoThietBiDaTiepNhanTuDVQLSDStrategy>(DeviceReportType.BaoCaoThietBiDaTiepNhanTuDVQLSD.ToString())
                .RegisterType<ISearchDeviceForReportingService, BaoCaoThietBiBanGiaoChoDVQLSDStrategy>(DeviceReportType.BaoCaoThietBiBanGiaoChoDVQLSD.ToString())
                .RegisterType<ISearchDeviceForReportingService, BaoCaoThietBiTonKhoStrategy>(DeviceReportType.BaoCaoThietBiTonKho.ToString())
                .RegisterType<ISearchDeviceForReportingService, BaoCaoThietBiTotSauSuaChuaGiaoChoDVQLSDStrategy>(DeviceReportType.BaoCaoThietBiTotSauSuaChuaGiaoChoDVQLSD.ToString())
                .RegisterType<ISearchDeviceForReportingService, BaoCaoThietBiNFFGiaoChoDVQLSDStrategy>(DeviceReportType.BaoCaoThietBiNFFGiaoChoDVQLSD.ToString())
                .RegisterType<ISearchDeviceForReportingService, BaoCaoThietBiKoTheSuaGiaoChoDVQLSDStrategy>(DeviceReportType.BaoCaoThietBiKoTheSuaGiaoChoDVQLSD.ToString())
                .RegisterType<ISearchDeviceForReportingService, BaoCaoThietBiDoiTacChuaBanGiaoStrategy>(DeviceReportType.BaoCaoThietBiDoiTacChuaBanGiao.ToString())
                .RegisterType<ISearchDeviceForReportingService, BaoCaoThietBiDoiTacTuChoiHTKTStrategy>(DeviceReportType.BaoCaoThietBiDoiTacTuChoiHTKT.ToString())
                .RegisterType<ISearchDeviceForReportingService, BaoCaoThietBiSLADaBanGiaoStrategy>(DeviceReportType.BaoCaoThietBiSLADaBanGiao.ToString())
                .RegisterType<ISearchDeviceForReportingService, BaoCaoThietBiQuayLaiSuaChuaStrategy>(DeviceReportType.BaoCaoThietBiQuayLaiSuaChua.ToString())
                .RegisterType<ISearchDeviceForReportingService, BaoCaoThietBiQuaThoiGianSuaChuaStrategy>(DeviceReportType.BaoCaoThietBiQuaThoiGianSuaChua.ToString())
                .RegisterType<IDeviceRepairService, DeviceRepairService>()
                .RegisterType<IDocumentService, DocumentService>();
        }
    }
}
