﻿using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Business.DataContracts.DeviceStatistic;
using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cmms.Business.ComponentServices
{
    public class AnalyzeReportService : IAnalyzeReportService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;

        public AnalyzeReportService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public DeviceReportByYearResponse GetReceivedDeviceReportByYear(DeviceReportByYearQuery request)
        {
            IEnumerable<DeviceStatisticByMonth> queryResult = null;

            using (var uow = unitOfWorkFactory())
            {
                queryResult = uow.GetRepository<IAnalyzeReportRepository>().GetReceivedDeviceReportByYear(request);
            }

            var result = new DeviceReportByYearResponse();
            if (queryResult == null)
            {
                return result;
            }

            foreach (var s in queryResult)
            {
                if (request.Chinhanh.Equals("ALL", StringComparison.InvariantCultureIgnoreCase) ||
                    s.Chinhanh.Equals(request.Chinhanh, StringComparison.InvariantCultureIgnoreCase))
                {
                    if (request.DVQLSD.Equals("ALL", StringComparison.InvariantCultureIgnoreCase) ||
                        request.DVQLSD.Equals(s.DVQLSD1, StringComparison.InvariantCultureIgnoreCase))
                    {
                        result.AddDeviceToMonth(s.Tong, s.Thang);
                    }
                }
            }

            return result;
        }

        public DeviceReportByYearResponse GetSentDeviceReportByYear(DeviceReportByYearQuery request)
        {
            IEnumerable<DeviceStatisticByMonth> queryResult = null;

            using (var uow = unitOfWorkFactory())
            {
                queryResult = uow.GetRepository<IAnalyzeReportRepository>().GetSentDeviceReportByYear(request);
            }

            var result = new DeviceReportByYearResponse();
            if (queryResult == null)
            {
                return result;
            }

            foreach (var s in queryResult)
            {
                if (request.Chinhanh.Equals("ALL", StringComparison.InvariantCultureIgnoreCase) ||
                    s.Chinhanh.Equals(request.Chinhanh, StringComparison.InvariantCultureIgnoreCase))
                {
                    if (request.DVQLSD.Equals("ALL", StringComparison.InvariantCultureIgnoreCase) ||
                        request.DVQLSD.Equals(s.DVQLSD1, StringComparison.InvariantCultureIgnoreCase))
                    {
                        result.AddDeviceToMonth(s.Tong, s.Thang);
                    }
                }
            }

            return result;
        }

        public FulfillReportByYearResponse GetCenterFulfillReportByYear(CenterFulfillReportByYearQuery request)
        {
            decimal fulfillment = 0;

            using (var uow = unitOfWorkFactory())
            {
                fulfillment = uow.GetRepository<IAnalyzeReportRepository>().GetCenterFulfillReportByYear(request);
            }

            var result = new FulfillReportByYearResponse
            {
                Fulfillment = fulfillment
            };

            return result;
        }

        public FulfillReportByYearResponse GetBranchFulfillReportByYear(BranchFulfillReportByYearQuery request)
        {
            decimal fulfillment = 0;

            using (var uow = unitOfWorkFactory())
            {
                fulfillment = uow.GetRepository<IAnalyzeReportRepository>().GetBranchFulfillReportByYear(request);
            }

            var result = new FulfillReportByYearResponse
            {
                Fulfillment = fulfillment
            };

            return result;
        }

        public NumberOfDevicesReportByYearResponse GetCenterNumberOfReceivedDevicesReportByYear(CenterNumberOfDevicesReportByYearQuery request)
        {
            int numberOfDevices = 0;

            using (var uow = unitOfWorkFactory())
            {
                numberOfDevices = uow.GetRepository<IAnalyzeReportRepository>().GetCenterNumberOfReceivedDevicesReportByYear(request);
            }

            var result = new NumberOfDevicesReportByYearResponse
            {
                NumberOfDevices = numberOfDevices
            };

            return result;
        }

        public NumberOfDevicesReportByYearResponse GetBranchNumberOfReceivedDevicesReportByYear(BranchNumberOfDevicesReportByYearQuery request)
        {
            int numberOfDevices = 0;

            using (var uow = unitOfWorkFactory())
            {
                numberOfDevices = uow.GetRepository<IAnalyzeReportRepository>().GetBranchNumberOfReceivedDevicesReportByYear(request);
            }

            var result = new NumberOfDevicesReportByYearResponse
            {
                NumberOfDevices = numberOfDevices
            };

            return result;
        }

        public NumberOfDevicesReportByYearResponse GetCenterNumberOfSentDevicesReportByYear(CenterNumberOfDevicesReportByYearQuery request)
        {
            int numberOfDevices = 0;

            using (var uow = unitOfWorkFactory())
            {
                numberOfDevices = uow.GetRepository<IAnalyzeReportRepository>().GetCenterNumberOfSentDevicesReportByYear(request);
            }

            var result = new NumberOfDevicesReportByYearResponse
            {
                NumberOfDevices = numberOfDevices
            };

            return result;
        }

        public NumberOfDevicesReportByYearResponse GetBranchNumberOfSentDevicesReportByYear(BranchNumberOfDevicesReportByYearQuery request)
        {
            int numberOfDevices = 0;

            using (var uow = unitOfWorkFactory())
            {
                numberOfDevices = uow.GetRepository<IAnalyzeReportRepository>().GetBranchNumberOfSentDevicesReportByYear(request);
            }

            var result = new NumberOfDevicesReportByYearResponse
            {
                NumberOfDevices = numberOfDevices
            };

            return result;
        }

        public SummaryByBranchAndByYear GetSummaryByBranchAndByYear(SummaryByBranchAndByYearQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IAnalyzeReportRepository>().GetSummaryByBranchAndByYear(request);
            }
        }
    }
}