﻿using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Commands;
using System;
using Cmms.Domain.Entities;
using Cmms.Business.Common.ExceptionHandlers;
using Cmms.Data.Constants;

namespace Cmms.Business.ComponentServices
{
    public class RequisitionCommandService : IRequisitionCommandService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;

        public RequisitionCommandService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public NotificationItem ApproveRequisition(ApproveRequisitionCommand request)
        {
            using (var uow = unitOfWorkFactory())
            {
                var requisitionDetails = uow.GetRepository<IRequisitionRepository>().GetRequisition(request.RequisitionCode);
                if (requisitionDetails == null)
                {
                    throw new ReadBusinessException(BusinessExceptionConstant.REQUISITION_NOT_FOUND);
                }

                if (requisitionDetails.MaTinhTrangPhieu == "U-SC")
                {
                    request.NewStatus = "A-SC";
                }

                if (string.IsNullOrWhiteSpace(request.NewStatus))
                {
                    throw new WriteBusinessException(BusinessExceptionConstant.STATUS_CODE_INVALID);
                }

                uow.GetRepository<IRequisitionCommandRepository>().UpdateRequisitionStatus(request);

                return new NotificationItem
                {
                    PrimaryId = requisitionDetails.MaSophieu,
                    EntityType = EntityTypeConstants.Requisition,
                    FromStatus = requisitionDetails.MaTinhTrangPhieu,
                    ToStatus = request.NewStatus
                };
            }
        }
    }
}
