﻿using Cmms.Business.ComponentServices.Abstracts;
using MBoL.Utils;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Cmms.Business.ComponentServices
{
    public class LogService : ILogService
    {
        
        public void WriteRequestLogAsync<T>(T entity)
        {
            if (Config.IsWriteRequestLog)
            {
                var httpContext = HttpContext.Current;
                Task.Factory.StartNew(() =>
                {
                    WriteLog(entity, httpContext.Request.Url.AbsoluteUri);
                });
            }
        }

        private void WriteLog<T>(T entity, string url)
        {
            var type = entity.GetType();
            var properties = type.GetProperties();
            var logMessage = new StringBuilder();
            logMessage.AppendLine("URL: " + url);
            logMessage.AppendLine();
            foreach (var property in properties)
            {
                logMessage.AppendLine(property.Name + " = " + property.GetValue(entity));
            }
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Temp");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var filePath = Path.Combine(path, "log" + DateTime.Now.ToString("dd_MM_yy_HH_mm_ss_fff") + ".txt");

            using (StreamWriter writer = new StreamWriter(filePath))
            {
                writer.WriteLine(logMessage.ToString());
            }
        }
    }
}
