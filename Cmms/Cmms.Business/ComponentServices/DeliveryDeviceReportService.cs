﻿using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Business.DataContracts.DeviceStatistic;
using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Queries;

namespace Cmms.Business.ComponentServices
{
    public class DeliveryDeviceReportService : IDeliveryDeviceReportService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;
        public DeliveryDeviceReportService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public DeliveryDeviceStatisticProviderResponse GetStatisticByProvider(DeviceReportQuery request)
        {
            IEnumerable<DeliveryDeviceStatisticByProvider> queryResult = null;

            using (var uow = unitOfWorkFactory())
            {
                request.Organizations = uow.GetRepository<IUserRepository>().GetOranizations(request.Username);
                queryResult = uow.GetRepository<IDeliveryDeviceReportRepository>().GetDeliveryDeviceStatisticProvider(request);
            }

            var result = new DeliveryDeviceStatisticProviderResponse();
            if (queryResult != null)
            {
                var branches = queryResult.GroupBy(d => d.ChiNhanh).Select(d => d.Key);
                foreach (var b in branches)
                {
                    var branch = new DeliveryDeviceStatisticProvider_Branch();
                    branch.Ten = b;

                    var dsDoituongByBranch = queryResult.Where(d => d.ChiNhanh == b);
                    var dsDoituong = dsDoituongByBranch.GroupBy(d => d.DoiTuongTB).Select(d => d.Key);

                    foreach (var dt in dsDoituong)
                    {
                        var doituong = new DeliveryDeviceStatisticProvider_DoiTuongTB();
                        doituong.Ten = dt;

                        var dsNhacungcap = dsDoituongByBranch.Where(d => d.DoiTuongTB == dt);
                        foreach(var ncc in dsNhacungcap)
                        {
                            var nhacungcap = new DeliveryDeviceStatisticProvider_NhaCungCap
                            {
                                Ten = ncc.NhaCungCap,
                                TongSoThietBi = ncc.Tong
                            };

                            doituong.TongSoThietBi += nhacungcap.TongSoThietBi;
                            doituong.DanhSachNhaCungCap.Add(nhacungcap);
                        }

                        branch.TongSoThietBi += doituong.TongSoThietBi;
                        branch.DanhSachDoiTuongTB.Add(doituong);
                    }

                    result.TongSoThietBi += branch.TongSoThietBi;
                    result.DanhSachChiNhanh.Add(branch);
                }
            }

            return result;
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByProvider(DeliveryDeviceReportByProviderQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDeliveryDeviceReportRepository>().GetDeviceReportSummaryByProvider(request);
            }
        }

        public DeliveryDeviceStatisticDVQLSDDaiResponse GetDeviceStatisticByDVQLSD_Dai(DeviceReportQuery request)
        {
            IEnumerable<DeliveryDeviceStatisticByDaiVT> queryResult = null;
            using (var uow = unitOfWorkFactory())
            {
                request.Organizations = uow.GetRepository<IUserRepository>().GetOranizations(request.Username);
                queryResult = uow.GetRepository<IDeliveryDeviceReportRepository>().GetDeviceStatisticByDVQLSD_Dai(request);
            }

            var result = new DeliveryDeviceStatisticDVQLSDDaiResponse();
            if (queryResult != null)
            {
                var branches = queryResult.GroupBy(d => d.ChiNhanh).Select(d => d.Key);
                foreach (var b in branches)
                {
                    var branch = new DeliveryDeviceStatisticDVQLSDDai_Branch();
                    branch.Ten = b;

                    var dsDVQLSDByBranch = queryResult.Where(d => d.ChiNhanh == b);
                    var dsDVQLSD = dsDVQLSDByBranch.GroupBy(d => d.DVQLSD).Select(d => d.Key);

                    foreach (var dv in dsDVQLSD)
                    {
                        var dvqlsd = new DeliveryDeviceStatisticDVQLSDDai_DVQLSD();
                        dvqlsd.Ten = dv;

                        var dsDaiVT = dsDVQLSDByBranch.Where(d => d.DVQLSD == dv);
                        foreach (var dvt in dsDaiVT)
                        {
                            var daiVT = new DeliveryDeviceStatisticDVQLSDDai_DaiVT()
                            {
                                Ten = dvt.DaiVT,
                                TongSoThietBi = dvt.Tong
                            };

                            dvqlsd.TongSoThietBi += daiVT.TongSoThietBi;
                            dvqlsd.DanhSachDai.Add(daiVT);
                        }

                        branch.TongSoThietBi += dvqlsd.TongSoThietBi;
                        branch.DanhSachDVQLSD.Add(dvqlsd);
                    }

                    result.TongSoThietBi += branch.TongSoThietBi;
                    result.DanhSachChiNhanh.Add(branch);
                }
            }

            return result;
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVQLSD_Dai(DeliveryDeviceReportByDVQLSDDaiQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDeliveryDeviceReportRepository>().GetDeviceReportSummaryByDVQLSD_Dai(request);
            }
        }
    }
}
