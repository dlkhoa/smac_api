﻿using AutoMapper;
using Cmms.Business.Common.ExceptionHandlers;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Business.DataContracts;
using Cmms.Business.DataContracts.DeviceRepair;
using Cmms.Business.DataContracts.DeviceStatistic;
using Cmms.Data.Constants;
using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Commands;
using Cmms.Domain.Entities;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries.DeviceRepair;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cmms.Business.ComponentServices
{
    public class DeviceRepairService : IDeviceRepairService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;

        public DeviceRepairService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public RepairDeviceDetails GetRepairDevice(RepairDeviceQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDeviceRepairRepository>().GetRepairDevice(request);
            }
        }

        public RepairDeviceDetails GetRepairDeviceById(RepairDeviceQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDeviceRepairRepository>().GetRepairDeviceById(request);
            }
        }

        public RepairDeviceDetails GetRepairedDevice(RepairDeviceQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                var user = uow.GetRepository<IUserRepository>().GetUser(request.Username);
                if (user == null)
                {
                    throw new ReadBusinessException(BusinessExceptionConstant.INVALID_USER);
                }
                request.Branch = user.DefaultOrg;
                return uow.GetRepository<IDeviceRepairRepository>().GetRepairedDevice(request);
            }
        }

        public IEnumerable<string> GetDeviceNames()
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDeviceRepairRepository>().GetDeviceNames();
            }
        }

        public string AddRepairDevice(AddRepairDeviceCommand command)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDeviceRepairRepository>().AddRepairDevice(command);
            }
        }

        public IEnumerable<string> GetCenterList(string username)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDeviceRepairRepository>().GetCenterList(username);
            }
        }

        public RepairDeviceSummarize SummarizeReport(DeviceRepairSummarizeReportQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                var user = uow.GetRepository<IUserRepository>().GetUser(request.Username);
                if (user == null)
                {
                    throw new ReadBusinessException(BusinessExceptionConstant.INVALID_USER);
                }
                request.UserGroup = user.GroupCode;
                if (user.GroupCode == GroupCodeConstants.ADMIN ||
                    user.GroupCode == GroupCodeConstants.TVT ||
                    user.GroupCode == GroupCodeConstants.DVT)
                {
                    var userUnit = uow.GetRepository<IDeviceRepairRepository>().GetUserUnit(user.UserCode);
                    if (user.GroupCode == GroupCodeConstants.TVT &&
                        string.IsNullOrEmpty(request.TVT))
                    {
                        request.TVT = userUnit.TVT;
                    }
                    if (user.GroupCode == GroupCodeConstants.DVT)
                    {
                        request.DVT = userUnit.DVT;
                    }
                }
                request.Organizations = uow.GetRepository<IUserRepository>().GetOranizations(request.Username);
                return uow.GetRepository<IDeviceRepairRepository>().SummarizeReport(request);
            }
        }

        public IPagedResult<RepairDeviceDetails> SummarizeListReport(DeviceRepairSummarizeReportQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                var user = uow.GetRepository<IUserRepository>().GetUser(request.Username);
                if (user == null)
                {
                    throw new ReadBusinessException(BusinessExceptionConstant.INVALID_USER);
                }
                request.UserGroup = user.GroupCode;
                if (user.GroupCode == GroupCodeConstants.ADMIN ||
                    user.GroupCode == GroupCodeConstants.TVT ||
                    user.GroupCode == GroupCodeConstants.DVT)
                {
                    var userUnit = uow.GetRepository<IDeviceRepairRepository>().GetUserUnit(user.UserCode);
                    if (user.GroupCode == GroupCodeConstants.TVT &&
                        string.IsNullOrEmpty(request.TVT))
                    {
                        request.TVT = userUnit.TVT;
                    }
                    if (user.GroupCode == GroupCodeConstants.DVT)
                    {
                        request.DVT = userUnit.DVT;
                    }
                }
                request.Organizations = uow.GetRepository<IUserRepository>().GetOranizations(request.Username);
                return uow.GetRepository<IDeviceRepairRepository>().SummarizeListReport(request);
            }
        }

        public IPagedResult<AvailableDeviceDetails> GetAvailableDeviceList(AvailableDeviceListQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDeviceRepairRepository>().GetAvailableDeviceList(request);
            }
        }

        public IPagedResult<PriorityRepairDeviceDetails> GetPriorityRepairDeviceList(PriorityRepairDeviceListQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                var user = uow.GetRepository<IUserRepository>().GetUser(request.Username);
                if (user == null)
                {
                    throw new ReadBusinessException(BusinessExceptionConstant.INVALID_USER);
                }
                request.UserGroup = user.GroupCode;
                if (user.GroupCode == GroupCodeConstants.ADMIN ||
                    user.GroupCode == GroupCodeConstants.TVT ||
                    user.GroupCode == GroupCodeConstants.DVT)
                {
                    var userUnit = uow.GetRepository<IDeviceRepairRepository>().GetUserUnit(user.UserCode);
                    request.TVT = userUnit.TVT;
                    request.DVT = userUnit.DVT;
                }
                request.Organizations = uow.GetRepository<IUserRepository>().GetOranizations(request.Username);
                return uow.GetRepository<IDeviceRepairRepository>().GetPriorityRepairDeviceList(request);
            }
        }

        public IPagedResult<RepairDeviceDetails> GetRepairDeviceList(RepairDeviceListQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                var user = uow.GetRepository<IUserRepository>().GetUser(request.Username);
                if (user == null)
                {
                    throw new ReadBusinessException(BusinessExceptionConstant.INVALID_USER);
                }

                request.UserGroup = user.GroupCode;

                if (user.GroupCode == GroupCodeConstants.ADMIN ||
                    user.GroupCode == GroupCodeConstants.TVT ||
                    user.GroupCode == GroupCodeConstants.DVT)
                {
                    var userUnit = uow.GetRepository<IDeviceRepairRepository>().GetUserUnit(user.UserCode);
                    request.TVT = userUnit.TVT;
                    request.DVT = userUnit.DVT;
                }

                return uow.GetRepository<IDeviceRepairRepository>().GetRepairDeviceList(request);
            }
        }

        public string ReceiveDeviceFromTVT(ReceiveDeviceFromTVTCommand command)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDeviceRepairRepository>().ReceiveDeviceFromTVT(command);
            }
        }

        public IEnumerable<TVT> GetTVTList(string dvt)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDeviceRepairRepository>().GetTVTList(dvt);
            }
        }

        public string GenerateBarcode(string dvt)
        {
            return dvt + "123";
        }

        public string ReceiveDeviceFromTTDKSC(ReceiveDeviceFromTTDKSCCommand command)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDeviceRepairRepository>().ReceiveDeviceFromTTDKSC(command);
            }
        }

        public string DeliveryDevicesToTVT(DeliveryDevicesToTVTCommand command)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDeviceRepairRepository>().DeliveryDevicesToTVT(command);
            }
        }

        public IEnumerable<WorkingDeviceDetails> GetWorkingDevices(string serial)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDeviceRepairRepository>().GetWorkingDevices(serial);
            }
        }

        public string SetWorkingDate(SetWorkingDateCommand command)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDeviceRepairRepository>().SetWorkingDate(command);
            }
        }

        IEnumerable<RepairKPIReportItem> IDeviceRepairService.RepairKPIReport(RepairKPIReportQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDeviceRepairRepository>().RepairKPIReport(request);
            }
        }

        IEnumerable<RepairTimeReportItem> IDeviceRepairService.RepairTimeReport(RepairTimeReportQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDeviceRepairRepository>().RepairTimeReport(request);
            }
        }

        IEnumerable<RepairInventoryReportItem> IDeviceRepairService.RepairInventoryReport(RepairInventoryReportQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDeviceRepairRepository>().RepairInventoryReport(request);
            }
        }
    }
}
