﻿using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Business.DataContracts.DeviceStatistic;
using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cmms.Business.ComponentServices
{
    public class DeliveryNffDeviceReportService : IDeliveryNffDeviceReportService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;

        public DeliveryNffDeviceReportService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public DeliveryGoodDeviceStatisticProviderResponse GetStatisticByProvider(DeviceReportQuery request)
        {
            IEnumerable<DeliveryGoodDeviceStatisticByProvider> queryResult = null;

            using (var uow = unitOfWorkFactory())
            {
                request.Organizations = uow.GetRepository<IUserRepository>().GetOranizations(request.Username);
                queryResult = uow.GetRepository<IDeliveryNffDeviceReportRepository>().GetDeliveryDeviceStatisticProvider(request);
            }

            var result = new DeliveryGoodDeviceStatisticProviderResponse();
            if (queryResult != null)
            {
                var branches = queryResult.GroupBy(d => d.ChiNhanh).Select(d => d.Key);
                foreach (var b in branches)
                {
                    var branch = new DeliveryGoodDeviceStatisticProvider_Branch();
                    branch.Ten = b;

                    var dsDoituongByBranch = queryResult.Where(d => d.ChiNhanh == b);
                    var dsDoituong = dsDoituongByBranch.GroupBy(d => d.DoiTuongTB).Select(d => d.Key);

                    foreach (var dt in dsDoituong)
                    {
                        var doituong = new DeliveryGoodDeviceStatisticProvider_DoiTuongTB();
                        doituong.Ten = dt;

                        var dsNhacungcap = dsDoituongByBranch.Where(d => d.DoiTuongTB == dt);
                        foreach (var ncc in dsNhacungcap)
                        {
                            var nhacungcap = new DeliveryGoodDeviceStatisticProvider_NhaCungCap
                            {
                                Ten = ncc.NhaCungCap,
                                TongSoThietBi = ncc.Tong
                            };

                            doituong.TongSoThietBi += nhacungcap.TongSoThietBi;
                            doituong.DanhSachNhaCungCap.Add(nhacungcap);
                        }

                        branch.TongSoThietBi += doituong.TongSoThietBi;
                        branch.DanhSachDoiTuongTB.Add(doituong);
                    }

                    result.TongSoThietBi += branch.TongSoThietBi;
                    result.DanhSachChiNhanh.Add(branch);
                }
            }

            return result;
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByProvider(DeliveryDeviceReportByProviderQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDeliveryNffDeviceReportRepository>().GetDeviceReportSummaryByProvider(request);
            }
        }

        public DeliveryGoodDeviceStatisticDVQLSDDaiResponse GetDeviceStatisticByDVQLSD_Dai(DeviceReportQuery request)
        {
            IEnumerable<DeliveryGoodDeviceStatisticByDaiVT> queryResult = null;
            using (var uow = unitOfWorkFactory())
            {
                request.Organizations = uow.GetRepository<IUserRepository>().GetOranizations(request.Username);
                queryResult = uow.GetRepository<IDeliveryNffDeviceReportRepository>().GetDeviceStatisticByDVQLSD_Dai(request);
            }

            var result = new DeliveryGoodDeviceStatisticDVQLSDDaiResponse();
            if (queryResult != null)
            {
                var branches = queryResult.GroupBy(d => d.ChiNhanh).Select(d => d.Key);
                foreach (var b in branches)
                {
                    var branch = new DeliveryGoodDeviceStatisticDVQLSDDai_Branch();
                    branch.Ten = b;

                    var dsDVQLSDByBranch = queryResult.Where(d => d.ChiNhanh == b);
                    var dsDVQLSD = dsDVQLSDByBranch.GroupBy(d => d.DVQLSD).Select(d => d.Key);

                    foreach (var dv in dsDVQLSD)
                    {
                        var dvqlsd = new DeliveryGoodDeviceStatisticDVQLSDDai_DVQLSD();
                        dvqlsd.Ten = dv;

                        var dsDaiVT = dsDVQLSDByBranch.Where(d => d.DVQLSD == dv);
                        foreach (var dvt in dsDaiVT)
                        {
                            var daiVT = new DeliveryGoodDeviceStatisticDVQLSDDai_DaiVT()
                            {
                                Ten = dvt.DaiVT,
                                TongSoThietBi = dvt.Tong
                            };

                            dvqlsd.TongSoThietBi += daiVT.TongSoThietBi;
                            dvqlsd.DanhSachDai.Add(daiVT);
                        }

                        branch.TongSoThietBi += dvqlsd.TongSoThietBi;
                        branch.DanhSachDVQLSD.Add(dvqlsd);
                    }

                    result.TongSoThietBi += branch.TongSoThietBi;
                    result.DanhSachChiNhanh.Add(branch);
                }
            }

            return result;
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVQLSD_Dai(DeliveryDeviceReportByDVQLSDDaiQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDeliveryNffDeviceReportRepository>().GetDeviceReportSummaryByDVQLSD_Dai(request);
            }
        }
    }
}
