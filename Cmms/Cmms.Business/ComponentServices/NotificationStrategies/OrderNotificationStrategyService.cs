﻿using System.Collections.Generic;
using Cmms.Data.Constants;
using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;

namespace Cmms.Business.ComponentServices.NotificationStrategies
{
    public class OrderNotificationStrategyService : INotificationStrategyService
    {
        public string GetMessage(string primaryId, string fromStatus, string toStatus)
        {
            if (string.IsNullOrWhiteSpace(fromStatus) && toStatus.Equals("U"))
            {
                return string.Format("Phiếu {0} cần được phê duyệt", primaryId);
            }


            if (string.IsNullOrWhiteSpace(fromStatus) && toStatus.Equals("U-CN"))
            {
                return string.Format("Phiếu {0} cần được phê duyệt lệnh tại chi nhánh", primaryId);
            }

            if (fromStatus.Equals("U") && toStatus.Equals("A-CN"))
            {
                return string.Format("Phiếu {0} đã được phê duyệt", primaryId);
            }


            if (fromStatus.Equals("U-CN") && toStatus.Equals("W-CN"))
            {
                return string.Format("Phiếu {0} đã được phê duyệt tại chi nhánh, chờ lệnh điều động", primaryId);
            }

            if (fromStatus.Equals("W-CN") && toStatus.Equals("A-SC"))
            {
                return string.Format("Phiếu {0} đã được phê duyệt", primaryId);
            }

            return string.Empty;
        }

        public List<string> GetUser(IUnitOfWork uow, string code, string fromStatus, string toStatus)
        {
            var order = uow.GetRepository<IOrderRepository>().GetOrder(code);

            if (order == null)
            {
                throw new System.Exception(string.Format("{0}:Phiếu {1} không tìm thấy", EntityTypeConstants.Order, code));
            }

            var userRepository = uow.GetRepository<IUserRepository>();
            //9.1
            if (string.IsNullOrWhiteSpace(fromStatus) && toStatus == "U")
            {
                return userRepository.GetUserManagers(order.ChiNhanh);
            }

            //9.2
            if (string.IsNullOrEmpty(fromStatus) && toStatus == "U-CN")
            {
                return userRepository.GetUserManagers(order.ChiNhanh);
            }

            if (fromStatus == "U-CN" && toStatus == "W-CN")
            {
                return userRepository.Get_LD_TP_Users(order.ChiNhanh);
            }

            if ((fromStatus == "U" && toStatus == "A-CN") || (fromStatus == "W-CN" && toStatus == "A-SC"))
            {
                return userRepository.GetAccountingUsers(order.ChiNhanh);
            }

            return new List<string> { order.NguoiDeNghi };
        }
    }
}
