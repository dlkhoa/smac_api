﻿using System.Collections.Generic;
using Cmms.Data.Constants;
using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;

namespace Cmms.Business.ComponentServices.NotificationStrategies
{
    public class PicklistNotificationStrategyService : INotificationStrategyService
    {
        public string GetMessage(string primaryId, string fromStatus, string toStatus)
        {
            if (string.IsNullOrWhiteSpace(fromStatus))
            {
                return string.Format("Phiếu {0} cần được phê duyệt", primaryId);
            }

            if (fromStatus.Equals("U") && toStatus.Equals("R"))
            {
                return string.Format("Phiếu {0} đã được phê duyệt tại chi nhánh, chờ lệnh điều động", primaryId);
            }

            if (fromStatus.Equals("R") && toStatus.Equals("A"))
            {
                return string.Format("Phiếu {0} đã được phê duyệt", primaryId);
            }

            return string.Empty;
        }

        public List<string> GetUser(IUnitOfWork uow, string code, string fromStatus, string toStatus)
        {
            var picklist = uow.GetRepository<IPicklistRepository>().GetPicklist(code);

            if(picklist == null)
            {
                throw new System.Exception(string.Format("{0}:Phiếu {1} không tìm thấy", EntityTypeConstants.Picklist, code));
            }

            if (string.IsNullOrEmpty(fromStatus) && toStatus == "U")
            {
                return uow.GetRepository<IUserRepository>().GetUserManagers(picklist.ChiNhanh);
            }

            if (fromStatus == "U" && toStatus == "R")
            {
                return uow.GetRepository<IUserRepository>().Get_LD_TP_Users(picklist.ChiNhanh);
            }

            if (fromStatus == "R" && toStatus == "A")
            {
                return uow.GetRepository<IUserRepository>().GetAccountingUsers(picklist.ChiNhanh);
            }

            return new List<string> { picklist.NguoiDeNghi };
        }
    }
}
