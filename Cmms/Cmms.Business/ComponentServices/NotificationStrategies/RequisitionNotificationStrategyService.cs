﻿using System.Collections.Generic;
using Cmms.Data.Constants;
using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;

namespace Cmms.Business.ComponentServices.NotificationStrategies
{
    public class RequisitionNotificationStrategyService : INotificationStrategyService
    {
        public string GetMessage(string primaryId, string fromStatus, string toStatus)
        {
            if (string.IsNullOrWhiteSpace(fromStatus) || fromStatus == "U-CN")
            {
                return string.Format("Phiếu {0} cần được phê duyệt", primaryId);
            }

            return string.Format("Phiếu {0} đã được phê duyệt", primaryId);
        }

        public List<string> GetUser(IUnitOfWork uow, string code, string fromStatus, string toStatus)
        {
            var requisition = uow.GetRepository<IRequisitionRepository>().GetRequisition(code);
            if (requisition == null)
            {
                throw new System.Exception(string.Format("{0}:Phiếu {1} không tìm thấy", EntityTypeConstants.Requisition, code));
            }

            var userRepository = uow.GetRepository<IUserRepository>();

            if (fromStatus == "U-CN" && toStatus == "U-SC")
            {
                return userRepository.Get_LD_TP_Users(requisition.ChiNhanh);
            }

            if (fromStatus == "U-SC" && toStatus == "A-SC")
            {
                return userRepository.Get_NV_Users(requisition.ChiNhanh);
            }

            return new List<string> { requisition.NguoiNhan };
        }
    }
}
