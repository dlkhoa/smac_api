﻿using System.Collections.Generic;
using Cmms.Data.Infrastructure;

namespace Cmms.Business.ComponentServices.NotificationStrategies
{
    public interface INotificationStrategyService
    {
        List<string> GetUser(IUnitOfWork uow, string code, string fromStatus, string toStatus);
        string GetMessage(string primaryId, string fromStatus, string toStatus);
    }
}
