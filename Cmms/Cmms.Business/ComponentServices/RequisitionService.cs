﻿using AutoMapper;
using Cmms.Business.Common.ExceptionHandlers;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Business.DataContracts;
using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using System;
using System.Collections.Generic;

namespace Cmms.Business.ComponentServices
{
    public class RequisitionService : IRequisitionService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;

        public RequisitionService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public IPagedResult<RequisitionSummary> LayDanhMucPhieuTiepNhanThietBi(RequisitionQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IRequisitionRepository>().GetRequisitions(request);
                //return new PagedResult<RequisitionSummary>(query, request.Page, request.RecordsPerPage);
            }
        }

        public IPagedResult<RequisitionSummary> LayDanhMucPhieuTiepNhanThietBiChoPheDuyet(RequisitionQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
               return uow.GetRepository<IRequisitionRepository>().GetAwaitingRequisitions(request);
                //return new PagedResult<RequisitionSummary>(query, request.Page, request.RecordsPerPage);
            }
        }

        public RequisitionDetailsResponse GetRequisition(string username, string requisitionCode)
        {
            using (var uow = unitOfWorkFactory())
            {
                var requisition = uow.GetRepository<IRequisitionRepository>().GetRequisition(requisitionCode);
                var result = Mapper.Map<RequisitionDetails, RequisitionDetailsResponse>(requisition);
                if(result == null)
                {
                    throw new ReadBusinessException(BusinessExceptionConstant.REQUISITION_NOT_FOUND);
                }

                result.DanhSachThietBi = uow.GetRepository<IRequisitionLineRepository>().GetDevicesForRequisition(username, requisitionCode);
                return result;
            }
        }

        public List<string> GetNotifiedUsers(string requisitionCode)
        {
            using (var uow = unitOfWorkFactory())
            {
                var picklistRepository = uow.GetRepository<IRequisitionRepository>();
                var createdUser = picklistRepository.GetCreatedUser(requisitionCode);
                return new List<string> { createdUser };
            }
        }
    }
}
