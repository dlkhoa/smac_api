﻿using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Cmms.Business.ComponentServices
{
    public class OrganizationService :IOrganizationService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;

        public OrganizationService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public IEnumerable<Organization> GetBranches()
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IOrganizationRepository>().GetBranches();
            }
        }
    }
}
