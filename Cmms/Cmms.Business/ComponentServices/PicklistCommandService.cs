﻿using Cmms.Business.Common.ExceptionHandlers;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Commands;
using System;
using Cmms.Domain.Entities;
using Cmms.Data.Constants;

namespace Cmms.Business.ComponentServices
{
    public class PicklistCommandService : IPicklistCommandService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;

        public PicklistCommandService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public NotificationItem ApprovePicklist(ApprovePicklistCommand command)
        {
            using (var uow = unitOfWorkFactory())
            {
                var picklistRepository = uow.GetRepository<IPicklistRepository>();
                var picklist = picklistRepository.GetPicklistForUpdate(command.PicklistCode);

                if (picklist == null)
                {
                    throw new WriteBusinessException(BusinessExceptionConstant.PICKLIST_NOT_FOUND);
                }

                //Check status

                if (picklist.Status.Equals("U", StringComparison.OrdinalIgnoreCase))
                {
                    command.NewStatus = "R";
                }
                else if(picklist.Status.Equals("R", StringComparison.OrdinalIgnoreCase))
                {
                    command.NewStatus = "A";
                }

                if (string.IsNullOrWhiteSpace(command.NewStatus))
                {
                    throw new WriteBusinessException(BusinessExceptionConstant.STATUS_CODE_INVALID);
                }

                uow.GetRepository<IPicklistCommandRepository>().UpdatePicklistStatus(command);

                return new NotificationItem
                {
                    PrimaryId = picklist.Code,
                    EntityType = EntityTypeConstants.Picklist,
                    FromStatus = picklist.Status,
                    ToStatus = command.NewStatus
                };
            }
        }
    }
}
