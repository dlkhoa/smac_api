﻿using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities;
using Cmms.Domain.Queries;
using System;

namespace Cmms.Business.ComponentServices
{
    public class ProviderService : IProviderService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;
        public ProviderService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }
        public IPagedResult<Provider> GetProviders(ProviderQuery providerQuery)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IProviderRepository>().GetProviders(providerQuery);
            }
        }
    }
}
