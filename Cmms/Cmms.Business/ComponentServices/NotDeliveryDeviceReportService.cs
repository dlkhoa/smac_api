﻿using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Business.DataContracts.DeviceStatistic;
using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cmms.Business.ComponentServices
{
    public class NotDeliveryDeviceReportService : INotDeliveryDeviceReportService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;

        public NotDeliveryDeviceReportService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }
        public NotDeliveryDeviceStatisticDTSCResponse GetStatisticByDTSC(DeviceReportQuery request)
        {
            IEnumerable<NotDeliveryDeviceStatisticByDTSC> queryResult = null;
            using (var uow = unitOfWorkFactory())
            {
                request.Organizations = uow.GetRepository<IUserRepository>().GetOranizations(request.Username);
                queryResult = uow.GetRepository<INotDeliveryDeviceReportRepository>().GetStatisticByDTSC(request);
            }

            var result = new NotDeliveryDeviceStatisticDTSCResponse();

            if (queryResult == null)
            {
                return result;
            }

            var branches = queryResult.GroupBy(d => d.ChiNhanh).Select(d => d.Key);
            foreach (var b in branches)
            {
                var branch = new NotDeliveryDeviceStatisticDTSC_Branch();
                branch.Ten = b;

                var dsDoituongByBranch = queryResult.Where(d => d.ChiNhanh == b);
                var dsDoituong = dsDoituongByBranch.GroupBy(d => d.DoiTuongTB).Select(d => d.Key);

                foreach (var dt in dsDoituong)
                {
                    var doituong = new NotDeliveryDeviceStatisticDTSC_DoiTuongTB();
                    doituong.Ten = dt;

                    var dsNhaCungCapByDTTB = dsDoituongByBranch.Where(d => d.DoiTuongTB == dt);
                    var dsNhacungcap = dsNhaCungCapByDTTB.GroupBy(d => d.NhaCungCap).Select(d => d.Key);
                    foreach (var ncc in dsNhacungcap)
                    {
                        var nhacungcap = new NotDeliveryDeviceStatisticDTSC_NhaCungCap();
                        nhacungcap.Ten = ncc;

                        var dsDoiTac = dsNhaCungCapByDTTB.Where(d => d.NhaCungCap == ncc);

                        foreach (var dtsc in dsDoiTac)
                        {
                            var doitac = new NotDeliveryDeviceStatisticDTSC_DoiTac
                            {
                                Ten = dtsc.DoiTac,
                                TongSoThietBi = dtsc.Tong
                            };

                            nhacungcap.TongSoThietBi += doitac.TongSoThietBi;
                            nhacungcap.DanhSachDoiTac.Add(doitac);
                        }

                        doituong.TongSoThietBi += nhacungcap.TongSoThietBi;
                        doituong.DanhSachNhaCungCap.Add(nhacungcap);
                    }

                    branch.TongSoThietBi += doituong.TongSoThietBi;
                    branch.DanhSachDoiTuongTB.Add(doituong);
                }

                result.TongSoThietBi += branch.TongSoThietBi;
                result.DanhSachChiNhanh.Add(branch);
            }

            return result;
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDTSC(NotDeliveryDeviceReportByDTSCQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<INotDeliveryDeviceReportRepository>().GetDeviceReportSummaryByDTSC(request);
            }
        }

        public NotDeliveryDeviceStatisticDVTResponse GetDeviceStatisticByDVT(DeviceReportQuery request)
        {
            IEnumerable<NotDeliveryDeviceStatisticByDVT> queryResult = null;
            using (var uow = unitOfWorkFactory())
            {
                request.Organizations = uow.GetRepository<IUserRepository>().GetOranizations(request.Username);
                queryResult = uow.GetRepository<INotDeliveryDeviceReportRepository>().GetDeviceStatisticByDVT(request);
            }

            var result = new NotDeliveryDeviceStatisticDVTResponse();
            if (queryResult != null)
            {
                var branches = queryResult.GroupBy(d => d.ChiNhanh).Select(d => d.Key);
                foreach (var b in branches)
                {
                    var branch = new NotDeliveryDeviceStatisticDVT_Branch();
                    branch.Ten = b;

                    var dsDVQLSDByBranch = queryResult.Where(d => d.ChiNhanh == b);
                    var dsDVQLSD = dsDVQLSDByBranch.GroupBy(d => d.DVQLSD).Select(d => d.Key);

                    foreach (var dv in dsDVQLSD)
                    {
                        var dvqlsd = new NotDeliveryDeviceStatisticDVT_DVQLSD();
                        dvqlsd.Ten = dv;

                        var dsDaiVT = dsDVQLSDByBranch.Where(d => d.DVQLSD == dv);
                        foreach (var dvt in dsDaiVT)
                        {
                            var daiVT = new NotDeliveryDeviceStatisticDVT_DVT()
                            {
                                Ten = dvt.DaiVT,
                                TongSoThietBi = dvt.Tong
                            };

                            dvqlsd.TongSoThietBi += daiVT.TongSoThietBi;
                            dvqlsd.DanhSachDai.Add(daiVT);
                        }

                        branch.TongSoThietBi += dvqlsd.TongSoThietBi;
                        branch.DanhSachDVQLSD.Add(dvqlsd);
                    }

                    result.TongSoThietBi += branch.TongSoThietBi;
                    result.DanhSachChiNhanh.Add(branch);
                }
            }

            return result;
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVT(NotDeliveryDeviceReportByDVTQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<INotDeliveryDeviceReportRepository>().GetDeviceReportSummaryByDVT(request);
            }
        }
    }
}
