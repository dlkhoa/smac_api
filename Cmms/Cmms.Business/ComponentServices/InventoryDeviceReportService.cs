﻿using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Business.DataContracts.DeviceStatistic;
using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Queries;

namespace Cmms.Business.ComponentServices
{
    public class InventoryDeviceReportService : IInventoryDeviceReportService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;
        

        public InventoryDeviceReportService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public InventoryDeviceStatisticDVQLSDLoaiTBResponse GetInventoryDeviceStatisticByLoaiThietBi(DeviceReportQuery request)
        {
            IEnumerable<InventoryDeviceStatisticByLoaiTB> queryResult = null;
            using (var uow = unitOfWorkFactory())
            {
                request.Organizations = uow.GetRepository<IUserRepository>().GetOranizations(request.Username);
                queryResult = uow.GetRepository<IInventoryDeviceReportRepository>().GetInventoryDeviceStatisticByLoaiThietBi(request);
            }
            var result = new InventoryDeviceStatisticDVQLSDLoaiTBResponse();

            if (queryResult == null)
            {
                return result;
            }
            var branches = queryResult.GroupBy(item => item.ChiNhanh).Select(item => item.Key);

            foreach (var b in branches)
            {
                var branch = new InventoryDeviceStatisticDVQLSDLoaiTB_Branch();
                branch.Ten = b;

                var dsDVQLSDByBranch = queryResult.Where(d => d.ChiNhanh == b);
                var dsDVQLSD = dsDVQLSDByBranch.GroupBy(d => d.DVQLSD).Select(d => d.Key);

                foreach (var dv in dsDVQLSD)
                {
                    var dvqlsd = new InventoryDeviceStatisticDVQLSDLoaiTB_DVQLSD();
                    dvqlsd.Ten = dv;

                    var dsLoaiTB = dsDVQLSDByBranch.Where(d => d.DVQLSD == dv);
                    foreach (var tb in dsLoaiTB)
                    {
                        var loaiTB = new InventoryDeviceStatisticDVQLSDLoaiTB_LoaiTB()
                        {
                            Ten = tb.LoaiTB,
                            TongSoThietBi = tb.Tong
                        };

                        dvqlsd.TongSoThietBi += loaiTB.TongSoThietBi;
                        dvqlsd.DanhSachLoaiThietBi.Add(loaiTB);
                    }

                    branch.TongSoThietBi += dvqlsd.TongSoThietBi;
                    branch.DanhSachDVQLSD.Add(dvqlsd);
                }

                result.TongSoThietBi += branch.TongSoThietBi;
                result.DanhSachChiNhanh.Add(branch);
            }
            return result;
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVQLSD_LoaiTB(InventoryDeviceReportByDVQLSDLoaiTBQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IInventoryDeviceReportRepository>().GetDeviceReportSummaryByDVQLSD_LoaiTB(request);
            }
        }

        public InventoryDeviceStatisticDVQLSDBPCResponse GetInventoryDeviceStatisticBPCByDVQLSD(DeviceReportQuery request)
        {
            var result = new InventoryDeviceStatisticDVQLSDBPCResponse();
            IEnumerable<InventoryDeviceStatisticBPCByDVQLSD> queryResult = null;

            using (var uow = unitOfWorkFactory())
            {
                request.Organizations = uow.GetRepository<IUserRepository>().GetOranizations(request.Username);
                queryResult = uow.GetRepository<IInventoryDeviceReportRepository>().GetInventoryDeviceStatisticBPCByDVQLSD(request);
            }

            if (queryResult == null)
            {
                return result;
            }

            var branches = queryResult.GroupBy(item => item.ChiNhanh).Select(item => item.Key);
            foreach (var b in branches)
            {
                var branch = new InventoryDeviceStatisticDVQLSDBPC_Branch();
                branch.Ten = b;

                var dsDVQLSD = queryResult.Where(item => item.ChiNhanh == b);
                foreach(var dv in dsDVQLSD)
                {
                    if(!string.IsNullOrWhiteSpace(dv.DVQLSD))
                    {
                        var dvqlsd = new InventoryDeviceStatisticBPC_DVQLSD();
                        dvqlsd.Ten = dv.DVQLSD;
                        dvqlsd.SoLuongChoSuaChua = dv.SoLuongChoSuaChua;
                        dvqlsd.SoLuongChuaTest = dv.SoLuongChuaTest;
                        dvqlsd.SoLuongKhongTheSuaChuaGiao = dv.SoLuongKiemTraSSCChuaGiao;
                        dvqlsd.SoLuongNFFChuaGiao = dv.SoLuongNFFChuaGiao;
                        dvqlsd.SoLuongTotChuaGiao = dv.SoLuongTotChuaGiao;

                        branch.DanhSachDVQLSD.Add(dvqlsd);
                    }
                    
                    branch.SoLuongChoSuaChua += dv.SoLuongChoSuaChua;
                    branch.SoLuongChuaTest += dv.SoLuongChuaTest;
                    branch.SoLuongKhongTheSuaChuaGiao += dv.SoLuongKiemTraSSCChuaGiao;
                    branch.SoLuongNFFChuaGiao += dv.SoLuongNFFChuaGiao;
                    branch.SoLuongTotChuaGiao += dv.SoLuongTotChuaGiao;
                }

                result.DanhSachChiNhanh.Add(branch);
                result.SoLuongChoSuaChua += branch.SoLuongChoSuaChua;
                result.SoLuongChuaTest += branch.SoLuongChuaTest;
                result.SoLuongKhongTheSuaChuaGiao += branch.SoLuongKhongTheSuaChuaGiao;
                result.SoLuongNFFChuaGiao += branch.SoLuongNFFChuaGiao;
                result.SoLuongTotChuaGiao += branch.SoLuongTotChuaGiao;
            }

            return result;
        }

        public InventoryDeviceStatisticLHDBPCResponse GetInventoryDeviceStatisticBPCByLHD(DeviceReportQuery request)
        {
            var result = new InventoryDeviceStatisticLHDBPCResponse();
            IEnumerable<InventoryDeviceStatisticBPCByLHD> queryResult = null;

            using (var uow = unitOfWorkFactory())
            {
                request.Organizations = uow.GetRepository<IUserRepository>().GetOranizations(request.Username);
                queryResult = uow.GetRepository<IInventoryDeviceReportRepository>().GetInventoryDeviceStatisticBPCByLHD(request);
            }

            if (queryResult == null)
            {
                return result;
            }

            var branches = queryResult.GroupBy(item => item.ChiNhanh).Select(item => item.Key);
            foreach (var b in branches)
            {
                var branch = new InventoryDeviceStatisticLHDBPC_Branch();
                branch.Ten = b;

                var dsLHD = queryResult.Where(item => item.ChiNhanh == b);
                foreach (var hd in dsLHD)
                {
                    if(!string.IsNullOrEmpty(hd.LoaiHD))
                    {
                        var loaiHD = new InventoryDeviceStatisticBPC_LHD();
                        loaiHD.Ten = hd.LoaiHD;
                        loaiHD.SoLuongChoSuaChua = hd.SoLuongChoSuaChua;
                        loaiHD.SoLuongChuaTest = hd.SoLuongChuaTest;
                        loaiHD.SoLuongKhongTheSuaChuaGiao = hd.SoLuongKiemTraSSCChuaGiao;
                        loaiHD.SoLuongNFFChuaGiao = hd.SoLuongNFFChuaGiao;
                        loaiHD.SoLuongTotChuaGiao = hd.SoLuongTotChuaGiao;

                        branch.DanhSachLoaiHD.Add(loaiHD);
                    }
                    
                    branch.SoLuongChoSuaChua += hd.SoLuongChoSuaChua;
                    branch.SoLuongChuaTest += hd.SoLuongChuaTest;
                    branch.SoLuongKhongTheSuaChuaGiao += hd.SoLuongKiemTraSSCChuaGiao;
                    branch.SoLuongNFFChuaGiao += hd.SoLuongNFFChuaGiao;
                    branch.SoLuongTotChuaGiao += hd.SoLuongTotChuaGiao;
                }

                result.DanhSachChiNhanh.Add(branch);
                result.SoLuongChoSuaChua += branch.SoLuongChoSuaChua;
                result.SoLuongChuaTest += branch.SoLuongChuaTest;
                result.SoLuongKhongTheSuaChuaGiao += branch.SoLuongKhongTheSuaChuaGiao;
                result.SoLuongNFFChuaGiao += branch.SoLuongNFFChuaGiao;
                result.SoLuongTotChuaGiao += branch.SoLuongTotChuaGiao;
            }

            return result;
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVQLSD(InventoryDeviceReportByDVQLSDQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IInventoryDeviceReportRepository>().GetDeviceReportSummaryByDVQLSD(request);
            }
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByLoaiHD(InventoryDeviceReportByLoaiHDQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IInventoryDeviceReportRepository>().GetDeviceReportSummaryByLoaiHD(request);
            }
        }
    }
}
