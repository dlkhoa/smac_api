﻿using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cmms.Business.DataContracts.DeviceStatistic;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using Cmms.Data.Repositories.Abstracts;

namespace Cmms.Business.ComponentServices
{
    public class OverRepairTimeDeviceReportService : IOverRepairTimeDeviceReportService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;

        public OverRepairTimeDeviceReportService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public OverRepairTimeDeviceDTSCResponse GetStatisticByDoiTac(DeviceReportQuery request)
        {
            IEnumerable<OverRepairTimeDeviceStatisticDTSCByDTSC> queryResult = null;

            using (var uow = unitOfWorkFactory())
            {
                request.Organizations = uow.GetRepository<IUserRepository>().GetOranizations(request.Username);
                queryResult = uow.GetRepository<IOverRepairTimeDeviceReportRepository>().GetStatisticByDoiTac(request);
            }

            var result = new OverRepairTimeDeviceDTSCResponse();

            if(queryResult == null)
            {
                return result;
            }

            var branches = queryResult.GroupBy(item => item.ChiNhanh).Select(item => item.Key);
            foreach(var b in branches)
            {
                var branch = new OverRepairTimeDeviceDTSC_Branch();
                branch.Ten = b;

                var dsDT = queryResult.Where(item => item.ChiNhanh == b);
                foreach(var dt in dsDT)
                {
                    var doitac = new OverRepairTimeDeviceDTSC_DoiTac();
                    doitac.Ten = dt.DoiTac;
                    doitac.TongSoThietBi = dt.Tong;

                    branch.DanhSachDoiTac.Add(doitac);
                    branch.TongSoThietBi += doitac.TongSoThietBi;
                }

                result.DanhSachChiNhanh.Add(branch);
                result.TongSoThietBi += branch.TongSoThietBi;
            }

            return result;
        }


        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDTSC(OverRepairTimeDeviceReportByDTSCQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IOverRepairTimeDeviceReportRepository>().GetDeviceReportSummaryByDTSC(request);
            }
        }

    }
}
