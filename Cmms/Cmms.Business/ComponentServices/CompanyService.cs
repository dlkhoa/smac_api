﻿using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using System;

namespace Cmms.Business.ComponentServices
{
    public class CompanyService : ICompanyService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;

        public CompanyService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public IPagedResult<DanhMucDoiTacSuaChua> LayDanhMucDoiTacSuaChua(CompanyQuery companyQuery)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<ICompanyRepository>().LayDanhMucDoiTacSuaChua(companyQuery);
            }
        }

        public IPagedResult<DanhMucDVQLSDTB> LayDanhMucDVAQLSDTB(CompanyQuery companyQuery)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<ICompanyRepository>().LayDanhMucDVQLSDTB(companyQuery);
            }
        }

        public IPagedResult<CodeName> LayDanhMucLoaiThietbi(CompanyQuery companyQuery)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<ICompanyRepository>().LayDanhMucLoaiThietbi(companyQuery);
            }
        }

        public IPagedResult<CodeName> LayDanhMucDaiVienthong(CompanyQuery companyQuery)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<ICompanyRepository>().LayDanhMucDaiVienthong(companyQuery);
            }
        }

        public IPagedResult<CodeName> LayDanhMucDonviQLSDTB(CompanyQuery companyQuery)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<ICompanyRepository>().LayDanhMucDonviQLSDTB(companyQuery);
            }
        }

        public IPagedResult<CodeName> LayDanhMucKetquaKiemtraTruocSC(CompanyQuery companyQuery)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<ICompanyRepository>().LayDanhMucKetquaKiemtraTruocSC(companyQuery);
            }
        }

        public IPagedResult<CodeName> LayDanhMucKetquaKiemtraSauSC(CompanyQuery companyQuery)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<ICompanyRepository>().LayDanhMucKetquaKiemtraSauSC(companyQuery);
            }
        }

        public IPagedResult<CodeName> LayDanhMucTrangthaiThietbi(CompanyQuery companyQuery)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<ICompanyRepository>().LayDanhMucTrangthaiThietbi(companyQuery);
            }
        }

        public IPagedResult<CodeName> LayDanhMucDoituongThietbi(CompanyQuery companyQuery)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<ICompanyRepository>().LayDanhMucDoituongThietbi(companyQuery);
            }
        }

        public IPagedResult<CodeName> LayDanhMucChinhanh(CompanyQuery companyQuery)
        {
            using (var uow = unitOfWorkFactory())
            {
               return uow.GetRepository<ICompanyRepository>().LayDanhMucChinhanh(companyQuery);
            }
        }

        public IPagedResult<CodeName> LayDanhMucNam(CompanyQuery companyQuery)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<ICompanyRepository>().LayDanhMucNam(companyQuery);
            }
        }
    }
}
