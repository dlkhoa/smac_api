﻿using System;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;

namespace Cmms.Business.ComponentServices.SearchDeviceReportStrategies
{
    public class BaoCaoThietBiBanGiaoChoDVQLSDStrategy : ISearchDeviceForReportingService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;

        public BaoCaoThietBiBanGiaoChoDVQLSDStrategy(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDevicesForReporting(DeviceReportAllLevelQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDeliveryDeviceReportRepository>().GetDeviceReportSummaryAllLevel(request);
            }
        }
    }
}
