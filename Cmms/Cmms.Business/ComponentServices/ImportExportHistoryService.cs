﻿using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using System;

namespace Cmms.Business.ComponentServices
{
    public class ImportExportHistoryService : IImportExportHistoryService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;
        public ImportExportHistoryService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public IPagedResult<ImportExportHistorySummary> GetHistories(ImportExportHistoryQuery query)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IImportExportHistoryRepository>().GetHistories(query);
            }
        }
    }
}
