﻿using AutoMapper;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Business.DataContracts;
using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Cmms.Business.ComponentServices
{
    public class EntityStatusService: IEntityStatusService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;

        public EntityStatusService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public IEnumerable<EntityStatusResponse> GetStatuses(string entityCode)
        {
            using (var uow = unitOfWorkFactory())
            {
                var statuses = uow.GetRepository<IEntityStatusRepository>().GetStatuses(entityCode);
                return Mapper.Map<IEnumerable<EntityStatus>, IEnumerable<EntityStatusResponse>>(statuses);
            }
        }
    }
}
