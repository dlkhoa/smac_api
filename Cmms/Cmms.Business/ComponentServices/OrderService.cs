﻿using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using System;
using Cmms.Business.DataContracts;
using AutoMapper;
using Cmms.Business.Common.ExceptionHandlers;
using System.Collections.Generic;

namespace Cmms.Business.ComponentServices
{
    public class OrderService : IOrderService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;

        public OrderService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public IPagedResult<OrderSummary> GetOrders(OrderQuery orderQuery)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IOrderRepository>().GetOrders(orderQuery);
            }
        }

        public IPagedResult<OrderSummary> GetAwatingOrders(AwatingOrderQuery orderQuery)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IOrderRepository>().GetAwatingOrders(orderQuery);
            }
        }

        public OrderDetailsResponse GetOrder(string username, string orderCode)
        {
            using (var uow = unitOfWorkFactory())
            {
                var order = uow.GetRepository<IOrderRepository>().GetOrder(orderCode);
                if (order == null)
                {
                    throw new ReadBusinessException(BusinessExceptionConstant.ORDER_NOT_FOUND);
                }
                var orderResponse = Mapper.Map<OrderDetails, OrderDetailsResponse>(order);
                orderResponse.DanhSachThietBi = uow.GetRepository<IOrderLineRepository>().GetOrderLines(orderCode);
                return orderResponse;
            }
        }

        public List<string> GetNotifiedUsers(string orderCode)
        {
            using (var uow = unitOfWorkFactory())
            {
                var orderRepository = uow.GetRepository<IOrderRepository>();
                var createdUser = orderRepository.GetCreatedUser(orderCode);
                return new List<string> { createdUser };
            }
        }
    }
}
