﻿using AutoMapper;
using Cmms.Business.Common.ExceptionHandlers;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Business.DataContracts;
using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using System;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Queries;

namespace Cmms.Business.ComponentServices
{
    public class DeviceService : IDeviceService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;

        public DeviceService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public DeviceDetailsResponse GetDevice(string username, string serial)
        {
            using (var uow = unitOfWorkFactory())
            {
                var device= uow.GetRepository<IDeviceRepository>().GetDevice(username, serial);
                if(device == null)
                {
                    throw new ReadBusinessException(BusinessExceptionConstant.DEVICE_NOT_FOUND);
                }
                var result = Mapper.Map<DeviceDetails, DeviceDetailsResponse>(device);

                return result;
            }
        }

        public IPagedResult<DeviceHistorySummary> GetDeviceHistories(DeviceHistoryQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDeviceHistoryRepository>().GetDeviceHistories(request);
            }
        }

        public IPagedResult<DeviceSummary> GetDevices(DeviceQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDeviceRepository>().GetDevices(request);
            }
        }

        public IPagedResult<DeviceSummary> GetAdvancedDevices(DeviceAdvancedQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDeviceRepository>().GetAdvancedDevices(request);
            }
        }
    }
}
