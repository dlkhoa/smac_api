﻿using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Business.DataContracts.DeviceStatistic;
using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cmms.Business.ComponentServices
{
    public class RefusedHTKTDeviceReportService : IRefusedHTKTDeviceReportService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;

        public RefusedHTKTDeviceReportService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public RefusedHTKTDeviceStatisticProviderResponse GetStatisticByProvider(DeviceReportQuery request)
        {
            IEnumerable<RefusedHTKTDeviceStatisticByProvider> queryResult = null;

            using (var uow = unitOfWorkFactory())
            {
                request.Organizations = uow.GetRepository<IUserRepository>().GetOranizations(request.Username);
                queryResult = uow.GetRepository<IRefusedHTKTDeviceReportRepository>().GetDeliveryDeviceStatisticProvider(request);
            }

            var result = new RefusedHTKTDeviceStatisticProviderResponse();
            if (queryResult == null)
            {
                return result;
            }
            var branches = queryResult.GroupBy(d => d.ChiNhanh).Select(d => d.Key);
            foreach (var b in branches)
            {
                var branch = new RefusedHTKTDeviceStatisticProvider_Branch();
                branch.Ten = b;

                var dsNhacungcap = queryResult.Where(d => d.ChiNhanh == b);
                foreach (var ncc in dsNhacungcap)
                {
                    var nhacungcap = new RefusedHTKTDeviceStatisticProvider_NhaCungCap
                    {
                        Ten = ncc.NhaCungCap,
                        TongSoThietBi = ncc.Tong
                    };


                    branch.TongSoThietBi += nhacungcap.TongSoThietBi;
                    branch.DanhSachNhaCungCap.Add(nhacungcap);
                }

                result.TongSoThietBi += branch.TongSoThietBi;
                result.DanhSachChiNhanh.Add(branch);
            }

            return result;
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByProvider(RefusedHTKTDeviceReportByProviderQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IRefusedHTKTDeviceReportRepository>().GetDeviceReportSummaryByProvider(request);
            }
        }

    }
}
