﻿using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Business.DataContracts.DeviceStatistic;
using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cmms.Business.ComponentServices
{
    public class DeliverySLADeviceReportService : IDeliverySLADeviceReportService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;

        public DeliverySLADeviceReportService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public DeliverySLADeviceStatisticMonthResponse GetStatisticByMonth(DeviceReportQuery request)
        {
            IEnumerable<DeliverySLADeviceStatisticByMonth> queryResult = null;

            using (var uow = unitOfWorkFactory())
            {
                request.Organizations = uow.GetRepository<IUserRepository>().GetOranizations(request.Username);
                queryResult = uow.GetRepository<IDeliverySLADeviceReportRepository>().GetStatisticByMonth(request);
            }

            var result = new DeliverySLADeviceStatisticMonthResponse();
            if (queryResult == null)
            {
                return result;
            }

            var branches = queryResult.GroupBy(d => d.ChiNhanh).Select(d => d.Key);
            foreach (var b in branches)
            {
                var branch = new DeliverySLADeviceStatisticMonth_Branch();
                branch.Ten = b;

                var dsThang = queryResult
                    .Where(d => d.ChiNhanh == b)
                    .OrderBy(d => d.NgayBanGiao);

                foreach (var th in dsThang)
                {
                    var existedItem = branch.DanhSachThang.FirstOrDefault(item => item.Ten == th.NgayBanGiao.ToString("MM-yyyy"));
                    if(existedItem != null)
                    {
                        existedItem.TongSoThietBi += th.Tong;
                        
                    }
                    else
                    {
                        var thang = new DeliverySLADeviceStatisticMonth_Month();
                        thang.Ten = th.NgayBanGiao.ToString("MM-yyyy");
                        thang.TongSoThietBi = th.Tong;
                        branch.DanhSachThang.Add(thang);
                    }
                    branch.TongSoThietBi += th.Tong;
                    
                }

                result.TongSoThietBi += branch.TongSoThietBi;
                result.DanhSachChiNhanh.Add(branch);
            }

            return result;
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByMonth(DeliverySLADeviceReportByMonthQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDeliverySLADeviceReportRepository>().GetDeviceReportSummaryByMonth(request);
            }
        }

    }

}
