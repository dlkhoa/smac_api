﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Business.DataContracts;
using Cmms.Data.Constants;
using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;

namespace Cmms.Business.ComponentServices
{
    public class AwaitingItemNotificationService : IAwaitingItemNotificationService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;

        public AwaitingItemNotificationService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public DanhSachPhieuChoPheDuyetResponse LayDanhSachPhieuChoPheDuyet(AwaitingItemNotificationQuery request)
        {
            IPagedResult<OrderSummary> orders;
            IPagedResult<PicklistSummary> picklists;
            IPagedResult<RequisitionSummary> requisitions;

            var orderRequest = new AwatingOrderQuery
            {
                PermissionFunction = "'PSPORF','PSPORD','PSPORG','PSPOBH'",
                Type = "'U','U-CN','W-CN'",
                Username = request.Username,
                Branch = string.Empty,
                RecordsPerPage = 500
            };

            var picklistRequest = new PicklistQuery
            {
                PermissionFunction = "'SSPICH','SSPICK','SSPICR'",
                Status = "'U','R'",
                Username = request.Username,
                Branch = string.Empty,
                RecordsPerPage = 500
            };

            var requisitionRequest = new RequisitionQuery
            {
                PermissionFunction = string.Format("'{0}','{1}','{2}','{3}'",
                    RequisitionConstant.TIEP_NHAN_THIET_BI_HONG,
                    RequisitionConstant.TU_CHOI_NHAN_BAO_HANH,
                    RequisitionConstant.TIEP_NHAN_THIET_BI_SAU_SUA_CHUA,
                    RequisitionConstant.TIEP_NHAN_THIET_BI_MOI),
                RequisitionClass = string.Format("'{0}','{1}','{2}','{3}'",
                    PermissionFunctionConstant.TIEP_NHAN_THIET_BI_HONG,
                    PermissionFunctionConstant.TIEP_NHAN_THIET_BI_SUA_CHUA,
                    PermissionFunctionConstant.TIEP_NHAN_THIET_BI_MOI,
                    PermissionFunctionConstant.TIEP_NHAN_THIET_BI_BAO_HANH),
                Status = "'U-CN','U-SC'",
                Username = request.Username,
                Branch = string.Empty,
                RecordsPerPage = 500
            };

            using (var uow = unitOfWorkFactory())
            {
                orders = uow.GetRepository<IOrderRepository>().GetAwatingOrders(orderRequest);
                picklists = uow.GetRepository<IPicklistRepository>().GetAwaitingPicklists(picklistRequest);
                requisitions = uow.GetRepository<IRequisitionRepository>().GetAwaitingRequisitions(requisitionRequest);
            }

            var result = new DanhSachPhieuChoPheDuyetResponse();
            result.TotalRecords = orders.TotalRecords + picklists.TotalRecords + requisitions.TotalRecords;

            result.DanhSachPhieu.AddRange(Mapper.Map<IEnumerable<OrderSummary>, IEnumerable<PhieuChoPheDuyetResponse>>(orders.DataResult));
            result.DanhSachPhieu.AddRange(Mapper.Map<IEnumerable<PicklistSummary>, IEnumerable<PhieuChoPheDuyetResponse>>(picklists.DataResult));
            result.DanhSachPhieu.AddRange(Mapper.Map<IEnumerable<RequisitionSummary>, IEnumerable<PhieuChoPheDuyetResponse>>(requisitions.DataResult));

            return result;
        }
    }
}
