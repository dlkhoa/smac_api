﻿using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using System;

namespace Cmms.Business.ComponentServices
{
    public class ImportExportDeviceReportService : IImportExportDeviceReportService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;

        public ImportExportDeviceReportService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public IPagedResult<ImportExportDeviceReport> GetAwaitingRepairDevices(ImportExportDeviceReportQuery request)
        {
            using(var uow = unitOfWorkFactory())
            {
                var repository = uow.GetRepository<IImportExportDeviceReportRepository>();
                request.Organizations = uow.GetRepository<IUserRepository>().GetOranizations(request.Username);
                return repository.GetAwaitingRepairDevices(request);
            }
        }

        public IPagedResult<ImportExportDeviceReport> GetAfterRepairedDevices(ImportExportDeviceReportQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                var repository = uow.GetRepository<IImportExportDeviceReportRepository>();
                request.Organizations = uow.GetRepository<IUserRepository>().GetOranizations(request.Username);
                return repository.GetAfterRepairedDevices(request);
            }
        }

        public IPagedResult<ImportExportFixingDeviceReport> GetFixingDevices(ImportExportDeviceReportQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                var repository = uow.GetRepository<IImportExportDeviceReportRepository>();
                request.Organizations = uow.GetRepository<IUserRepository>().GetOranizations(request.Username);
                return repository.GetFixingDevices(request);
            }
        }
    }
}
