﻿using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Commands;
using System;
using Cmms.Business.Common.ExceptionHandlers;
using Cmms.Domain.Entities;
using Cmms.Data.Constants;

namespace Cmms.Business.ComponentServices
{
    public class OrderCommandService : IOrderCommandService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;

        public OrderCommandService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public NotificationItem ApproveOrder(ApproveOrderCommand command)
        {
            using (var uow = unitOfWorkFactory())
            {
                var cmdOrderRepository = uow.GetRepository<IOrderCommandRepository>();

                var orderRepository = uow.GetRepository<IOrderRepository>();
                var orderDetails = orderRepository.GetOrder(command.OrderCode);
                if(orderDetails == null)
                {
                    throw new ReadBusinessException(BusinessExceptionConstant.ORDER_NOT_FOUND);
                }
                if(orderDetails.MaTinhTrangPhieu == "U")
                {
                    command.NewStatus = "A-CN";
                }
                else if(orderDetails.MaTinhTrangPhieu == "U-CN")
                {
                    command.NewStatus = "W-CN";
                }
                else if(orderDetails.MaTinhTrangPhieu == "W-CN")
                {
                    command.NewStatus = "A-SC";
                }

                if (string.IsNullOrWhiteSpace(command.NewStatus))
                {
                    throw new WriteBusinessException(BusinessExceptionConstant.STATUS_CODE_INVALID);
                }

                cmdOrderRepository.UpdateOrderStatus(command);

                return new NotificationItem
                {
                    PrimaryId = command.OrderCode,
                    EntityType = EntityTypeConstants.Order,
                    FromStatus = orderDetails.MaTinhTrangPhieu,
                    ToStatus = command.NewStatus
                };
            }
        }
    }
}
