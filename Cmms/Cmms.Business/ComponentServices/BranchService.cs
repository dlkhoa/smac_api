﻿using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using System;

namespace Cmms.Business.ComponentServices
{
    public class BranchService : IBranchService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;

        public BranchService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }
        public IPagedResult<Branch> GetBranches(BranchQuery branchQuery)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IBranchRepository>().GetBranches(branchQuery);
            }
        }
    }
}
