﻿using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Business.DataContracts.DeviceStatistic;
using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cmms.Business.ComponentServices
{
    public class DeviceStatisticProviderService : IDeviceStatisticProviderService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;
        public DeviceStatisticProviderService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public DeviceStatisticResponse GetDeviceStatisticProvider(DeviceReportQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                request.Organizations = uow.GetRepository<IUserRepository>().GetOranizations(request.Username);
                var deviceStatRepository = uow.GetRepository<IDeviceStatisticRepository>();
                var queryResult = deviceStatRepository.GetDeviceStatisticProvider(request);
                var result = new DeviceStatisticResponse();

                var branches = queryResult.GroupBy(item => item.Branch).Select(item => item.Key);

                foreach (var b in branches)
                {
                    var branch = new DeviceStatisticBranchProviderResponse();
                    branch.Ten = b;
                    var providers = queryResult.Where(item => item.Branch.Equals(b));

                    foreach (var p in providers)
                    {
                        branch.TongSoThietBi += p.Total;
                        var provider = new DeviceStatisticProviderResponse
                        {
                            Ten = p.Provider,
                            TongSoThietBi = p.Total,
                            // ThietBi = deviceStatRepository.GetDeviceReportSummaryByProvider(b, p.Provider)
                        };
                        branch.DanhSachNhaCungCap.Add(provider);
                    }

                    result.DanhSachChiNhanh.Add(branch);
                    result.TongSoThietBi += branch.TongSoThietBi;
                }
                return result;
            }
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByProvider(DeviceReportByProviderQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDeviceStatisticRepository>().GetDeviceReportSummaryByProvider(request);
            }
        }

        public DeviceStatisticDVQLSDDai GetDeviceStatisticByDVQLSD_Dai(DeviceReportQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                var result = new DeviceStatisticDVQLSDDai();
                request.Organizations = uow.GetRepository<IUserRepository>().GetOranizations(request.Username);
                var queryResult = uow.GetRepository<IDeviceStatisticRepository>().GetDeviceStatisticByDVQLSD_Dai(request);
                var branches = queryResult.GroupBy(item => item.Branch).Select(item => item.Key);
                foreach (var b in branches)
                {
                    var branch = new DeviceStatisticDVQLSDDai_Branch();
                    branch.Ten = b;
                    var dvqlsd = queryResult.Where(item => item.Branch.Equals(b)).GroupBy(item => item.DVQLSD1).Select(item => item.Key);

                    foreach (var dv in dvqlsd)
                    {

                        var dvItem = new DeviceStatisticDVQLSDDai_DVQLSD
                        {
                            Ten = dv
                        };
                        var daiVTs = queryResult.Where(item => item.Branch.Equals(b) && (string.IsNullOrEmpty(item.DVQLSD1) || item.DVQLSD1.Equals(dv)));
                        foreach (var dai in daiVTs)
                        {
                            var daiItem = new DeviceStatisticDVQLSDDai_Dai();
                            daiItem.TongSoThietBi = dai.Total;
                            daiItem.Ten = dai.DaiVT;
                            dvItem.TongSoThietBi += daiItem.TongSoThietBi;
                            dvItem.DanhSachDai.Add(daiItem);
                        }

                        branch.TongSoThietBi += dvItem.TongSoThietBi;
                        branch.DanhSachDVQLSD.Add(dvItem);
                    }

                    result.DanhSachChiNhanh.Add(branch);
                    result.TongSoThietBi += branch.TongSoThietBi;
                }
                return result;
            }
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVQLSD_Dai(DeviceReportByDVQLSDDaiQuery request)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDeviceStatisticRepository>().GetDeviceReportSummaryByDVQLSD_Dai(request);
            }
        }
    }
}
