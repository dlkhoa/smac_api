﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Business.ComponentServices.NotificationStrategies;
using Cmms.Business.DataContracts;
using Cmms.Data.Constants;
using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Commands;
using Cmms.Domain.Entities;
using MBoL.Utils;
using Microsoft.Practices.ServiceLocation;

namespace Cmms.Business.ComponentServices
{
    public class NotificationService : INotificationService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;

        private Dictionary<string, string> entityTypeDic;
        public NotificationService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
            entityTypeDic = new Dictionary<string, string>
            {
                {EntityTypeConstants.Order, EntityTypeConstants.PhieuDeNghiNhapKho },
                {EntityTypeConstants.Picklist, EntityTypeConstants.PhieuDeNghiXuatKho},
                {EntityTypeConstants.Requisition, EntityTypeConstants.PhieuTiepNhanThietBi },
            };
        }

        public int RegisterDevice(RegisterNotificationCommand command)
        {
            using (var uow = unitOfWorkFactory())
            {
                var notificationRepository = uow.GetRepository<INotificationRepository>();
                var deviceId = command.DeviceId;
                var device = notificationRepository.GetDevice(deviceId);
                if (device != null)
                {
                    notificationRepository.DeleteMobileDevice(device);
                }
                device = new MobileDevice();
                device.DeviceId = deviceId;
                device.Username = command.Username.ToUpper();
                device.DeviceOS = ((DeviceOSType)command.OSType).ToString();
                return notificationRepository.AddNotificationDevice(device);

                //uow.SaveChanges();
            }

        }

        public void PushNotification(NotificationItem notificationItem)
        {
            PushNotificationAsync(notificationItem);
        }

        public void PushNotificationByTimer(object state)
        {
            using (var uow = unitOfWorkFactory())
            {
                try
                {
                    var logMessage = new StringBuilder();
                    logMessage.AppendLine("Notification Items: ");
                    var notiRepo = uow.GetRepository<INotificationRepository>();
                    var notificationItems = notiRepo.GetUnsendNotifications();

                    foreach (var item in notificationItems)
                    {
                        var log = ExecutePushing(uow, notiRepo, item);
                        if (!string.IsNullOrEmpty(log))
                        {
                            logMessage.AppendLine(log);
                        }
                    }

                    Logger.WriteLog(logMessage.ToString(), "Notification");
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(ex.GetFullMessage(), "Notification");
                }
            }

        }

        private string ExecutePushing(IUnitOfWork uow, INotificationRepository notificationRepository, NotificationItem item)
        {
            try
            {
                var notificationStategy = ServiceLocator.Current.GetInstance<INotificationStrategyService>(item.EntityType);
                var users = notificationStategy.GetUser(uow, item.PrimaryId, item.FromStatus, item.ToStatus);
                var deviceIDs = GetMobileDeviceId(uow, users);
                if (deviceIDs != null && deviceIDs.Count > 0)
                {
                    var notiMessage = notificationStategy.GetMessage(item.PrimaryId, item.FromStatus, item.ToStatus);

                    var pushResult = ExecutePushing(deviceIDs,
                        notiMessage,
                        new NotificationDataResponse { MaPhieu = item.PrimaryId, LoaiPhieu = entityTypeDic[item.EntityType] });
                    if (pushResult.Success > 0)
                    {
                        notificationRepository.UpdateNotificationStatus(item.PrimaryId, item.EntityType, item.ToStatus);
                    }
                    var error = pushResult.Results == null || pushResult.Results.Count() == 0 ? string.Empty : string.Join("+", pushResult.Results.Select(r => r.Error).ToArray());
                    return string.Format("EntityCode: {0}, EntityType: {1}, User: {2}, Number of Device: {3}, Success: {4}, Failure: {5}, Error: {6}",
                        item.PrimaryId,
                        item.EntityType,
                        string.Join(",", users),
                        deviceIDs.Count,
                        pushResult.Success,
                        pushResult.Failure,
                        error);
                }

                return null;
            }
            catch (Exception ex)
            {
                return ex.GetFullMessage();
            }
        }

        private Task PushNotificationAsync(NotificationItem notificationItem)
        {
            return Task.Factory.StartNew(() =>
            {
                using (var uow = unitOfWorkFactory())
                {
                    var notiRepository = uow.GetRepository<INotificationRepository>();
                    notiRepository.AddNotificationItem(notificationItem);
                    uow.SaveChanges();

                    var log = ExecutePushing(uow, notiRepository, notificationItem);
                    Logger.WriteLog(log, "Notification");
                }
            });
        }

        private List<string> GetMobileDeviceId(IUnitOfWork uow, List<string> usernames)
        {
            return uow.GetRepository<INotificationRepository>().GetDeviceIDs(usernames);
        }

        private FcmResponse ExecutePushing(List<string> deviceIds, string message, object data)
        {
            var result = new FcmResponse();
            var maxItemToSend = 1000;
            if (deviceIds != null && deviceIds.Count > 0)
            {
                if (deviceIds.Count <= maxItemToSend)
                {
                    result = FCMServiceHelper.PushNotification(deviceIds, message, data);
                }
                else
                {
                    var n = ((deviceIds.Count - 1) / maxItemToSend + 1);

                    for (var i = 0; i < n; i++)
                    {
                        var IDS = deviceIds.OrderBy(d => d).Skip(maxItemToSend * i).Take(maxItemToSend).ToList();
                        var resultItem = FCMServiceHelper.PushNotification(IDS, message, data);
                        result.Success += resultItem.Success;
                        result.Failure += resultItem.Failure;
                    }
                }
            }

            return result;
        }

        public int Delete(string deviceID)
        {
            using (var uow = unitOfWorkFactory())
            {
                var notificationRepository = uow.GetRepository<INotificationRepository>();

                if(deviceID == "*")
                {
                    return notificationRepository.DeleteAll();
                }
                else
                {
                    return notificationRepository.DeleteMobileDevice(new MobileDevice { DeviceId = deviceID });
                }
            }
        }
    }
}
