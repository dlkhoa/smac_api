﻿using Cmms.Business.DataContracts.DeviceStatistic;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;

namespace Cmms.Business.ComponentServices.Abstracts
{
    public interface IDeliveryFailureDeviceReportService
    {
        DeliveryDeviceStatisticProviderResponse GetStatisticByProvider(DeviceReportQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByProvider(DeliveryDeviceReportByProviderQuery request);
        DeliveryDeviceStatisticDVQLSDDaiResponse GetDeviceStatisticByDVQLSD_Dai(DeviceReportQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVQLSD_Dai(DeliveryDeviceReportByDVQLSDDaiQuery request);
    }
}
