﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;

namespace Cmms.Business.ComponentServices.Abstracts
{
    public interface ISearchDeviceForReportingService
    {
        IPagedResult<DeviceDetailsReportSummary> GetDevicesForReporting(DeviceReportAllLevelQuery request);
    }
}
