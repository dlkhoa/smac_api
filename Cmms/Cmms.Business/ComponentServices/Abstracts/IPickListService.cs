﻿using System.Collections.Generic;
using Cmms.Business.DataContracts;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;

namespace Cmms.Business.ComponentServices.Abstracts
{
    public interface IPicklistService
    {
        IPagedResult<PicklistSummary> GetPicklists(PicklistQuery picklistQuery);
        IPagedResult<PicklistSummary> GetAwaitingPicklists(PicklistQuery picklistQuery);
        PicklistDetailsResponse GetPicklist(string username, string picklistCode);
        List<string> GetNotifiedUsers(string picklistCode);
    }
}
