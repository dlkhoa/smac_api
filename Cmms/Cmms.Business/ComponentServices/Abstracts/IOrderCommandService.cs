﻿using Cmms.Domain.Commands;
using Cmms.Domain.Entities;

namespace Cmms.Business.ComponentServices.Abstracts
{
    public interface IOrderCommandService
    {
        NotificationItem ApproveOrder(ApproveOrderCommand command);
    }
}
