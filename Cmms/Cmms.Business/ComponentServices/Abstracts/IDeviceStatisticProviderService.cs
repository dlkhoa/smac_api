﻿using Cmms.Business.DataContracts.DeviceStatistic;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;

namespace Cmms.Business.ComponentServices.Abstracts
{
    public interface IDeviceStatisticProviderService
    {
        DeviceStatisticResponse GetDeviceStatisticProvider(DeviceReportQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByProvider(DeviceReportByProviderQuery request);
        DeviceStatisticDVQLSDDai GetDeviceStatisticByDVQLSD_Dai(DeviceReportQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVQLSD_Dai(DeviceReportByDVQLSDDaiQuery request);
    }
}