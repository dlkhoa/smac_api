﻿using Cmms.Business.DataContracts;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;

namespace Cmms.Business.ComponentServices.Abstracts
{
    public interface IDeviceService
    {
        DeviceDetailsResponse GetDevice(string username, string serialCode);
        IPagedResult<DeviceSummary> GetDevices(DeviceQuery request);
        IPagedResult<DeviceHistorySummary> GetDeviceHistories(DeviceHistoryQuery request);
        IPagedResult<DeviceSummary> GetAdvancedDevices(DeviceAdvancedQuery request);
    }
}
