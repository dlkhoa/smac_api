﻿using Cmms.Domain.Commands;
using Cmms.Domain.Entities;

namespace Cmms.Business.ComponentServices.Abstracts
{
    public interface IPicklistCommandService
    {
        NotificationItem ApprovePicklist(ApprovePicklistCommand command);
    }
}
