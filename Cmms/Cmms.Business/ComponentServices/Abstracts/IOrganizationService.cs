﻿using Cmms.Domain.Entities;
using System.Collections.Generic;

namespace Cmms.Business.ComponentServices.Abstracts
{
    public interface IOrganizationService
    {
        IEnumerable<Organization> GetBranches();
    }
}
