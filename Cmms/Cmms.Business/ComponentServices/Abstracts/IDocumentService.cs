﻿using Cmms.Domain.Entities.DataContracts;
using System.Collections.Generic;

namespace Cmms.Business.ComponentServices.Abstracts
{
    public interface IDocumentService
    {
        IEnumerable<DocumentDetails> GetDocumentList(string type, string orderCode);
    }
}
