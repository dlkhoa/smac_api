﻿using Cmms.Business.DataContracts.DeviceStatistic;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;

namespace Cmms.Business.ComponentServices.Abstracts
{
    public interface IInventoryDeviceReportService
    {
        InventoryDeviceStatisticDVQLSDLoaiTBResponse GetInventoryDeviceStatisticByLoaiThietBi(DeviceReportQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVQLSD_LoaiTB(InventoryDeviceReportByDVQLSDLoaiTBQuery request);
        InventoryDeviceStatisticDVQLSDBPCResponse GetInventoryDeviceStatisticBPCByDVQLSD(DeviceReportQuery request);
        InventoryDeviceStatisticLHDBPCResponse GetInventoryDeviceStatisticBPCByLHD(DeviceReportQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVQLSD(InventoryDeviceReportByDVQLSDQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByLoaiHD(InventoryDeviceReportByLoaiHDQuery request);
    }
}
