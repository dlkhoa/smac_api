﻿using System.Collections.Generic;
using Cmms.Business.DataContracts;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;

namespace Cmms.Business.ComponentServices.Abstracts
{
    public interface IRequisitionService
    {
        IPagedResult<RequisitionSummary> LayDanhMucPhieuTiepNhanThietBi(RequisitionQuery request);
        IPagedResult<RequisitionSummary> LayDanhMucPhieuTiepNhanThietBiChoPheDuyet(RequisitionQuery request);
        RequisitionDetailsResponse GetRequisition(string username, string requisitionCode);
        List<string> GetNotifiedUsers(string requisitionCode);
    }
}
