﻿using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;

namespace Cmms.Business.ComponentServices.Abstracts
{
    public interface IImportExportDeviceReportService
    {
        IPagedResult<ImportExportDeviceReport> GetAwaitingRepairDevices(ImportExportDeviceReportQuery request);
        IPagedResult<ImportExportDeviceReport> GetAfterRepairedDevices(ImportExportDeviceReportQuery request);
        IPagedResult<ImportExportFixingDeviceReport> GetFixingDevices(ImportExportDeviceReportQuery request);
    }
}
