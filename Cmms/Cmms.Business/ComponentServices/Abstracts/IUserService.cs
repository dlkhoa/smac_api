﻿using System.Collections.Generic;
using Cmms.Business.DataContracts;

namespace Cmms.Business.ComponentServices.Abstracts
{
    public interface IUserService
    {
        UserResponse Authenticate(string username, string password);
    }
}
