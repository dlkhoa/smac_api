﻿using Cmms.Business.DataContracts.DeviceStatistic;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;

namespace Cmms.Business.ComponentServices.Abstracts
{
    public interface INotDeliveryDeviceReportService
    {
        NotDeliveryDeviceStatisticDTSCResponse GetStatisticByDTSC(DeviceReportQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDTSC(NotDeliveryDeviceReportByDTSCQuery request);
        NotDeliveryDeviceStatisticDVTResponse GetDeviceStatisticByDVT(DeviceReportQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVT(NotDeliveryDeviceReportByDVTQuery request);
    }
}
