﻿using Cmms.Business.DataContracts.DeviceStatistic;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;

namespace Cmms.Business.ComponentServices.Abstracts
{
    public interface IOverRepairTimeDeviceReportService
    {
        OverRepairTimeDeviceDTSCResponse GetStatisticByDoiTac(DeviceReportQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDTSC(OverRepairTimeDeviceReportByDTSCQuery request);
    }
}
