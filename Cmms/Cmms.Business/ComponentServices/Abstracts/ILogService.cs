﻿namespace Cmms.Business.ComponentServices.Abstracts
{
    public interface ILogService
    {
        void WriteRequestLogAsync<T>(T entity);
    }
}
