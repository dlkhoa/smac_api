﻿using System.Collections.Generic;
using Cmms.Domain.Commands;
using System;
using Cmms.Domain.Entities;

namespace Cmms.Business.ComponentServices.Abstracts
{
    public interface INotificationService
    {
        int RegisterDevice(RegisterNotificationCommand command);
        void PushNotification(NotificationItem notificationItem);
        void PushNotificationByTimer(object state);
        int Delete(string deviceID);
    }
}
