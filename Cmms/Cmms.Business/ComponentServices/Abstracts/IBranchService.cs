﻿using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;

namespace Cmms.Business.ComponentServices.Abstracts
{
    public interface IBranchService
    {
        IPagedResult<Branch> GetBranches(BranchQuery branchQuery);
    }
}
