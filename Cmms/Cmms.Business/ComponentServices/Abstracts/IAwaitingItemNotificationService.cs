﻿using Cmms.Business.DataContracts;
using Cmms.Domain.Queries;

namespace Cmms.Business.ComponentServices.Abstracts
{
    public interface IAwaitingItemNotificationService
    {
        DanhSachPhieuChoPheDuyetResponse LayDanhSachPhieuChoPheDuyet(AwaitingItemNotificationQuery request);
    }
}
