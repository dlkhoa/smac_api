﻿using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities;
using Cmms.Domain.Queries;

namespace Cmms.Business.ComponentServices.Abstracts
{
    public interface IProviderService
    {
        IPagedResult<Provider> GetProviders(ProviderQuery providerQuery);
    }
}
