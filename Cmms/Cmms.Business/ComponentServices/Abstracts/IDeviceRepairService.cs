﻿using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Commands;
using Cmms.Domain.Entities;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries.DeviceRepair;
using System.Collections.Generic;

namespace Cmms.Business.ComponentServices.Abstracts
{
    public interface IDeviceRepairService
    {
        RepairDeviceDetails GetRepairDevice(RepairDeviceQuery request);
        RepairDeviceDetails GetRepairDeviceById(RepairDeviceQuery request);
        RepairDeviceDetails GetRepairedDevice(RepairDeviceQuery request);
        string AddRepairDevice(AddRepairDeviceCommand command);
        RepairDeviceSummarize SummarizeReport(DeviceRepairSummarizeReportQuery request);
        IPagedResult<RepairDeviceDetails> SummarizeListReport(DeviceRepairSummarizeReportQuery request);
        IPagedResult<AvailableDeviceDetails> GetAvailableDeviceList(AvailableDeviceListQuery request);
        IPagedResult<PriorityRepairDeviceDetails> GetPriorityRepairDeviceList(PriorityRepairDeviceListQuery request);
        IPagedResult<RepairDeviceDetails> GetRepairDeviceList(RepairDeviceListQuery request);
        string ReceiveDeviceFromTVT(ReceiveDeviceFromTVTCommand command);
        string ReceiveDeviceFromTTDKSC(ReceiveDeviceFromTTDKSCCommand command);
        string DeliveryDevicesToTVT(DeliveryDevicesToTVTCommand command);
        IEnumerable<WorkingDeviceDetails> GetWorkingDevices(string serial);
        string SetWorkingDate(SetWorkingDateCommand command);

        IEnumerable<string> GetDeviceNames();
        IEnumerable<string> GetCenterList(string username);
        IEnumerable<TVT> GetTVTList(string dvt);
        string GenerateBarcode(string dvt);

        IEnumerable<RepairKPIReportItem> RepairKPIReport(RepairKPIReportQuery request);
        IEnumerable<RepairTimeReportItem> RepairTimeReport(RepairTimeReportQuery request);
        IEnumerable<RepairInventoryReportItem> RepairInventoryReport(RepairInventoryReportQuery request);
    }
}
