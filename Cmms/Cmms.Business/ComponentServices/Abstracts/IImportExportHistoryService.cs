﻿using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;

namespace Cmms.Business.ComponentServices.Abstracts
{
    public interface IImportExportHistoryService
    {
        IPagedResult<ImportExportHistorySummary> GetHistories(ImportExportHistoryQuery query);
    }
}
