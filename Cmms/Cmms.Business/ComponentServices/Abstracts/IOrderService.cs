﻿using Cmms.Business.DataContracts;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using System.Collections.Generic;

namespace Cmms.Business.ComponentServices.Abstracts
{
    public interface IOrderService
    {
        IPagedResult<OrderSummary> GetOrders(OrderQuery orderQuery);
        IPagedResult<OrderSummary> GetAwatingOrders(AwatingOrderQuery orderQuery);
        OrderDetailsResponse GetOrder(string username, string orderCode);
        List<string> GetNotifiedUsers(string orderCode);
    }
}
