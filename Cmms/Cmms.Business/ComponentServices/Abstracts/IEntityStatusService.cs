﻿using Cmms.Business.DataContracts;
using System.Collections.Generic;

namespace Cmms.Business.ComponentServices.Abstracts
{
    public interface IEntityStatusService
    {
        IEnumerable<EntityStatusResponse> GetStatuses(string entityCode);
    }
}
