﻿using Cmms.Business.DataContracts.DeviceStatistic;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;

namespace Cmms.Business.ComponentServices.Abstracts
{
    public interface IAnalyzeReportService
    {
        DeviceReportByYearResponse GetReceivedDeviceReportByYear(DeviceReportByYearQuery request);
        DeviceReportByYearResponse GetSentDeviceReportByYear(DeviceReportByYearQuery request);
        FulfillReportByYearResponse GetCenterFulfillReportByYear(CenterFulfillReportByYearQuery request);
        FulfillReportByYearResponse GetBranchFulfillReportByYear(BranchFulfillReportByYearQuery request);
        NumberOfDevicesReportByYearResponse GetCenterNumberOfReceivedDevicesReportByYear(CenterNumberOfDevicesReportByYearQuery request);
        NumberOfDevicesReportByYearResponse GetBranchNumberOfReceivedDevicesReportByYear(BranchNumberOfDevicesReportByYearQuery request);
        NumberOfDevicesReportByYearResponse GetCenterNumberOfSentDevicesReportByYear(CenterNumberOfDevicesReportByYearQuery request);
        NumberOfDevicesReportByYearResponse GetBranchNumberOfSentDevicesReportByYear(BranchNumberOfDevicesReportByYearQuery request);
        SummaryByBranchAndByYear GetSummaryByBranchAndByYear(SummaryByBranchAndByYearQuery request);
    }
}
