﻿using AutoMapper;
using Cmms.Business.Common.ExceptionHandlers;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Business.DataContracts;
using Cmms.Data.Constants;
using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Cmms.Business.ComponentServices
{
    public class UserService : IUserService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;

        public UserService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public UserResponse Authenticate(string username, string password)
        {
            using (var uow = unitOfWorkFactory())
            {
                var user = uow.GetRepository<IUserRepository>().GetUser(username);
                if (user == null)
                {
                    throw new ReadBusinessException(BusinessExceptionConstant.INVALID_USER);
                }

                if (!Extension.Jar.PasswordChecker.IsEqual(user.Password, password.ToUpper(), user.UserCode))
                {
                    throw new ReadBusinessException(BusinessExceptionConstant.INVALID_PASSWORD);
                }

                var result = Mapper.Map<User, UserResponse>(user);

                var roles = uow.GetRepository<IRoleRepository>().GetRoleByGroupCode(user.GroupCode);
                result.Roles.AddRange(Mapper.Map<IEnumerable<Role>, IEnumerable<RoleResponse>>(roles));

                if (user.GroupCode == GroupCodeConstants.ADMIN ||
                    user.GroupCode == GroupCodeConstants.TVT ||
                    user.GroupCode == GroupCodeConstants.DVT)
                {
                    var userUnit = uow.GetRepository<IDeviceRepairRepository>().GetUserUnit(user.UserCode);
                    result.TVT = userUnit.TVT;
                    result.DVT = userUnit.DVT;
                }

                return result;
            }
        }
    }
}
