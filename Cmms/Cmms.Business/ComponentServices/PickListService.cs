﻿using AutoMapper;
using Cmms.Business.Common.ExceptionHandlers;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Business.DataContracts;
using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using System;
using System.Collections.Generic;

namespace Cmms.Business.ComponentServices
{
    public class PicklistService : IPicklistService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;

        public PicklistService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public IPagedResult<PicklistSummary> GetPicklists(PicklistQuery picklistQuery)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IPicklistRepository>().GetPicklists(picklistQuery);
            }
        }

        public PicklistDetailsResponse GetPicklist(string username, string picklistCode)
        {
            using (var uow = unitOfWorkFactory())
            {
                var picklist = uow.GetRepository<IPicklistRepository>()
                    .GetPicklist(picklistCode);

                if (picklist == null)
                {
                    throw new ReadBusinessException(BusinessExceptionConstant.PICKLIST_NOT_FOUND);
                }

                var result = Mapper.Map<PicklistDetails, PicklistDetailsResponse>(picklist);
                result.DanhSachThietBi = uow.GetRepository<IPicklistPartRepository>().GetPicklistPartsForPicklist(picklistCode);
                return result;
            }
        }
        public IPagedResult<PicklistSummary> GetAwaitingPicklists(PicklistQuery picklistQuery)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IPicklistRepository>().GetAwaitingPicklists(picklistQuery);
            }
        }

        public List<string> GetNotifiedUsers(string picklistCode)
        {
            using (var uow = unitOfWorkFactory())
            {
                var picklistRepository = uow.GetRepository<IPicklistRepository>();
                var createdUser = picklistRepository.GetCreatedUser(picklistCode);
                return new List<string> { createdUser };
            }
        }
    }
}
