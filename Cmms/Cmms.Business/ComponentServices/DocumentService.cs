﻿using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Business.DataContracts.DeviceStatistic;
using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cmms.Business.ComponentServices
{
    public class DocumentService : IDocumentService
    {
        private readonly Func<IUnitOfWork> unitOfWorkFactory;

        public DocumentService(Func<IUnitOfWork> unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public IEnumerable<DocumentDetails> GetDocumentList(string type, string orderCode)
        {
            using (var uow = unitOfWorkFactory())
            {
                return uow.GetRepository<IDocumentRepository>().GetDocumentList(type, orderCode);
            }
        }
    }
}