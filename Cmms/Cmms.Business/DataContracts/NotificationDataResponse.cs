﻿namespace Cmms.Business.DataContracts
{
    public class NotificationDataResponse
    {
        public string MaPhieu { get; set; }
        public string LoaiPhieu { get; set; }
    }
}
