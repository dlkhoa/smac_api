﻿using Cmms.Domain.Entities.DataContracts;
using System;
using System.Collections.Generic;

namespace Cmms.Business.DataContracts
{
    public class PicklistDetailsResponse
    {
        public string MaSoPhieu { get; set; }
        public string ChiNhanh { get; set; }
        public string MoTa { get; set; }
        public string Kho { get; set; }
        public string MaTinhTrangPhieu { get; set; }
        public string TinhTrangPhieu { get; set; }
        public string DoiTacDVQLSD { get; set; }
        public DateTime? NgayDeNghi { get; set; }
        public string NguoiDeNghi { get; set; }
        public DateTime? NgayPheDuyet { get; set; }
        public string NguoiPheDuyet { get; set; }
        public string LyDoXuatKho { get; set; }

        public IEnumerable<PicklistPartSummary> DanhSachThietBi { get; set; }
    }
}
