﻿namespace Cmms.Business.DataContracts
{
    public enum DeviceReportType
    {
        BaoCaoThietBiDaTiepNhanTuDVQLSD = 1,
        BaoCaoThietBiBanGiaoChoDVQLSD = 2,
        BaoCaoThietBiTonKho = 3,
        BaoCaoThietBiTotSauSuaChuaGiaoChoDVQLSD = 4,
        BaoCaoThietBiNFFGiaoChoDVQLSD = 5,
        BaoCaoThietBiKoTheSuaGiaoChoDVQLSD = 6,
        BaoCaoThietBiDoiTacChuaBanGiao = 7,
        BaoCaoThietBiDoiTacTuChoiHTKT = 8,
        BaoCaoThietBiSLADaBanGiao = 9,
        BaoCaoThietBiQuayLaiSuaChua = 10,
        BaoCaoThietBiQuaThoiGianSuaChua = 11
    }
}
