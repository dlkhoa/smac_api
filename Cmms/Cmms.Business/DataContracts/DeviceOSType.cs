﻿namespace Cmms.Business.DataContracts
{
    public enum DeviceOSType : byte
    {
        Android = 0,
        iOS = 1
    }
}
