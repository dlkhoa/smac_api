﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class DeliveryDeviceStatisticDVQLSDDaiResponse : DeviceStatisticItem
    {
        public List<DeliveryDeviceStatisticDVQLSDDai_Branch> DanhSachChiNhanh { get; set; }

        public DeliveryDeviceStatisticDVQLSDDaiResponse()
        {
            DanhSachChiNhanh = new List<DeliveryDeviceStatisticDVQLSDDai_Branch>();
        }

    }
}
