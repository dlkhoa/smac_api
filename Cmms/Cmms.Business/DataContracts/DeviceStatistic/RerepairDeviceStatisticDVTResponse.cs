﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class RerepairDeviceStatisticDVTResponse : DeviceStatisticItem
    {
        public List<RerepairDeviceStatisticDVT_Branch> DanhSachChiNhanh { get; set; }
        public RerepairDeviceStatisticDVTResponse()
        {
            DanhSachChiNhanh = new List<RerepairDeviceStatisticDVT_Branch>();
        }
    }
}
