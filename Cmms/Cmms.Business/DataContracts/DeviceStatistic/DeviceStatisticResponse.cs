﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class DeviceStatisticResponse : DeviceStatisticItem
    {
        public List<DeviceStatisticBranchProviderResponse> DanhSachChiNhanh { get; set; }

        public DeviceStatisticResponse()
        {
            DanhSachChiNhanh = new List<DeviceStatisticBranchProviderResponse>();
        }
    }
}
