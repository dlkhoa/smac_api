﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class DeliveryDeviceStatisticDVQLSDDai_DVQLSD : DeviceStatisticItem
    {
        public List<DeliveryDeviceStatisticDVQLSDDai_DaiVT> DanhSachDai { get; set; }

        public DeliveryDeviceStatisticDVQLSDDai_DVQLSD()
        {
            DanhSachDai = new List<DeliveryDeviceStatisticDVQLSDDai_DaiVT>();
        }
    }
}