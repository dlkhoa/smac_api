﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class FulfillReportByYearResponse
    {
        public decimal Fulfillment { get; set; }

        public FulfillReportByYearResponse()
        {
            Fulfillment = 0;
        }
    }
}
