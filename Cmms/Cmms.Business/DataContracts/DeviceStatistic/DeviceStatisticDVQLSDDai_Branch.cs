﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class DeviceStatisticDVQLSDDai_Branch : DeviceStatisticItem
    {
        public List<DeviceStatisticDVQLSDDai_DVQLSD> DanhSachDVQLSD { get; set; }

        public DeviceStatisticDVQLSDDai_Branch()
        {
            DanhSachDVQLSD = new List<DeviceStatisticDVQLSDDai_DVQLSD>();
        }
    }
}
