﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class RerepairDeviceStatisticDTSC_Branch : DeviceStatisticItem
    {
        public List<RerepairDeviceStatisticDTSC_DoiTuongTB> DanhSachDoiTuongTB { get; set; }

        public RerepairDeviceStatisticDTSC_Branch()
        {
            DanhSachDoiTuongTB = new List<RerepairDeviceStatisticDTSC_DoiTuongTB>();
        }
    }
}