﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class InventoryDeviceStatisticLHDBPC_Branch :InventoryDeviceStatisticBPCItem
    {
        public List<InventoryDeviceStatisticBPC_LHD> DanhSachLoaiHD { get; set; }

        public InventoryDeviceStatisticLHDBPC_Branch()
        {
            DanhSachLoaiHD = new List<InventoryDeviceStatisticBPC_LHD>();
        }
    }
}