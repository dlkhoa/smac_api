﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class DeliveryGoodDeviceStatisticDVQLSDDai_Branch : DeviceStatisticItem
    {
        public List<DeliveryGoodDeviceStatisticDVQLSDDai_DVQLSD> DanhSachDVQLSD { get; set; }

        public DeliveryGoodDeviceStatisticDVQLSDDai_Branch()
        {
            DanhSachDVQLSD = new List<DeliveryGoodDeviceStatisticDVQLSDDai_DVQLSD>();
        }
    }
}