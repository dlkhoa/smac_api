﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class NumberOfDevicesReportByYearResponse
    {
        public int NumberOfDevices { get; set; }

        public NumberOfDevicesReportByYearResponse()
        {
            NumberOfDevices = 0;
        }
    }
}
