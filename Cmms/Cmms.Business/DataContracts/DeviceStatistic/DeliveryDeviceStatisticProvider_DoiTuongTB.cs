﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class DeliveryDeviceStatisticProvider_DoiTuongTB : DeviceStatisticItem   
    {
        public List<DeliveryDeviceStatisticProvider_NhaCungCap> DanhSachNhaCungCap { get; set; }

        public DeliveryDeviceStatisticProvider_DoiTuongTB()
        {
            DanhSachNhaCungCap = new List<DeliveryDeviceStatisticProvider_NhaCungCap>();
        }
    }
}