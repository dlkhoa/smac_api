﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class InventoryDeviceStatisticLHDBPCResponse : InventoryDeviceStatisticBPCItem
    {
        public List<InventoryDeviceStatisticLHDBPC_Branch> DanhSachChiNhanh { get; set; }

        public InventoryDeviceStatisticLHDBPCResponse()
        {
            DanhSachChiNhanh = new List<InventoryDeviceStatisticLHDBPC_Branch>();
        }
    }
}
