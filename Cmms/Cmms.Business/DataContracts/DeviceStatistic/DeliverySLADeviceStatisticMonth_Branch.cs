﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class DeliverySLADeviceStatisticMonth_Branch : DeviceStatisticItem
    {
        public List<DeliverySLADeviceStatisticMonth_Month> DanhSachThang { get; set; }

        public DeliverySLADeviceStatisticMonth_Branch()
        {
            DanhSachThang = new List<DeliverySLADeviceStatisticMonth_Month>();
        }
    }
}