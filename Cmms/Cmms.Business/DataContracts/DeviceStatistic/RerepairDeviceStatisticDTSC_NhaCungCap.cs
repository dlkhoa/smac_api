﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class RerepairDeviceStatisticDTSC_NhaCungCap : DeviceStatisticItem
    {
        public List<RerepairDeviceStatisticDTSC_DoiTac> DanhSachDoiTac { get; set; }

        public RerepairDeviceStatisticDTSC_NhaCungCap()
        {
            DanhSachDoiTac = new List<RerepairDeviceStatisticDTSC_DoiTac>();
        }
    }
}