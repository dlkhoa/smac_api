﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class RefusedHTKTDeviceStatisticProvider_Branch : DeviceStatisticItem
    {
        public List<RefusedHTKTDeviceStatisticProvider_NhaCungCap> DanhSachNhaCungCap { get; set; }

        public RefusedHTKTDeviceStatisticProvider_Branch()
        {
            DanhSachNhaCungCap = new List<RefusedHTKTDeviceStatisticProvider_NhaCungCap>();
        }
    }
}