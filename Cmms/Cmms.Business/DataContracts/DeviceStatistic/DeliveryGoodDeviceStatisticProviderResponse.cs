﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class DeliveryGoodDeviceStatisticProviderResponse : DeviceStatisticItem
    {
        public List<DeliveryGoodDeviceStatisticProvider_Branch> DanhSachChiNhanh { get; set; }

        public DeliveryGoodDeviceStatisticProviderResponse()
        {
            DanhSachChiNhanh = new List<DeliveryGoodDeviceStatisticProvider_Branch>();
        }
    }
}
