﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class RefusedHTKTDeviceStatisticProviderResponse : DeviceStatisticItem
    {
        public List<RefusedHTKTDeviceStatisticProvider_Branch> DanhSachChiNhanh { get; set; }

        public RefusedHTKTDeviceStatisticProviderResponse()
        {
            DanhSachChiNhanh = new List<RefusedHTKTDeviceStatisticProvider_Branch>();
        }
    }
}
