﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class RerepairDeviceStatisticDTSCResponse : DeviceStatisticItem
    {
        public List<RerepairDeviceStatisticDTSC_Branch> DanhSachChiNhanh { get; set; }

        public RerepairDeviceStatisticDTSCResponse()
        {
            DanhSachChiNhanh = new List<RerepairDeviceStatisticDTSC_Branch>();
        }
    }
}
