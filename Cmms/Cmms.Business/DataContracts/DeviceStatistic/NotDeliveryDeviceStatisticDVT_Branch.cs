﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class NotDeliveryDeviceStatisticDVT_Branch : DeviceStatisticItem
    {
        public List<NotDeliveryDeviceStatisticDVT_DVQLSD> DanhSachDVQLSD { get; set; }
        public NotDeliveryDeviceStatisticDVT_Branch()
        {
            DanhSachDVQLSD = new List<NotDeliveryDeviceStatisticDVT_DVQLSD>();
        }
    }
}