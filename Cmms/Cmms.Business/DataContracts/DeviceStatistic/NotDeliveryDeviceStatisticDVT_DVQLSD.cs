﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class NotDeliveryDeviceStatisticDVT_DVQLSD : DeviceStatisticItem
    {
        public List<NotDeliveryDeviceStatisticDVT_DVT> DanhSachDai { get; set; }

        public NotDeliveryDeviceStatisticDVT_DVQLSD()
        {
            DanhSachDai = new List<NotDeliveryDeviceStatisticDVT_DVT>();
        }
    }
}