﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class NotDeliveryDeviceStatisticDVTResponse : DeviceStatisticItem
    {
        public List<NotDeliveryDeviceStatisticDVT_Branch> DanhSachChiNhanh { get; set; }

        public NotDeliveryDeviceStatisticDVTResponse()
        {
            DanhSachChiNhanh = new List<NotDeliveryDeviceStatisticDVT_Branch>();
        }
    }
}
