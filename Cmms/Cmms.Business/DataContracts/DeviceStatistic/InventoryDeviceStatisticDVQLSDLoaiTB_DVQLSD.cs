﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class InventoryDeviceStatisticDVQLSDLoaiTB_DVQLSD : DeviceStatisticItem
    {
        public List<InventoryDeviceStatisticDVQLSDLoaiTB_LoaiTB> DanhSachLoaiThietBi { get; set; }

        public InventoryDeviceStatisticDVQLSDLoaiTB_DVQLSD()
        {
            DanhSachLoaiThietBi = new List<InventoryDeviceStatisticDVQLSDLoaiTB_LoaiTB>();
        }
    }
}