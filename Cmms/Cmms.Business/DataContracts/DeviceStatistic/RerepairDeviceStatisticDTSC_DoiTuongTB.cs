﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class RerepairDeviceStatisticDTSC_DoiTuongTB : DeviceStatisticItem
    {
        public List<RerepairDeviceStatisticDTSC_NhaCungCap> DanhSachNhaCungCap { get; set; }

        public RerepairDeviceStatisticDTSC_DoiTuongTB()
        {
            DanhSachNhaCungCap = new List<RerepairDeviceStatisticDTSC_NhaCungCap>();
        }
    }
}