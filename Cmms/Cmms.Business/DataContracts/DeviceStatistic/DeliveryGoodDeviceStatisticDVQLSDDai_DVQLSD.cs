﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class DeliveryGoodDeviceStatisticDVQLSDDai_DVQLSD : DeviceStatisticItem
    {
        public List<DeliveryGoodDeviceStatisticDVQLSDDai_DaiVT> DanhSachDai { get; set; }

        public DeliveryGoodDeviceStatisticDVQLSDDai_DVQLSD()
        {
            DanhSachDai = new List<DeliveryGoodDeviceStatisticDVQLSDDai_DaiVT>();
        }
    }
}