﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class OverRepairTimeDeviceDTSCResponse : DeviceStatisticItem
    {
        public List<OverRepairTimeDeviceDTSC_Branch> DanhSachChiNhanh { get; set; }

        public OverRepairTimeDeviceDTSCResponse()
        {
            DanhSachChiNhanh = new List<OverRepairTimeDeviceDTSC_Branch>();
        }
    }
}
