﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class DeliveryGoodDeviceStatisticDVQLSDDaiResponse : DeviceStatisticItem
    {
        public List<DeliveryGoodDeviceStatisticDVQLSDDai_Branch> DanhSachChiNhanh { get; set; }

        public DeliveryGoodDeviceStatisticDVQLSDDaiResponse()
        {
            DanhSachChiNhanh = new List<DeliveryGoodDeviceStatisticDVQLSDDai_Branch>();
        }

    }
}
