﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class NotDeliveryDeviceStatisticDTSC_NhaCungCap : DeviceStatisticItem
    {
        public List<NotDeliveryDeviceStatisticDTSC_DoiTac> DanhSachDoiTac { get; set; }

        public NotDeliveryDeviceStatisticDTSC_NhaCungCap()
        {
            DanhSachDoiTac = new List<NotDeliveryDeviceStatisticDTSC_DoiTac>();
        }
    }
}