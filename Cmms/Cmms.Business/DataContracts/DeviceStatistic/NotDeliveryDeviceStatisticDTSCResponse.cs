﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class NotDeliveryDeviceStatisticDTSCResponse : DeviceStatisticItem
    {
        public List<NotDeliveryDeviceStatisticDTSC_Branch> DanhSachChiNhanh { get; set; }

        public NotDeliveryDeviceStatisticDTSCResponse()
        {
            DanhSachChiNhanh = new List<NotDeliveryDeviceStatisticDTSC_Branch>();
        }
    }
}
