﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class DeviceReportByYearResponse
    {
        public List<DeviceMonthItem> SoThietbiTheoThang { get; set; }

        public DeviceReportByYearResponse()
        {
            SoThietbiTheoThang = new List<DeviceMonthItem>();
            for (int i = 1; i <= 12; i++)
            {
                var deviceMonthItem = new DeviceMonthItem(i, 0);
                SoThietbiTheoThang.Add(deviceMonthItem);
            }
        }

        public void AddDeviceToMonth(int n, int m)
        {
            for (int i = 0; i < SoThietbiTheoThang.Count; i++)
            {
                var deviceMonthItem = SoThietbiTheoThang[i];
                if (deviceMonthItem.Thang == m)
                {
                    deviceMonthItem.SoThietBi += n;
                    break;
                }
            }
        }
    }
}
