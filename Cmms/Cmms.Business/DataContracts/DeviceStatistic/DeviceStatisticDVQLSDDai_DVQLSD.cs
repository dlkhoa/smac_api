﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class DeviceStatisticDVQLSDDai_DVQLSD : DeviceStatisticItem
    {
        public List<DeviceStatisticDVQLSDDai_Dai> DanhSachDai { get; set; }

        public DeviceStatisticDVQLSDDai_DVQLSD()
        {
            DanhSachDai = new List<DeviceStatisticDVQLSDDai_Dai>();
        }
    }
}
