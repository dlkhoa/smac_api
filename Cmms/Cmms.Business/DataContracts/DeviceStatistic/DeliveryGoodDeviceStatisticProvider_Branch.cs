﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class DeliveryGoodDeviceStatisticProvider_Branch : DeviceStatisticItem
    {
        public List<DeliveryGoodDeviceStatisticProvider_DoiTuongTB> DanhSachDoiTuongTB { get; set; }

        public DeliveryGoodDeviceStatisticProvider_Branch()
        {
            DanhSachDoiTuongTB = new List<DeliveryGoodDeviceStatisticProvider_DoiTuongTB>();
        }
    }
}