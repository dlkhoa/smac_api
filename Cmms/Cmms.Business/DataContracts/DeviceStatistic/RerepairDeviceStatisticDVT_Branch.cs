﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class RerepairDeviceStatisticDVT_Branch : DeviceStatisticItem
    {
        public List<RerepairDeviceStatisticDVT_DVQLSD> DanhSachDVQLSD { get; set; }

        public RerepairDeviceStatisticDVT_Branch()
        {
            DanhSachDVQLSD = new List<RerepairDeviceStatisticDVT_DVQLSD>();
        }
    }
}