﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class DeliveryDeviceStatisticDVQLSDDai_Branch : DeviceStatisticItem
    {
        public List<DeliveryDeviceStatisticDVQLSDDai_DVQLSD> DanhSachDVQLSD { get; set; }

        public DeliveryDeviceStatisticDVQLSDDai_Branch()
        {
            DanhSachDVQLSD = new List<DeliveryDeviceStatisticDVQLSDDai_DVQLSD>();
        }
    }
}