﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class DeliveryDeviceStatisticProvider_Branch : DeviceStatisticItem
    {
        public List<DeliveryDeviceStatisticProvider_DoiTuongTB> DanhSachDoiTuongTB { get; set; }

        public DeliveryDeviceStatisticProvider_Branch()
        {
            DanhSachDoiTuongTB = new List<DeliveryDeviceStatisticProvider_DoiTuongTB>();
        }
    }
}