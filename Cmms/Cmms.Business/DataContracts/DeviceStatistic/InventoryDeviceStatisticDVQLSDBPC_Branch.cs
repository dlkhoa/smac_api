﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class InventoryDeviceStatisticDVQLSDBPC_Branch : InventoryDeviceStatisticBPCItem
    {
        public List<InventoryDeviceStatisticBPC_DVQLSD> DanhSachDVQLSD { get; set; }

        public InventoryDeviceStatisticDVQLSDBPC_Branch()
        {
            DanhSachDVQLSD = new List<InventoryDeviceStatisticBPC_DVQLSD>();
        }
    }
}