﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class OverRepairTimeDeviceDTSC_Branch : DeviceStatisticItem
    {
        public List<OverRepairTimeDeviceDTSC_DoiTac> DanhSachDoiTac { get; set; }

        public OverRepairTimeDeviceDTSC_Branch()
        {
            DanhSachDoiTac = new List<OverRepairTimeDeviceDTSC_DoiTac>();
        }
    }
}