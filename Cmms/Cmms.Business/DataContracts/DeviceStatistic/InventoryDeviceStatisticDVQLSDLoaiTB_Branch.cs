﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class InventoryDeviceStatisticDVQLSDLoaiTB_Branch : DeviceStatisticItem
    {
        public List<InventoryDeviceStatisticDVQLSDLoaiTB_DVQLSD> DanhSachDVQLSD { get; set; }

        public InventoryDeviceStatisticDVQLSDLoaiTB_Branch()
        {
            DanhSachDVQLSD = new List<InventoryDeviceStatisticDVQLSDLoaiTB_DVQLSD>();
        }
    }
}
