﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class DeliverySLADeviceStatisticMonthResponse : DeviceStatisticItem
    {
        public List<DeliverySLADeviceStatisticMonth_Branch> DanhSachChiNhanh { get; set; }

        public DeliverySLADeviceStatisticMonthResponse()
        {
            DanhSachChiNhanh = new List<DeliverySLADeviceStatisticMonth_Branch>();
        }
    }
}
