﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class InventoryDeviceStatisticDVQLSDLoaiTBResponse : DeviceStatisticItem
    {
        public List<InventoryDeviceStatisticDVQLSDLoaiTB_Branch> DanhSachChiNhanh { get; set; }

        public InventoryDeviceStatisticDVQLSDLoaiTBResponse()
        {
            DanhSachChiNhanh = new List<InventoryDeviceStatisticDVQLSDLoaiTB_Branch>();
        }
    }
}
