﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class RerepairDeviceStatisticDVT_DVQLSD : DeviceStatisticItem
    {
        public List<RerepairDeviceStatisticDVT_DaiVT> DanhSachDai { get; set; }
        public RerepairDeviceStatisticDVT_DVQLSD()
        {
            DanhSachDai = new List<RerepairDeviceStatisticDVT_DaiVT>();
        }
    }
}