﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class NotDeliveryDeviceStatisticDTSC_DoiTuongTB : DeviceStatisticItem
    {
        public List<NotDeliveryDeviceStatisticDTSC_NhaCungCap> DanhSachNhaCungCap { get; set; }

        public NotDeliveryDeviceStatisticDTSC_DoiTuongTB()
        {
            DanhSachNhaCungCap = new List<NotDeliveryDeviceStatisticDTSC_NhaCungCap>();
        }
    }
}