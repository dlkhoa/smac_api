﻿namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class InventoryDeviceStatisticBPCItem
    {
        public string Ten { get; set; }
        public int SoLuongChuaTest { get; set; }
        public int SoLuongChoSuaChua { get; set; }
        public int SoLuongNFFChuaGiao { get; set; }
        public int SoLuongTotChuaGiao { get; set; }
        public int SoLuongKhongTheSuaChuaGiao { get; set; }
    }
}