﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class DeliveryGoodDeviceStatisticProvider_DoiTuongTB : DeviceStatisticItem
    {
        public List<DeliveryGoodDeviceStatisticProvider_NhaCungCap> DanhSachNhaCungCap { get; set; }

        public DeliveryGoodDeviceStatisticProvider_DoiTuongTB()
        {
            DanhSachNhaCungCap = new List<DeliveryGoodDeviceStatisticProvider_NhaCungCap>();
        }
    }
}