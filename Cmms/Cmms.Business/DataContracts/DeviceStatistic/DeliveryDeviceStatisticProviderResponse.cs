﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class DeliveryDeviceStatisticProviderResponse : DeviceStatisticItem
    {
        public List<DeliveryDeviceStatisticProvider_Branch> DanhSachChiNhanh { get; set; }
        public DeliveryDeviceStatisticProviderResponse()
        {
            DanhSachChiNhanh = new List<DeliveryDeviceStatisticProvider_Branch>();
        }
    }
}
