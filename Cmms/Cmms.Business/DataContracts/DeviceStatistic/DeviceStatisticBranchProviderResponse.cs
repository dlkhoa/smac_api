﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class DeviceStatisticBranchProviderResponse : DeviceStatisticItem
    {
        public List<DeviceStatisticProviderResponse> DanhSachNhaCungCap { get; set; }

        public DeviceStatisticBranchProviderResponse()
        {
            DanhSachNhaCungCap = new List<DeviceStatisticProviderResponse>();
        }
    }
}
