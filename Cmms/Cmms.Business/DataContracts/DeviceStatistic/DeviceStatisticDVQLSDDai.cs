﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class DeviceStatisticDVQLSDDai : DeviceStatisticItem
    {
        public List<DeviceStatisticDVQLSDDai_Branch> DanhSachChiNhanh { get; set; }

        public DeviceStatisticDVQLSDDai()
        {
            DanhSachChiNhanh = new List<DeviceStatisticDVQLSDDai_Branch>();
        }
    }
}
