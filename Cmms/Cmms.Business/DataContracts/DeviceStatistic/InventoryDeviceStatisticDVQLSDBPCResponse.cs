﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public    class InventoryDeviceStatisticDVQLSDBPCResponse : InventoryDeviceStatisticBPCItem
    {
        public List<InventoryDeviceStatisticDVQLSDBPC_Branch> DanhSachChiNhanh { get; set; }

        public InventoryDeviceStatisticDVQLSDBPCResponse()
        {
            DanhSachChiNhanh = new List<InventoryDeviceStatisticDVQLSDBPC_Branch>();
        }
    }
}
