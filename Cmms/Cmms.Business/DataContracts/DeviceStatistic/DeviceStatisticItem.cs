﻿namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class DeviceStatisticItem
    {
        public string Ten { get; set; }
        public int TongSoThietBi { get; set; }

        public DeviceStatisticItem()
        {
            Ten = string.Empty;
        }
    }
}
