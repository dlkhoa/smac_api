﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class NotDeliveryDeviceStatisticDTSC_Branch : DeviceStatisticItem
    {
        public List<NotDeliveryDeviceStatisticDTSC_DoiTuongTB> DanhSachDoiTuongTB { get; set; }

        public NotDeliveryDeviceStatisticDTSC_Branch()
        {
            DanhSachDoiTuongTB = new List<NotDeliveryDeviceStatisticDTSC_DoiTuongTB>();
        }
    }
}