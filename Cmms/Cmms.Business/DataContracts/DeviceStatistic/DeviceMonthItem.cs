﻿namespace Cmms.Business.DataContracts.DeviceStatistic
{
    public class DeviceMonthItem
    {
        public int Thang { get; set; }
        public int SoThietBi { get; set; }

        public DeviceMonthItem()
        {
            Thang = 0;
            SoThietBi = 0;
        }

        public DeviceMonthItem(int m, int n)
        {
            Thang = m;
            SoThietBi = n;
        }
    }
}
