﻿namespace Cmms.Business.DataContracts
{
    public class PhieuChoPheDuyetResponse
    {
        public string MaSoPhieu { get; set; }
        public string ChiNhanh { get; set; }
        public string MoTa { get; set; }
        public string LoaiPhieu { get; set; }
    }
}
