﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts
{
    public class DanhSachPhieuChoPheDuyetResponse
    {
        public int TotalRecords { get; set; }
        public List<PhieuChoPheDuyetResponse> DanhSachPhieu { get; set; }

        public DanhSachPhieuChoPheDuyetResponse()
        {
            DanhSachPhieu = new List<PhieuChoPheDuyetResponse>();
        }
    }
}
