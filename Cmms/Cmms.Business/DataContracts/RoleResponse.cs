﻿namespace Cmms.Business.DataContracts
{
    public class RoleResponse
    {
        public string RoleCode { get; set; }
        public string Description { get; set; }
    }
}