﻿using System;
using AutoMapper;
using Cmms.Business.DataContracts.DeviceRepair;
using Cmms.Data.Constants;
using Cmms.Domain.Entities;
using Cmms.Domain.Entities.DataContracts;

namespace Cmms.Business.DataContracts.Mappings
{
    public class ModelToDataContractProfile : Profile
    {
        public override string ProfileName
        {
            get
            {
                return this.GetType().FullName;
            }
        }

        public ModelToDataContractProfile()
        {
            MapUserToResponse();
            MapRoleToResponse();
            MapRequisitionDetailsToResponse();
            MapOrderDetailsToResponse();
            MapPicklistDetailsToResponse();
            MapDeviceDetailsToResponse();
            MapBranchToDTSC();
            MapBranchToDVQLSDTB();
            MapEntityStatusToResponse();
            MapOrderSummaryToPhieuChoPheDuyetResponse();
            MapPicklistSummaryToPhieuChoPheDuyetResponse();
            MapRequisitionSummaryToPhieuChoPheDuyetResponse();
            MapDeviceRepairResponse();
        }

        private void MapRequisitionSummaryToPhieuChoPheDuyetResponse()
        {
            CreateMap<RequisitionSummary, PhieuChoPheDuyetResponse>()
                .ForMember(s => s.LoaiPhieu, o => o.MapFrom(d => EntityTypeConstants.PhieuTiepNhanThietBi));
        }

        private void MapPicklistSummaryToPhieuChoPheDuyetResponse()
        {
            CreateMap<PicklistSummary, PhieuChoPheDuyetResponse>()
                .ForMember(s => s.LoaiPhieu, o => o.MapFrom(d => EntityTypeConstants.PhieuDeNghiXuatKho));
        }

        private void MapOrderSummaryToPhieuChoPheDuyetResponse()
        {
            CreateMap<OrderSummary, PhieuChoPheDuyetResponse>()
                .ForMember(s => s.LoaiPhieu, o => o.MapFrom(d => EntityTypeConstants.PhieuDeNghiNhapKho));
        }

        private void MapUserToResponse()
        {
            CreateMap<User, UserResponse>()
                .ForMember(s => s.Username, o => o.MapFrom(d => d.UserCode))
                .ForMember(s => s.Fullname, o => o.MapFrom(d => d.Description))
                .ForMember(s => s.GroupName, o => o.MapFrom(d => d.Group.Description))
                .ForMember(s => s.Organization, o => o.MapFrom(d => d.DefaultOrg));
        }

        private void MapRoleToResponse()
        {
            CreateMap<Role, RoleResponse>();
        }

        private void MapRequisitionDetailsToResponse()
        {
            CreateMap<RequisitionDetails, RequisitionDetailsResponse>();
        }

        private void MapOrderDetailsToResponse()
        {
            CreateMap<OrderDetails, OrderDetailsResponse>();
        }

        private void MapPicklistDetailsToResponse()
        {
            CreateMap<PicklistDetails, PicklistDetailsResponse>();
        }

        private void MapDeviceDetailsToResponse()
        {
            CreateMap<DeviceDetails, DeviceDetailsResponse>()
                .ForMember(s => s.ThietBiMoi, o => o.MapFrom(d => d.ThietBiMoi == CommonConstant.TrueValueString));
        }

        private void MapBranchToDVQLSDTB()
        {
            CreateMap<Company, DanhMucDVQLSDTB>()
                .ForMember(s => s.MaDVQLSD, o => o.MapFrom(d => d.Code))
                .ForMember(s => s.TenDVQLSD, o => o.MapFrom(d => d.Description))
                .ForMember(s => s.DVQLSDCha, o => o.MapFrom(d => d.Parent));
        }

        private void MapBranchToDTSC()
        {
            CreateMap<Company, DanhMucDoiTacSuaChua>()
                .ForMember(s => s.MaDoiTac, o => o.MapFrom(d => d.Code))
                .ForMember(s => s.TenDoiTac, o => o.MapFrom(d => d.Description))
                .ForMember(s => s.DoiTacCha, o => o.MapFrom(d => d.Parent));
        }

        private void MapEntityStatusToResponse()
        {
            CreateMap<EntityStatus, EntityStatusResponse>()
                .ForMember(s => s.Status, o => o.MapFrom(d => d.StatusCode));
        }

        private void MapDeviceRepairResponse()
        {
            CreateMap<RepairDeviceDetails, RepairDeviceDetailsResponse>();
        }

    }
}
