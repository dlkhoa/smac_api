﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts.DeviceRepair
{
    public class RepairDeviceDetailsResponse
    {
        public string Barcode { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }

    }
}