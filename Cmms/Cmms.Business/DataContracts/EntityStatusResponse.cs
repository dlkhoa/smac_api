﻿namespace Cmms.Business.DataContracts
{
    public class EntityStatusResponse
    {
        public string Status { get; set; }
        public string Description { get; set; }
    }
}
