﻿using System.Collections.Generic;

namespace Cmms.Business.DataContracts
{
    public class UserResponse
    {
        public string Username { get; set; }
        public string Fullname { get; set; }

        public string GroupCode { get; set; }
        public string GroupName { get; set; }
        public string Organization { get; set; }
        public List<RoleResponse> Roles { get; set; }

        public string TVT { get; set; }
        public string DVT { get; set; }

        public UserResponse()
        {
            Roles = new List<RoleResponse>();
        }
    }
}
