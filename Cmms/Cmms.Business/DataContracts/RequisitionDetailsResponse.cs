﻿using Cmms.Domain.Entities.DataContracts;
using System;
using System.Collections.Generic;

namespace Cmms.Business.DataContracts
{
    public class RequisitionDetailsResponse
    {
        public string MaSophieu { get; set; }
        public string MoTa { get; set; }
        public string ChiNhanh { get; set; }
        public string TinhTrangPhieu { get; set; }

        public string BenGiao { get; set; }
        public string NguoiGiao { get; set; }
        public string LoaiTiepNhan { get; set; }

        public string BenNhan { get; set; }
        public string Kho { get; set; }
        public string NguoiNhan { get; set; }
        public DateTime? NgayTiepNhan { get; set; }

        //public decimal? TongGiaTri { get; set; }
        public string NoiDungBanGiao { get; set; }

        public DateTime? NgayGuiYeuCau { get; set; }
        public DateTime? NgayHoanThanhPhanLoai { get; set; }

        public IEnumerable<RequisitionLineSummary> DanhSachThietBi { get; set; }
    }
}
