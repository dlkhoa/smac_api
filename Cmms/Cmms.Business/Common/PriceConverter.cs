﻿using System;

namespace Cmms.Business.Common
{
    public class PriceConverter
    {
        private const int valueForNull = int.MinValue;
        public static decimal ConvertFromNullableValue(decimal? price)
        {
            price = price ?? valueForNull;
            return price.Value;
        }

        public static decimal? ConvertToNullableValue(decimal? price)
        {
            if (price == valueForNull)
            {
                return null;
            }
            return price;
        }

        public static int ConvertFromNullableValue(int? price)
        {
            price = price ?? valueForNull;
            return price.Value;
        }

        public static int? ConvertToNullableValue(int? price)
        {
            if (price == valueForNull)
            {
                return null;
            }
            return price;
        }

    }
}
