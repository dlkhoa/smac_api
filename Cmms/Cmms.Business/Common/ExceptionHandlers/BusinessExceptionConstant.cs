﻿namespace Cmms.Business.Common.ExceptionHandlers
{
    public static class BusinessExceptionConstant
    {
        internal const string INVALID_USER = "User không tồn tại";
        internal const string INVALID_PASSWORD = "Password không hợp lệ";
        internal const string DEVICE_NOT_FOUND = "Không tìm thấy thiết bị";
        internal const string REQUISITION_NOT_FOUND = "Không tìm thấy phiếu";
        internal const string ORDER_NOT_FOUND = "Không tìm thấy phiếu";
        internal const string PICKLIST_NOT_FOUND = "Không tìm thấy phiếu";
        internal const string APPOVE_PICKLIST_NOT_ALLOW = "Không đủ quyền để phê duyệt phiếu";
        internal const string STATUS_CODE_INVALID = "Trạng thái phiếu không hợp lệ";

        public const string DVQLSD_EMPTY = "Chưa chọn DVQLSD";
    }
}
