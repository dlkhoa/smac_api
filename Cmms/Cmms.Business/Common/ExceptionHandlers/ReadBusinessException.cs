﻿using System;

namespace Cmms.Business.Common.ExceptionHandlers
{
    public class ReadBusinessException : Exception
    {
        public ReadBusinessException(string message) : base(message)
        {

        }
    }
}
