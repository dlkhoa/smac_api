﻿using System;

namespace Cmms.Business.Common.ExceptionHandlers
{
    public class WriteBusinessException : Exception
    {
        public WriteBusinessException(string message):base(message)
        {

        }
    }
}
