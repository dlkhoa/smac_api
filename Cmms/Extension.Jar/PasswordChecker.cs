﻿namespace Extension.Jar
{
    public class PasswordChecker
    {
        public static bool IsEqual(string hashedPassword, string password, string username)
        {
            return com.dstm.common.tools.pw.PwHasher.isEqual(hashedPassword, password, username);
        }
    }
}
