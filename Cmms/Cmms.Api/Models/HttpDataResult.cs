﻿using System.Net;

namespace Cmms.Api.Models
{
    public class HttpDataResult<T>
    {
        public T Data { get; set; }
        public string StatusMessage { get; set; }
        public int StatusCode { get; set; }
        public HttpDataResult(T data)
        {
            this.Data = data;
            this.StatusMessage = "Success";
            this.StatusCode = HttpStatusCode.OK.GetHashCode();
        }

        public HttpDataResult(T data, string message, int statusCode)
        {
            this.Data = data;
            this.StatusMessage = message;
            this.StatusCode = statusCode;
        }
    }
}