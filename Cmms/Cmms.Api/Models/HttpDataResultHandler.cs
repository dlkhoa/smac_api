﻿using System.Net;

namespace Cmms.Api.Models
{
    public class HttpDataResultHandler
    {
        public static HttpDataResult<string> CreateBadRequestError(string message)
        {
            return CreateError(message, HttpStatusCode.BadRequest);
        }

        public static HttpDataResult<string> CreateInternalServerError(string message)
        {
            return CreateError(message, HttpStatusCode.InternalServerError);
        }

        public static HttpDataResult<string> CreateWriteBusinessError(string message)
        {
            return CreateError(message, HttpStatusCode.NotModified);
        }

        public static HttpDataResult<string> CreateError(string message, HttpStatusCode httpStatus)
        {
            return new HttpDataResult<string>(null, message, httpStatus.GetHashCode());
        }

        public static HttpDataResult<T> CreateResult<T>(T data)
        {
            return new HttpDataResult<T>(data);
        }

        public static HttpDataResult<T> CreateResult<T>(T data, string message)
        {
            return new HttpDataResult<T>(data, message, HttpStatusCode.OK.GetHashCode());
        }
    }
}