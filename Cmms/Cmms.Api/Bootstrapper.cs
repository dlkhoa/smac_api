﻿using Cmms.Business.ComponentServices.Abstracts;
using MBoL.Utils;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using System;
using System.Threading;

namespace Cmms.Api
{
    public class Bootstrapper
    {
        private static Timer timer;
        public static void RegisterComponents(IUnityContainer container)
        {
            Business.Bootstrapper.RegisterServiceComponents(container);
        }

        public static void RunNotificationTimer()
        {
            if (Config.EnabledNotificationTimer)
            {
                var notificationService = ServiceLocator.Current.GetInstance<INotificationService>();
                Logger.WriteLog("Timer is started", "TimerStart", true);
                timer = new Timer(new TimerCallback(notificationService.PushNotificationByTimer), null, 1000, Config.TimerPeriod);
            }

        }
        public static void DisposeTimer()
        {
            Logger.WriteLog("Timer is stopped", "TimerStop", true);
            if (timer != null)
            {
                timer.Dispose();
            }
        }
    }
}