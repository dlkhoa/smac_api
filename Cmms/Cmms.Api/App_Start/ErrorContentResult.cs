﻿using Newtonsoft.Json.Serialization;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace Cmms.Api
{
    public class ErrorContentResult<T> : IHttpActionResult
    {
        private readonly HttpRequestMessage _request;
        private readonly T _content;

        public ErrorContentResult(HttpRequestMessage request, T content)
        {
            _request = request;
            _content = content;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var jsonFormatter = new JsonMediaTypeFormatter();
            jsonFormatter.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter { CamelCaseText = true });
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            var response = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new ObjectContent<object>(_content, jsonFormatter),
                RequestMessage = _request
            };
            return Task.FromResult(response);
        }
    }
}