﻿using Cmms.Api.Models;
using Cmms.Business.Common.ExceptionHandlers;
using MBoL.Utils;
using System;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Web.Http.ExceptionHandling;

namespace Cmms.Api
{
    public class ApiExceptionHandler : ExceptionHandler
    {
        public override bool ShouldHandle(ExceptionHandlerContext context)
        {
            return true;

        }
        public override void Handle(ExceptionHandlerContext context)
        {
            var errorResult = context.Exception is WriteBusinessException ?
                HttpDataResultHandler.CreateWriteBusinessError(context.Exception.Message) :
                HttpDataResultHandler.CreateInternalServerError(GetExceptionMessage(context.Exception));

            context.Result = new ErrorContentResult<HttpDataResult<string>>(context.Request, errorResult);
            Logger.WriteLog(errorResult.StatusMessage, "ExceptionHandler", true);
            base.Handle(context);
        }

        private string GetExceptionMessage(Exception exception)
        {
            var message = string.Empty;
            if (exception is DbEntityValidationException)
            {
                foreach (var eve in ((DbEntityValidationException)exception).EntityValidationErrors)
                {
                    message += string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        message += string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }
            else if (exception is DbUpdateException)
            {
                var lastInnerException = exception.GetLastInnerException();
                message = StringHelper.GetFirstLineInString(lastInnerException.Message);
            }
            else
            {
                message = exception.GetFullMessage();
            }
            return message;
        }
    }
}