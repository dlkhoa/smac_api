﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Linq;
using System.Net.Http.Formatting;

namespace Cmms.Api
{
    public class FormatterConfig
    {
        public static void ConfigureFormatters(MediaTypeFormatterCollection formatters)
        {
            var jsonFormatters = formatters.OfType<JsonMediaTypeFormatter>();
            foreach (var jsonFormatter in jsonFormatters)
            {
                jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                jsonFormatter.SerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
                jsonFormatter.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
            }
            formatters.XmlFormatter.SupportedMediaTypes.Clear();
        }
    }
}