﻿using AutoMapper;
using Cmms.Business.DataContracts.Mappings;

namespace Cmms.Api
{
    public class MappingConfig
    {
        public static void SetupMappings()
        {
            Mapper.Initialize(
            cfg =>
            {
                cfg.AddProfile<ModelToDataContractProfile>();
            }
        );
        }
    }
}