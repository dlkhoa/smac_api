﻿using Cmms.Api.Models;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Domain.Queries;
using System.Web.Http;

namespace Cmms.Api.Controllers
{
    [RoutePrefix("Api/Danhmuc/NhaCungCap")]
    public class ProviderController : BaseApiController
    {
        private readonly IProviderService providerService;

        public ProviderController(IProviderService providerService)
        {
            this.providerService = providerService;
        }

        [HttpGet]
        [Route("")]
        public IHttpActionResult GetProvider([FromUri] ProviderQuery request)
        {
            request = request ?? new ProviderQuery();
            request.ProviderCode = request.ProviderCode ?? string.Empty;
            var result = providerService.GetProviders(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }
    }
}
