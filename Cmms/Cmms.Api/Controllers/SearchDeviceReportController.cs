﻿using System.Web.Http;
using Cmms.Api.Models;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Business.DataContracts;
using Cmms.Domain.Queries;
using Microsoft.Practices.ServiceLocation;

namespace Cmms.Api.Controllers
{
    [RoutePrefix("Api/Thietbi")]
    public class SearchDeviceReportController : BaseApiController
    {
        [HttpGet]
        [Route("BaoCao/{loaiBaoCao}")]
        public IHttpActionResult LayDanhSachThietBiChoBaoCao([FromUri] DeviceReportAllLevelQuery request)
        {
            var reportType = (DeviceReportType)request.LoaiBaoCao;
            var searchDeviceReportService = ServiceLocator.Current.GetInstance<ISearchDeviceForReportingService>(reportType.ToString());
            var result = searchDeviceReportService.GetDevicesForReporting(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }
    }
}
