﻿using Cmms.Api.Models;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Domain.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Cmms.Api.Controllers
{
    [RoutePrefix("Api/BaoCaoThietBiSLADaBanGiao")]
    public class DeliverySLADeviceReportController : BaseApiController
    {
        private readonly IDeliverySLADeviceReportService deliverySLADeviceReportService;

        public DeliverySLADeviceReportController(IDeliverySLADeviceReportService deliverySLADeviceReportService)
        {
            this.deliverySLADeviceReportService = deliverySLADeviceReportService;
        }

        [HttpGet]
        [Route("ChiNhanh_Thang")]
        public IHttpActionResult GetStatisticByMonth([FromUri]DeviceReportQuery request)
        {
            var result = deliverySLADeviceReportService.GetStatisticByMonth(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("ChiNhanh_Thang/{chinhanh}/{thang}")]
        public IHttpActionResult GetDeviceReportSummaryByMonth([FromUri]DeliverySLADeviceReportByMonthQuery request)
        {
            int month;
            int year;
            if(int.TryParse(request.Thang.Split('-')[0], out month) && int.TryParse(request.Thang.Split('-')[1], out year))
            {
                request.MonthValue = month;
                request.YearValue = year;
            }

            var result = deliverySLADeviceReportService.GetDeviceReportSummaryByMonth(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }
    }
}
