﻿using Cmms.Api.Models;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Domain.Queries;
using System.Web.Http;

namespace Cmms.Api.Controllers
{
    [RoutePrefix("Api/BaoCaoThietBiQuayLaiSuaChua")]
    public class RerepairDeviceReportController : BaseApiController
    {
        private readonly IRerepairDeviceReportService rerepairDeviceReportService;

        public RerepairDeviceReportController(IRerepairDeviceReportService rerepairDeviceReportService)
        {
            this.rerepairDeviceReportService = rerepairDeviceReportService;
        }

        [HttpGet]
        [Route("ChiNhanh_DoiTuongTB_NhaCungCap_DoiTac")]
        public IHttpActionResult GetStatisticByDoiTac([FromUri]DeviceReportQuery request)
        {
            var result = rerepairDeviceReportService.GetStatisticByDTSC(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("ChiNhanh_DoiTuongTB_NhaCungCap_DoiTac/{chinhanh}/{doiTuongTb}/{nhaCungCap}/{doitac}")]
        public IHttpActionResult GetDeviceReportSummaryByDTSC([FromUri]RerepairDeviceReportByDTSCQuery request)
        {
            var result = rerepairDeviceReportService.GetDeviceReportSummaryByDTSC(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("ChiNhanh_DVQLSD_Dai")]
        public IHttpActionResult GetDeviceStatisticByDVT([FromUri] DeviceReportQuery request)
        {
            var result = rerepairDeviceReportService.GetDeviceStatisticByDVT(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }


        [HttpGet]
        [Route("ChiNhanh_DVQLSD_Dai/{chinhanh}/{dvqlsd}/{daiVT}")]
        public IHttpActionResult GetDeviceReportSummaryByDVT([FromUri]RerepairDeviceReportByDVTQuery request)
        {
            var result = rerepairDeviceReportService.GetDeviceReportSummaryByDVT(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }
    }
}
