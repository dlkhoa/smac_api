﻿using Cmms.Api.Models;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Domain.Queries;
using System.Web.Http;

namespace Cmms.Api.Controllers
{
    [RoutePrefix("Api/BaoCaoThietBiNFFGiaoChoDVQLSD")]
    public class DeliveryNFFDeviceReportController : BaseApiController
    {
        private readonly IDeliveryNffDeviceReportService deliveryNffDeviceReportService;
        public DeliveryNFFDeviceReportController(IDeliveryNffDeviceReportService deliveryNffDeviceReportService)
        {
            this.deliveryNffDeviceReportService = deliveryNffDeviceReportService;
        }

        [HttpGet]
        [Route("ChiNhanh_DoiTuongTB_NhaCungCap")]
        public IHttpActionResult GetStatisticByProvider([FromUri]DeviceReportQuery request)
        {
            var result = deliveryNffDeviceReportService.GetStatisticByProvider(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("ChiNhanh_DoiTuongTb_NhaCungCap/{chinhanh}/{doiTuongTb}/{nhaCungCap}")]
        public IHttpActionResult GetDeviceReportSummaryByProvider([FromUri]DeliveryDeviceReportByProviderQuery request)
        {
            var result = deliveryNffDeviceReportService.GetDeviceReportSummaryByProvider(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("ChiNhanh_DVQLSD_Dai")]
        public IHttpActionResult GetDeviceStatisticByDVQLSD_Dai([FromUri]DeviceReportQuery request)
        {
            var result = deliveryNffDeviceReportService.GetDeviceStatisticByDVQLSD_Dai(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }


        [HttpGet]
        [Route("ChiNhanh_DVQLSD_Dai/{chinhanh}/{dvqlsd}/{daiVT}")]
        public IHttpActionResult GetDeviceReportSummaryByDVQLSD_Dai([FromUri]DeliveryDeviceReportByDVQLSDDaiQuery request)
        {
            var result = deliveryNffDeviceReportService.GetDeviceReportSummaryByDVQLSD_Dai(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }
    }
}
