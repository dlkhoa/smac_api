﻿using Cmms.Api.Models;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Domain.Queries;
using System.Web.Http;

namespace Cmms.Api.Controllers
{
    [RoutePrefix("Api/Danhmuc")]
    public class CompanyController : BaseApiController
    {
        private readonly ICompanyService companyService;

        public CompanyController(ICompanyService companyService)
        {
            this.companyService = companyService;
        }

        [HttpGet]
        [Route("DVQLSDTB")]
        public IHttpActionResult LayDanhMucDVQLSDTB([FromUri] CompanyQuery request)
        {
            request = request ?? new CompanyQuery();
            request.Code = request.Code ?? string.Empty;
            var result = companyService.LayDanhMucDVAQLSDTB(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("DoiTacSuaChua")]
        public IHttpActionResult LayDanhMucDoiTacSuaChua([FromUri] CompanyQuery request)
        {
            request = request ?? new CompanyQuery();
            request.Code = request.Code ?? string.Empty;
            var result = companyService.LayDanhMucDoiTacSuaChua(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("LoaiThietbi")]
        public IHttpActionResult LayDanhMucLoaiThietbi([FromUri] CompanyQuery request)
        {
            request = request ?? new CompanyQuery();
            request.Code = request.Code ?? string.Empty;
            var result = companyService.LayDanhMucLoaiThietbi(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        /*[HttpGet]
        [Route("DaiVienthong/{code}")]
        public IHttpActionResult LayDanhMucDaiVienthongByDVQLSD([FromUri] CompanyQuery request)
        {
            request = request ?? new CompanyQuery();
            request.Code = request.Code ?? string.Empty;
            var result = companyService.LayDanhMucDaiVienthong(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }*/

        [HttpGet]
        [Route("DaiVienthong/{username}")]
        public IHttpActionResult LayDanhMucDaiVienthongByDVQLSD([FromUri] CompanyQuery request)
        {
            request = request ?? new CompanyQuery();
            request.Code = request.Code ?? string.Empty;
            var result = companyService.LayDanhMucDaiVienthong(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("DaiVienthong")]
        public IHttpActionResult LayDanhMucDaiVienthong([FromUri] CompanyQuery request)
        {
            request = request ?? new CompanyQuery();
            request.Code = request.Code ?? string.Empty;
            var result = companyService.LayDanhMucDaiVienthong(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("DonviQLSDTB")]
        public IHttpActionResult LayDanhMucDonviQLSDTB([FromUri] CompanyQuery request)
        {
            request = request ?? new CompanyQuery();
            request.Code = request.Code ?? string.Empty;
            var result = companyService.LayDanhMucDonviQLSDTB(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        /*[HttpGet]
        [Route("DonviQLSDTB/{code}")]
        public IHttpActionResult LayDanhMucDonviQLSDTBByChinhanh([FromUri] CompanyQuery request)
        {
            request = request ?? new CompanyQuery();
            request.Code = request.Code ?? string.Empty;
            var result = companyService.LayDanhMucDonviQLSDTB(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }*/

        [HttpGet]
        [Route("DonviQLSDTB/{username}")]
        public IHttpActionResult LayDanhMucDonviQLSDTBByUser([FromUri] CompanyQuery request)
        {
            request = request ?? new CompanyQuery();
            request.Code = request.Code ?? string.Empty;
            var result = companyService.LayDanhMucDonviQLSDTB(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("KetquaKiemtraTruocSC")]
        public IHttpActionResult LayDanhMucKetquaKiemtraTruocSC([FromUri] CompanyQuery request)
        {
            request = request ?? new CompanyQuery();
            request.Code = request.Code ?? string.Empty;
            var result = companyService.LayDanhMucKetquaKiemtraTruocSC(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("KetquaKiemtraSauSC")]
        public IHttpActionResult LayDanhMucKetquaKiemtraSauSC([FromUri] CompanyQuery request)
        {
            request = request ?? new CompanyQuery();
            request.Code = request.Code ?? string.Empty;
            var result = companyService.LayDanhMucKetquaKiemtraSauSC(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("TrangthaiThietbi")]
        public IHttpActionResult LayDanhMucTrangthaiThietbi([FromUri] CompanyQuery request)
        {
            request = request ?? new CompanyQuery();
            request.Code = request.Code ?? string.Empty;
            var result = companyService.LayDanhMucTrangthaiThietbi(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("DoituongThietbi")]
        public IHttpActionResult LayDanhMucDoituongThietbi([FromUri] CompanyQuery request)
        {
            request = request ?? new CompanyQuery();
            request.Code = request.Code ?? string.Empty;
            var result = companyService.LayDanhMucDoituongThietbi(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("Chinhanh/{username}")]
        public IHttpActionResult LayDanhMucChinhanh([FromUri] CompanyQuery request)
        {
            request = request ?? new CompanyQuery();
            request.Code = request.Code ?? string.Empty;
            var result = companyService.LayDanhMucChinhanh(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("Nam")]
        public IHttpActionResult LayDanhMucNam([FromUri] CompanyQuery request)
        {
            request = request ?? new CompanyQuery();
            request.Code = request.Code ?? string.Empty;
            var result = companyService.LayDanhMucNam(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }
    }
}
