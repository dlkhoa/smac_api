﻿using Cmms.Api.Models;
using Cmms.Business.Common;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Domain.Commands;
using Cmms.Domain.Queries;
using System.Web.Http;
using Cmms.Data.Constants;

namespace Cmms.Api.Controllers
{
    [RoutePrefix("Api/Users/{username}")]
    public class PicklistController : BaseApiController
    {
        private readonly IPicklistService picklistService;
        private readonly IPicklistCommandService picklistCommandService;
        private readonly INotificationService notificationService;

        public PicklistController(IPicklistService picklistService, IPicklistCommandService picklistCommandService, INotificationService notificationService)
        {
            this.picklistService = picklistService;
            this.picklistCommandService = picklistCommandService;
            this.notificationService = notificationService;
        }

        [HttpGet]
        [Route("PhieuDeNghiXuatKho")]
        public IHttpActionResult LayPhieuDeNghiXuatKho([FromUri] PicklistQuery request)
        {
            request.Branch = request.Branch ?? string.Empty;
            request.PermissionFunction = "'SSPICH','SSPICK','SSPICR'";
            var result = picklistService.GetPicklists(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("PhieuDeNghiXuatKho/{picklistCode}")]
        public IHttpActionResult LayChiTietPhieuDeNghiXuatKho(string username, string picklistCode)
        {
            var result = picklistService.GetPicklist(username, picklistCode);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("PhieuDeNghiXuatKhoChoPheDuyet")]
        public IHttpActionResult LayPhieuDeNghiXuatKhoChoPheDuyet([FromUri] PicklistQuery request)
        {
            request.Branch = request.Branch ?? string.Empty;
            request.PermissionFunction = "'SSPICH','SSPICK','SSPICR'";
            request.Status = "'U','R'";
            var result = picklistService.GetAwaitingPicklists(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpPut]
        [Route("PhieuDeNghiXuatKho/{picklistCode}/PheDuyet")]
        public IHttpActionResult PheDuyetPhieuDeNghiXuatKho(string username, string picklistCode)
        {
            var command = new ApprovePicklistCommand
            {
                Username = username,
                PicklistCode = picklistCode
            };

            var notificationItem = picklistCommandService.ApprovePicklist(command);

            notificationService.PushNotification(notificationItem);
            return Ok(HttpDataResultHandler.CreateResult(true));
        }
    }
}
