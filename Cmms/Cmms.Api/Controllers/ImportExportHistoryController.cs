﻿using Cmms.Api.Models;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Domain.Queries;
using System.Web.Http;

namespace Cmms.Api.Controllers
{
    [RoutePrefix("Api/DanhMuc/ThongTinNhapXuatKhoThietBi")]
    public class ImportExportHistoryController : BaseApiController
    {
        private readonly IImportExportHistoryService importExportHistoryService;

        public ImportExportHistoryController(IImportExportHistoryService importExportHistoryService)
        {
            this.importExportHistoryService = importExportHistoryService;
        }

        [HttpGet]
        [Route("{serial}/{branch}")]
        public IHttpActionResult LayDanhmucThongTinNhapXuatKhoThietBi([FromUri] ImportExportHistoryQuery request)
        {
            var result = importExportHistoryService.GetHistories(request);
            return (Ok(HttpDataResultHandler.CreateResult(result)));
        }
    }
}
