﻿using Cmms.Api.Models;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Domain.Queries;
using System.Web.Http;

namespace Cmms.Api.Controllers
{
    [RoutePrefix("Api/BaoCaoThietBiQuaThoiGianSuaChua")]
    public class OverRepairTimeDeviceReportController : BaseApiController
    {
        private readonly IOverRepairTimeDeviceReportService overRepairTimeDeviceReportService;

        public OverRepairTimeDeviceReportController(IOverRepairTimeDeviceReportService overRepairTimeDeviceReportService)
        {
            this.overRepairTimeDeviceReportService = overRepairTimeDeviceReportService;
        }

        [HttpGet]
        [Route("ChiNhanh_DoiTac")]
        public IHttpActionResult GetStatisticByDoiTac([FromUri]DeviceReportQuery request)
        {
            var result = overRepairTimeDeviceReportService.GetStatisticByDoiTac(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("ChiNhanh_DoiTac/{chinhanh}/{doitac}")]
        public IHttpActionResult GetDeviceReportSummaryByDTSC([FromUri]OverRepairTimeDeviceReportByDTSCQuery request)
        {
            var result = overRepairTimeDeviceReportService.GetDeviceReportSummaryByDTSC(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

    }
}
