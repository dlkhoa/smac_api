﻿using Cmms.Api.Models;
using Cmms.Business.Common;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Data.Constants;
using Cmms.Domain.Commands;
using Cmms.Domain.Queries;
using System.Web.Http;

namespace Cmms.Api.Controllers
{
    [RoutePrefix("Api/Users/{username}")]
    public class RequisitionController : BaseApiController
    {
        private readonly IRequisitionService requisitionService;
        private readonly IRequisitionCommandService requisitionCommandService;
        private readonly INotificationService notificationService;
        public RequisitionController(IRequisitionService requisitionService, IRequisitionCommandService requisitionCommandService, INotificationService notificationService)
        {
            this.requisitionService = requisitionService;
            this.requisitionCommandService = requisitionCommandService;
            this.notificationService = notificationService;
        }

        [HttpGet]
        [Route("PhieuTiepNhanThietBi/{requisitionCode}")]
        public IHttpActionResult GetRequisitionDetails(string username, string requisitionCode)
        {
            var result = requisitionService.GetRequisition(username, requisitionCode);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("PhieuTiepNhanThietBi")]
        public IHttpActionResult LayDanhMucPhieuTiepNhanThietBi([FromUri] RequisitionQuery request)
        {
            request.Branch = request.Branch ?? string.Empty;
            /*request.RequisitionClass = string.Format("'{0}','{1}','{2}','{3}'",
                RequisitionConstant.TIEP_NHAN_THIET_BI_HONG,
                RequisitionConstant.TU_CHOI_NHAN_BAO_HANH,
                RequisitionConstant.TIEP_NHAN_THIET_BI_SAU_SUA_CHUA,
                RequisitionConstant.TIEP_NHAN_THIET_BI_MOI);*/
            request.RequisitionClass = string.Format("'{0}'", RequisitionConstant.TIEP_NHAN_THIET_BI_HONG);
            /*request.PermissionFunction = string.Format("'{0}','{1}','{2}','{3}'",
                PermissionFunctionConstant.TIEP_NHAN_THIET_BI_HONG,
                PermissionFunctionConstant.TIEP_NHAN_THIET_BI_SUA_CHUA,
                PermissionFunctionConstant.TIEP_NHAN_THIET_BI_MOI,
                PermissionFunctionConstant.TIEP_NHAN_THIET_BI_BAO_HANH);*/
            request.PermissionFunction = string.Format("'{0}'", PermissionFunctionConstant.TIEP_NHAN_THIET_BI_HONG);

            var result = requisitionService.LayDanhMucPhieuTiepNhanThietBi(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("PhieuTuChoiTiepNhanBaoHanh")]
        public IHttpActionResult LayDanhMucPhieuTuChoiTiepNhanBaoHanh([FromUri] RequisitionQuery request)
        {
            request.Branch = request.Branch ?? string.Empty;
            request.RequisitionClass = string.Format("'{0}'", RequisitionConstant.TU_CHOI_NHAN_BAO_HANH);
            request.PermissionFunction = string.Format("'{0}'", PermissionFunctionConstant.TIEP_NHAN_THIET_BI_BAO_HANH);

            var result = requisitionService.LayDanhMucPhieuTiepNhanThietBi(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }
        //TODO: dang lay tat ca phieu tiep nhan thiet bi hong ( da phe duyet + cho phe duyet)
        [HttpGet]
        [Route("PhieuTiepNhanThietBiHongChoPheDuyet")]
        public IHttpActionResult LayDanhMucPhieuTiepNhanThietBiHongChoPheDuyet([FromUri] RequisitionQuery request)
        {
            request.Branch = request.Branch ?? string.Empty;
            request.PermissionFunction = string.Format("'{0}'", PermissionFunctionConstant.TIEP_NHAN_THIET_BI_HONG);
            request.RequisitionClass = string.Format("'{0}'", RequisitionConstant.TIEP_NHAN_THIET_BI_HONG);
            request.Status = "'U-CN','U-SC'";
            var result = requisitionService.LayDanhMucPhieuTiepNhanThietBiChoPheDuyet(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpPut]
        [Route("PhieuTiepNhanThietBi/{requisitionCode}/PheDuyet")]
        public IHttpActionResult PheDuyetLenhDieuDong(string username, string requisitionCode)
        {
            var command = new ApproveRequisitionCommand
            {
                Username = username,
                RequisitionCode = requisitionCode,
                AuthUser = username
            };
            var notificationItem = requisitionCommandService.ApproveRequisition(command);

            notificationService.PushNotification(notificationItem);
            return Ok(HttpDataResultHandler.CreateResult(true));
        }
    }
}
