﻿using Cmms.Api.Models;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Domain.Queries;
using System.Web.Http;

namespace Cmms.Api.Controllers
{
    [RoutePrefix("Api/BaoCaoThietBiTonKho")]
    public class InventoryDeviceReportController : BaseApiController
    {
        private readonly IInventoryDeviceReportService inventoryDeviceReportService;
        public InventoryDeviceReportController(IInventoryDeviceReportService inventoryDeviceReportService)
        {
            this.inventoryDeviceReportService = inventoryDeviceReportService;
        }

        [HttpGet]
        [Route("ChiNhanh_DVQLSD_LoaiThietBi")]
        public IHttpActionResult GetInventoryDeviceStatisticByLoaiThietBi([FromUri]DeviceReportQuery request)
        {
            var result = inventoryDeviceReportService.GetInventoryDeviceStatisticByLoaiThietBi(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("ChiNhanh_DVQLSD_LoaiThietBi/{chinhanh}/{dvqlsd}/{loaiTB}")]
        public IHttpActionResult GetDeviceReportSummaryByDVQLSD_LoaiTB([FromUri]InventoryDeviceReportByDVQLSDLoaiTBQuery request)
        {
            var result = inventoryDeviceReportService.GetDeviceReportSummaryByDVQLSD_LoaiTB(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("ChiNhanh_DVQLSD")]
        public IHttpActionResult GetInventoryDeviceStatisticBPCByDVQLSD([FromUri] DeviceReportQuery request)
        {
            var result = inventoryDeviceReportService.GetInventoryDeviceStatisticBPCByDVQLSD(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("ChiNhanh_DVQLSD/{loaiBaoCao}")]
        public IHttpActionResult GetDeviceReportSummaryByDVQLSD([FromUri] InventoryDeviceReportByDVQLSDQuery request)
        {
            request.ChiNhanh = request.ChiNhanh ?? string.Empty;
            request.DVQLSD = request.DVQLSD ?? string.Empty;
            var result = inventoryDeviceReportService.GetDeviceReportSummaryByDVQLSD(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("ChiNhanh_LoaiHD")]
        public IHttpActionResult GetInventoryDeviceStatisticBPCByLoaiHD([FromUri] DeviceReportQuery request)
        {
            var result = inventoryDeviceReportService.GetInventoryDeviceStatisticBPCByLHD(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("ChiNhanh_LoaiHD/{loaiBaoCao}")]
        public IHttpActionResult GetDeviceReportSummaryByLoaiHD([FromUri] InventoryDeviceReportByLoaiHDQuery request)
        {
            request.ChiNhanh = request.ChiNhanh ?? string.Empty;
            request.LoaiHD = request.LoaiHD ?? string.Empty;
            var result = inventoryDeviceReportService.GetDeviceReportSummaryByLoaiHD(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }
    }
}
