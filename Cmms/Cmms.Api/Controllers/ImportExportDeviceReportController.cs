﻿using Cmms.Api.Models;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Domain.Queries;
using System.Web.Http;

namespace Cmms.Api.Controllers
{
    [RoutePrefix("Api/BaoCaoThietBiXuatNhapTonKho")]
    public class ImportExportDeviceReportController : BaseApiController
    {
        private readonly IImportExportDeviceReportService importExportDeviceReportService;
        public ImportExportDeviceReportController(IImportExportDeviceReportService importExportDeviceReportService)
        {
            this.importExportDeviceReportService = importExportDeviceReportService;
        }

        [Route("ThietBiHongChoSuaChua")]
        public IHttpActionResult GetAwaitingRepairDevices([FromUri] ImportExportDeviceReportQuery request)
        {
            var result = importExportDeviceReportService.GetAwaitingRepairDevices(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [Route("ThietBiSauSuaChua")]
        public IHttpActionResult GetAfterRepairedDevices([FromUri] ImportExportDeviceReportQuery request)
        {
            var result = importExportDeviceReportService.GetAfterRepairedDevices(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [Route("ThietBiLuuKhoTrongQuaTrinhSuaChua")]
        public IHttpActionResult GetFixingDevices([FromUri] ImportExportDeviceReportQuery request)
        {
            var result = importExportDeviceReportService.GetFixingDevices(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }
    }
}
