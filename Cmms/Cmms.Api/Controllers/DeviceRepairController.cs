﻿using Cmms.Api.Models;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Data.Constants;
using Cmms.Domain.Commands;
using Cmms.Domain.Queries.DeviceRepair;
using System;
using System.IO;
using System.Web.Http;

namespace Cmms.Api.Controllers
{
    [RoutePrefix("Api/DeviceRepair")]
    public class DeviceRepairController : BaseApiController
    {
        private readonly IDeviceRepairService deviceRepairService;

        public DeviceRepairController(IDeviceRepairService deviceRepairService)
        {
            this.deviceRepairService = deviceRepairService;
        }

        [HttpGet]
        [Route("GetRepairDevice/{username}/{barcode}")]
        public IHttpActionResult GetRepairDevice([FromUri]RepairDeviceQuery request)
        {
            var result = deviceRepairService.GetRepairDevice(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("GetRepairDeviceById/{username}/{id}")]
        public IHttpActionResult GetRepairDeviceById([FromUri]RepairDeviceQuery request)
        {
            var result = deviceRepairService.GetRepairDeviceById(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("GetRepairedDevice/{username}/{serial}")]
        public IHttpActionResult GetRepairedDevice([FromUri]RepairDeviceQuery request)
        {
            var result = deviceRepairService.GetRepairedDevice(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("GetDeviceNames")]
        public IHttpActionResult GetDeviceNames()
        {
            var result = deviceRepairService.GetDeviceNames();
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpPost]
        [Route("AddRepairDevice")]
        public IHttpActionResult AddRepairDevice([FromBody] AddRepairDeviceCommand command)
        {
            var result = deviceRepairService.AddRepairDevice(command);
            
            if (!string.IsNullOrEmpty(result) &&
                command.Attachments != null &&
                command.Attachments.Count > 0)
            {
                // Upload files if any
                int iUploadedCnt = 0;

                // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
                string sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/" + CommonConstant.UploadedImagesDiectory + "/" + result + "/");
                if (!Directory.Exists(sPath)) Directory.CreateDirectory(sPath);

                // CHECK THE FILE COUNT.
                for (int iCnt = 0; iCnt <= command.Attachments.Count - 1; iCnt++)
                {
                    var hf = command.Attachments[iCnt];
                    if (hf.Buffer != null && hf.Buffer.Length > 0)
                    {
                        // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                        var saveAsPath = sPath + DateTime.Now.Ticks.ToString() + "_" + Path.GetFileName(hf.FileName);
                        if (!File.Exists(saveAsPath))
                        {
                            // SAVE THE FILES IN THE FOLDER.
                            File.WriteAllBytes(saveAsPath, hf.Buffer);
                            iUploadedCnt = iUploadedCnt + 1;
                        }
                    }
                }
            }
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("GetCenterList/{username}")]
        public IHttpActionResult GetCenterList(string username)
        {
            var result = deviceRepairService.GetCenterList(username);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("SummarizeReport")]
        public IHttpActionResult SummarizeReport([FromUri]DeviceRepairSummarizeReportQuery request)
        {
            var result = deviceRepairService.SummarizeReport(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("SummarizeListReport")]
        public IHttpActionResult SummarizeListReport([FromUri]DeviceRepairSummarizeReportQuery request)
        {
            var result = deviceRepairService.SummarizeListReport(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("GetAvailableDeviceList")]
        public IHttpActionResult GetAvailableDeviceList([FromUri]AvailableDeviceListQuery request)
        {
            var result = deviceRepairService.GetAvailableDeviceList(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("GetPriorityRepairDeviceList")]
        public IHttpActionResult GetPriorityRepairDeviceList([FromUri]PriorityRepairDeviceListQuery request)
        {
            var result = deviceRepairService.GetPriorityRepairDeviceList(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("GetRepairDeviceList")]
        public IHttpActionResult GetRepairDeviceList([FromUri]RepairDeviceListQuery request)
        {
            var result = deviceRepairService.GetRepairDeviceList(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpPost]
        [Route("ReceiveDeviceFromTVT")]
        public IHttpActionResult ReceiveDeviceFromTVT([FromBody] ReceiveDeviceFromTVTCommand command)
        {
            var result = deviceRepairService.ReceiveDeviceFromTVT(command);

            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("GetTVTList")]
        public IHttpActionResult GetTVTList()
        {
            return GetTVTList(string.Empty);
        }

        [HttpGet]
        [Route("GetTVTList/{dvt}")]
        public IHttpActionResult GetTVTList(string dvt)
        {
            var result = deviceRepairService.GetTVTList(dvt);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("GenerateBarcode/{dvt}")]
        public IHttpActionResult GenerateBarcode(string dvt)
        {
            var result = deviceRepairService.GenerateBarcode(dvt);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpPost]
        [Route("ReceiveDeviceFromTTDKSC")]
        public IHttpActionResult ReceiveDeviceFromTTDKSC([FromBody] ReceiveDeviceFromTTDKSCCommand command)
        {
            var result = deviceRepairService.ReceiveDeviceFromTTDKSC(command);

            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpPost]
        [Route("DeliveryDevicesToTVT")]
        public IHttpActionResult DeliveryDevicesToTVT([FromBody] DeliveryDevicesToTVTCommand command)
        {
            var result = deviceRepairService.DeliveryDevicesToTVT(command);

            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("GetWorkingDevices/{serial}")]
        public IHttpActionResult GetWorkingDevices(string serial)
        {
            var result = deviceRepairService.GetWorkingDevices(serial);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpPost]
        [Route("SetWorkingDate")]
        public IHttpActionResult SetWorkingDate([FromBody] SetWorkingDateCommand command)
        {
            var result = deviceRepairService.SetWorkingDate(command);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("RepairKPIReport")]
        public IHttpActionResult RepairKPIReport([FromUri] RepairKPIReportQuery request)
        {
            var result = deviceRepairService.RepairKPIReport(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("RepairTimeReport")]
        public IHttpActionResult RepairTimeReport([FromUri] RepairTimeReportQuery request)
        {
            var result = deviceRepairService.RepairTimeReport(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("RepairInventoryReport")]
        public IHttpActionResult RepairInventoryReport([FromUri] RepairInventoryReportQuery request)
        {
            var result = deviceRepairService.RepairInventoryReport(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }
    }
}
