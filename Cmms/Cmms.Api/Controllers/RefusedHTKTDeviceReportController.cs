﻿using Cmms.Api.Models;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Domain.Queries;
using System.Web.Http;

namespace Cmms.Api.Controllers
{
    [RoutePrefix("API/BaoCaoThietBiDoiTacTuChoiHTKT")]
    public class RefusedHTKTDeviceReportController : BaseApiController
    {
        private readonly IRefusedHTKTDeviceReportService refusedHTKTDeviceReportService;

        public RefusedHTKTDeviceReportController(IRefusedHTKTDeviceReportService refusedHTKTDeviceReportService)
        {
            this.refusedHTKTDeviceReportService = refusedHTKTDeviceReportService;
        }

        [HttpGet]
        [Route("ChiNhanh_NhaCungCap")]
        public IHttpActionResult GetStatisticByProvider([FromUri]DeviceReportQuery request)
        {
            var result = refusedHTKTDeviceReportService.GetStatisticByProvider(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("ChiNhanh_NhaCungCap/{chinhanh}/{nhaCungCap}")]
        public IHttpActionResult GetDeviceReportSummaryByProvider([FromUri]RefusedHTKTDeviceReportByProviderQuery request)
        {
            var result = refusedHTKTDeviceReportService.GetDeviceReportSummaryByProvider(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }
    }
}
