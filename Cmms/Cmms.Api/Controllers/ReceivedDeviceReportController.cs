﻿using Cmms.Api.Models;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Domain.Queries;
using System.Web.Http;

namespace Cmms.Api.Controllers
{
    [RoutePrefix("Api/BaoCaoThietBiDaTiepNhanTuDVQLSD")]
    public class ReceivedDeviceReportController : BaseApiController
    {
        private readonly IDeviceStatisticProviderService deviceStatisticService;
        public ReceivedDeviceReportController(IDeviceStatisticProviderService deviceStatisticService)
        {
            this.deviceStatisticService = deviceStatisticService;
        }

        [HttpGet]
        [Route("ChiNhanh_NhaCungCap")]
        public  IHttpActionResult GetDeviceStatisticProvider([FromUri]DeviceReportQuery request)
        {
            var result = deviceStatisticService.GetDeviceStatisticProvider(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("ChiNhanh_NhaCungCap/{branch}/{provider}")]
        public IHttpActionResult GetDeviceReportSummaryByProvider([FromUri] DeviceReportByProviderQuery request)
        {
            var result = deviceStatisticService.GetDeviceReportSummaryByProvider(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("ChiNhanh_DVQLSD_Dai")]
        public IHttpActionResult GetDeviceStatisticByDVQLSD_Dai([FromUri]DeviceReportQuery request)
        {
            var result = deviceStatisticService.GetDeviceStatisticByDVQLSD_Dai(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("ChiNhanh_DVQLSD_Dai/{branch}/{dvqlsd}/{daiVT}")]
        public IHttpActionResult GetDeviceReportSummaryByDVQLSD_Dai([FromUri]DeviceReportByDVQLSDDaiQuery request)
        {
            var result = deviceStatisticService.GetDeviceReportSummaryByDVQLSD_Dai(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }
    }
}
