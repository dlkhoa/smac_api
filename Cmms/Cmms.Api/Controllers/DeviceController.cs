﻿using System.Web.Http;
using Cmms.Api.Models;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Domain.Queries;

namespace Cmms.Api.Controllers
{
    [RoutePrefix("Api/Users/{username}/Thietbi")]
    public class DeviceController : BaseApiController
    {
        private readonly IDeviceService deviceService;
        public DeviceController(IDeviceService deviceService)
        {
            this.deviceService = deviceService;
        }

        [HttpGet]
        [Route("")]
        public IHttpActionResult LayDanhSachThietBi([FromUri] DeviceQuery request)
        {
            request.Serial = request.Serial ?? string.Empty;
            request.Branch = request.Branch ?? string.Empty;
            var result = deviceService.GetDevices(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("{serial}")]
        public IHttpActionResult TraCuuThietBi(string username, string serial)
        {
            var result = deviceService.GetDevice(username, serial);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("LichSuKiemTra/{serial}/{branch}")]
        public IHttpActionResult LayDanhMucThongTinLichSuKiemTraThietBi([FromUri]DeviceHistoryQuery request)
        {
            var result = deviceService.GetDeviceHistories(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("Advanced")]
        public IHttpActionResult AdvancedSearch([FromUri] DeviceAdvancedQuery request)
        {
            request.Serial = request.Serial ?? string.Empty;
            request.Name = request.Name ?? string.Empty;
            request.CodePartNumber = request.CodePartNumber ?? string.Empty;
            request.Status = request.Status ?? string.Empty;
            request.Provider = request.Provider ?? string.Empty;
            request.Object = request.Object ?? string.Empty;
            request.Classify = request.Classify ?? string.Empty;
            request.Branch = request.Branch ?? string.Empty;
            request.Station = request.Station ?? string.Empty;
            request.InputTestResult = request.InputTestResult ?? string.Empty;
            request.OutputTestResult = request.OutputTestResult ?? string.Empty;
            request.RepairPartner = request.RepairPartner ?? string.Empty;
            request.DVQLSD = request.DVQLSD ?? string.Empty;
            var result = deviceService.GetAdvancedDevices(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }
    }
}
