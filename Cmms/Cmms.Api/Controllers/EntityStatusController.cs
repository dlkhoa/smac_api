﻿using Cmms.Api.Models;
using Cmms.Business.ComponentServices.Abstracts;
using System.Web.Http;

namespace Cmms.Api.Controllers
{
    [RoutePrefix("Api")]
    public class EntityStatusController : BaseApiController
    {
        private readonly IEntityStatusService entityStatusService;

        public EntityStatusController(IEntityStatusService entityStatusService)
        {
            this.entityStatusService = entityStatusService;
        }

        [HttpGet]
        [Route("PhieuDeNghiNhapKho/DanhSachTrangThai")]
        public IHttpActionResult GetOrderStatuses()
        {
            var result = entityStatusService.GetStatuses("DOST");
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("PhieuDeNghiXuatKho/DanhSachTrangThai")]
        public IHttpActionResult GetPicklistStatuses()
        {
            var result = entityStatusService.GetStatuses("PLST");
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("PhieuTiepNhanThietBi/DanhSachTrangThai")]
        public IHttpActionResult GetRequisitionStatuses()
        {
            var result = entityStatusService.GetStatuses("RQST");
            return Ok(HttpDataResultHandler.CreateResult(result));
        }
    }
}
