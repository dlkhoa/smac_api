﻿using Cmms.Api.Models;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Domain.Queries;
using System.Web.Http;

namespace Cmms.Api.Controllers
{
    [RoutePrefix("Api/BaoCaoThietBiBanGiaoChoDVQLSD")]
    public class DeliveryDeviceReportController : BaseApiController
    {
        private readonly IDeliveryDeviceReportService deliveryDeviceReportService;
        public DeliveryDeviceReportController(IDeliveryDeviceReportService deliveryDeviceReportService)
        {
            this.deliveryDeviceReportService = deliveryDeviceReportService;
        }

        [HttpGet]
        [Route("ChiNhanh_DoiTuongTb_NhaCungCap")]
        public IHttpActionResult GetStatisticByProvider([FromUri]DeviceReportQuery request)
        {
            var result = deliveryDeviceReportService.GetStatisticByProvider(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("ChiNhanh_DoiTuongTb_NhaCungCap/{chinhanh}/{doiTuongTb}/{nhaCungCap}")]
        public IHttpActionResult GetDeviceReportSummaryByProvider([FromUri]DeliveryDeviceReportByProviderQuery request)
        {
            var result = deliveryDeviceReportService.GetDeviceReportSummaryByProvider(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("ChiNhanh_DVQLSD_Dai")]
        public IHttpActionResult GetDeviceStatisticByDVQLSD_Dai([FromUri]DeviceReportQuery request)
        {
            var result = deliveryDeviceReportService.GetDeviceStatisticByDVQLSD_Dai(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }


        [HttpGet]
        [Route("ChiNhanh_DVQLSD_Dai/{chinhanh}/{dvqlsd}/{daiVT}")]
        public IHttpActionResult GetDeviceReportSummaryByDVQLSD_Dai([FromUri]DeliveryDeviceReportByDVQLSDDaiQuery request)
        {
            var result = deliveryDeviceReportService.GetDeviceReportSummaryByDVQLSD_Dai(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }
    }
}
