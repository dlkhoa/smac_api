﻿using Cmms.Api.Models;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Domain.Queries;
using System.Web.Http;

namespace Cmms.Api.Controllers
{
    [RoutePrefix("Api/BaoCaoThietBiTotSauSuaChuaGiaoChoDVQLSD")]
    public class DeliveryGoodDeviceReportController : BaseApiController
    {
        private readonly IDeliveryGoodDeviceReportService deliveryGoodDeviceReportService;
        public DeliveryGoodDeviceReportController(IDeliveryGoodDeviceReportService deliveryGoodDeviceReportService)
        {
            this.deliveryGoodDeviceReportService = deliveryGoodDeviceReportService;
        }

        [HttpGet]
        [Route("ChiNhanh_DoiTuongTB_NhaCungCap")]
        public IHttpActionResult GetStatisticByProvider([FromUri]DeviceReportQuery request)
        {
            var result = deliveryGoodDeviceReportService.GetStatisticByProvider(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("ChiNhanh_DoiTuongTb_NhaCungCap/{chinhanh}/{doiTuongTb}/{nhaCungCap}")]
        public IHttpActionResult GetDeviceReportSummaryByProvider([FromUri]DeliveryDeviceReportByProviderQuery request)
        {
            var result = deliveryGoodDeviceReportService.GetDeviceReportSummaryByProvider(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("ChiNhanh_DVQLSD_Dai")]
        public IHttpActionResult GetDeviceStatisticByDVQLSD_Dai([FromUri]DeviceReportQuery request)
        {
            var result = deliveryGoodDeviceReportService.GetDeviceStatisticByDVQLSD_Dai(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }


        [HttpGet]
        [Route("ChiNhanh_DVQLSD_Dai/{chinhanh}/{dvqlsd}/{daiVT}")]
        public IHttpActionResult GetDeviceReportSummaryByDVQLSD_Dai([FromUri]DeliveryDeviceReportByDVQLSDDaiQuery request)
        {
            var result = deliveryGoodDeviceReportService.GetDeviceReportSummaryByDVQLSD_Dai(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }
    }
}
