﻿using Cmms.Api.Models;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Domain.Queries;
using System.Web.Http;

namespace Cmms.Api.Controllers
{
    [RoutePrefix("Api/BaoCaoThietBiDoiTacChuaBanGiao")]
    public class NotDeliveryDeviceReportController : BaseApiController
    {
        private readonly INotDeliveryDeviceReportService notDeliveryDeviceReportService;

        public NotDeliveryDeviceReportController(INotDeliveryDeviceReportService notDeliveryDeviceReportService)
        {
            this.notDeliveryDeviceReportService = notDeliveryDeviceReportService;
        }

        [HttpGet]
        [Route("ChiNhanh_DoiTuongTB_NhaCungCap_DoiTac")]
        public IHttpActionResult GetStatisticByDoiTac([FromUri]DeviceReportQuery request)
        {
            var result = notDeliveryDeviceReportService.GetStatisticByDTSC(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("ChiNhanh_DoiTuongTB_NhaCungCap_DoiTac/{chinhanh}/{doiTuongTb}/{nhaCungCap}/{doitac}")]
        public IHttpActionResult GetDeviceReportSummaryByDTSC([FromUri]NotDeliveryDeviceReportByDTSCQuery request)
        {
            var result = notDeliveryDeviceReportService.GetDeviceReportSummaryByDTSC(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("ChiNhanh_DVQLSD_Dai")]
        public IHttpActionResult GetDeviceStatisticByDVT([FromUri]DeviceReportQuery request)
        {
            var result = notDeliveryDeviceReportService.GetDeviceStatisticByDVT(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }


        [HttpGet]
        [Route("ChiNhanh_DVQLSD_Dai/{chinhanh}/{dvqlsd}/{daiVT}")]
        public IHttpActionResult GetDeviceReportSummaryByDVT([FromUri]NotDeliveryDeviceReportByDVTQuery request)
        {
            var result = notDeliveryDeviceReportService.GetDeviceReportSummaryByDVT(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }
    }
}
