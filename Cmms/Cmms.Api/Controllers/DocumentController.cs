﻿using Cmms.Api.Models;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Domain.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Cmms.Api.Controllers
{
    [RoutePrefix("Api/Document")]
    public class DocumentController : BaseApiController
    {
        private readonly IDocumentService documentService;

        public DocumentController(IDocumentService documentService)
        {
            this.documentService = documentService;
        }

        [HttpGet]
        [Route("List/{type}/{orderCode}")]
        public IHttpActionResult GetDocumentList(string type, string orderCode)
        {
            var result = documentService.GetDocumentList(type, orderCode);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

    }
}
