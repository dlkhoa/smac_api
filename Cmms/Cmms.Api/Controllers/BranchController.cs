﻿using Cmms.Api.Models;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Domain.Queries;
using System.Web.Http;

namespace Cmms.Api.Controllers
{
    [RoutePrefix("Api/Danhmuc/Chinhanh")]
    public class BranchController : BaseApiController
    {
        private readonly IBranchService branchService;

        public BranchController(IBranchService branchService)
        {
            this.branchService = branchService;
        }

        [HttpGet]
        [Route("")]
        public IHttpActionResult GetBranches([FromUri] BranchQuery request)
        {
            request = request ?? new BranchQuery();
            request.Branch = request.Branch ?? string.Empty;
            var result = branchService.GetBranches(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }
    }
}
