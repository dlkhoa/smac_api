﻿using Cmms.Api.Models;
using Cmms.Business.Common;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Data.Constants;
using Cmms.Domain.Commands;
using Cmms.Domain.Queries;
using System.Web.Http;

namespace Cmms.Api.Controllers
{
    [RoutePrefix("Api/Users/{username}")]
    public class OrderController : BaseApiController
    {
        private readonly IOrderService orderService;
        private readonly IOrderCommandService orderCommandService;
        private readonly INotificationService notificationService;

        public OrderController(IOrderService orderService, IOrderCommandService orderCommandService, INotificationService notificationService)
        {
            this.orderService = orderService;
            this.orderCommandService = orderCommandService;
            this.notificationService = notificationService;
        }

        [HttpGet]
        [Route("PhieuDeNghiNhapKho")]
        public IHttpActionResult LayDanhMucPhieuDeNghiNhapKho([FromUri] OrderQuery request)
        {
            request.Branch = request.Branch ?? string.Empty;
            request.PermissionFunction = "'PSPORF','PSPORD','PSPORG','PSPOBH'";
            var result = orderService.GetOrders(request);

            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("PhieuDeNghiNhapKho/{orderCode}")]
        public IHttpActionResult LayChiTietPhieuDeNghiNhapKho(string username, string orderCode)
        {
            var result = orderService.GetOrder(username, orderCode);

            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("PhieuDeNghiNhapKhoChoPheDuyet")]
        public IHttpActionResult LayDanhMucPhieuDeNghiNhapKhoChoPheDuyet([FromUri] AwatingOrderQuery request)
        {
            request.Branch = request.Branch ?? string.Empty;
            request.PermissionFunction = "'PSPORF','PSPORD','PSPORG','PSPOBH'";
            request.Type = string.Format("'{0}','{1}'", OrderStatusConstant.ChoLanhDaoChiNhanhPheDuyet, OrderStatusConstant.ChoLenhDieuDong);
            var result = orderService.GetAwatingOrders(request);

            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("PhieuDeNghiNhapKhoChoPheDuyetTaiChiNhanh")]
        public IHttpActionResult LayDanhMucPhieuDeNghiNhapKhoChoPheDuyetTaiChiNhanh([FromUri] AwatingOrderQuery request)
        {
            request.Branch = request.Branch ?? string.Empty;
            request.PermissionFunction = "'PSPORF','PSPORD','PSPORG','PSPOBH'";
            request.Type = string.Format("'{0}'", OrderStatusConstant.ChoLanhDaoChiNhanhPheDuyet);
            var result = orderService.GetAwatingOrders(request);

            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("PhieuDeNghiNhapKhoChoLenhDieuDong")]
        public IHttpActionResult LayDanhMucPhieuDeNghiNhapKhoChoLenhDieuDong([FromUri] AwatingOrderQuery request)
        {
            request.Branch = request.Branch ?? string.Empty;
            request.PermissionFunction = "'PSPORF','PSPORD','PSPORG','PSPOBH'";
            request.Type = string.Format("'{0}'", OrderStatusConstant.ChoLenhDieuDong);
            var result = orderService.GetAwatingOrders(request);

            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        //[HttpPut]
        //[Route("PhieuDeNghiNhapKho/{orderCode}/PheDuyetLenhDieuDongNhap")]
        //public IHttpActionResult PheDuyetLenhDieuDongNhap(string username, string orderCode)
        //{
        //    var command = new ApproveOrderCommand
        //    {
        //        Username = username,
        //        OrderCode = orderCode
        //    };
        //    orderCommandService.ApproveOrder(command);
        //    return Ok(HttpDataResultHandler.CreateResult(true));
        //}

        [HttpPut]
        [Route("PhieuDeNghiNhapKho/{orderCode}/PheDuyet")]
        public IHttpActionResult PheDuyet(string username, string orderCode)
        {
            var command = new ApproveOrderCommand
            {
                Username = username,
                OrderCode = orderCode
            };
            var notificationItem = orderCommandService.ApproveOrder(command);

            notificationService.PushNotification(notificationItem);
            return Ok(HttpDataResultHandler.CreateResult(true));
        }
    }
}
