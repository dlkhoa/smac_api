﻿using Cmms.Api.Models;
using Cmms.Business.ComponentServices.Abstracts;
using System.Web.Http;

namespace Cmms.Api.Controllers
{
    [RoutePrefix("Api/Users")]
    public class UserController : BaseApiController
    {
        private readonly IUserService userService;

        public UserController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpGet]
        [Route("Authentication")]
        public IHttpActionResult Authenticate(string username, string password)
        {
            var result = userService.Authenticate(username, password);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }
    }
}
