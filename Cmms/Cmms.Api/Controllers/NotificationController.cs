﻿using Cmms.Api.Models;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Domain.Commands;
using System.Web.Http;
using Cmms.Domain.Queries;

namespace Cmms.Api.Controllers
{
    [RoutePrefix("Api/Notifications")]
    public class NotificationController : BaseApiController
    {
        private readonly INotificationService notificationService;
        private readonly IAwaitingItemNotificationService awaitingItemNotificationService;

        public NotificationController(INotificationService notificationService, IAwaitingItemNotificationService awaitingItemNotificationService)
        {
            this.notificationService = notificationService;
            this.awaitingItemNotificationService = awaitingItemNotificationService;
        }

        [HttpPost]
        [Route("Register")]
        public IHttpActionResult Register([FromBody] RegisterNotificationCommand command)
        {
            return Ok(HttpDataResultHandler.CreateResult(notificationService.RegisterDevice(command)));
        }

        [HttpPost]
        [Route("Push")]
        public IHttpActionResult Push([FromBody] PushNotificationCommand command)
        {
            notificationService.PushNotification(new Domain.Entities.NotificationItem
            {
                PrimaryId = command.OrderCode,
                EntityType = command.OrderType,
                FromStatus = command.FromStatus,
                ToStatus = command.ToStatus
            });
            return Ok(HttpDataResultHandler.CreateResult(true));
        }

        [HttpPost]
        [Route("PushToSpecify")]
        public IHttpActionResult PushToSpecify([FromBody] PushNotificationCommand command)
        {
            var result = MBoL.Utils.FCMServiceHelper.PushNotification(new System.Collections.Generic.List<string> { command.DeviceId },
                command.Message,
                new Business.DataContracts.NotificationDataResponse { MaPhieu = command.OrderCode, LoaiPhieu = command.OrderType });

            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpDelete]
        [Route("")]
        public IHttpActionResult Delete([FromBody] RegisterNotificationCommand command)
        {
            notificationService.Delete(command.DeviceId);
            return Ok(HttpDataResultHandler.CreateResult(true));
        }

        [HttpGet]
        [Route("DanhSachPhieuChoPheDuyet")]
        public IHttpActionResult LayDanhSachPhieuChoPheDuyet([FromUri] AwaitingItemNotificationQuery request)
        {
            var result = awaitingItemNotificationService.LayDanhSachPhieuChoPheDuyet(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }
    }
}
