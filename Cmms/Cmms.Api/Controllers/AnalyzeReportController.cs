﻿using Cmms.Api.Models;
using Cmms.Business.ComponentServices.Abstracts;
using Cmms.Domain.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Cmms.Api.Controllers
{
    [RoutePrefix("Api/BaocaoPhantich")]
    public class AnalyzeReportController : BaseApiController
    {
        private readonly IAnalyzeReportService analyzeReportService;

        public AnalyzeReportController(IAnalyzeReportService analyzeReportService)
        {
            this.analyzeReportService = analyzeReportService;
        }

        [HttpGet]
        [Route("ThietbiTiepnhan/{chinhanh}/{dvqlsd}/{nam}")]
        public IHttpActionResult GetReceivedDeviceReportByYear([FromUri]DeviceReportByYearQuery request)
        {
            var result = analyzeReportService.GetReceivedDeviceReportByYear(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("ThietbiBangiao/{chinhanh}/{dvqlsd}/{nam}")]
        public IHttpActionResult GetSentDeviceReportByYear([FromUri]DeviceReportByYearQuery request)
        {
            var result = analyzeReportService.GetSentDeviceReportByYear(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("TyleHoanthanhTrungtam/{nam}")]
        public IHttpActionResult GetCenterFulfillReportByYear([FromUri]CenterFulfillReportByYearQuery request)
        {
            var result = analyzeReportService.GetCenterFulfillReportByYear(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("TyleHoanthanhChinhanh/{chinhanh}/{nam}")]
        public IHttpActionResult GetBranchFulfillReportByYear([FromUri]BranchFulfillReportByYearQuery request)
        {
            var result = analyzeReportService.GetBranchFulfillReportByYear(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("SoluongThietbiTiepnhanTrungtam/{nam}")]
        public IHttpActionResult GetCenterNumberOfReceivedDevicesReportByYear([FromUri]CenterNumberOfDevicesReportByYearQuery request)
        {
            var result = analyzeReportService.GetCenterNumberOfReceivedDevicesReportByYear(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("SoluongThietbiTiepnhanChinhanh/{chinhanh}/{dvqlsd}/{nam}")]
        public IHttpActionResult GetBranchNumberOfReceivedDevicesReportByYear([FromUri]BranchNumberOfDevicesReportByYearQuery request)
        {
            var result = analyzeReportService.GetBranchNumberOfReceivedDevicesReportByYear(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("SoluongThietbiBangiaoTrungtam/{nam}")]
        public IHttpActionResult GetCenterNumberOfSentDevicesReportByYear([FromUri]CenterNumberOfDevicesReportByYearQuery request)
        {
            var result = analyzeReportService.GetCenterNumberOfSentDevicesReportByYear(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("SoluongThietbiBangiaoChinhanh/{chinhanh}/{dvqlsd}/{nam}")]
        public IHttpActionResult GetBranchNumberOfSentDevicesReportByYear([FromUri]BranchNumberOfDevicesReportByYearQuery request)
        {
            var result = analyzeReportService.GetBranchNumberOfSentDevicesReportByYear(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }

        [HttpGet]
        [Route("Summary/{username}/{branch}/{year}")]
        public IHttpActionResult GetSummaryByBranchAndByYear([FromUri]SummaryByBranchAndByYearQuery request)
        {
            var result = analyzeReportService.GetSummaryByBranchAndByYear(request);
            return Ok(HttpDataResultHandler.CreateResult(result));
        }
    }
}
