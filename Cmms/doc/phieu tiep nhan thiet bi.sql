SELECT o.ord_code sophieu,o.ord_desc
        diengiai,o.ord_org chinhanh,r5o7.o7get_desc('VI','UCOD',
        ord_status,'DOST', '') tinhtrangphieu,
        o.ord_store kho,ord_udfchar01 donvidenghi,o.ord_supplier
        doitacDVQLSD,(ord_udfchar02 || ord_udfchar03) lydo,
        o.ord_origin nguoidenghi,o.ord_due ngaydenghi,o.ord_auth
        nguoipheduyet,o.ord_approv ngaypheduyet,ord_udfdate03
        ngayCNguoiKHVC,
        ord_udfdate02 NgayKTDVXK,ord_udfdate01
        NgayCNNTK,ord_udfdate05 NgayDVVCBG,ord_udfdate04
        NgayDVVCTN 
FROM r5orders o, r5companies c,r5permissions p, r5userorganization u
WHERE o.ord_supplier = c.com_code
  AND o.ord_supplier_org = c.com_org
  AND u.uog_org=o.ord_org
  AND u.uog_user = 'UserID'
  AND u.uog_group = p.prm_group
  AND p.prm_select = '?'
  AND p.prm_function IN ('PSPORF','PSPORD','PSPORG','PSPOBH') 
  AND o.ord_supplier IN (SELECT com_code 
                        FROM r5companies 
                        WHERE nvl(com_parent,com_code) IN (SELECT dse_mrc 
                                                          FROM r5departmentsecurity 
                                                          WHERE dse_user ='UserID'))
ORDER BY o.ord_date DESC, o.ord_code DESC;

