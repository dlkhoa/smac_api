select * from r5users;
select * from r5permissions;
select * from r5userorganization;
select * from r5auth;
select * from auth_role;
select req_class from r5requisitions group by req_class;
select req_date,req_rtype,req_class, req_desc Mota,req_udfdate01 ngaytiepnhan,REQ_ORG Chinhanh,r5o7.o7get_desc('VI','UCOD', req_status,'RQST', '') Tinhtrangphieu,REQ_TOCODE Kho,
REQ_FROMCODE Bengiao,req_udfchar02 Nguoigiao1,req_udfchar03 Chucvu1,REQ_UDFCHAR11 Nguoigiao2,REQ_UDFCHAR13 Chucvu2,req_udfchar01 Bennhan,req_origin Nguoinhan1,
REQ_UDFCHAR10 Nguoinhan2,REQ_UDFCHAR12 ChucvuNhan2,req_udfchar04 DVVanchuyen,req_udfchar05 Nguoivanchuyen,REQ_UDFCHAR09 Chucvunhan1,
req_udfchar06 ChuvuVC,req_auth Nguoitiepnhan,req_dateapproved Ngaytiepnhan,req_udfchar07 NoidungBg,req_udfchar08 udfchar08,REQ_DELADDRESS Diadiem,REQ_CODE Sophieu 
FROM r5requisitions r
WHERE  req_rtype = 'RECV'
  AND exists (SELECT 1 FROM r5permissions, r5userorganization
                      WHERE uog_user = 'ADMIN' AND uog_org = req_org
                      AND uog_group = prm_group AND prm_select = '?' AND prm_function =  'TNTBH') 
  and (req_class='TNTBHH'
    and req_fromcode in ( select com_code 
                          from r5companies 
                          where nvl(com_parent,com_code) in( SELECT dse_mrc 
                                                            FROM r5departmentsecurity 
                                                              WHERE dse_user ='ADMIN'))) 
order by req_date DESC, REQ_CODE DESC;

SELECT * FROM r5permissions, r5userorganization
                      WHERE uog_user = 'SUPPORT' 
                      AND uog_group = prm_group AND prm_select = '?' AND prm_function =  'TNTBH';
select * from r5roles;
select * from r5users where usr_code='SUPPORT';
select * from R5DEPARTMENTSECURITY where dse_user='SUPPORT';

SELECT count(*) FROM r5permissions
                      WHERE prm_group = 'ADMIN'
                      AND uog_group = prm_group