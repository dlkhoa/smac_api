﻿namespace Cmms.Domain.Entities
{
    public class Organization
    {
        public string OrgCode { get; set; }
        public string Description { get; set; }
    }
}
