﻿namespace Cmms.Domain.Entities
{
    public class UserOrganization
    {
        public string Username { get; set; }
        public string Organization { get; set; }
    }
}
