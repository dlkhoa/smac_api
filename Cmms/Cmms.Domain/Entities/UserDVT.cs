﻿namespace Cmms.Domain.Entities
{
    public class UserDVT
    {
        public string UserCode { get; set; }
        public string DVT { get; set; }
    }
}
