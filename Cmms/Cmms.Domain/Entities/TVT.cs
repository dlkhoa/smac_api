﻿namespace Cmms.Domain.Entities
{
    public class TVT
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Organization { get; set; }
        public string DVT { get; set; }
        public string DVTOrganization { get; set; }
    }
}
