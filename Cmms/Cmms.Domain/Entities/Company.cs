﻿namespace Cmms.Domain.Entities
{
    public class Company
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Parent { get; set; }
        public string UdfChkBox01 { get; set; }
        public string UdfChkBox02 { get; set; }
    }
}
