﻿using System.Collections.Generic;

namespace Cmms.Domain.Entities
{
    public class User
    {
        public string UserCode { get; set; }
        public string Password { get; set; }
        public string Description { get; set; }
        public string GroupCode { get; set; }
        public string DefaultOrg { get; set; }
        public virtual Group Group { get; set; }

        public virtual ICollection<DepartmentSecurity> DepartmentSecurities { get; set; }

        public User()
        {
            DepartmentSecurities = new List<DepartmentSecurity>();
        }
    }
}
