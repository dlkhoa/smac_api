﻿namespace Cmms.Domain.Entities
{
    public class Provider
    {
        public string MaNhaCungCap { get; set; }
        public string TenNhaCungCap { get; set; }
        public string DiaChi { get; set; }
        public string NguoiLienHe { get; set; }
        public string DienThoai { get; set; }
        public string Email { get; set; }
    }
}
