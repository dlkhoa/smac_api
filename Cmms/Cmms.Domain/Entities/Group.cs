﻿using System.Collections.Generic;

namespace Cmms.Domain.Entities
{
    public class Group
    {
        public string GroupCode { get; set; }
        public string Description { get; set; }
        public short? SessionTimeout { get; set; }

        public virtual ICollection<User> Users { get; set; }

        public Group()
        {
            Users = new List<User>();
        }
    }
}
