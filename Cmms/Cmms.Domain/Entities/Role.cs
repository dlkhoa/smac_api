﻿namespace Cmms.Domain.Entities
{
    public class Role
    {
        public string RoleCode { get; set; }
        public string Description { get; set; }
        public string GroupCode { get; set; }
    }
}
