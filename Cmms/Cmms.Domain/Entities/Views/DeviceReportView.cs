﻿using System;

namespace Cmms.Domain.Entities.Views
{
    public class DeviceReportView
    {
        public string ChiNhanh { get; set; }
        public string TenThietBi { get; set; }
        public string Serial { get; set; }
        public string NhaCungCap { get; set; }
        public string DoiTuongThietBi { get; set; }
        public string DaiVienThong { get; set; }
        public string DVQLSD { get; set; }
        public string TinhTrang { get; set; }
        public string ReqCode { get; set; }
        public string DVQLSD2 { get; set; }
        public string LoaiTB { get; set; }
        public string CodeKQSSC { get; set; }
        public string CodeKQTSC { get; set; }
        public string CodeDTTB { get; set; }
        public string DoiTac { get; set; }
        public DateTime? NgayDTBG { get; set; }
        public DateTime? NgayBanGiaoDVQLSD { get; set; }
        public string TinhTrangBanGiao { get; set; }
        public int SLXH { get; set; }
        public DateTime? NgayCNNTK { get; set; }
        public DateTime? NgayBGDT { get; set; }
    }
}
