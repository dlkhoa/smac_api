﻿using System;

namespace Cmms.Domain.Entities.Views
{
    public class ImportExportHistoryView
    {
        public string LoaiPhieu { get; set; }
        public string TiepNhan { get; set; }
        public string DeNghi { get; set; }
        public string Lo { get; set; }
        public string SoGiaoDich { get; set; }
        public DateTime? Ngay { get; set; }
        public string TinhTrangPhieu { get; set; }
        public string Supplier { get; set; }
        public string Serial { get; set; }
        public string Org { get; set; }
    }
}
