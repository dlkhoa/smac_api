﻿namespace Cmms.Domain.Entities
{
    public class MobileDevice
    {
        public string Username { get; set; }
        public string DeviceId { get; set; }
        public string DeviceOS { get; set; }
    }
}
