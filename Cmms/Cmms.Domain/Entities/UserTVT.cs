﻿namespace Cmms.Domain.Entities
{
    public class UserTVT
    {
        public string UserCode { get; set; }
        public string TVT { get; set; }
        public string DVT { get; set; }
    }
}
