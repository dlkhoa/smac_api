﻿namespace Cmms.Domain.Entities
{
    public class Order
    {
        public string OrderCode { get; set; }
        public string Org { get; set; }
        public string CreatedUser { get; set; }
    }
}
