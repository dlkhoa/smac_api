﻿namespace Cmms.Domain.Entities
{
    public class Requisition
    {
        public string RequisitionCode { get; set; }
        public string CreatedUser { get; set; }
    }
}
