﻿namespace Cmms.Domain.Entities
{
    public class EntityStatus
    {
        public string Entity { get; set; }
        public string StatusCode { get; set; }
        public string Description { get; set; }
        public string NotUsed { get; set; }
    }
}
