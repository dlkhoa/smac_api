﻿namespace Cmms.Domain.Entities
{
    public class DepartmentSecurity
    {
        public string UserCode { get; set; }
        public string MrcCode { get; set; }

        public virtual User User { get; set; }
        public virtual Mrc Mrc { get; set; }
    }
}
