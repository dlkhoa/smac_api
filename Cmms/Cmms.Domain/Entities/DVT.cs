﻿namespace Cmms.Domain.Entities
{
    public class DVT
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Organization { get; set; }
    }
}
