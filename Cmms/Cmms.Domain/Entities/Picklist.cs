﻿namespace Cmms.Domain.Entities
{
    public class Picklist
    {
        public string Code { get; set; }
        public string Status { get; set; }
        public string RStatus { get; set; }
        public string Store { get; set; }
        public string CreatedUser { get; set; }
    }
}
