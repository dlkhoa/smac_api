﻿namespace Cmms.Domain.Entities.DataContracts
{
    public class PicklistPartSummary
    {
        public string Serial { get; set; }
        public string TenThietBi { get; set; }
        public string DonViTinh { get; set; }
        public decimal SoLuong { get; set; }
    }
}
