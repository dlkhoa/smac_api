﻿namespace Cmms.Domain.Entities.DataContracts
{
    public class DeviceStatisticByMonth
    {
        public string Chinhanh { get; set; }
        public string DVQLSD1 { get; set; }
        public string DAIVT { get; set; }
        public int Thang { get; set; }
        public int Tong { get; set; }
    }
}
