﻿using System.Collections.Generic;

namespace Cmms.Domain.Entities.DataContracts
{
    public class SummaryByBranchAndByYearRepair
    {
        public decimal Overall { get; set; } = 0;
        public decimal Received { get; set; } = 0;
        public decimal Organized { get; set; } = 0;
        public decimal TestBefore { get; set; } = 0;
        public decimal TestAfter { get; set; } = 0;
        public decimal Stock { get; set; } = 0;
        public decimal Delivery { get; set; } = 0;
    }
}
