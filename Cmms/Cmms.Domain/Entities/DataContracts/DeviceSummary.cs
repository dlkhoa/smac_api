﻿namespace Cmms.Domain.Entities.DataContracts
{
    public class DeviceSummary
    {
        public string Serial { get; set; }
        public string TenThietBi { get; set; }
        public string ChiNhanh { get; set; }
        public string TrangThai { get; set; }
    }
}
