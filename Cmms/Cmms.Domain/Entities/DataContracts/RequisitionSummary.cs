﻿using System;

namespace Cmms.Domain.Entities.DataContracts
{
    public class RequisitionSummary
    {
        public string MaSophieu { get; set; }
        public string MoTa { get; set; }
        public string ChiNhanh { get; set; }
        public string TinhTrangPhieu { get; set; }
        public string Loai { get; set; }
    }
}
