﻿using System;

namespace Cmms.Domain.Entities.DataContracts
{
    public class UserUnit
    {
        public string UserCode { get; set; }
        public string TVT { get; set; }
        public string DVT { get; set; }

    }
}
