﻿using System.Collections.Generic;

namespace Cmms.Domain.Entities.DataContracts
{
    public class RepairDeviceSummarize
    {
        public string UserCode { get; set; }

        public List<RepairDeviceSummarizeItem> Items { get; set; }

        public RepairDeviceSummarize()
        {
            Items = new List<RepairDeviceSummarizeItem>();
        }
    }
}