﻿using System;

namespace Cmms.Domain.Entities.DataContracts
{
    public class DeliverySLADeviceStatisticByMonth
    {
        public string ChiNhanh { get; set; }
        public DateTime NgayBanGiao { get; set; }
        public int Tong { get; set; }
    }
}
