﻿namespace Cmms.Domain.Entities.DataContracts
{
    public class DeviceStatisticByProvider : DeviceStatistic
    {
        public string Provider { get; set; }
    }
}
