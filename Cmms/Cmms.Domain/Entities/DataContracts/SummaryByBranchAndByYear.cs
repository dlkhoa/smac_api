﻿using System.Collections.Generic;

namespace Cmms.Domain.Entities.DataContracts
{
    public class SummaryByBranchAndByYear
    {
        public int BrokenReceived { get; set; } = 0;
        public int GoodReturn { get; set; } = 0;
        public int Quota { get; set; } = 95;
        public IEnumerable<SummaryByBranchAndByYearSuccessRatio> SuccessRatios { get; set; }
        public SummaryByBranchAndByYearRebroken Rebroken { get; set; }
        public SummaryByBranchAndByYearSLA SLA { get; set; }
        public SummaryByBranchAndByYearRepair RepairDays { get; set; }
        public SummaryByBranchAndByYearStock Stock { get; set; } 
    }
}
