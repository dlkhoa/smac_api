﻿namespace Cmms.Domain.Entities.DataContracts
{
    public class PicklistForApproval
    {
        public string PicklistCode { get; set; }
        public string Status { get; set; }
        public string Store { get; set; }
    }
}
