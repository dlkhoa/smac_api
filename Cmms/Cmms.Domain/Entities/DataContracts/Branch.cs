﻿namespace Cmms.Domain.Entities.DataContracts
{
    public class Branch
    {
        public string MaChiNhanh { get; set; }
        public  string TenChiNhanh { get; set; }
        public string TienTe { get; set; }
        public string ViTri { get; set; }
    }
}
