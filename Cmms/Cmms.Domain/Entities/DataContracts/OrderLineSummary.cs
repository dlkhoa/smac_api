﻿namespace Cmms.Domain.Entities.DataContracts
{
    public class OrderLineSummary
    {
        public int SoThuTu { get; set; }
        public string Serial { get; set; }
        public string TenThietBi { get; set; }
        public decimal SoLuong { get; set; }
        public string DonViTinh { get; set; }
        public string MaSoPhieuTiepNhan { get; set; }
    }
}
