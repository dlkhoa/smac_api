﻿namespace Cmms.Domain.Entities.DataContracts
{
    public class NotDeliveryDeviceStatisticByDVT
    {   
        public string ChiNhanh { get; set; }
        public string DVQLSD { get; set; }
        public string DaiVT { get; set; }
        public int Tong { get; set; }
    }
}
