﻿namespace Cmms.Domain.Entities.DataContracts
{
    public class DanhMucDoiTacSuaChua
    {
        public string MaDoiTac { get; set; }
        public string TenDoiTac { get; set; }
        public string DoiTacCha { get; set; }
    }
}
