﻿using System;

namespace Cmms.Domain.Entities.DataContracts
{
    public class DocumentDetails
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Filename { get; set; }
    }
}
