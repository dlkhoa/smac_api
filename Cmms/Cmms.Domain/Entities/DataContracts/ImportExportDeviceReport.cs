﻿namespace Cmms.Domain.Entities.DataContracts
{
    public class ImportExportDeviceReport
    {
        public string TenThietBi { get; set; }
        public int TonDau { get; set; }
        public int NhapTrongKy { get; set; }
        public int XuatTrongKy { get; set; }
        public int TonCuoi { get; set; }
        public string ChiNhanh { get; set; }
        public string Kho { get; set; }
    }
}
