﻿using System.Collections.Generic;

namespace Cmms.Domain.Entities.DataContracts
{
    public class SummaryByBranchAndByYearSLA
    {
        public decimal SLA1 { get; set; } = 0;
        public decimal SLA2 { get; set; } = 0;
        public decimal SLA3 { get; set; } = 0;
    }
}
