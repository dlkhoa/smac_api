﻿namespace Cmms.Domain.Entities.DataContracts
{
    public class DeviceStatisticByDaiVT : DeviceStatistic
    {
        public string DVQLSD1 { get; set; }
        public string DaiVT { get; set; }
    }
}
