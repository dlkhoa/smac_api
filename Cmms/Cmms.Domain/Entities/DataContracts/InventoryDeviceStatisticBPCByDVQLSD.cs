﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmms.Domain.Entities.DataContracts
{
    public class InventoryDeviceStatisticBPCByDVQLSD: InventoryDeviceStatisticBPC
    {
        public string DVQLSD { get; set; }
    }
}
