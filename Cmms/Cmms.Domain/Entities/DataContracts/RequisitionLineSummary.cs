﻿namespace Cmms.Domain.Entities.DataContracts
{
    public class RequisitionLineSummary
    {
        public string LoaiThietBi { get; set; }
        public string Serial { get; set; }
        public string TenThietBi { get; set; }
        public decimal SoLuong { get; set; }
        public string DonViTinh { get; set; }
    }
}
