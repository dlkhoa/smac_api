﻿using System.Collections.Generic;

namespace Cmms.Domain.Entities.DataContracts
{
    public class SummaryByBranchAndByYearSuccessRatio
    {
        public string Month { get; set; } = "";
        public int Ratio { get; set; } = 0;
    }
}
