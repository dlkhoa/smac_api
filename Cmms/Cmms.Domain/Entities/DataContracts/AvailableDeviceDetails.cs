﻿using System.Collections.Generic;

namespace Cmms.Domain.Entities.DataContracts
{
    public class AvailableDeviceDetails
    {
        public string SerialNumber { get; set; }
        public string DeviceName { get; set; }
        public string Status { get; set; }
        public string StationName { get; set; }

    }
}