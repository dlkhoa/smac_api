﻿namespace Cmms.Domain.Entities.DataContracts
{
    public class ImportExportFixingDeviceReport
    {
        public string TenThietBi { get; set; }
        public string Kho { get; set; }
        public int SoLuong { get; set; }

    }
}
