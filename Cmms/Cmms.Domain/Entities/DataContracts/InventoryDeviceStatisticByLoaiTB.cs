﻿namespace Cmms.Domain.Entities.DataContracts
{
    public class InventoryDeviceStatisticByLoaiTB
    {
        public string ChiNhanh { get; set; }
        public string DVQLSD { get; set; }
        public string LoaiTB { get; set; }
        public int Tong { get; set; }
    }
}
