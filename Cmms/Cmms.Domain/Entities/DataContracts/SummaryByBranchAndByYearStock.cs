﻿using System.Collections.Generic;

namespace Cmms.Domain.Entities.DataContracts
{
    public class SummaryByBranchAndByYearStock
    {
        public int InStock { get; set; } = 0;
        public int BeforeRepair { get; set; } = 0;
        public int InRepair { get; set; } = 0;
        public int AfterRepair { get; set; } = 0;
        public int Good { get; set; } = 0;
    }
}
