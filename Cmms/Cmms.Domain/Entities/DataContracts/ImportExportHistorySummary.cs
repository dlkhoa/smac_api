﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmms.Domain.Entities.DataContracts
{
    public class ImportExportHistorySummary
    {
        public string LoaiPhieu { get; set; }
        public string DeNghi { get; set; }
        public string SoGiaoDich { get; set; }
        public DateTime? Ngay { get; set; }
        public string TinhTrangPhieu { get; set; }
        public string Supplier { get; set; }
    }
}
