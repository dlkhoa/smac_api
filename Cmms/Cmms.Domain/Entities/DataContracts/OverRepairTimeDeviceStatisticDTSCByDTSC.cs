﻿namespace Cmms.Domain.Entities.DataContracts
{
    public class OverRepairTimeDeviceStatisticDTSCByDTSC
    {
        public string ChiNhanh { get; set; }
        public string DoiTac { get; set; }
        public int Tong { get; set; }
    }
}
