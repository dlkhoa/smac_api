﻿using System.Collections.Generic;

namespace Cmms.Domain.Entities.DataContracts
{
    public class SummaryByBranchAndByYearRebroken
    {
        public int Number { get; set; } = 0;
        public decimal Ratio { get; set; } = 0;
    }
}
