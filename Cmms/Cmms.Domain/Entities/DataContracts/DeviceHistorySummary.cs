﻿using System;

namespace Cmms.Domain.Entities.DataContracts
{
    public class DeviceHistorySummary
    {
        public string MaSophieuKiemTra { get; set; }
        public string ChiNhanh { get; set; }
        public string LoaiKiemTra { get; set; }
        public string TinhTrangPhieu { get; set; }
        //public string NguoiGiamSat { get; set; }
        //public string NguoiThucHien { get; set; }
        public string TinhTrangSauKiemTra { get; set; }
        public string KetQuaKiemTra { get; set; }
        public DateTime? NgayBatDau { get; set; }
        public DateTime? NgayHoanThanh { get; set; }
        public string Serial { get; set; }
    }
}
