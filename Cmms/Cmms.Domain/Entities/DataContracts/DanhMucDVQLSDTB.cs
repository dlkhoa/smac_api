﻿namespace Cmms.Domain.Entities.DataContracts
{
    public class DanhMucDVQLSDTB
    {
        public string MaDVQLSD { get; set; }
        public string TenDVQLSD { get; set; }
        public string DVQLSDCha { get; set; }
    }
}
