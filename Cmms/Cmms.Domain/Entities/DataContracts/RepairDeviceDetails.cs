﻿using System;
using System.Collections.Generic;

namespace Cmms.Domain.Entities.DataContracts
{
    public class RepairDeviceDetails
    {
        public string Id { get; set; }
        public string Barcode { get; set; }
        public string DeviceName { get; set; }
        public string Status { get; set; }
        public string PriorityStr { get; set; }
        public bool Priority
        {
            get
            {
                return PriorityStr == "+";
            }
            set
            {
                PriorityStr = value ? "+" : "-";
            }
        }
        public string RepairedSuccessStr { get; set; }
        public bool RepairedSuccess
        {
            get
            {
                return RepairedSuccessStr == "+";
            }
            set
            {
                RepairedSuccessStr = value ? "+" : "-";
            }
        }
        public string SerialAfter { get; set; }
        public string TVT { get; set; }
        public string DVT { get; set; }
        public string Notes { get; set; }
        public IEnumerable<string> Attachments;
        public DateTime BrokenDate { get; set; }
        public DateTime BrokenReceivedDate { get; set; }
    }
}
