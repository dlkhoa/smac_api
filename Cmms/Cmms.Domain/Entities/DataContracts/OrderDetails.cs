﻿using System;

namespace Cmms.Domain.Entities.DataContracts
{
    public class OrderDetails
    {
        public string MaSoPhieu { get; set; }
        public string MoTa { get; set; }
        public string ChiNhanh { get; set; }
        public string MaTinhTrangPhieu { get; set; }
        public string TinhTrangPhieu { get; set; }

        public string Kho { get; set; }
        public string NguoiDeNghi { get; set; }
        public DateTime? NgayDeNghi { get; set; }

        public string NguoiPheDuyet { get; set; }
        public DateTime? NgayPheDuyet { get; set; }

        public string LyDoNhapKho { get; set; }
        public string NhanVeTu { get; set; }
        public string Loai { get; set; }
    }
}
