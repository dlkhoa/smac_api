﻿namespace Cmms.Domain.Entities.DataContracts
{
    public class InventoryDeviceStatisticBPC
    {
        public string ChiNhanh { get; set; }
        public int SoLuongChuaTest { get; set; }
        public int SoLuongChoSuaChua { get; set; }
        public int SoLuongNFFChuaGiao { get; set; }
        public int SoLuongTotChuaGiao { get; set; }
        public int SoLuongKiemTraSSCChuaGiao { get; set; }
    }
}
