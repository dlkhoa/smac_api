﻿namespace Cmms.Domain.Entities.DataContracts
{
    public class InventoryDeviceStatisticBPCByLHD : InventoryDeviceStatisticBPC
    {
        public string LoaiHD { get; set; }
    }
}
