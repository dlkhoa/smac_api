﻿using System;

namespace Cmms.Domain.Entities.DataContracts
{
    public class RepairTimeReportItem
    {
        public string Branch { get; set; }
        public decimal? Receiving { get; set; }
        public decimal? TestingBefore { get; set; }
        public decimal? Waiting { get; set; }
        public decimal? Repairing { get; set; }
        public decimal? TestingAfter { get; set; }
        public decimal? Deliverying { get; set; }
        public decimal? AllTime { get; set; }
    }
}
