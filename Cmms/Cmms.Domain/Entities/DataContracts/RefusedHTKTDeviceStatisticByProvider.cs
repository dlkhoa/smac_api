﻿namespace Cmms.Domain.Entities.DataContracts
{
    public class RefusedHTKTDeviceStatisticByProvider
    {
        public string ChiNhanh { get; set; }
        public string NhaCungCap { get; set; }
        public int Tong { get; set; }
    }
}
