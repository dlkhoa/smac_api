﻿namespace Cmms.Domain.Entities.DataContracts
{
    public class RerepairDeliveryDeviceStatisticByDTSC
    {
        public string ChiNhanh { get; set; }
        public string DoiTuongTB { get; set; }
        public string NhaCungCap { get; set; }
        public string DoiTac { get; set; }
        public int Tong { get; set; }
    }
}
