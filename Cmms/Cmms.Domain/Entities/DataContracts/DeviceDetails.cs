﻿using System;

namespace Cmms.Domain.Entities.DataContracts
{
    public class DeviceDetails
    {
        public string Serial { get; set; }
        public string PartNumber { get; set; }
        public string TenThietBi { get; set; }
        public string LoaiThietBi { get; set; }
        public string NhaCungCap { get; set; }
        public string DoiTuongThietBi { get; set; }
        public string TrangThai { get; set; }
        public string ChiNhanh { get; set; }
        public string ThietBiMoi { get; set; }
        public string DaiVienThong { get; set; }
        public string GiaoTraDaiVienThong { get; set; }
        public string DVQLSD { get; set; }
        public string TinhTrangNhan { get; set; }
        public string GhiChu { get; set; }
        public string NhaSX { get; set; }
        public DateTime? NgayTiepNhan { get; set; }
        public string ControlNumber { get; set; }
        public string SoHopDong { get; set; }
        public string KetQuaKtraTruocSuaChua { get; set; }
        public string TrangThaiTruocSuaChua { get; set; }
        public string DoiTac { get; set; }
        public DateTime? NgayBanGiaoDoiTac { get; set; }
        public string NoiDungSuaChua { get; set; }
        public string SerialMoiSauSuaChua { get; set; }
        public DateTime? NgayDoiTacBanGiao { get; set; }
        public string KiemTraSauSuaChua { get; set; }
        public string TinhTrangSauSuaChua { get; set; }
        public DateTime? NgayXuatKhoChoDVQLSD { get; set; }
        public int SoLanHong { get; set; }

    }
}
