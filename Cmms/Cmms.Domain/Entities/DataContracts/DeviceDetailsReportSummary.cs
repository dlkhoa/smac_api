﻿namespace Cmms.Domain.Entities.DataContracts
{
    public class DeviceDetailsReportSummary
    {
        public string Serial { get; set; }
        public string TenThietBi { get; set; }
        public string TinhTrang { get; set; }
    }
}
