﻿namespace Cmms.Domain.Entities.DataContracts
{
    public class DeliveryDeviceStatisticByProvider
    {
        public string ChiNhanh { get; set; }
        public string DoiTuongTB { get; set; }
        public string NhaCungCap { get; set; }
        public int Tong { get; set; }
    }
}
