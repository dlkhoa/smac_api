﻿using System;

namespace Cmms.Domain.Entities.DataContracts
{
    public class RepairKPIReportItem
    {
        public string Branch { get; set; }
        public decimal? SuccessRatio { get; set; }
        public decimal? SLA { get; set; }
        public decimal? SLA1 { get; set; }
        public decimal? SLA2 { get; set; }
        public decimal? SLA3 { get; set; }
        public decimal? RebrokenRatio { get; set; }
        public int? RebrokenQty { get; set; }
    }
}
