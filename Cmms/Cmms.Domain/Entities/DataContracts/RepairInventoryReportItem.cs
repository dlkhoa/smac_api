﻿using System;

namespace Cmms.Domain.Entities.DataContracts
{
    public class RepairInventoryReportItem
    {
        public string Branch { get; set; }
        public int? BeforeRepairing { get; set; }
        public int? InRepairing { get; set; }
        public int? AfterRepairing { get; set; }
        public int? InTTDKSCStock { get; set; }
        public int? GoodToDelivery { get; set; }
    }
}
