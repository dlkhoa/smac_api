﻿using System;

namespace Cmms.Domain.Entities.DataContracts
{
    public class RequisitionDetails
    {
        public string MaSophieu { get; set; }
        public string MoTa { get; set; } //req_desc
        public string ChiNhanh { get; set; }//REQ_ORG
        public string TinhTrangPhieu { get; set; }//r5o7.o7get_desc('VI','UCOD', req_status,'RQST', '')
        public string MaTinhTrangPhieu { get; set; }

        public string BenGiao { get; set; }//REQ_FROMCODE
        public string NguoiGiao { get; set; }//req_udfchar02
        public string Loai { get; set; } //REQ_CLASS

        public string BenNhan { get; set; }//req_udfchar01
        public string Kho { get; set; }//REQ_TOCODE
        public string NguoiNhan { get; set; }//REQ_ORIGIN
        public DateTime? NgayTiepNhan { get; set; } //REQ_UDFDATE01

        //public decimal? TongGiaTri { get; set; }
        public string NoiDungBanGiao { get; set; }//REQ_UDFCHAR07

        public DateTime? NgayGuiYeuCau { get; set; }//REQ_UDFDATE02
        public DateTime? NgayHoanThanhPhanLoai { get; set; }//REQ_UDFDATE04
    }
}
