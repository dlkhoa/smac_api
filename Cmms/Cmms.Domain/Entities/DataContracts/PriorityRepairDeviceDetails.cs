﻿using System;
using System.Collections.Generic;

namespace Cmms.Domain.Entities.DataContracts
{
    public class PriorityRepairDeviceDetails
    {
        public string Id { get; set; }
        public string Barcode { get; set; }
        public string DeviceName { get; set; }
        public DateTime BrokenDate { get; set; }
        public string Status { get; set; }
        public int NbBrokenDays { get; set; }

    }
}