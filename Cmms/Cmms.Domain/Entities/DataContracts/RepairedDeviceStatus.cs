﻿using System;

namespace Cmms.Domain.Entities.DataContracts
{
    public class RepairedDeviceStatus
    {
        public string Barcode { get; set; }
        public string SerialBefore { get; set; }
        public string SerialAfter { get; set; }
        public string RepairedSuccessStr { get; set; }
        public bool RepairedSuccess
        {
            get
            {
                return RepairedSuccessStr == "+";
            }
            set
            {
                RepairedSuccessStr = value ? "+" : "-";
            }
        }
    }
}
