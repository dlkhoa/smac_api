﻿namespace Cmms.Domain.Entities.DataContracts
{
    public class CodeName
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
