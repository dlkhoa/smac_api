﻿namespace Cmms.Domain.Entities.DataContracts
{
    public class DeviceStatistic
    {
        public string Branch { get; set; }
        public string DVQLSD { get; set; }
        public int Total { get; set; }
    }
}
