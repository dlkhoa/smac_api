﻿using System;

namespace Cmms.Domain.Entities.DataContracts
{
    public class WorkingDeviceDetails
    {
        public string Serial { get; set; }
        public string DeviceName { get; set; }
        public string PickCode { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public DateTime? WorkingDate { get; set; }
        public string CenterName { get; set; }
        public DateTime? ReBrokenDate { get; set; }
        public string StationCode { get; set; }
        public string SerialScc { get; set; }
    }
}
