﻿namespace Cmms.Domain.Entities.DataContracts
{
    public class RepairDeviceSummarizeItem
    {
        public string Name { get; set; }
        public int NbDevices { get; set; }
        public int SummarizeType { get; set; }

        public RepairDeviceSummarizeItem()
        {
            Name = string.Empty;
        }
    }
}
