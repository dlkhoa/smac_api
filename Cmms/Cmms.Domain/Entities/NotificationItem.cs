﻿namespace Cmms.Domain.Entities
{
    public class NotificationItem
    {
        public string PrimaryId { get; set; }
        public string EntityType { get; set; }
        public string FromStatus { get; set; }
        public string ToStatus { get; set; }
        public string IsNotified { get; set; }
    }
}
