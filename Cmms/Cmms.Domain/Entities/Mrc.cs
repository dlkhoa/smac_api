﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmms.Domain.Entities
{
    public class Mrc
    {
        public string MrcCode { get; set; }
        public string Description { get; set; }

        public ICollection<DepartmentSecurity> DepartmentSecurities { get; set; }

        public Mrc()
        {
            DepartmentSecurities = new List<DepartmentSecurity>();
        }
    }
}
