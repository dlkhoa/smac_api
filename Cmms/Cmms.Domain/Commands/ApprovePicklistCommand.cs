﻿namespace Cmms.Domain.Commands
{
    public class ApprovePicklistCommand
    {
        public string Username { get; set; }
        public string PicklistCode { get; set; }
        public string NewStatus { get; set; }
    }
}
