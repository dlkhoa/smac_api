﻿using System;
using System.Collections.Generic;

namespace Cmms.Domain.Commands
{
    public class DeliveryDevicesToTVTCommand
    {
        public string Username { get; set; }
        public DateTime RepairedDeliveredDate { get; set; }
        public string Description { get; set; }
        public string Receiver { get; set; }
        public string ReceiverTitle { get; set; }
        public string Deliverer { get; set; }
        public string DelivererTitle { get; set; }
        public List<string> Ids { get; set; }
    }
}
