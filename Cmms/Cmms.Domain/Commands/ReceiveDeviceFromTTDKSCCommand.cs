﻿using System;

namespace Cmms.Domain.Commands
{
    public class ReceiveDeviceFromTTDKSCCommand
    {
        public string Username { get; set; }
        public string Id { get; set; }
        public string SerialAfter { get; set; }
        public DateTime RepairedReceivedDate { get; set; }
        public bool RepairedSuccess { get; set; }
        public bool ToStock { get; set; }
    }
}
