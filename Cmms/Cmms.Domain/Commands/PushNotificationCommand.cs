﻿namespace Cmms.Domain.Commands
{
    public class PushNotificationCommand
    {
        public string Message { get; set; }
        public string OrderCode { get; set; }
        public string OrderType { get; set; }
        public string DeviceId { get; set; }
        public string FromStatus { get; set; }
        public string ToStatus { get; set; }
    }
}
