﻿namespace Cmms.Domain.Commands
{
    public class RegisterNotificationCommand
    {
        public string DeviceId { get; set; }
        public string Username { get; set; }
        public byte OSType { get; set; }
    }
}
