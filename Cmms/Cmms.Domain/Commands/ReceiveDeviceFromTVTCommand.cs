﻿using System;

namespace Cmms.Domain.Commands
{
    public class ReceiveDeviceFromTVTCommand
    {
        public string Username { get; set; }
        public string Id { get; set; }
        public DateTime BrokenReceivedDate { get; set; }
    }
}
