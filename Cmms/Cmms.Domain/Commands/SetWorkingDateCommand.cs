﻿using System;
using System.Collections.Generic;

namespace Cmms.Domain.Commands
{
    public class SetWorkingDateCommand
    {
        public string Serial { get; set; }
        public string PickCode { get; set; }
        public DateTime WorkingDate { get; set; }
        public string StationCode { get; set; }
        public DateTime? ReBrokenDate { get; set; }
    }
}
