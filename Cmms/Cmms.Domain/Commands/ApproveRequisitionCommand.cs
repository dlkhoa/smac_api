﻿namespace Cmms.Domain.Commands
{
    public class ApproveRequisitionCommand
    {
        public string Username { get; set; }
        public string AuthUser { get; set; }
        public string RequisitionCode { get; set; }
        public string NewStatus { get; set; }
    }
}
