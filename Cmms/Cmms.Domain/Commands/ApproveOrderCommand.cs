﻿namespace Cmms.Domain.Commands
{
    public class ApproveOrderCommand
    {
        public string Username { get; set; }
        public string OrderCode { get; set; }
        public string NewStatus { get; set; }
    }
}
