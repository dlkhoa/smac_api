﻿using MultipartDataMediaFormatter.Infrastructure;
using System;
using System.Collections.Generic;

namespace Cmms.Domain.Commands
{
    public class AddRepairDeviceCommand
    {
        public string Username { get; set; }
        public string Barcode { get; set; }
        public string DeviceName { get; set; }
        public string TVT { get; set; }
        public string DVT { get; set; }
        public string Status { get; set; }
        public DateTime BrokenDate { get; set; }
        public DateTime BrokenReceivedDate { get; set; }
        public bool Priority { get; set; }
        public bool SystemBarcode { get; set; }
        public string Notes { get; set; }
        public List<HttpFile> Attachments { get; set; }
    }
}
