﻿namespace Cmms.Domain.Queries
{
    public class DeviceQuery : DataQueryBase
    {
        public string Serial { get; set; }
        public string Branch { get; set; }
    }
}
