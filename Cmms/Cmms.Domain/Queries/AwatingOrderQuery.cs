﻿namespace Cmms.Domain.Queries
{
    public class AwatingOrderQuery : OrderQuery
    {
        public string Type { get; set; }
    }
}
