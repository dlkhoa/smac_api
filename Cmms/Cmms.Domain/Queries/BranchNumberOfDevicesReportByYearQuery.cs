﻿namespace Cmms.Domain.Queries
{
    public class BranchNumberOfDevicesReportByYearQuery
    {
        public string Chinhanh { get; set; }
        public string DVQLSD { get; set; }
        public int Nam { get; set; }
    }
}
