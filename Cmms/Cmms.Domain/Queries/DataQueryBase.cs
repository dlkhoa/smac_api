﻿namespace Cmms.Domain.Queries
{
    public class DataQueryBase
    {
        public int Page { get; set; }

        public int RecordsPerPage { get; set; }

        public string Username { get; set; }

        public DataQueryBase()
        {
            Page = 1;
            RecordsPerPage = 100;
        }
    }
}
