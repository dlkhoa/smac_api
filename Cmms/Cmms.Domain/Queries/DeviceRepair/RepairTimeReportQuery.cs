﻿using System;
using System.Collections.Generic;

namespace Cmms.Domain.Queries.DeviceRepair
{
    public class RepairTimeReportQuery : DataQueryBase
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}
