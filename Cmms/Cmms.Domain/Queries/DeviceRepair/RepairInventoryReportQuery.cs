﻿using System;
using System.Collections.Generic;

namespace Cmms.Domain.Queries.DeviceRepair
{
    public class RepairInventoryReportQuery : DataQueryBase
    {
        public int NbDays { get; set; }
        public DateTime ToDate { get; set; }
    }
}
