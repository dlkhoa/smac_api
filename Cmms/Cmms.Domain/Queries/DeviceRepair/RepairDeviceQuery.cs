﻿namespace Cmms.Domain.Queries.DeviceRepair
{
    public class RepairDeviceQuery : DataQueryBase
    {
        public string Id { get; set; }
        public string Barcode { get; set; }
        public string Serial { get; set; }
        public string Branch { get; set; }
    }
}
