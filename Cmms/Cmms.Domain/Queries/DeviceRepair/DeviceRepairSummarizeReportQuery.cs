﻿using System;
using System.Collections.Generic;

namespace Cmms.Domain.Queries.DeviceRepair
{
    public class DeviceRepairSummarizeReportQuery : DataQueryBase
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string DeviceName { get; set; }
        public int ListType { get; set; }

        public string UserGroup { get; set; }
        public string TVT { get; set; }
        public string DVT { get; set; }
        public List<string> Organizations { get; set; }
    }
}
