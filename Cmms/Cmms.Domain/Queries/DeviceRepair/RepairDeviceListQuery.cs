﻿using System.Collections.Generic;

namespace Cmms.Domain.Queries.DeviceRepair
{
    public class RepairDeviceListQuery : DataQueryBase
    {
        public string TextToSearch { get; set; }
        public string UserGroup { get; set; }
        public string TVT { get; set; }
        public string DVT { get; set; }
        public List<string> Statuses { get; set; }
    }
}
