﻿namespace Cmms.Domain.Queries.DeviceRepair
{
    public class AvailableDeviceListQuery : DataQueryBase
    {
        public string CodepartNumber { get; set; }
        public string DeviceName { get; set; }
    }
}
