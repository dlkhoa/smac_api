﻿using System.Collections.Generic;

namespace Cmms.Domain.Queries.DeviceRepair
{
    public class PriorityRepairDeviceListQuery : DataQueryBase
    {
        public string TextToSearch { get; set; }
        public string UserGroup { get; set; }
        public string TVT { get; set; }
        public string DVT { get; set; }
        public List<string> Organizations { get; set; }
    }
}
