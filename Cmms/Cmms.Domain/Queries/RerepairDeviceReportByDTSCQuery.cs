﻿namespace Cmms.Domain.Queries
{
    public class RerepairDeviceReportByDTSCQuery : DeviceReportQuery
    {
        public string ChiNhanh { get; set; }
        public string DoiTuongTB { get; set; }
        public string NhaCungCap { get; set; }
        public string DoiTac { get; set; }
    }
}
