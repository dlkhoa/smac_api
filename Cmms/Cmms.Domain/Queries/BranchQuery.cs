﻿namespace Cmms.Domain.Queries
{
    public class BranchQuery : DataQueryBase
    {
        public string Branch { get; set; }
    }
}
