﻿namespace Cmms.Domain.Queries
{
    public class InventoryDeviceReportByDVQLSDLoaiTBQuery : DeviceReportQuery
    {
        public string ChiNhanh { get; set; }
        public string DVQLSD { get; set; }
        public string LoaiTB { get; set; }
    }
}
