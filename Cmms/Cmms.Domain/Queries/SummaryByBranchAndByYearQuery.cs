﻿namespace Cmms.Domain.Queries
{
    public class SummaryByBranchAndByYearQuery : DataQueryBase
    {
        public string Branch { get; set; }
        public int Year { get; set; }
    }
}
