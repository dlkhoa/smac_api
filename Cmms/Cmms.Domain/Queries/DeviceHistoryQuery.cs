﻿namespace Cmms.Domain.Queries
{
    public class DeviceHistoryQuery : DataQueryBase
    {
        public string Serial { get; set; }
        public string Branch { get; set; }
    }
}
