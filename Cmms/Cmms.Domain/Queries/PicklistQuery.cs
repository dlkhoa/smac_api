﻿namespace Cmms.Domain.Queries
{
    public class PicklistQuery : DataQueryBase
    {
        public string Branch { get; set; }
        public string PermissionFunction { get; set; }
        public string Status { get; set; }
    }
}
