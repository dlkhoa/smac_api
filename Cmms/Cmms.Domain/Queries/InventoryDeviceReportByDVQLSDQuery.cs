﻿namespace Cmms.Domain.Queries
{
    public class InventoryDeviceReportByDVQLSDQuery : DeviceReportQuery
    {
        public string ChiNhanh { get; set; }
        public string DVQLSD { get; set; }
        public string LoaiBaoCao { get; set; }
    }
}
