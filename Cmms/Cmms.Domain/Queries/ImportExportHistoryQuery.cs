﻿namespace Cmms.Domain.Queries
{
    public class ImportExportHistoryQuery :DataQueryBase
    {
        public string Serial { get; set; }
        public string Branch { get; set; }
    }
}
