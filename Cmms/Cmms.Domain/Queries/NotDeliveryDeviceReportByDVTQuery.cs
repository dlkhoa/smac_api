﻿namespace Cmms.Domain.Queries
{
    public class NotDeliveryDeviceReportByDVTQuery : DeviceReportQuery
    {
        public string ChiNhanh { get; set; }
        public string DVQLSD { get; set; }
        public string DaiVT { get; set; }
    }
}
