﻿namespace Cmms.Domain.Queries
{
    public class OverRepairTimeDeviceReportByDTSCQuery : DeviceReportQuery
    {
        public string ChiNhanh { get; set; }
        public string DoiTac { get; set; }
    }
}
