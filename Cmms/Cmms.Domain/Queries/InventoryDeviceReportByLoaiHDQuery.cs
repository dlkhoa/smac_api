﻿namespace Cmms.Domain.Queries
{
    public class InventoryDeviceReportByLoaiHDQuery : DeviceReportQuery
    {
        public string ChiNhanh { get; set; }
        public string LoaiHD { get; set; }
        public string LoaiBaoCao { get; set; }
    }
}
