﻿namespace Cmms.Domain.Queries
{
    public class DeliveryDeviceReportByProviderQuery : DeviceReportQuery
    {
        public string ChiNhanh { get; set; }
        public string DoiTuongTb { get; set; }
        public string NhaCungCap { get; set; }
    }
}
