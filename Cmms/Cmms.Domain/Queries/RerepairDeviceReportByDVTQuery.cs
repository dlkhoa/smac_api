﻿namespace Cmms.Domain.Queries
{
    public class RerepairDeviceReportByDVTQuery : DeviceReportQuery
    {
        public string ChiNhanh { get; set; }
        public string DVQLSD { get; set; }
        public string DaiVT { get; set; }
    }
}
