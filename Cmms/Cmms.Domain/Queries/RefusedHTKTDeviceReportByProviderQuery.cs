﻿namespace Cmms.Domain.Queries
{
    public class RefusedHTKTDeviceReportByProviderQuery : DeviceReportQuery
    {
        public string ChiNhanh { get; set; }
        public string NhaCungCap { get; set; }
    }
}
