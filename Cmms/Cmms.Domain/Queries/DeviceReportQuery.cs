﻿using System;
using System.Collections.Generic;

namespace Cmms.Domain.Queries
{
    public class DeviceReportQuery : DataQueryBase
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<string> Organizations { get; set; }
    }
}
