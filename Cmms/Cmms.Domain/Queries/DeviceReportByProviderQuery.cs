﻿namespace Cmms.Domain.Queries
{
    public class DeviceReportByProviderQuery : DeviceReportQuery
    {
        public string Branch { get; set; }
        public string Provider { get; set; }
    }
}
