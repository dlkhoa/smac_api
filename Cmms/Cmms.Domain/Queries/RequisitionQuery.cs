﻿namespace Cmms.Domain.Queries
{
    public class RequisitionQuery : DataQueryBase
    {
        public string RequisitionClass { get; set; }
        public string PermissionFunction { get; set; }
        public string Branch { get; set; }
        public string Status { get; set; }
    }
}
