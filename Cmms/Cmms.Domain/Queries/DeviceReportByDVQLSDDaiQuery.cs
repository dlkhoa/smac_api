﻿namespace Cmms.Domain.Queries
{
    public class DeviceReportByDVQLSDDaiQuery : DeviceReportQuery
    {
        public string Branch { get; set; }
        public string DVQLSD { get; set; }
        public string DaiVT { get; set; }
    }
}
