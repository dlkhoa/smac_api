﻿namespace Cmms.Domain.Queries
{
    public class OrderQuery : DataQueryBase
    {
        public string Branch { get; set; }
        public string PermissionFunction { get; set; }
    }
}
