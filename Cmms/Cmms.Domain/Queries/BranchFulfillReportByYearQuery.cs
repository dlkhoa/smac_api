﻿namespace Cmms.Domain.Queries
{
    public class BranchFulfillReportByYearQuery
    {
        public string Chinhanh { get; set; }
        public int Nam { get; set; }
    }
}
