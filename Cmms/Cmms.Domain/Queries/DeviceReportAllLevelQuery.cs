﻿namespace Cmms.Domain.Queries
{
    public class DeviceReportAllLevelQuery : DeviceReportQuery
    {
        public string ChiNhanh { get; set; }
        public string DVQLSD { get; set; }
        public string DaiVT { get; set; }
        public string DoiTuongTB { get; set; }
        public string NhaCungCap { get; set; }
        public string Thang { get; set; }
        public int Nam { get; set; }
        public string LoaiTB { get; set; }
        public string DoiTac { get; set; }
        public int LoaiBaoCao { get; set; }
    }
}
