﻿namespace Cmms.Domain.Queries
{
    public class DeliverySLADeviceReportByMonthQuery : DeviceReportQuery
    {
        public string ChiNhanh { get; set; }
        public string Thang { get; set; }
        public int YearValue { get; set; }
        public int MonthValue { get; set; }
    }
}
