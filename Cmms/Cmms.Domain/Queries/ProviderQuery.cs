﻿namespace Cmms.Domain.Queries
{
    public class ProviderQuery : DataQueryBase
    {
        public string ProviderCode { get; set; }
    }
}
