﻿namespace Cmms.Domain.Queries
{
    public class CompanyQuery : DataQueryBase
    {
        public  string Code { get; set; }
    }
}
