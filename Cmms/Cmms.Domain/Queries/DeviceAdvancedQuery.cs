﻿namespace Cmms.Domain.Queries
{
    public class DeviceAdvancedQuery : DataQueryBase
    {
        public string Serial { get; set; }
        public string Name { get; set; }
        public string CodePartNumber { get; set; }
        public string Status { get; set; }
        public string Provider { get; set; }
        public string Object { get; set; }
        public string Classify { get; set; }
        public string Branch { get; set; }
        public string Station { get; set; }
        public string InputTestResult { get; set; }
        public string OutputTestResult { get; set; }
        public string RepairPartner { get; set; }
        public string DVQLSD { get; set; }
    }
}
