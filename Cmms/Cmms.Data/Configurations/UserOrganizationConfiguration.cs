﻿using System.Data.Entity.ModelConfiguration;
using Cmms.Domain.Entities;

namespace Cmms.Data.Configurations
{
    public class UserOrganizationConfiguration:EntityTypeConfiguration<UserOrganization>
    {
        public UserOrganizationConfiguration()
        {
            ConfigureProperties();
            ConfigureTable();
        }

        private void ConfigureProperties()
        {
            Property(c => c.Username)
                .HasColumnName("UOG_USER")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(30);

            Property(c => c.Organization)
                .HasColumnName("UOG_ORG")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(15);
        }

        private void ConfigureTable()
        {
            ToTable("R5USERORGANIZATION");
            HasKey(o => new { o.Username, o.Organization });
        }
    }
}
