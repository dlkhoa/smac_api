﻿using Cmms.Domain.Entities;
using System;
using System.Data.Entity.ModelConfiguration;

namespace Cmms.Data.Configurations
{
    public class OrderConfiguration : EntityTypeConfiguration<Order>
    {
        public OrderConfiguration()
        {
            ConfigureProperties();
            ConfigureTable();
        }

        private void ConfigureProperties()
        {
            Property(o => o.OrderCode)
                .HasColumnName("ORD_CODE")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(30);

            Property(o => o.CreatedUser)
                .HasColumnName("ORD_ORIGIN")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(15);

            Property(o => o.Org)
                .HasColumnName("ORD_ORG")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(15);
        }

        private void ConfigureTable()
        {
            ToTable("R5ORDERS");
            HasKey(o => new { o.OrderCode, o.Org });
        }
    }
}
