﻿using Cmms.Domain.Entities.Views;
using System.Data.Entity.ModelConfiguration;

namespace Cmms.Data.Configurations
{
    public class ImportExportHistoryViewConfiguration : EntityTypeConfiguration<ImportExportHistoryView>
    {
        public ImportExportHistoryViewConfiguration()
        {
            ConfigureProperties();
            ConfigureView();
        }

        private void ConfigureProperties()
        {
            Property(v => v.Serial)
                .HasColumnName("SERIAL")
                .HasColumnType("VARCHAR2");

            Property(v => v.Org)
                .HasColumnName("ORG")
                .HasColumnType("VARCHAR2");

            Property(v => v.LoaiPhieu)
                .HasColumnName("LOAIPHIEU")
                .HasColumnType("VARCHAR2");

            Property(v => v.Ngay)
                .HasColumnName("NGAY")
                .HasColumnType("DATE");

            Property(v => v.TiepNhan)
                .HasColumnName("TIEPNHAN")
                .HasColumnType("VARCHAR2");

            Property(v => v.DeNghi)
                .HasColumnName("DENGHI")
                .HasColumnType("VARCHAR2");

            Property(v => v.SoGiaoDich)
                .HasColumnName("SOGIAODICH")
                .HasColumnType("VARCHAR2");

            Property(v => v.TinhTrangPhieu)
                .HasColumnName("TINHTRANGPHIEU")
                .HasColumnType("VARCHAR2");

            Property(v => v.Supplier)
                .HasColumnName("SUPPLIER")
                .HasColumnType("VARCHAR2");

            Property(v => v.Lo)
                .HasColumnName("LO")
                .HasColumnType("VARCHAR2");
        }

        private void ConfigureView()
        {
            ToTable("U5LICHSUNHAPXUAT");
            HasKey(v => new { v.Serial, v.Org, v.DeNghi, v.SoGiaoDich, v.Supplier, v.LoaiPhieu });
        }
    }
}
