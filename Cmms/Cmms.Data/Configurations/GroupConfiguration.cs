﻿using Cmms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmms.Data.Configurations
{
    public class GroupConfiguration : EntityTypeConfiguration<Group>
    {
        public GroupConfiguration()
        {
            ConfigureProperties();
            ConfigureTable();
        }

        private void ConfigureProperties()
        {
            Property(g => g.Description)
                .HasColumnName("UGR_DESC")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(80);

            Property(g => g.GroupCode)
                .HasColumnName("UGR_CODE")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(12);

            Property(g => g.SessionTimeout)
                .HasColumnName("UGR_SESSIONTIMEOUT")
                .HasColumnType("NUMBER");

        }

        private void ConfigureTable()
        {
            ToTable("R5GROUPS");
            HasKey(g => g.GroupCode);
        }
    }
}
