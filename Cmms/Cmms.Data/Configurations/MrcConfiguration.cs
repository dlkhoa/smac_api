﻿using Cmms.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Cmms.Data.Configurations
{
    public class MrcConfiguration : EntityTypeConfiguration<Mrc>
    {
        public MrcConfiguration()
        {
            ConfigureProperties();
            ConfigureTable();
        }

        private void ConfigureProperties()
        {
            Property(m => m.MrcCode)
                .HasColumnName("MRC_CODE")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(15);

            Property(m => m.Description)
                .HasColumnName("MRC_DESC")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(80);
        }

        private void ConfigureTable()
        {
            ToTable("R5MRCS");
            HasKey(m => m.MrcCode);
        }
    }
}
