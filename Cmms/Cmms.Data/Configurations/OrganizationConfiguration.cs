﻿using Cmms.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Cmms.Data.Configurations
{
    public class OrganizationConfiguration : EntityTypeConfiguration<Organization>
    {
        public OrganizationConfiguration()
        {
            ConfigureProperties();
            ConfigureTable();
        }

        private void ConfigureTable()
        {
            ToTable("R5ORGANIZATION");
            HasKey(o => o.OrgCode);
        }

        private void ConfigureProperties()
        {
            Property(o => o.OrgCode)
                .HasColumnName("ORG_CODE")
                .HasColumnType("VARCHAR2");

            Property(o => o.Description)
                .HasColumnName("ORG_DESC")
                .HasColumnType("VARCHAR2");
        }
    }
}
