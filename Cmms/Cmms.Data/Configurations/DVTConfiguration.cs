﻿using System.Data.Entity.ModelConfiguration;
using Cmms.Domain.Entities;

namespace Cmms.Data.Configurations
{
    public class DVTConfiguration : EntityTypeConfiguration<DVT>
    {
        public DVTConfiguration()
        {
            ConfigureProperties();
            ConfigureTable();
        }

        private void ConfigureProperties()
        {
            Property(c => c.Code)
                .HasColumnName("DVT_CODE")
                .HasColumnType("VARCHAR2");

            Property(c => c.Name)
                .HasColumnName("DVT_DESC")
                .HasColumnType("VARCHAR2");

            Property(c => c.Organization)
                .HasColumnName("DVT_ORG")
                .HasColumnType("VARCHAR2");

        }

        private void ConfigureTable()
        {
            ToTable("U5DAIVT");
            HasKey(o => new { o.Code, o.Organization });
        }
    }
}
