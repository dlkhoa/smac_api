﻿using Cmms.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Cmms.Data.Configurations
{
    public class CompanyConfiguration : EntityTypeConfiguration<Company>
    {
        public CompanyConfiguration()
        {
            ConfigureProperties();
            ConfigureTable();
        }

        private void ConfigureProperties()
        {
            Property(c => c.Code)
                .HasColumnName("COM_CODE")
                .HasColumnType("VARCHAR2");

            Property(c => c.Description)
                .HasColumnName("COM_DESC")
                .HasColumnType("VARCHAR2");

            Property(c => c.Parent)
                .HasColumnName("COM_PARENT")
                .HasColumnType("VARCHAR2");

            Property(c => c.UdfChkBox01)
                .HasColumnName("COM_UDFCHKBOX01")
                .HasColumnType("VARCHAR2");

            Property(c => c.UdfChkBox02)
                .HasColumnName("COM_UDFCHKBOX02")
                .HasColumnType("VARCHAR2");
        }

        private void ConfigureTable()
        {
            ToTable("R5COMPANIES");
            HasKey(c => c.Code);
        }
    }
}
