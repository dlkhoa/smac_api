﻿using System.Data.Entity.ModelConfiguration;
using Cmms.Domain.Entities;

namespace Cmms.Data.Configurations
{
    public class TVTConfiguration : EntityTypeConfiguration<TVT>
    {
        public TVTConfiguration()
        {
            ConfigureProperties();
            ConfigureTable();
        }

        private void ConfigureProperties()
        {
            Property(c => c.Code)
                .HasColumnName("TVT_CODE")
                .HasColumnType("VARCHAR2");

            Property(c => c.Name)
                .HasColumnName("TVT_DESC")
                .HasColumnType("VARCHAR2");

            Property(c => c.Organization)
                .HasColumnName("TVT_ORG")
                .HasColumnType("VARCHAR2");

            Property(c => c.DVT)
                .HasColumnName("TVT_DVT")
                .HasColumnType("VARCHAR2");

            Property(c => c.DVTOrganization)
                .HasColumnName("TVT_DVT_ORG")
                .HasColumnType("VARCHAR2");
        }

        private void ConfigureTable()
        {
            ToTable("U5TOVT");
            HasKey(o => new { o.Code, o.DVT, o.Organization });
        }
    }
}
