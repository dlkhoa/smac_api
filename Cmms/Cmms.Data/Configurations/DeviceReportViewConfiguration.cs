﻿using Cmms.Domain.Entities.Views;
using System.Data.Entity.ModelConfiguration;

namespace Cmms.Data.Configurations
{
    public class DeviceReportViewConfiguration : EntityTypeConfiguration<DeviceReportView>
    {
        public DeviceReportViewConfiguration()
        {
            ConfigureProperties();
            ConfigureView();
        }

        private void ConfigureProperties()
        {
            Property(d => d.ChiNhanh)
                .HasColumnName("CHINHANH")
                .HasColumnType("VARCHAR2");

            Property(d => d.TenThietBi)
                .HasColumnName("TENTB")
                .HasColumnType("VARCHAR2");

            Property(d => d.Serial)
                .HasColumnName("SERIAL")
                .HasColumnType("VARCHAR2");

            Property(d => d.NhaCungCap)
                .HasColumnName("NHACC")
                .HasColumnType("VARCHAR2");

            Property(d => d.DoiTuongThietBi)
                .HasColumnName("DOITUONGTB")
                .HasColumnType("VARCHAR2");

            Property(d => d.DaiVienThong)
                .HasColumnName("DAIVT")
                .HasColumnType("VARCHAR2");

            Property(d => d.DVQLSD)
                .HasColumnName("DVQLSD1")
                .HasColumnType("VARCHAR2");

            Property(d => d.DVQLSD2)
                .HasColumnName("DVQLSD2")
                .HasColumnType("VARCHAR2");

            Property(d => d.TinhTrang)
                .HasColumnName("TINHTRANG")
                .HasColumnType("VARCHAR2");

            Property(d => d.ReqCode)
                .HasColumnName("REQ_CODE")
                .HasColumnType("VARCHAR2");

            Property(d => d.LoaiTB)
                .HasColumnName("LOAITB")
                .HasColumnType("VARCHAR2");

            Property(d => d.CodeKQSSC)
                .HasColumnName("CODEKQSSC")
                .HasColumnType("VARCHAR2");

            Property(d => d.CodeKQTSC)
                .HasColumnName("CODEKQTSC")
                .HasColumnType("VARCHAR2");

            Property(d => d.DoiTac)
                .HasColumnName("DOITAC")
                .HasColumnType("VARCHAR2");

            Property(d => d.NgayDTBG)
                .HasColumnName("NGAYDTBG")
                .HasColumnType("DATE");

            Property(d => d.TinhTrangBanGiao)
                .HasColumnName("TINHTRANGBANGIAO")
                .HasColumnType("VARCHAR2");

            Property(d => d.CodeDTTB)
                .HasColumnName("CODEDTTB")
                .HasColumnType("VARCHAR2");

            Property(d => d.NgayBanGiaoDVQLSD)
                .HasColumnName("NGAYBGDVQLSD")
                .HasColumnType("DATE");

            Property(d => d.NgayCNNTK)
                .HasColumnName("NGAYCNNTK")
                .HasColumnType("DATE");

            Property(d => d.NgayBGDT)
               .HasColumnName("NGAYBGDT")
               .HasColumnType("DATE");
        }

        private void ConfigureView()
        {
            ToTable("MV_BCCHITIET");
            HasKey(d => new { d.Serial, d.ReqCode });
        }
    }
}
