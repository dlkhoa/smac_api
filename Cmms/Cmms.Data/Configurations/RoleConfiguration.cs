﻿using Cmms.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Cmms.Data.Configurations
{
    public class RoleConfiguration : EntityTypeConfiguration<Role>
    {
        public RoleConfiguration()
        {
            ConfigureProperties();
            ConfigureTable();
        }

        private void ConfigureProperties()
        {
            Property(r => r.RoleCode)
                .HasColumnName("ROL_CODE")
                .HasColumnType("VARCHAR2");

            Property(r => r.Description)
                .HasColumnName("ROL_DESC")
                .HasColumnType("VARCHAR2");

            Property(r => r.GroupCode)
                .HasColumnName("ROL_GROUP")
                .HasColumnType("VARCHAR2");
        }

        private void ConfigureTable()
        {
            ToTable("R5ROLES");
            HasKey(r => r.RoleCode);
        }
    }
}
