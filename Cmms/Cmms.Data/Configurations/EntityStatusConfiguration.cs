﻿using Cmms.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Cmms.Data.Configurations
{
    public class EntityStatusConfiguration : EntityTypeConfiguration<EntityStatus>
    {
        public EntityStatusConfiguration()
        {
            ConfigureProperties();
            ConfigureTable();
        }

        private void ConfigureProperties()
        {
            Property(s => s.Entity)
                .HasColumnName("UCO_ENTITY")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(4);

            Property(s => s.StatusCode)
                .HasColumnName("UCO_CODE")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(8);

            Property(s => s.Description)
                .HasColumnName("UCO_DESC")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(80);

            Property(s => s.NotUsed)
                .HasColumnName("UCO_NOTUSED")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(1);
        }

        private void ConfigureTable()
        {
            ToTable("R5UCODES");
            HasKey(s => new { s.Entity, s.StatusCode });
        }
    }
}
