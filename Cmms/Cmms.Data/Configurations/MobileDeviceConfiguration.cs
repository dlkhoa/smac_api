﻿using System.Data.Entity.ModelConfiguration;
using Cmms.Domain.Entities;

namespace Cmms.Data.Configurations
{
    public class MobileDeviceConfiguration : EntityTypeConfiguration<MobileDevice>
    {
        public MobileDeviceConfiguration()
        {
            ConfigureProperties();
            ConfigureTable();
        }

        private void ConfigureProperties()
        {
            Property(d => d.Username)
                .HasColumnName("DI_USERNAME")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(30);

            Property(d => d.DeviceId)
                .HasColumnName("DI_DEVICEID")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(1000);

            Property(d => d.DeviceOS)
                .HasColumnName("DI_DEVICEOS")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(30);
        }

        private void ConfigureTable()
        {
            ToTable("R5DEVICESINFOR");
            HasKey(d => new { d.Username, d.DeviceId });
        }
    }
}
