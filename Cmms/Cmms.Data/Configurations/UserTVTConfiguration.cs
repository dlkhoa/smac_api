﻿using System.Data.Entity.ModelConfiguration;
using Cmms.Domain.Entities;

namespace Cmms.Data.Configurations
{
    public class UserTVTConfiguration : EntityTypeConfiguration<UserTVT>
    {
        public UserTVTConfiguration()
        {
            ConfigureProperties();
            ConfigureTable();
        }

        private void ConfigureProperties()
        {
            Property(c => c.UserCode)
                .HasColumnName("UT_USER")
                .HasColumnType("VARCHAR2");

            Property(c => c.TVT)
                .HasColumnName("UT_TVT")
                .HasColumnType("VARCHAR2");

            Property(c => c.DVT)
                .HasColumnName("UT_DVT")
                .HasColumnType("VARCHAR2");
        }

        private void ConfigureTable()
        {
            ToTable("U5USERTVT");
            HasKey(o => new { o.UserCode, o.TVT, o.DVT });
        }
    }
}
