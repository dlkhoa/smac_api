﻿using Cmms.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Cmms.Data.Configurations
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            ConfigureProperties();
            ConfigureRelationships();
            ConfigureTable();

        }

        private void ConfigureProperties()
        {

            Property(u => u.GroupCode)
                .HasColumnName("USR_GROUP")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(12);

            Property(u => u.Password)
                .HasColumnName("USR_PASSWORD")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(50);

            Property(u => u.UserCode)
                .HasColumnName("USR_CODE")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(30);

            Property(u => u.Description)
                .HasColumnName("USR_DESC")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(80);

            Property(u => u.DefaultOrg)
                .HasColumnName("USR_DEFAULT_ORG")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(15);
        }

        private void ConfigureTable()
        {
            ToTable("R5USERS");
            HasKey(u => u.UserCode);
        }

        private void ConfigureRelationships()
        {
            this.HasRequired(u => u.Group)
                .WithMany(g => g.Users)
                .HasForeignKey(u => u.GroupCode);
        }
    }
}