﻿using System.Data.Entity.ModelConfiguration;
using Cmms.Domain.Entities;

namespace Cmms.Data.Configurations
{
    public class UserDVTConfiguration : EntityTypeConfiguration<UserDVT>
    {
        public UserDVTConfiguration()
        {
            ConfigureProperties();
            ConfigureTable();
        }

        private void ConfigureProperties()
        {
            Property(c => c.UserCode)
                .HasColumnName("UD_USER")
                .HasColumnType("VARCHAR2");

            Property(c => c.DVT)
                .HasColumnName("UD_DVT")
                .HasColumnType("VARCHAR2");
        }

        private void ConfigureTable()
        {
            ToTable("U5USERDVT");
            HasKey(o => new { o.UserCode, o.DVT });
        }
    }
}
