﻿using System.Data.Entity.ModelConfiguration;
using Cmms.Domain.Entities;

namespace Cmms.Data.Configurations
{
    public class NotificationItemConfiguration : EntityTypeConfiguration<NotificationItem>
    {
        public NotificationItemConfiguration()
        {
            ConfigureProperties();
            ConfigureTable();
        }

        private void ConfigureProperties()
        {
            Property(n => n.PrimaryId)
                .HasColumnName("AVA_PRIMARYID")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(30);

            Property(n => n.EntityType)
                .HasColumnName("AVA_TABLE")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(30);

            Property(n => n.FromStatus)
                .HasColumnName("AVA_FROM")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(30);

            Property(n => n.ToStatus)
                .HasColumnName("AVA_TO")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(30);

            Property(n => n.IsNotified)
                .HasColumnName("NOTIFICATION")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(1);
        }

        private void ConfigureTable()
        {
            ToTable("R5SENDEMAIL");
            HasKey(n => new { n.PrimaryId, n.EntityType, n.FromStatus, n.ToStatus });
        }
    }
}
