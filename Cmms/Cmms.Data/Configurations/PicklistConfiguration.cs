﻿using Cmms.Domain.Entities;
using System;
using System.Data.Entity.ModelConfiguration;

namespace Cmms.Data.Configurations
{
    public class PicklistConfiguration : EntityTypeConfiguration<Picklist>
    {
        public PicklistConfiguration()
        {
            ConfigureProperties();
            ConfigureTable();
        }

        private void ConfigureProperties()
        {
            Property(p => p.Code)
                .HasColumnName("PIC_CODE")
                .HasColumnType("VARCHAR2");

            Property(p => p.Status)
                .HasColumnName("PIC_STATUS")
                .HasColumnType("VARCHAR2");

            Property(p => p.RStatus)
                .HasColumnName("PIC_RSTATUS")
                .HasColumnType("VARCHAR2");

            Property(p => p.Store)
                .HasColumnName("PIC_STORE")
                .HasColumnType("VARCHAR2");

            Property(p => p.CreatedUser)
                .HasColumnName("PIC_ORIGIN")
                .HasColumnType("VARCHAR2");
        }

        private void ConfigureTable()
        {
            ToTable("R5PICKLISTS");
            HasKey(p => p.Code);
        }
    }
}
