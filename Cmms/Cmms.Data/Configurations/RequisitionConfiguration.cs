﻿using Cmms.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Cmms.Data.Configurations
{
    public class RequisitionConfiguration:EntityTypeConfiguration<Requisition>
    {
        public RequisitionConfiguration()
        {
            ConfigureProperties();
            ConfigureTable();
        }

        private void ConfigureProperties()
        {
            Property(r => r.RequisitionCode)
                .HasColumnName("REQ_CODE")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(30);

            Property(r => r.CreatedUser)
                .HasColumnName("REQ_ORIGIN")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(15);
        }

        private void ConfigureTable()
        {
            ToTable("R5REQUISITIONS");
            HasKey(r => r.RequisitionCode);
        }
    }
}
