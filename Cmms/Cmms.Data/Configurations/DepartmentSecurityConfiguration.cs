﻿using Cmms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmms.Data.Configurations
{
    public class DepartmentSecurityConfiguration : EntityTypeConfiguration<DepartmentSecurity>
    {
        public DepartmentSecurityConfiguration()
        {
            ConfigureProperties();
            ConfigureTable();
            ConfigureRelationships();
        }

        private void ConfigureProperties()
        {
            Property(ds => ds.MrcCode)
                .HasColumnName("DSE_MRC")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(15);

            Property(ds => ds.UserCode)
                .HasColumnName("DSE_USER")
                .HasColumnType("VARCHAR2")
                .HasMaxLength(30);

        }

        private void ConfigureTable()
        {
            ToTable("R5DEPARTMENTSECURITY");
            HasKey(ds => new { ds.MrcCode, ds.UserCode });
        }

        private void ConfigureRelationships()
        {
            this.HasRequired(d => d.User)
                .WithMany(u => u.DepartmentSecurities)
                .HasForeignKey(d => d.UserCode);

            this.HasRequired(d => d.Mrc)
                .WithMany(m => m.DepartmentSecurities)
                .HasForeignKey(d => d.MrcCode);
        }
    }
}
