﻿using Cmms.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Cmms.Data.Configurations
{
    public class ProviderConfiguration : EntityTypeConfiguration<Provider>
    {
        public ProviderConfiguration()
        {
            ConfigureProperties();
            ConfigureTable();
        }

        private void ConfigureProperties()
        {
            Property(p => p.MaNhaCungCap)
                .HasColumnName("NCC_CODE")
                .HasColumnType("VARCHAR2");

            Property(p => p.TenNhaCungCap)
                .HasColumnName("NCC_DESC")
                .HasColumnType("VARCHAR2");

            Property(p => p.DiaChi)
                .HasColumnName("NCC_DIACHI")
                .HasColumnType("VARCHAR2");

            Property(p => p.NguoiLienHe)
                .HasColumnName("NCC_NGUOILIENHE")
                .HasColumnType("VARCHAR2");

            Property(p => p.DienThoai)
                .HasColumnName("NCC_DIENTHOAI")
                .HasColumnType("VARCHAR2");

            Property(p => p.Email)
                .HasColumnName("NCC_EMAIL")
                .HasColumnType("VARCHAR2");
        }

        private void ConfigureTable()
        {
            ToTable("U5NHACUNGCAP");
            HasKey(p => p.MaNhaCungCap);
        }
    }
}
