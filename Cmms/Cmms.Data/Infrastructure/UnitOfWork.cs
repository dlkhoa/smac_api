﻿using System;
using System.Linq;
using System.Reflection;

namespace Cmms.Data.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private CmmsDbContext dbContext;
        private bool isDisposed;

        public UnitOfWork(string nameOrConnectionString)
        {
            this.dbContext = dbContext ?? new CmmsDbContext(nameOrConnectionString);
        }

        public TIRepository GetRepository<TIRepository>()
        {
            var instances = from t in Assembly.GetExecutingAssembly().GetTypes()
                            where t.GetInterfaces().Contains(typeof(TIRepository))
                            select Activator.CreateInstance(t, dbContext);

            return (TIRepository)instances.FirstOrDefault();
            //var currentInstance = ServiceLocator.Current.GetInstance<TIRepository>();
            //return (TIRepository)instances.FirstOrDefault(i => i.GetType() == currentInstance.GetType());

        }

        public int SaveChanges()
        {
            dbContext.ChangeTracker.DetectChanges();
            return dbContext.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!isDisposed && disposing)
            {
                DisposeCore();
            }

            isDisposed = true;
        }

        private void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}
