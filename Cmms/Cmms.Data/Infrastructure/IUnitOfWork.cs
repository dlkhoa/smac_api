﻿using System;

namespace Cmms.Data.Infrastructure
{
    public interface IUnitOfWork : IDisposable
    {
        TIRepository GetRepository<TIRepository>();
        int SaveChanges();
    }
}
