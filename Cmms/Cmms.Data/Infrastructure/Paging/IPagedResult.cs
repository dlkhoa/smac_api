﻿using System.Collections.Generic;

namespace Cmms.Data.Infrastructure.Paging
{
    public interface IPagedResult<TDest>
    {
        int Page { get; }

        int RecordsPerPage { get; }

        int TotalRecords { get; }

        int TotalPages { get; }

        IEnumerable<TDest> DataResult { get; }
    }
}
