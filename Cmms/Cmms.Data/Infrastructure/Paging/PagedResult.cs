﻿using System.Collections.Generic;
using System.Linq;

namespace Cmms.Data.Infrastructure.Paging
{
    public class PagedResult<TData> : IPagedResult<TData>
    {
        public int Page { get; private set; }
        public int RecordsPerPage { get; private set; }
        public int TotalPages { get; private set; }
        public int TotalRecords { get; private set; }
        public IEnumerable<TData> DataResult { get; private set; }

        public PagedResult(IQueryable<TData> source, int currentPage, int recordsPerPage)
        {
            if (currentPage < 1)
            {
                currentPage = 1;
            }

            Page = currentPage;
            RecordsPerPage = recordsPerPage;

            TotalRecords = source.Count();
            TotalPages = ((TotalRecords - 1) / recordsPerPage) + 1;

            var sourceResult = source.Skip(recordsPerPage * (currentPage - 1)).Take(recordsPerPage);
            DataResult = sourceResult.ToArray();
        }
    }
}
