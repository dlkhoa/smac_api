﻿using Oracle.ManagedDataAccess.Client;
using System.Collections.Generic;
using System.Linq;

namespace Cmms.Data.Infrastructure.Paging
{
    public class SqlPagedResult<TData> : IPagedResult<TData>
    {
        public int Page { get; private set; }
        public int RecordsPerPage { get; private set; }
        public int TotalPages { get; private set; }
        public int TotalRecords { get; private set; }
        public IEnumerable<TData> DataResult { get; private set; }

        public SqlPagedResult(CmmsDbContext dbContext,
            string originalQuery,
            int currentPage,
            int recordsPerPage,
            params object[] paramaters)
        {
            if (currentPage < 1)
            {
                currentPage = 1;
            }

            Page = currentPage;
            RecordsPerPage = recordsPerPage;

            TotalRecords = CountRecords(dbContext, GenerateCountQuery(originalQuery), paramaters);
            TotalPages = ((TotalRecords - 1) / recordsPerPage) + 1;

            var query = GeneratePagingQuery(originalQuery);
            var lower = recordsPerPage * (currentPage - 1) + 1; //recalculate
            var upper = recordsPerPage + lower - 1;//recalcualte
            DataResult = QueryData(dbContext, query, lower, upper, paramaters);
        }

        private string GenerateCountQuery(string originalQuery)
        {
            return "select count(*) from (" + originalQuery + ")";
        }

        private int CountRecords(CmmsDbContext dbContext, string sql, params object[] parameters)
        {
            return dbContext.Database
                .SqlQuery<int>(sql, parameters)
                .FirstOrDefault();
        }

        private string GeneratePagingQuery(string originalQuery)
        {
            return "select * from (select rownum rnum, tbl.* from (" + originalQuery + ") tbl ) where rnum BETWEEN :Lower AND :Upper";
        }

        private IEnumerable<TData> QueryData(CmmsDbContext dbContext, string sql, int lower, int upper, params object[] parameters)
        {
            var lowerPara = new OracleParameter("Lower", lower);
            var upperPara = new OracleParameter("Upper", upper);
            var sqlParameters = new OracleParameter[parameters.Length + 2];
            for (var i = 0; i < parameters.Length; i++)
            {
                sqlParameters[i] = (OracleParameter)parameters[i];
            }
            sqlParameters[parameters.Length] = lowerPara;
            sqlParameters[parameters.Length + 1] = upperPara;
            return dbContext.Database
                .SqlQuery<TData>(sql, sqlParameters)
                .ToArray();
        }
    }
}
