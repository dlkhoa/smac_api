﻿namespace Cmms.Data.Constants
{
    public class StatusConstants
    {
        public const string Broken = "1";
        public const string BrokenReceived = "2";
        public const string BrokenDelivered = "3";
        public const string RepairedReceived = "4";
        public const string RepairedDelivered = "5";
    }
}
