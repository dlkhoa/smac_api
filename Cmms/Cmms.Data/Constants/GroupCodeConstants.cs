﻿namespace Cmms.Data.Constants
{
    public class GroupCodeConstants
    {
        public const string ADMIN = "ADMIN";
        public const string TVT = "TOVT";
        public const string DVT = "DAIVT";
    }
}
