﻿namespace Cmms.Data.Constants
{
    public class OrderStatusConstant
    {
        public const string ChoLanhDaoChiNhanhPheDuyet = "U-CN";
        public const string DaPheDuyetTaiChiNhanh = "W-CN";

        public const string ChoLenhDieuDong = "U-SC";
        public const string DaPheDuyetLenhDieuDong = "W-SC";
    }
}
