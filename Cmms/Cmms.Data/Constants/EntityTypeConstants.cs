﻿namespace Cmms.Data.Constants
{
    public class EntityTypeConstants
    {
        public const string PhieuTiepNhanThietBi = "PHIEU_TIEP_NHAN_THIET_BI";
        public const string PhieuDeNghiNhapKho = "PHIEU_DE_NGHI_NHAP_KHO";
        public const string PhieuDeNghiXuatKho = "PHIEU_DE_NGHI_XUAT_KHO";

        public const string Requisition = "R5REQUISITIONS";
        public const string Order = "R5ORDERS";
        public const string Picklist = "R5PICKLISTS";
    }
}
