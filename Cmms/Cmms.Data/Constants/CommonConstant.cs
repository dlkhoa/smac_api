﻿namespace Cmms.Data.Constants
{
    public class CommonConstant
    {
        public const string TrueValueString = "+";
        public const string FalseValueString = "-";

        public const string UploadedImagesDiectory = "UploadedImages";
    }
}
