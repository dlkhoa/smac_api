﻿namespace Cmms.Data.Constants
{
    public class RequisitionConstant
    {
        public const string TIEP_NHAN_THIET_BI_MOI = "TNTBM";
        public const string TIEP_NHAN_THIET_BI_SAU_SUA_CHUA = "TNTBSSC";
        public const string TIEP_NHAN_THIET_BI_HONG = "TNTBHH";
        public const string TU_CHOI_NHAN_BAO_HANH = "TCNBH";
    }
}
