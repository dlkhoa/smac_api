﻿namespace Cmms.Data.Constants
{
    public class PermissionFunctionConstant
    {
        public const string TIEP_NHAN_THIET_BI_HONG = "TNTBH";
        public const string TIEP_NHAN_THIET_BI_MOI = "TNTBM";
        public const string TIEP_NHAN_THIET_BI_BAO_HANH = "TNTBBH";
        public const string TIEP_NHAN_THIET_BI_SUA_CHUA = "TNTBSC";
    }
}
