﻿namespace Cmms.Data.Constants
{
    public class SummarizeTypeConstants
    {
        public const int Broken = 1;
        public const int BrokenReceived = 2;
        public const int BrokenDelivered = 3;
        public const int RepairedReceived = 4;
        public const int RepairedDelivered = 5;
        public const int PriorityBrokenDelivered = 6;
    }
}
