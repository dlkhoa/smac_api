﻿using Cmms.Data.Infrastructure;
using System.Data.Entity;

namespace Cmms.Data.Repositories
{
    public class BaseRepository
    {
        protected readonly CmmsDbContext dbContext;

        public BaseRepository(CmmsDbContext dbContext)
        {
            this.dbContext = dbContext;
            this.dbContext.Database.Log = log => System.Diagnostics.Debug.WriteLine(log);
        }

        public IDbSet<TEntity> GetDbEntity<TEntity>() where TEntity : class
        {
            return dbContext.Set<TEntity>();
        }
    }
}
