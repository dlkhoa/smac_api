﻿using System.Collections.Generic;
using System.Linq;
using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using MBoL.Utils;
using Oracle.ManagedDataAccess.Client;

namespace Cmms.Data.Repositories
{
    public class OverRepairTimeDeviceReportRepository : BaseRepository, IOverRepairTimeDeviceReportRepository
    {
        public OverRepairTimeDeviceReportRepository(CmmsDbContext dbContext) : base(dbContext)
        {

        }
        
        public IEnumerable<OverRepairTimeDeviceStatisticDTSCByDTSC> GetStatisticByDoiTac(DeviceReportQuery request)
        {
            var orgs = StringHelper.MakeOrgSearch(request.Organizations);
            var sql = string.Format(@"select chinhanh, Doitac, count(1) tong 
                        from MV_BCCHITIET
                        where (NGAYBGDT between to_date(:StartDate,'dd/mm/yyyy') and to_date(:EndDate,'dd/mm/yyyy'))
                            and (NGAYDTBG-NGAYBGDT) > TO_NUMBER(SONGAYSC) and chinhanh in ({0})
                        GROUP BY chinhanh,Doitac", orgs);

            return dbContext.Database.SqlQuery<OverRepairTimeDeviceStatisticDTSCByDTSC>(sql,
                new OracleParameter("StartDate", request.StartDate.ToString("dd/MM/yyyy")),
                new OracleParameter("EndDate", request.EndDate.ToString("dd/MM/yyyy"))).ToArray();
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDTSC(OverRepairTimeDeviceReportByDTSCQuery request)
        {
            var sql = @"select Serial, TENTB TenThietBi, TinhTrang 
                        from MV_BCCHITIET
                        where (NGAYBGDT between to_date(:StartDate,'dd/mm/yyyy') and to_date(:EndDate,'dd/mm/yyyy'))
                            and (NGAYDTBG-NGAYBGDT) > TO_NUMBER(SONGAYSC)
                            AND chinhanh=:ChiNhanh
                            AND doitac=:DoiTac
                        ORDER BY REQ_CODE DESC, SERIAL ASC";
            var parameters = new OracleParameter[4];
            parameters[0] = new OracleParameter("StartDate", request.StartDate.ToString("dd/MM/yyyy"));
            parameters[1] = new OracleParameter("EndDate", request.EndDate.ToString("dd/MM/yyyy"));
            parameters[2] = new OracleParameter("ChiNhanh", request.ChiNhanh.ToUpper());
            parameters[3] = new OracleParameter("DoiTac", request.DoiTac.ToUpper());

            return new SqlPagedResult<DeviceDetailsReportSummary>(dbContext, sql, request.Page, request.RecordsPerPage, parameters);
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryAllLevel(DeviceReportAllLevelQuery request)
        {
            var sql = @"select Serial, TENTB TenThietBi, TinhTrang 
                        from MV_BCCHITIET
                        where (NGAYBGDT between to_date(:StartDate,'dd/mm/yyyy') and to_date(:EndDate,'dd/mm/yyyy'))
                            and (NGAYDTBG-NGAYBGDT) > TO_NUMBER(SONGAYSC)
                            AND chinhanh=:ChiNhanh
                            AND doitac like '%'||:DoiTac||'%'
                        ORDER BY REQ_CODE DESC, SERIAL ASC";
            var parameters = new OracleParameter[4];
            parameters[0] = new OracleParameter("StartDate", request.StartDate.ToString("dd/MM/yyyy"));
            parameters[1] = new OracleParameter("EndDate", request.EndDate.ToString("dd/MM/yyyy"));
            parameters[2] = new OracleParameter("ChiNhanh", request.ChiNhanh.ToUpper());
            parameters[3] = new OracleParameter("DoiTac", request.DoiTac.ToUpper());

            return new SqlPagedResult<DeviceDetailsReportSummary>(dbContext, sql, request.Page, request.RecordsPerPage, parameters);
        }
    }
}
