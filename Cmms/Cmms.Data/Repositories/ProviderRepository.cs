﻿using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities;
using Cmms.Domain.Queries;
using System.Linq;

namespace Cmms.Data.Repositories
{
    public class ProviderRepository : BaseRepository, IProviderRepository
    {
        public ProviderRepository(CmmsDbContext dbContext) : base(dbContext)
        {

        }

        public IPagedResult<Provider> GetProviders(ProviderQuery providerQuery)
        {
            var query = GetDbEntity<Provider>().AsQueryable();

            if (!string.IsNullOrEmpty(providerQuery.ProviderCode))
            {
                query = query.Where(p => p.MaNhaCungCap.ToUpper().Contains(providerQuery.ProviderCode.ToUpper()));
            }
            var providers = query.OrderBy(c => c.MaNhaCungCap).ToList();
            providers.Insert(0, new Provider { MaNhaCungCap = "", TenNhaCungCap = "[TẤT CẢ]", });

            return new PagedResult<Provider>(providers.AsQueryable(), providerQuery.Page, providerQuery.RecordsPerPage);
        }
    }
}
