﻿using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Entities.Views;
using System.Collections.Generic;
using System.Linq;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Queries;
using System;
using Oracle.ManagedDataAccess.Client;

namespace Cmms.Data.Repositories
{
    public class AnalyzeReportRepository : BaseRepository, IAnalyzeReportRepository
    {
        public AnalyzeReportRepository(CmmsDbContext dbContext)
            : base(dbContext)
        {

        }

        public IEnumerable<DeviceStatisticByMonth> GetReceivedDeviceReportByYear(DeviceReportByYearQuery request)
        {
            var fromYear = request.Nam == 0 ? 2010 : request.Nam;
            var toYear = request.Nam == 0 ? DateTime.Today.Year : request.Nam;
            var fromDate = fromYear + "0101";
            var toDate = toYear + "1231";

            var sql = @"select chinhanh,DVQLSD1,DAIVT, month(NGAYCNNTK) Thang, count(1) tong from MV_BCCHITIET where
NGAYCNNTK between to_date(:FromDate,'yyyymmdd') and to_date(:ToDate,'yyyymmdd')
group by chinhanh, DVQLSD1,DAIVT, month(NGAYCNNTK)";
            return dbContext.Database.SqlQuery<DeviceStatisticByMonth>(sql,
                new OracleParameter("FromDate", fromDate),
                new OracleParameter("ToDate", toDate)).ToArray();
        }

        public IEnumerable<DeviceStatisticByMonth> GetSentDeviceReportByYear(DeviceReportByYearQuery request)
        {
            var fromYear = request.Nam == 0 ? 2010 : request.Nam;
            var toYear = request.Nam == 0 ? DateTime.Today.Year : request.Nam;
            var fromDate = fromYear + "0101";
            var toDate = toYear + "1231";

            var sql = @"select chinhanh,DVQLSD1,DAIVT, month(NGAYBGDVQLSD) Thang, count(1) tong from MV_BCCHITIET where
NGAYBGDVQLSD between to_date(:FromDate,'yyyymmdd') and to_date(:ToDate,'yyyymmdd')
group by chinhanh, DVQLSD1,DAIVT, month(NGAYBGDVQLSD)";
            return dbContext.Database.SqlQuery<DeviceStatisticByMonth>(sql,
                new OracleParameter("FromDate", fromDate),
                new OracleParameter("ToDate", toDate)).ToArray();
        }

        public decimal GetCenterFulfillReportByYear(CenterFulfillReportByYearQuery request)
        {
            var newRequest = new CenterNumberOfDevicesReportByYearQuery
            {
                Nam = request.Nam
            };
            var totalIn = GetCenterNumberOfReceivedDevicesReportByYear(newRequest);
            var totalOut = GetCenterNumberOfSentDevicesReportByYear(newRequest);
            return totalIn == 0 ? 0 : (decimal)totalOut * 100 / (decimal)totalIn;
        }

        public decimal GetBranchFulfillReportByYear(BranchFulfillReportByYearQuery request)
        {
            var newRequest = new BranchNumberOfDevicesReportByYearQuery
            {
                Chinhanh = request.Chinhanh,
                Nam = request.Nam
            };
            var totalIn = GetBranchNumberOfReceivedDevicesReportByYear(newRequest);
            var totalOut = GetBranchNumberOfSentDevicesReportByYear(newRequest);
            return totalIn == 0 ? 0 : (decimal)totalOut * 100 / (decimal)totalIn;
        }

        public int GetCenterNumberOfReceivedDevicesReportByYear(CenterNumberOfDevicesReportByYearQuery request)
        {
            var fromYear = request.Nam == 0 ? 2010 : request.Nam;
            var toYear = request.Nam == 0 ? DateTime.Today.Year : request.Nam;
            var fromDate = fromYear + "0101";
            var toDate = toYear + "1231";
            var sql = @"select chinhanh branch, DVQLSD1 DVQLSD, count(1) total from MV_BCCHITIET where
NGAYCNNTK between to_date(:FromDate,'yyyymmdd') and to_date(:ToDate,'yyyymmdd')
group by chinhanh, DVQLSD1";
            IEnumerable<DeviceStatistic> branchIns =
                dbContext.Database.SqlQuery<DeviceStatistic>(sql,
                    new OracleParameter("FromDate", fromDate),
                    new OracleParameter("ToDate", toDate)).ToArray();
            var totalIn = 0;
            foreach (var bi in branchIns)
            {
                totalIn += bi.Total;
            }
            return totalIn;
        }

        public int GetBranchNumberOfReceivedDevicesReportByYear(BranchNumberOfDevicesReportByYearQuery request)
        {
            var fromYear = request.Nam == 0 ? 2010 : request.Nam;
            var toYear = request.Nam == 0 ? DateTime.Today.Year : request.Nam;
            var fromDate = fromYear + "0101";
            var toDate = toYear + "1231";
            var sql = @"select chinhanh branch, DVQLSD1 DVQLSD, count(1) total from MV_BCCHITIET where
NGAYCNNTK between to_date(:FromDate,'yyyymmdd') and to_date(:ToDate,'yyyymmdd')
group by chinhanh, DVQLSD1";
            IEnumerable<DeviceStatistic> branchIns =
                dbContext.Database.SqlQuery<DeviceStatistic>(sql,
                    new OracleParameter("FromDate", fromDate),
                    new OracleParameter("ToDate", toDate)).ToArray();
            var totalIn = 0;
            foreach (var bi in branchIns)
            {
                if (request.Chinhanh.Equals("ALL", StringComparison.InvariantCultureIgnoreCase) ||
                    bi.Branch.Equals(request.Chinhanh, StringComparison.InvariantCultureIgnoreCase))
                {
                    if (request.DVQLSD.Equals("ALL", StringComparison.InvariantCultureIgnoreCase) ||
                        bi.DVQLSD != null && bi.DVQLSD.Equals(request.DVQLSD, StringComparison.InvariantCultureIgnoreCase))
                    {
                        totalIn += bi.Total;
                    }
                }
            }
            return totalIn;
        }

        public int GetCenterNumberOfSentDevicesReportByYear(CenterNumberOfDevicesReportByYearQuery request)
        {
            var fromYear = request.Nam == 0 ? 2010 : request.Nam;
            var toYear = request.Nam == 0 ? DateTime.Today.Year : request.Nam;
            var fromDate = fromYear + "0101";
            var toDate = toYear + "1231";
            var sql = @"select chinhanh branch, DVQLSD1 DVQLSD, count(1) total from MV_BCCHITIET where
NGAYBGDVQLSD between to_date(:FromDate,'yyyymmdd') and to_date(:ToDate,'yyyymmdd')
group by chinhanh, DVQLSD1";
            IEnumerable<DeviceStatistic> branchOuts =
                dbContext.Database.SqlQuery<DeviceStatistic>(sql,
                    new OracleParameter("FromDate", fromDate),
                    new OracleParameter("ToDate", toDate)).ToArray();
            var totalOut = 0;
            foreach (var bo in branchOuts)
            {
                totalOut += bo.Total;
            }
            return totalOut;
        }

        public int GetBranchNumberOfSentDevicesReportByYear(BranchNumberOfDevicesReportByYearQuery request)
        {
            var fromYear = request.Nam == 0 ? 2010 : request.Nam;
            var toYear = request.Nam == 0 ? DateTime.Today.Year : request.Nam;
            var fromDate = fromYear + "0101";
            var toDate = toYear + "1231";
            var sql = @"select chinhanh branch, DVQLSD1 DVQLSD, count(1) total from MV_BCCHITIET where
NGAYBGDVQLSD between to_date(:FromDate,'yyyymmdd') and to_date(:ToDate,'yyyymmdd')
group by chinhanh, DVQLSD1";
            IEnumerable<DeviceStatistic> branchOuts =
                dbContext.Database.SqlQuery<DeviceStatistic>(sql,
                    new OracleParameter("FromDate", fromDate),
                    new OracleParameter("ToDate", toDate)).ToArray();
            var totalOut = 0;
            foreach (var bo in branchOuts)
            {
                if (request.Chinhanh.Equals("ALL", StringComparison.InvariantCultureIgnoreCase) ||
                    bo.Branch.Equals(request.Chinhanh, StringComparison.InvariantCultureIgnoreCase))
                {
                    if (request.DVQLSD.Equals("ALL", StringComparison.InvariantCultureIgnoreCase) ||
                        bo.DVQLSD != null && bo.DVQLSD.Equals(request.DVQLSD, StringComparison.InvariantCultureIgnoreCase))
                    {
                        totalOut += bo.Total;
                    }
                }
            }
            return totalOut;
        }

        public SummaryByBranchAndByYear GetSummaryByBranchAndByYear(SummaryByBranchAndByYearQuery request)
        {
            var response = new SummaryByBranchAndByYear();
            /*var branches = new List<string>();
            if (request.Branch.ToUpper() == "TTDKSC")
            {
                branches.Add("CNHN");
                branches.Add("CNDN");
                branches.Add("CNHCM");
            }
            else
            {
                branches.Add(request.Branch.ToUpper());
            }*/
            var branchWhere = " Upper(CHINHANH) IN (";
            if (request.Branch.ToUpper() == "TTDKSC")
            {
                branchWhere += "'CNHN',";
                branchWhere += "'CNDN',";
                branchWhere += "'CNHCM'";
            }
            else
            {
                branchWhere += "'" + request.Branch.ToUpper() + "'";
            }
            branchWhere += ")";
            var sqlParameters = new OracleParameter[2];
            sqlParameters[0] = new OracleParameter("Year", request.Year);
            sqlParameters[1] = new OracleParameter("Username", request.Username.ToUpper());

            var sql = string.Format(@"SELECT NVL(SUM(CASE WHEN (NGAYCNNTK IS NOT NULL) THEN 1 ELSE 0  END),0) AS NHANHONG
FROM MV_BCCHITIET WHERE TO_NUMBER(TO_CHAR(NGAYCNNTK, 'YYYY')) = :Year AND {0}
AND NVL(DVQLSD1,'*') IN( SELECT DSE_MRC FROM R5DEPARTMENTSECURITY WHERE Upper(DSE_USER) = :Username)", branchWhere);
            response.BrokenReceived =
                dbContext.Database.SqlQuery<int>(sql, sqlParameters).FirstOrDefault();

            sqlParameters = new OracleParameter[2];
            sqlParameters[0] = new OracleParameter("Year", request.Year);
            sqlParameters[1] = new OracleParameter("Username", request.Username.ToUpper());
            sql = string.Format(@"SELECT NVL(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL AND (CODEKQTSC = 2 OR CODEKQSSC = 3)) THEN 1 ELSE 0  END),0) AS TRAVE
FROM MV_BCCHITIET WHERE TO_NUMBER(TO_CHAR(NGAYBGDVQLSD, 'YYYY')) = :Year AND {0}
AND NVL(DVQLSD1,'*') IN(SELECT DSE_MRC FROM R5DEPARTMENTSECURITY WHERE DSE_USER = :Username)", branchWhere);
            response.GoodReturn =
                dbContext.Database.SqlQuery<int>(sql, sqlParameters).FirstOrDefault();

            sql = string.Format(@"SELECT 'T1' AS Month, ROUND(100 * NVL(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL  AND (CODEKQTSC = 2 OR CODEKQSSC = 3)) THEN 1 ELSE 0  END) / 
NULLIF(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL) THEN 1 ELSE 0  END), 0), 1), 2) Ratio
FROM MV_BCCHITIET WHERE TO_NUMBER(TO_CHAR(NGAYBGDVQLSD, 'MM')) = 01 AND TO_NUMBER(TO_CHAR(NGAYBGDVQLSD, 'YYYY')) = :Year AND {0}
AND NVL(DVQLSD1,'*') IN( SELECT DSE_MRC FROM R5DEPARTMENTSECURITY WHERE DSE_USER = :Username)
UNION ALL
SELECT 'T2' AS Month, ROUND(100 * NVL(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL  AND (CODEKQTSC = 2 OR CODEKQSSC = 3)) THEN 1 ELSE 0  END) / 
NULLIF(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL) THEN 1 ELSE 0  END), 0), 1), 2) Ratio
FROM MV_BCCHITIET WHERE TO_NUMBER(TO_CHAR(NGAYBGDVQLSD, 'MM')) = 02 AND TO_NUMBER(TO_CHAR(NGAYBGDVQLSD, 'YYYY')) = :Year AND {0}
AND NVL(DVQLSD1,'*') IN( SELECT DSE_MRC FROM R5DEPARTMENTSECURITY WHERE DSE_USER =:Username)
UNION ALL
SELECT 'T3' AS Month, ROUND(100 * NVL(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL  AND (CODEKQTSC = 2 OR CODEKQSSC = 3)) THEN 1 ELSE 0  END) / 
NULLIF(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL) THEN 1 ELSE 0  END), 0), 1), 2) Ratio
FROM MV_BCCHITIET WHERE TO_NUMBER(TO_CHAR(NGAYBGDVQLSD, 'MM')) = 03 AND TO_NUMBER(TO_CHAR(NGAYBGDVQLSD, 'YYYY')) = :Year AND {0}
AND NVL(DVQLSD1,'*') IN( SELECT DSE_MRC FROM R5DEPARTMENTSECURITY WHERE DSE_USER =:Username)
UNION ALL
SELECT 'T4' AS Month, ROUND(100 * NVL(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL  AND (CODEKQTSC = 2 OR CODEKQSSC = 3)) THEN 1 ELSE 0  END) / 
NULLIF(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL) THEN 1 ELSE 0  END), 0), 1), 2) Ratio
FROM MV_BCCHITIET WHERE TO_NUMBER(TO_CHAR(NGAYBGDVQLSD, 'MM')) = 04 AND TO_NUMBER(TO_CHAR(NGAYBGDVQLSD, 'YYYY')) = :Year AND {0}
AND NVL(DVQLSD1,'*') IN( SELECT DSE_MRC FROM R5DEPARTMENTSECURITY WHERE DSE_USER =:Username)
UNION ALL
SELECT 'T5' AS Month, ROUND(100 * NVL(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL  AND (CODEKQTSC = 2 OR CODEKQSSC = 3)) THEN 1 ELSE 0  END) / 
NULLIF(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL) THEN 1 ELSE 0  END), 0), 1), 2) Ratio
FROM MV_BCCHITIET WHERE TO_NUMBER(TO_CHAR(NGAYBGDVQLSD, 'MM')) = 05 AND TO_NUMBER(TO_CHAR(NGAYBGDVQLSD, 'YYYY')) = :Year AND {0}
AND NVL(DVQLSD1,'*') IN( SELECT DSE_MRC FROM R5DEPARTMENTSECURITY WHERE DSE_USER =:Username)
UNION ALL
SELECT 'T6' AS Month, ROUND(100 * NVL(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL  AND (CODEKQTSC = 2 OR CODEKQSSC = 3)) THEN 1 ELSE 0  END) / 
NULLIF(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL) THEN 1 ELSE 0  END), 0), 1), 2) Ratio
FROM MV_BCCHITIET WHERE TO_NUMBER(TO_CHAR(NGAYBGDVQLSD, 'MM')) = 06 AND TO_NUMBER(TO_CHAR(NGAYBGDVQLSD, 'YYYY')) = :Year AND {0}
AND NVL(DVQLSD1,'*') IN( SELECT DSE_MRC FROM R5DEPARTMENTSECURITY WHERE DSE_USER =:Username)
UNION ALL
SELECT 'T7' AS Month, ROUND(100 * NVL(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL  AND (CODEKQTSC = 2 OR CODEKQSSC = 3)) THEN 1 ELSE 0  END) / 
NULLIF(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL) THEN 1 ELSE 0  END), 0), 1), 2) Ratio
FROM MV_BCCHITIET WHERE TO_NUMBER(TO_CHAR(NGAYBGDVQLSD, 'MM')) = 07 AND TO_NUMBER(TO_CHAR(NGAYBGDVQLSD, 'YYYY')) = :Year AND {0}
AND NVL(DVQLSD1,'*') IN( SELECT DSE_MRC FROM R5DEPARTMENTSECURITY WHERE DSE_USER =:Username)
UNION ALL
SELECT 'T8' AS Month, ROUND(100 * NVL(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL  AND (CODEKQTSC = 2 OR CODEKQSSC = 3)) THEN 1 ELSE 0  END) / 
NULLIF(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL) THEN 1 ELSE 0  END), 0), 1), 2) Ratio
FROM MV_BCCHITIET WHERE TO_NUMBER(TO_CHAR(NGAYBGDVQLSD, 'MM')) = 08 AND TO_NUMBER(TO_CHAR(NGAYBGDVQLSD, 'YYYY')) = :Year AND {0}
AND NVL(DVQLSD1,'*') IN( SELECT DSE_MRC FROM R5DEPARTMENTSECURITY WHERE DSE_USER =:Username)
UNION ALL
SELECT 'T9' AS Month, ROUND(100 * NVL(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL  AND (CODEKQTSC = 2 OR CODEKQSSC = 3)) THEN 1 ELSE 0  END) / 
NULLIF(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL) THEN 1 ELSE 0  END), 0), 1), 2) Ratio
FROM MV_BCCHITIET WHERE TO_NUMBER(TO_CHAR(NGAYBGDVQLSD, 'MM')) = 09 AND TO_NUMBER(TO_CHAR(NGAYBGDVQLSD, 'YYYY')) = :Year AND {0}
AND NVL(DVQLSD1,'*') IN( SELECT DSE_MRC FROM R5DEPARTMENTSECURITY WHERE DSE_USER =:Username)
UNION ALL
SELECT 'T10' AS Month, ROUND(100 * NVL(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL  AND (CODEKQTSC = 2 OR CODEKQSSC = 3)) THEN 1 ELSE 0  END) / 
NULLIF(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL) THEN 1 ELSE 0  END), 0), 1), 2) Ratio
FROM MV_BCCHITIET WHERE TO_NUMBER(TO_CHAR(NGAYBGDVQLSD, 'MM')) = 10 AND TO_NUMBER(TO_CHAR(NGAYBGDVQLSD, 'YYYY')) = :Year AND {0}
AND NVL(DVQLSD1,'*') IN( SELECT DSE_MRC FROM R5DEPARTMENTSECURITY WHERE DSE_USER =:Username)
UNION ALL
SELECT 'T11' AS Month, ROUND(100 * NVL(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL  AND (CODEKQTSC = 2 OR CODEKQSSC = 3)) THEN 1 ELSE 0  END) / 
NULLIF(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL) THEN 1 ELSE 0  END), 0), 1), 2) Ratio
FROM MV_BCCHITIET WHERE TO_NUMBER(TO_CHAR(NGAYBGDVQLSD, 'MM')) = 11 AND TO_NUMBER(TO_CHAR(NGAYBGDVQLSD, 'YYYY')) = :Year AND {0}
AND NVL(DVQLSD1,'*') IN( SELECT DSE_MRC FROM R5DEPARTMENTSECURITY WHERE DSE_USER =:Username)
UNION ALL
SELECT 'T12' AS Month, ROUND(100 * NVL(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL  AND (CODEKQTSC = 2 OR CODEKQSSC = 3)) THEN 1 ELSE 0  END) / 
NULLIF(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL) THEN 1 ELSE 0  END), 0), 1), 2) Ratio
FROM MV_BCCHITIET WHERE TO_NUMBER(TO_CHAR(NGAYBGDVQLSD, 'MM')) = 12 AND TO_NUMBER(TO_CHAR(NGAYBGDVQLSD, 'YYYY')) = :Year AND {0}
AND NVL(DVQLSD1,'*') IN( SELECT DSE_MRC FROM R5DEPARTMENTSECURITY WHERE DSE_USER =:Username)", branchWhere);
            response.SuccessRatios =
                dbContext.Database.SqlQuery<SummaryByBranchAndByYearSuccessRatio>(sql, sqlParameters).ToArray();

            sqlParameters = new OracleParameter[1];
            sqlParameters[0] = new OracleParameter("Username", request.Username.ToUpper());
            sql = string.Format(@"SELECT NVL(SUM(CASE WHEN M2.SERIAL IS NULL	THEN 0	ELSE 1	END),0) AS ""Number""
,ROUND(100 * NVL(SUM(CASE WHEN M2.SERIAL IS NULL    THEN 0  ELSE 1  END) /
NULLIF(SUM(M1.MAUSO), 0), 1), 2) Ratio 
FROM (SELECT REQ_CODE, CHINHANH, CODELOAITB, LOAITB, 1 MAUSO, DVQLSD1, DOITAC, CODEDAIVT, DAIVT, NGAYBGDVQLSD, SERIAL, FIRSTDAY, LASTDAY
    FROM (SELECT A.*, TRUNC(TO_DATE(CONCAT('01/01/', '{1}'), 'dd/MM/yyyy')) FIRSTDAY,
    TRUNC(TO_DATE(CONCAT('31/12/', '{1}'), 'dd/MM/yyyy')) LASTDAY FROM MV_BCCHITIET A)
    WHERE NGAYBGDVQLSD BETWEEN FIRSTDAY AND LASTDAY AND(CODEKQTSC = '2'    OR CODEKQSSC = '3') AND {0}
    AND NVL(DVQLSD1, '*') IN(SELECT DSE_MRC FROM R5DEPARTMENTSECURITY WHERE DSE_USER =:Username)
    ) M1
LEFT OUTER JOIN MV_BCCHITIET M2 ON M1.REQ_CODE<> M2.REQ_CODE
   AND M1.CHINHANH = M2.CHINHANH AND M1.SERIAL = M2.SERIAL
    AND(M2.NGAYCNNTK - M1.NGAYBGDVQLSD) BETWEEN 1 AND 90
    AND M2.NGAYCNNTK BETWEEN FIRSTDAY AND LASTDAY AND M2.CODEKQTSC = '1'", branchWhere, request.Year);
            response.Rebroken =
                dbContext.Database.SqlQuery<SummaryByBranchAndByYearRebroken>(sql, sqlParameters).FirstOrDefault();

            sqlParameters = new OracleParameter[3];
            sqlParameters[0] = new OracleParameter("Year0", request.Year);
            sqlParameters[1] = new OracleParameter("Year", request.Year);
            sqlParameters[2] = new OracleParameter("Username", request.Username.ToUpper());
            sql = string.Format(@"SELECT ROUND(100 * NVL(SUM(CASE WHEN(NGAYBGDVQLSD - NGAYCNNTK) < 51 THEN 1 ELSE 0 END) / NULLIF(SUM(1), 0), 1), 2) SLA1,
ROUND(100 * NVL(SUM(CASE WHEN(NGAYBGDVQLSD - NGAYCNNTK) < 66 THEN 1 ELSE 0 END) / NULLIF(SUM(1), 0), 1), 2) SLA2,
ROUND(100 * NVL(SUM(CASE WHEN(NGAYBGDVQLSD - NGAYCNNTK) < 81 THEN 1 ELSE 0 END) / NULLIF(SUM(1), 0), 1), 2) SLA3
FROM(SELECT A.*, TRUNC(TO_DATE(CONCAT('01/01/', :Year0), 'dd/MM/yyyy')) FIRSTDAY,
TRUNC(TO_DATE(CONCAT('31/12/', :Year), 'dd/MM/yyyy')) LASTDAY FROM MV_BCCHITIET A)
WHERE NGAYCNNTK BETWEEN TRUNC(TO_DATE('13/09/2017', 'dd/MM/yyyy')) AND LASTDAY AND NGAYBGDVQLSD BETWEEN FIRSTDAY AND LASTDAY AND {0}
AND CODEDTTB NOT IN('1','2') AND(CODEKQTSC = '2' OR CODEKQSSC = '3')
AND NVL(DVQLSD1,'*') IN(SELECT DSE_MRC FROM R5DEPARTMENTSECURITY WHERE DSE_USER =:Username)", branchWhere);
            response.SLA =
                dbContext.Database.SqlQuery<SummaryByBranchAndByYearSLA>(sql, sqlParameters).FirstOrDefault();

            sqlParameters = new OracleParameter[1];
            sqlParameters[0] = new OracleParameter("Username", request.Username.ToUpper());
            sql = string.Format(@"SELECT NVL(ROUND(SUM(PKHUC7)/SUM(PKHUC7NUMBER),2),0) Overall,
	   NVL(ROUND(SUM(PKHUC1)/SUM(PKHUC1NUMBER),2),0) Received,
       NVL(ROUND(SUM(PKHUC2)/SUM(PKHUC2NUMBER),2),0) TestBefore,
       NVL(ROUND(SUM(PKHUC3)/SUM(PKHUC3NUMBER),2),0) Stock,
       NVL(ROUND(SUM(PKHUC4)/SUM(PKHUC4NUMBER),2),0) Organized,
       NVL(ROUND(SUM(PKHUC5)/SUM(PKHUC5NUMBER),2),0) TestAfter,
       NVL(ROUND(SUM(PKHUC6)/SUM(PKHUC6NUMBER),2),0) Delivery

FROM (
SELECT 
--PHAN KHUC 1
CASE WHEN (NGAYYCSC IS NOT NULL AND NGAYDVVCBG IS NOT NULL AND 
  NGAYYCSC BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYDVVCBG BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYYCSC <= NGAYDVVCBG)
  THEN (NGAYDVVCBG-NGAYYCSC) ELSE 0 END PKHUC1,
CASE WHEN (NGAYYCSC IS NOT NULL AND NGAYDVVCBG IS NOT NULL AND 
  NGAYYCSC BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYDVVCBG BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYYCSC <= NGAYDVVCBG)
  THEN 1 ELSE 0 END PKHUC1NUMBER,
--PHAN KHUC 2
CASE WHEN (NGAYDVVCBG IS NOT NULL AND NGAYHTKTTSC IS NOT NULL AND 
  NGAYDVVCBG BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYHTKTTSC BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYDVVCBG <= NGAYHTKTTSC)
  THEN (NGAYHTKTTSC-NGAYDVVCBG) ELSE 0 END PKHUC2,
CASE WHEN (NGAYDVVCBG IS NOT NULL AND NGAYHTKTTSC IS NOT NULL AND 
  NGAYDVVCBG BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYHTKTTSC BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYDVVCBG <= NGAYHTKTTSC)
  THEN 1 ELSE 0 END PKHUC2NUMBER,
--PHAN KHUC 3
CASE WHEN (NGAYHTKTTSC IS NOT NULL AND NGAYBGDT IS NOT NULL AND 
  NGAYHTKTTSC BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYBGDT BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYHTKTTSC <= NGAYBGDT)
  THEN (NGAYBGDT-NGAYHTKTTSC) ELSE 0 END PKHUC3,
CASE WHEN (NGAYHTKTTSC IS NOT NULL AND NGAYBGDT IS NOT NULL AND 
  NGAYBGDT BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYHTKTTSC BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYHTKTTSC <= NGAYBGDT)
  THEN 1 ELSE 0 END PKHUC3NUMBER,
 --PHAN KHUC 4
CASE WHEN (NGAYBGDT IS NOT NULL AND NGAYDTBG IS NOT NULL AND 
  NGAYDTBG BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYBGDT BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYBGDT <= NGAYDTBG)
  THEN (NGAYDTBG-NGAYBGDT) ELSE 0 END PKHUC4,
CASE WHEN (NGAYBGDT IS NOT NULL AND NGAYDTBG IS NOT NULL AND 
  NGAYDTBG BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYBGDT BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYBGDT <= NGAYDTBG)
  THEN 1 ELSE 0 END PKHUC4NUMBER,           
 --PHAN KHUC 5
CASE WHEN (NGAYBGDT IS NOT NULL AND NGAYHTKTSSC IS NOT NULL AND 
  NGAYDTBG BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYHTKTSSC BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYDTBG <= NGAYHTKTSSC)
  THEN (NGAYHTKTSSC-NGAYDTBG) ELSE 0 END PKHUC5,
CASE WHEN (NGAYHTKTSSC IS NOT NULL AND NGAYDTBG IS NOT NULL AND 
  NGAYDTBG BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYHTKTSSC BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYDTBG <= NGAYHTKTSSC)
  THEN 1 ELSE 0 END PKHUC5NUMBER,
 --PHAN KHUC 6
CASE WHEN (NGAYBGDVQLSD IS NOT NULL AND NGAYHTKTSSC IS NOT NULL AND 
  NGAYBGDVQLSD BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYHTKTSSC BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYHTKTSSC <= NGAYBGDVQLSD)
  THEN (NGAYBGDVQLSD-NGAYHTKTSSC) ELSE 0 END PKHUC6,
CASE WHEN (NGAYHTKTSSC IS NOT NULL AND NGAYBGDVQLSD IS NOT NULL AND 
  NGAYBGDVQLSD BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYHTKTSSC BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYHTKTSSC <= NGAYBGDVQLSD)
  THEN 1 ELSE 0 END PKHUC6NUMBER,
 --PHAN KHUC 7
CASE WHEN (NGAYBGDVQLSD IS NOT NULL AND NGAYCNNTK IS NOT NULL AND 
  NGAYBGDVQLSD BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYCNNTK BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYCNNTK <= NGAYBGDVQLSD)
  THEN (NGAYBGDVQLSD-NGAYCNNTK) ELSE 0 END PKHUC7,
CASE WHEN (NGAYCNNTK IS NOT NULL AND NGAYBGDVQLSD IS NOT NULL AND 
  NGAYBGDVQLSD BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYCNNTK BETWEEN  TO_DATE(CONCAT('01/01/', {0}), 'dd/MM/yyyy') AND TO_DATE(CONCAT('31/12/', {0}), 'dd/MM/yyyy') AND
  NGAYCNNTK <= NGAYBGDVQLSD)
  THEN 1 ELSE 0 END PKHUC7NUMBER
FROM MV_BCCHITIET WHERE {1} AND NVL(DVQLSD1,'*') IN( SELECT DSE_MRC FROM R5DEPARTMENTSECURITY WHERE DSE_USER =:Username))", request.Year, branchWhere);
            response.RepairDays =
                dbContext.Database.SqlQuery<SummaryByBranchAndByYearRepair>(sql, sqlParameters).FirstOrDefault();

            sqlParameters = new OracleParameter[1];
            sqlParameters[0] = new OracleParameter("Username", request.Username.ToUpper());
            /*sql = string.Format(@"SELECT 
NVL(SUM(CASE WHEN ((NGAYBGDVQLSD IS NULL OR (NGAYBGDVQLSD IS NOT NULL AND NGAYBGDVQLSD > SYSDATE )) AND NGAYCNNTK IS NOT NULL )	THEN 1	ELSE 0	END),0) AS InStock,
NVL(SUM(CASE WHEN(NGAYCNNTK IS NOT NULL AND(CODEKQTSC IS NULL OR CODEKQTSC = '1') AND(NGAYBGDT IS NULL OR(NGAYBGDT IS NOT NULL AND NGAYBGDT > SYSDATE))) THEN 1  ELSE 0  END),0) AS BeforeRepair,
NVL(SUM(CASE WHEN(NGAYBGDT IS NOT NULL AND(NGAYDTBG IS NULL OR(NGAYDTBG IS NOT NULL AND NGAYDTBG > SYSDATE)))   THEN 1  ELSE 0  END),0) AS InRepair,
NVL(SUM(CASE WHEN((CODEKQTSC IN('2', '5') AND NGAYBGDVQLSD IS NULL) OR(NGAYDTBG IS NOT NULL AND NGAYBGDVQLSD IS NULL))    THEN 1  ELSE 0  END),0) AS AfterRepair,
NVL(SUM(CASE WHEN((NGAYBGDVQLSD IS NULL OR(NGAYBGDVQLSD IS NOT NULL AND NGAYBGDVQLSD > SYSDATE)) AND NGAYCNNTK IS NOT NULL  AND(CODEKQTSC = 2 OR CODEKQSSC = 3))    THEN 1  ELSE 0  END),0) AS Good
FROM MV_BCCHITIET
WHERE {0}
AND NVL(DVQLSD1,'*') IN(SELECT DSE_MRC FROM R5DEPARTMENTSECURITY WHERE DSE_USER =:Username)", branchWhere);*/
            sql = string.Format(@"SELECT 
SUM(CASE WHEN (NGAYCNNTK IS NOT NULL AND NGAYBGDVQLSD IS NULL ) THEN 1  ELSE 0  END) AS InStock,
SUM(CASE WHEN (NGAYCNNTK IS NOT NULL AND NGAYBGDVQLSD IS NULL  AND (CODEKQTSC IS NULL OR CODEKQTSC='1') AND NGAYKTGPXSC IS NULL ) THEN 1  ELSE 0  END) AS BeforeRepair,
SUM(CASE WHEN (NGAYCNNTK IS NOT NULL AND NGAYBGDVQLSD IS NULL  AND NGAYKTGPXSC IS NOT NULL AND NGAYDTBG IS NULL) THEN 1  ELSE 0  END) AS InRepair,
SUM(CASE WHEN (NGAYCNNTK IS NOT NULL AND NGAYBGDVQLSD IS NULL  AND (CODEKQTSC IN ('2','5') OR NGAYDTBG IS NOT NULL))  THEN 1  ELSE 0  END) AS AfterRepair,
SUM(CASE WHEN (NGAYCNNTK IS NOT NULL AND NGAYBGDVQLSD IS NULL  AND (CODEKQTSC = 2 OR CODEKQSSC = 3))  THEN 1  ELSE 0  END) AS Good
FROM MV_BCCHITIET
WHERE {0}
AND NVL(DVQLSD1,'*') IN(SELECT DSE_MRC FROM R5DEPARTMENTSECURITY WHERE DSE_USER =:Username)", branchWhere);
            response.Stock =
                dbContext.Database.SqlQuery<SummaryByBranchAndByYearStock>(sql, sqlParameters).FirstOrDefault();

            return response;
        }
    }
}
