﻿using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IDeviceRepository
    {
        DeviceDetails GetDevice(string username, string serial);
        IPagedResult<DeviceSummary> GetDevices(DeviceQuery request);
        IPagedResult<DeviceSummary> GetAdvancedDevices(DeviceAdvancedQuery request);
    }
}
