﻿using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IOrderRepository
    {
        IPagedResult<OrderSummary> GetOrders(OrderQuery orderQuery);
        IPagedResult<OrderSummary> GetAwatingOrders(AwatingOrderQuery orderQuery);
        OrderDetails GetOrder(string orderCode);
        string GetCreatedUser(string orderCode);
    }
}
