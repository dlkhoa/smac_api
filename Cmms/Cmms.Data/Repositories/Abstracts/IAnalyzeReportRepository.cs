﻿using Cmms.Domain.Entities.DataContracts;
using System.Collections.Generic;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Queries;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IAnalyzeReportRepository
    {
        IEnumerable<DeviceStatisticByMonth> GetReceivedDeviceReportByYear(DeviceReportByYearQuery request);
        IEnumerable<DeviceStatisticByMonth> GetSentDeviceReportByYear(DeviceReportByYearQuery request);
        decimal GetCenterFulfillReportByYear(CenterFulfillReportByYearQuery request);
        decimal GetBranchFulfillReportByYear(BranchFulfillReportByYearQuery request);
        int GetCenterNumberOfReceivedDevicesReportByYear(CenterNumberOfDevicesReportByYearQuery request);
        int GetBranchNumberOfReceivedDevicesReportByYear(BranchNumberOfDevicesReportByYearQuery request);
        int GetCenterNumberOfSentDevicesReportByYear(CenterNumberOfDevicesReportByYearQuery request);
        int GetBranchNumberOfSentDevicesReportByYear(BranchNumberOfDevicesReportByYearQuery request);
        SummaryByBranchAndByYear GetSummaryByBranchAndByYear(SummaryByBranchAndByYearQuery request);
    }
}
