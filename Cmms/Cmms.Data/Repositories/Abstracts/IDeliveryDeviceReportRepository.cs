﻿using Cmms.Domain.Entities.DataContracts;
using System.Collections.Generic;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Queries;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IDeliveryDeviceReportRepository
    {
        IEnumerable<DeliveryDeviceStatisticByProvider> GetDeliveryDeviceStatisticProvider(DeviceReportQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByProvider(DeliveryDeviceReportByProviderQuery request);
        IEnumerable<DeliveryDeviceStatisticByDaiVT> GetDeviceStatisticByDVQLSD_Dai(DeviceReportQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVQLSD_Dai(DeliveryDeviceReportByDVQLSDDaiQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryAllLevel(DeviceReportAllLevelQuery request);
    }
}
