﻿using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using System.Collections.Generic;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface INotDeliveryDeviceReportRepository
    {
        IEnumerable<NotDeliveryDeviceStatisticByDTSC> GetStatisticByDTSC(DeviceReportQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDTSC(NotDeliveryDeviceReportByDTSCQuery request);
        IEnumerable<NotDeliveryDeviceStatisticByDVT> GetDeviceStatisticByDVT(DeviceReportQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVT(NotDeliveryDeviceReportByDVTQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryAllLevel(DeviceReportAllLevelQuery request);
    }
}
