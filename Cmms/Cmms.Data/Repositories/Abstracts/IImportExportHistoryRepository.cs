﻿using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IImportExportHistoryRepository
    {
        IPagedResult<ImportExportHistorySummary> GetHistories(ImportExportHistoryQuery query);
    }
}
