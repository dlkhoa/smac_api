﻿using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using System.Collections.Generic;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IRefusedHTKTDeviceReportRepository
    {
        IEnumerable<RefusedHTKTDeviceStatisticByProvider> GetDeliveryDeviceStatisticProvider(DeviceReportQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByProvider(RefusedHTKTDeviceReportByProviderQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryAllLevel(DeviceReportAllLevelQuery request);
    }
}
