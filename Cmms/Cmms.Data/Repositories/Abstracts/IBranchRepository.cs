﻿using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IBranchRepository
    {
        IPagedResult<Branch> GetBranches(BranchQuery branchQuery);
    }
}
