﻿using Cmms.Domain.Commands;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IOrderCommandRepository
    {
        void ApproveOrder(ApproveOrderCommand command);
        void UpdateOrderStatus(ApproveOrderCommand command);
    }
}
