﻿using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using System.Collections.Generic;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IRerepairDeviceReportRepository
    {
        IEnumerable<RerepairDeliveryDeviceStatisticByDTSC> GetStatisticByDTSC(DeviceReportQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDTSC(RerepairDeviceReportByDTSCQuery request);
        IEnumerable<RerepairDeviceStatisticByDVT> GetDeviceStatisticByDVT(DeviceReportQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVT(RerepairDeviceReportByDVTQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryAllLevel(DeviceReportAllLevelQuery request);
    }
}
