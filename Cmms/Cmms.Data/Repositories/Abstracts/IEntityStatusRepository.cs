﻿using Cmms.Domain.Entities;
using System.Collections.Generic;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IEntityStatusRepository
    {
        IEnumerable<EntityStatus> GetStatuses(string entityCode);
    }
}
