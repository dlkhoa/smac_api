﻿using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IPicklistRepository
    {
        IPagedResult<PicklistSummary> GetPicklists(PicklistQuery query);
        PicklistDetails GetPicklist(string picklistCode);
        IPagedResult<PicklistSummary> GetAwaitingPicklists(PicklistQuery picklistQuery);
        Picklist GetPicklistForUpdate(string code);
        string GetNewStatus(string username, string picklistStatus, string picklistStore);
        string GetCreatedUser(string picklistCode);
    }
}
