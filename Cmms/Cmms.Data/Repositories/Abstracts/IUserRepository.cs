﻿using System.Collections.Generic;
using Cmms.Domain.Entities;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IUserRepository
    {
        User GetUser(string username);
        string GetUserGroupCode(string username);
        List<string> GetUserManagers(string organization);
        List<string> Get_CN_SC_PT_Users(string organization);
        List<string> Get_LD_TP_Users(string organization);
        List<string> GetAccountingUsers(string organization);
        List<string> Get_NV_Users(string organization);
        List<string> GetOranizations(string username);
    }
}
