﻿using Cmms.Domain.Entities.DataContracts;
using System.Collections.Generic;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Queries;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IDocumentRepository
    {
        IEnumerable<DocumentDetails> GetDocumentList(string type, string orderCode);
    }
}
