﻿using Cmms.Domain.Entities.DataContracts;
using System.Collections.Generic;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IPicklistPartRepository
    {
        IEnumerable<PicklistPartSummary> GetPicklistPartsForPicklist(string picklistCode);
    }
}
