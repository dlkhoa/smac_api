﻿using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface ICompanyRepository
    {
        IPagedResult<DanhMucDoiTacSuaChua> LayDanhMucDoiTacSuaChua(CompanyQuery companyQuery);
        IPagedResult<DanhMucDVQLSDTB> LayDanhMucDVQLSDTB(CompanyQuery companyQuery);
        IPagedResult<CodeName> LayDanhMucLoaiThietbi(CompanyQuery companyQuery);
        IPagedResult<CodeName> LayDanhMucDaiVienthong(CompanyQuery companyQuery);
        IPagedResult<CodeName> LayDanhMucDonviQLSDTB(CompanyQuery companyQuery);
        IPagedResult<CodeName> LayDanhMucKetquaKiemtraTruocSC(CompanyQuery companyQuery);
        IPagedResult<CodeName> LayDanhMucKetquaKiemtraSauSC(CompanyQuery companyQuery);
        IPagedResult<CodeName> LayDanhMucTrangthaiThietbi(CompanyQuery companyQuery);
        IPagedResult<CodeName> LayDanhMucDoituongThietbi(CompanyQuery companyQuery);
        IPagedResult<CodeName> LayDanhMucChinhanh(CompanyQuery companyQuery);
        IPagedResult<CodeName> LayDanhMucNam(CompanyQuery companyQuery);
    }
}
