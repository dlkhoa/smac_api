﻿using Cmms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IOrganizationRepository
    {
        IEnumerable<Organization> GetBranches();
    }
}
