﻿using Cmms.Domain.Entities.DataContracts;
using System.Collections.Generic;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IOrderLineRepository
    {
        IEnumerable<OrderLineSummary> GetOrderLines(string orderCode);
    }
}
