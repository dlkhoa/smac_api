﻿using System.Collections.Generic;
using Cmms.Domain.Entities;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface INotificationRepository
    {
        MobileDevice GetDevice(string deviceId);
        bool IsExisted(string deviceId, string username);
        int AddNotificationDevice(MobileDevice mobileDevice);
        List<string> GetDeviceIDs(List<string> usernames);
        int DeleteMobileDevice(MobileDevice mobileDevice);
        IEnumerable<NotificationItem> GetUnsendNotifications();
        void AddNotificationItem(NotificationItem notificationItem);
        void UpdateNotificationStatus(string primaryId, string entityType, string toStatus);
        int DeleteAll();
    }
}
