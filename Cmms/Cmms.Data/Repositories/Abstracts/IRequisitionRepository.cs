﻿using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using System.Linq;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IRequisitionRepository
    {
        IPagedResult<RequisitionSummary> GetRequisitions(RequisitionQuery requisitionQuery);
        IPagedResult<RequisitionSummary> GetAwaitingRequisitions(RequisitionQuery requisitionQuery);
        RequisitionDetails GetRequisition(string requisitionCode);
        string GetCreatedUser(string requisitionCode);
    }
}
