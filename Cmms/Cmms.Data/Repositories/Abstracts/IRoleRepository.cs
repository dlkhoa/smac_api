﻿using Cmms.Domain.Entities;
using System.Collections.Generic;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IRoleRepository
    {
        IEnumerable<Role> GetRoleByGroupCode(string groupCode);
    }
}
