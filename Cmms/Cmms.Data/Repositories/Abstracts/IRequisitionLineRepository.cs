﻿using Cmms.Domain.Entities.DataContracts;
using System.Collections.Generic;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IRequisitionLineRepository
    {
        IEnumerable<RequisitionLineSummary> GetDevicesForRequisition(string username, string requisitionCode);
    }
}
