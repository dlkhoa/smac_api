﻿using System.Collections.Generic;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using Cmms.Data.Infrastructure.Paging;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IImportExportDeviceReportRepository
    {
        IPagedResult<ImportExportDeviceReport> GetAwaitingRepairDevices(ImportExportDeviceReportQuery request);
        IPagedResult<ImportExportDeviceReport> GetAfterRepairedDevices(ImportExportDeviceReportQuery request);
        IPagedResult<ImportExportFixingDeviceReport> GetFixingDevices(ImportExportDeviceReportQuery request);
    }
}
