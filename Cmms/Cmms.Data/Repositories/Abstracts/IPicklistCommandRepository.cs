﻿using Cmms.Domain.Commands;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IPicklistCommandRepository
    {
        //void ApprovePicklist(ApprovePicklistCommand command);
        void ApprovePicklist(string username, string picklistCode);
        void UpdatePicklistStatus(ApprovePicklistCommand command);
    }
}
