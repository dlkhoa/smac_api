﻿using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities;
using Cmms.Domain.Queries;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IProviderRepository
    {
        IPagedResult<Provider> GetProviders(ProviderQuery providerQuery);
    }
}
