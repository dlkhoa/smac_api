﻿using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Commands;
using Cmms.Domain.Entities;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries.DeviceRepair;
using System.Collections.Generic;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IDeviceRepairRepository
    {
        RepairDeviceDetails GetRepairDevice(RepairDeviceQuery request);
        RepairDeviceDetails GetRepairDeviceById(RepairDeviceQuery request);
        RepairDeviceDetails GetRepairedDevice(RepairDeviceQuery request);
        IEnumerable<string> GetDeviceNames();
        UserUnit GetUserUnit(string username);
        string AddRepairDevice(AddRepairDeviceCommand command);
        IEnumerable<string> GetCenterList(string username);
        IPagedResult<AvailableDeviceDetails> GetAvailableDeviceList(AvailableDeviceListQuery request);
        IPagedResult<PriorityRepairDeviceDetails> GetPriorityRepairDeviceList(PriorityRepairDeviceListQuery request);
        IPagedResult<RepairDeviceDetails> GetRepairDeviceList(RepairDeviceListQuery request);
        string ReceiveDeviceFromTVT(ReceiveDeviceFromTVTCommand command);
        string ReceiveDeviceFromTTDKSC(ReceiveDeviceFromTTDKSCCommand command);
        string DeliveryDevicesToTVT(DeliveryDevicesToTVTCommand command);
        IEnumerable<TVT> GetTVTList(string dvt);
        RepairDeviceSummarize SummarizeReport(DeviceRepairSummarizeReportQuery request);
        IPagedResult<RepairDeviceDetails> SummarizeListReport(DeviceRepairSummarizeReportQuery request);
        IEnumerable<WorkingDeviceDetails> GetWorkingDevices(string serial);
        string SetWorkingDate(SetWorkingDateCommand command);

        IEnumerable<RepairKPIReportItem> RepairKPIReport(RepairKPIReportQuery request);
        IEnumerable<RepairTimeReportItem> RepairTimeReport(RepairTimeReportQuery request);
        IEnumerable<RepairInventoryReportItem> RepairInventoryReport(RepairInventoryReportQuery request);
    }
}
