﻿using System.Collections.Generic;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IOverRepairTimeDeviceReportRepository
    {
        IEnumerable<OverRepairTimeDeviceStatisticDTSCByDTSC> GetStatisticByDoiTac(DeviceReportQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDTSC(OverRepairTimeDeviceReportByDTSCQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryAllLevel(DeviceReportAllLevelQuery request);
    }
}
