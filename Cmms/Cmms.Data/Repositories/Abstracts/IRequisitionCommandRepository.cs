﻿using Cmms.Domain.Commands;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IRequisitionCommandRepository
    {
        void ApproveRequisition(ApproveRequisitionCommand command);
        void UpdateRequisitionStatus(ApproveRequisitionCommand command);
    }
}
