﻿using Cmms.Domain.Entities.DataContracts;
using System.Collections.Generic;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Queries;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IInventoryDeviceReportRepository
    {
        IEnumerable<InventoryDeviceStatisticByLoaiTB> GetInventoryDeviceStatisticByLoaiThietBi(DeviceReportQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVQLSD_LoaiTB(InventoryDeviceReportByDVQLSDLoaiTBQuery request);
        IEnumerable<InventoryDeviceStatisticBPCByLHD> GetInventoryDeviceStatisticBPCByLHD(DeviceReportQuery request);
        IEnumerable<InventoryDeviceStatisticBPCByDVQLSD> GetInventoryDeviceStatisticBPCByDVQLSD(DeviceReportQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryAllLevel(DeviceReportAllLevelQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVQLSD(InventoryDeviceReportByDVQLSDQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByLoaiHD(InventoryDeviceReportByLoaiHDQuery request);
    }
}
