﻿using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using System.Collections.Generic;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IDeliveryGoodDeviceReportRepository
    {
        IEnumerable<DeliveryGoodDeviceStatisticByProvider> GetDeliveryDeviceStatisticProvider(DeviceReportQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByProvider(DeliveryDeviceReportByProviderQuery request);
        IEnumerable<DeliveryGoodDeviceStatisticByDaiVT> GetDeviceStatisticByDVQLSD_Dai(DeviceReportQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVQLSD_Dai(DeliveryDeviceReportByDVQLSDDaiQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryAllLevel(DeviceReportAllLevelQuery request);
    }
}
