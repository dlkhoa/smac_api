﻿using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using System.Collections.Generic;

namespace Cmms.Data.Repositories.Abstracts
{
    public interface IDeliverySLADeviceReportRepository
    {
        IEnumerable<DeliverySLADeviceStatisticByMonth> GetStatisticByMonth(DeviceReportQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByMonth(DeliverySLADeviceReportByMonthQuery request);
        IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryAllLevel(DeviceReportAllLevelQuery request);
    }
}
