﻿using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Oracle.ManagedDataAccess.Client;
using System.Linq;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Queries;
using System;

namespace Cmms.Data.Repositories
{
    public class DeviceRepository : BaseRepository, IDeviceRepository
    {
        public DeviceRepository(CmmsDbContext dbContext) : base(dbContext)
        {

        }

        public DeviceDetails GetDevice(string username, string serial)
        {
            var sql = @"SELECT o.obj_code serial,obj_udfchar01 Partnumber,obj_desc Tenthietbi, obj_class loaithietbi,
                                obj_udfchar20 NhaCungCap, d.des_text TrangThai,
                                r5o7.o7get_desc('VI','UCOD',obj_criticality,'OBCR', '') DoiTuongThietBi,
                                o.obj_org Chinhanh,obj_udfchkbox01 Thietbimoi, obj_udfchar04 DaiVienThong,
                                obj_udfchar16 GiaoTraDaiVienThong, obj_udfchar31 DVQLSD,obj_udfchar03 TinhtrangNhan,
                                obj_udfchar05 Ghichu,obj_udfchar29 NhaSX, obj_udfdate01 NgayTiepnhan,
                                obj_udfchar02 controlnumber,obj_udfchar07 SoHopDong,obj_udfchar08 KetQuaKtraTruocSuaChua,
                                obj_udfchar09 TrangthaiTruocSuaChua,obj_udfchar10 Doitac,obj_udfdate02 NgayBanGiaoDoiTac,
                                obj_udfchar12 NoidungSuaChua,obj_udfchar13 SerialMoiSauSuaChua, obj_udfdate03 NgayDoiTacBanGiao,
                                obj_udfchar14 KiemtraSauSuaChua,obj_udfchar15 TinhtrangSauSuaChua,obj_udfdate04 NgayXuatKhoChoDVQLSD,
                                obj_udfnum03 SoLanHong
                        FROM r5permissions,r5descriptions d,r5userorganization,r5organization,r5objects o
	                        LEFT OUTER JOIN r5reliabilityrankings ON ( rrk_code = obj_reliabilityranking 
												                        AND rrk_org = obj_reliabilityranking_org
												                        AND rrk_revision = obj_reliabilityrankingrev )
	                        LEFT OUTER JOIN r5classes ON (cls_code = obj_class 
									                        AND cls_org = obj_class_org
									                        AND cls_rentity = 'OBJ')
                        WHERE uog_user=:Username0 
	                        AND uog_org=o.obj_org
	                        AND org_code = o.obj_org
	                        AND uog_group = prm_group
	                        AND prm_select = '?'
	                        AND prm_function='OSOBJA'
	                        AND o.obj_obrtype ='A' 
	                        AND ( EXISTS( SELECT 'x'
					                        FROM r5fieldfiltertype
					                        WHERE fft_function = 'OSOBJA'
						                        AND o.obj_obtype = fft_type )
		                        OR NOT EXISTS( SELECT 'x'
				                        FROM r5fieldfiltertype
				                        WHERE fft_function ='OSOBJA' ) )
	                        AND ( EXISTS( SELECT 'x'
					                        FROM r5fieldfilterclass
					                        WHERE flc_function = 'OSOBJA' 
						                        AND o.obj_class = flc_class
						                        AND o.obj_class_org = flc_class_org)
		                        OR

			                        NOT EXISTS( SELECT 'x'
						                        FROM r5fieldfilterclass
						                        WHERE flc_function = 'OSOBJA' ) )
	                        AND d.des_lang = 'VI'
	                        AND d.des_rentity = 'UCOD' 
	                        AND d.des_code = o.obj_status
	                        AND d.des_rtype = 'OBST'
	                        AND ('OFF' ='OFF' OR obj_mrc IN( SELECT dse_mrc 
									                        FROM r5departmentsecurity 
									                        WHERE dse_user =:Username1 AND dse_mrc = obj_mrc))

	                        AND NOT (o.obj_rstatus = 'D' AND o.obj_status = 'EO' )
	                        AND (nvl(obj_udfchar31,'*') IN( SELECT dse_mrc 
									                        FROM r5departmentsecurity 
									                        WHERE dse_user = :Username2))
	                        AND OBJ_CODE=:Serial";

            var sqlParameters = new OracleParameter[4];
            sqlParameters[0] = new OracleParameter("Username0", username.ToUpper());
            sqlParameters[1] = new OracleParameter("Username1", username.ToUpper());
            sqlParameters[2] = new OracleParameter("Username2", username.ToUpper());
            sqlParameters[3] = new OracleParameter("Serial", serial.ToUpper());

            return dbContext.Database.SqlQuery<DeviceDetails>(sql, sqlParameters)
                .FirstOrDefault();
        }

        public IPagedResult<DeviceSummary> GetDevices(DeviceQuery deviceQuery)
        {
            var sql = @"SELECT o.obj_code serial,obj_desc Tenthietbi, d.des_text TrangThai, o.obj_org Chinhanh
                        FROM r5permissions,r5descriptions d,r5userorganization,r5organization,r5objects o
	                        LEFT OUTER JOIN r5reliabilityrankings ON ( rrk_code = obj_reliabilityranking 
												                        AND rrk_org = obj_reliabilityranking_org
												                        AND rrk_revision = obj_reliabilityrankingrev )
	                        LEFT OUTER JOIN r5classes ON (cls_code = obj_class 
									                        AND cls_org = obj_class_org
									                        AND cls_rentity = 'OBJ')
                        WHERE uog_user=:Username0 
	                        AND uog_org=o.obj_org
	                        AND org_code = o.obj_org
	                        AND uog_group = prm_group
	                        AND prm_select = '?'
	                        AND prm_function='OSOBJA'
	                        AND o.obj_obrtype ='A' 
	                        AND ( EXISTS( SELECT 'x'
					                        FROM r5fieldfiltertype
					                        WHERE fft_function = 'OSOBJA'
						                        AND o.obj_obtype = fft_type )
		                        OR NOT EXISTS( SELECT 'x'
				                        FROM r5fieldfiltertype
				                        WHERE fft_function ='OSOBJA' ) )
	                        AND ( EXISTS( SELECT 'x'
					                        FROM r5fieldfilterclass
					                        WHERE flc_function = 'OSOBJA' 
						                        AND o.obj_class = flc_class
						                        AND o.obj_class_org = flc_class_org)
		                        OR

			                        NOT EXISTS( SELECT 'x'
						                        FROM r5fieldfilterclass
						                        WHERE flc_function = 'OSOBJA' ) )
	                        AND d.des_lang = 'VI'
	                        AND d.des_rentity = 'UCOD' 
	                        AND d.des_code = o.obj_status
	                        AND d.des_rtype = 'OBST'
	                        AND ('OFF' ='OFF' OR obj_mrc IN( SELECT dse_mrc 
									                        FROM r5departmentsecurity 
									                        WHERE dse_user =:Username1 AND dse_mrc = obj_mrc))

	                        AND NOT (o.obj_rstatus = 'D' AND o.obj_status = 'EO' )
	                        AND (nvl(obj_udfchar31,'*') IN( SELECT dse_mrc 
									                        FROM r5departmentsecurity 
									                        WHERE dse_user = :Username2))
	                        AND OBJ_CODE  LIKE '%'||:Serial||'%'
                            AND obj_org like '%' ||:Branch || '%'
                        ORDER BY o.obj_code ASC, o.obj_org ASC";

            var sqlParameters = new OracleParameter[5];
            sqlParameters[0] = new OracleParameter("Username0", deviceQuery.Username.ToUpper());
            sqlParameters[1] = new OracleParameter("Username1", deviceQuery.Username.ToUpper());
            sqlParameters[2] = new OracleParameter("Username2", deviceQuery.Username.ToUpper());
            sqlParameters[3] = new OracleParameter("Serial", deviceQuery.Serial.ToUpper());
            sqlParameters[4] = new OracleParameter("Branch", deviceQuery.Branch.ToUpper());

            return new SqlPagedResult<DeviceSummary>(dbContext, sql, deviceQuery.Page, deviceQuery.RecordsPerPage, sqlParameters);
        }

        public IPagedResult<DeviceSummary> GetAdvancedDevices(DeviceAdvancedQuery deviceQuery)
        {
            var sql = @"SELECT o.obj_code serial,obj_desc Tenthietbi, d.des_text TrangThai, o.obj_org Chinhanh
                        FROM r5permissions,r5descriptions d,r5userorganization,r5organization,r5objects o
	                        LEFT OUTER JOIN r5reliabilityrankings ON ( rrk_code = obj_reliabilityranking 
												                        AND rrk_org = obj_reliabilityranking_org
												                        AND rrk_revision = obj_reliabilityrankingrev )
	                        LEFT OUTER JOIN r5classes ON (cls_code = obj_class 
									                        AND cls_org = obj_class_org
									                        AND cls_rentity = 'OBJ')
                        WHERE uog_user=:Username0 
	                        AND uog_org=o.obj_org
	                        AND org_code = o.obj_org
	                        AND uog_group = prm_group
	                        AND prm_select = '?'
	                        AND prm_function='OSOBJA'
	                        AND o.obj_obrtype ='A' 
	                        AND ( EXISTS( SELECT 'x'
					                        FROM r5fieldfiltertype
					                        WHERE fft_function = 'OSOBJA'
						                        AND o.obj_obtype = fft_type )
		                        OR NOT EXISTS( SELECT 'x'
				                        FROM r5fieldfiltertype
				                        WHERE fft_function ='OSOBJA' ) )
	                        AND ( EXISTS( SELECT 'x'
					                        FROM r5fieldfilterclass
					                        WHERE flc_function = 'OSOBJA' 
						                        AND o.obj_class = flc_class
						                        AND o.obj_class_org = flc_class_org)
		                        OR

			                        NOT EXISTS( SELECT 'x'
						                        FROM r5fieldfilterclass
						                        WHERE flc_function = 'OSOBJA' ) )
	                        AND d.des_lang = 'VI'
	                        AND d.des_rentity = 'UCOD' 
	                        AND d.des_code = o.obj_status
	                        AND d.des_rtype = 'OBST'
	                        AND ('OFF' ='OFF' OR obj_mrc IN( SELECT dse_mrc 
									                        FROM r5departmentsecurity 
									                        WHERE dse_user =:Username1 AND dse_mrc = obj_mrc))

	                        AND NOT (o.obj_rstatus = 'D' AND o.obj_status = 'EO' )
	                        AND (nvl(obj_udfchar31,'*') IN( SELECT dse_mrc 
									                        FROM r5departmentsecurity 
									                        WHERE dse_user = :Username2))
	                        AND OBJ_CODE LIKE '%'||:Serial||'%'
	                        AND UPPER(obj_desc) LIKE '%'||:Name||'%'
	                        AND UPPER(NVL(obj_udfchar01, ' ')) LIKE '%'||:CodePartNumber||'%'
	                        AND UPPER(obj_status) LIKE '%'||:Status||'%'
	                        AND UPPER(NVL(obj_udfchar20, ' ')) LIKE '%'||:Provider||'%'
                            AND UPPER(obj_criticality) LIKE '%'||:Object||'%'
                            AND UPPER(OBJ_CLASS) LIKE '%'||:Classify||'%'
                            AND UPPER(obj_org) LIKE '%'||:Branch||'%'
	                        AND UPPER(NVL(OBJ_UDFCHAR04, ' ')) LIKE '%'||:Station||'%'
	                        AND NVL(OBJ_UDFCHAR09, ' ') LIKE '%'||:InputTestResult||'%'
	                        AND NVL(OBJ_UDFCHAR15, ' ') LIKE '%'||:OutputTestResult||'%'
	                        AND UPPER(NVL(OBJ_UDFCHAR10, ' ')) LIKE '%'||:RepairPartner||'%'
                            AND UPPER(OBJ_UDFCHAR31) LIKE '%'||:DVQLSD||'%'
                        ORDER BY o.obj_code ASC, o.obj_org ASC";
            
            var sqlParameters = new OracleParameter[16];
            sqlParameters[0] = new OracleParameter("Username0", deviceQuery.Username.ToUpper());
            sqlParameters[1] = new OracleParameter("Username1", deviceQuery.Username.ToUpper());
            sqlParameters[2] = new OracleParameter("Username2", deviceQuery.Username.ToUpper());
            sqlParameters[3] = new OracleParameter("Serial", deviceQuery.Serial.ToUpper());
            sqlParameters[4] = new OracleParameter("Name", deviceQuery.Name.ToUpper());
            sqlParameters[5] = new OracleParameter("CodePartNumber", deviceQuery.CodePartNumber.ToUpper());
            sqlParameters[6] = new OracleParameter("Status", deviceQuery.Status.ToUpper());
            sqlParameters[7] = new OracleParameter("Provider", deviceQuery.Provider.ToUpper());
            sqlParameters[8] = new OracleParameter("Object", deviceQuery.Object.ToUpper());
            sqlParameters[9] = new OracleParameter("Classify", deviceQuery.Classify.ToUpper());
            sqlParameters[10] = new OracleParameter("Branch", deviceQuery.Branch.ToUpper());
            sqlParameters[11] = new OracleParameter("Station", deviceQuery.Station.ToUpper());
            sqlParameters[12] = new OracleParameter("InputTestResult", deviceQuery.InputTestResult);
            sqlParameters[13] = new OracleParameter("OutputTestResult", deviceQuery.OutputTestResult);
            sqlParameters[14] = new OracleParameter("RepairPartner", deviceQuery.RepairPartner.ToUpper());
            sqlParameters[15] = new OracleParameter("DVQLSD", deviceQuery.DVQLSD.ToUpper());

            return new SqlPagedResult<DeviceSummary>(dbContext, sql, deviceQuery.Page, deviceQuery.RecordsPerPage, sqlParameters);
        }
    }
}
