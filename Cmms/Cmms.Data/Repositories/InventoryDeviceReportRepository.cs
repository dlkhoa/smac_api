﻿using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Entities.Views;
using System.Collections.Generic;
using System.Linq;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Domain.Queries;
using System;
using Oracle.ManagedDataAccess.Client;
using MBoL.Utils;

namespace Cmms.Data.Repositories
{
    public class InventoryDeviceReportRepository : BaseRepository, IInventoryDeviceReportRepository
    {
        private readonly Dictionary<string, Func<InventoryDeviceReportByLoaiHDQuery, IPagedResult<DeviceDetailsReportSummary>>> GetDeviceReportSummaryByLoaiHDHandler;
        private readonly Dictionary<string, Func<InventoryDeviceReportByDVQLSDQuery, IPagedResult<DeviceDetailsReportSummary>>> GetDeviceReportSummaryByDVQLSDHandler;

        public InventoryDeviceReportRepository(CmmsDbContext dbContext) : base(dbContext)
        {
            GetDeviceReportSummaryByLoaiHDHandler = new Dictionary<string, Func<InventoryDeviceReportByLoaiHDQuery, IPagedResult<DeviceDetailsReportSummary>>>
            {
                { "SoLuongChuaTest".ToUpper(),GetDeviceReportSummaryByLoaiHD_SLNOTEST },
                { "SoLuongChoSuaChua".ToUpper(),GetDeviceReportSummaryByLoaiHD_SLCHOSC },
                { "SoLuongNFFChuaGiao".ToUpper(),GetDeviceReportSummaryByLoaiHD_SLNFFCHUAGIAO },
                { "SoLuongTotChuaGiao".ToUpper(),GetDeviceReportSummaryByLoaiHD_SLTOTCHUAGIAO },
                { "soLuongKhongTheSuaChuaGiao".ToUpper(),GetDeviceReportSummaryByLoaiHD_SLKTSCCHUAGIAO }
            };

            GetDeviceReportSummaryByDVQLSDHandler = new Dictionary<string, Func<InventoryDeviceReportByDVQLSDQuery, IPagedResult<DeviceDetailsReportSummary>>>
            {
                { "SoLuongChuaTest".ToUpper(),GetDeviceReportSummaryByDVQLSD_SLNOTEST },
                { "SoLuongChoSuaChua".ToUpper(),GetDeviceReportSummaryByDVQLSD_SLCHOSC },
                { "SoLuongNFFChuaGiao".ToUpper(),GetDeviceReportSummaryByDVQLSD_SLNFFCHUAGIAO },
                { "SoLuongTotChuaGiao".ToUpper(),GetDeviceReportSummaryByDVQLSD_SLTOTCHUAGIAO },
                { "SoLuongKhongTheSuaChuaGiao".ToUpper(),GetDeviceReportSummaryByDVQLSD_SLKTSCCHUAGIAO }
            };
        }
        
        public IEnumerable<InventoryDeviceStatisticByLoaiTB> GetInventoryDeviceStatisticByLoaiThietBi(DeviceReportQuery request)
        {
            /*var query = GetDbEntity<DeviceReportView>()
                .Where(d => ((d.NgayCNNTK >= request.StartDate && d.NgayCNNTK <= request.EndDate && d.NgayBGDT == null)
                    || (d.NgayDTBG >= request.StartDate && d.NgayDTBG <= request.EndDate && d.NgayBanGiaoDVQLSD == null)) && request.Organizations.Contains(d.ChiNhanh))
                .GroupBy(d => new { d.ChiNhanh, d.DVQLSD, d.LoaiTB })
                .Select(d => new InventoryDeviceStatisticByLoaiTB
                {
                    ChiNhanh = d.Key.ChiNhanh,
                    DVQLSD = d.Key.DVQLSD,
                    LoaiTB = d.Key.LoaiTB,
                    Tong = d.Count()
                });

            return query.ToArray();*/
            var orgs = StringHelper.MakeOrgSearch(request.Organizations);
            var sql = string.Format(@"
                SELECT
                    chinhanh,
                    dvqlsd1 DVQLSD,
                    loaitb,
                    COUNT(1) tong
                FROM
                    mv_bcchitiet
                WHERE
                    ngaycnntk <= TO_DATE('{1}', 'YYYY-MM-DD')
                    AND (ngaybgdvqlsd IS NULL OR (ngaybgdvqlsd IS NOT NULL AND trunc(ngaybgdvqlsd) > TO_DATE('{1}', 'YYYY-MM-DD')))
                    AND chinhanh IN ({0})
                GROUP BY
                    chinhanh,
                    dvqlsd1,
                    loaitb"
                , orgs, request.EndDate.ToString("yyyy-MM-dd"));
            return dbContext.Database.SqlQuery<InventoryDeviceStatisticByLoaiTB>(sql).ToArray();
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVQLSD_LoaiTB(InventoryDeviceReportByDVQLSDLoaiTBQuery request)
        {
            /*var query = GetDbEntity<DeviceReportView>()
                .Where(d => d.ChiNhanh.ToUpper() == request.ChiNhanh.ToUpper()
                    && d.DVQLSD.ToUpper() == request.DVQLSD.ToUpper()
                    && d.LoaiTB.ToUpper() == request.LoaiTB.ToUpper()
                    && ((d.NgayCNNTK >= request.StartDate && d.NgayCNNTK <= request.EndDate && d.NgayBGDT == null)
                    || (d.NgayDTBG >= request.StartDate && d.NgayDTBG <= request.EndDate && d.NgayBanGiaoDVQLSD == null)))
                .OrderByDescending(d => d.ReqCode)
                .ThenBy(d => d.Serial)
                .Select(d => new DeviceDetailsReportSummary
                {
                    Serial = d.Serial,
                    TenThietBi = d.TenThietBi,
                    TinhTrang = d.TinhTrang
                });

            return new PagedResult<DeviceDetailsReportSummary>(query, request.Page, request.RecordsPerPage);*/
            var sql = string.Format(@"
                SELECT
                    serial,
                    tentb TenThietBi,
                    TinhTrang
                FROM
                    mv_bcchitiet
                WHERE
                    ngaycnntk <= TO_DATE('{0}', 'YYYY-MM-DD')
                    AND (ngaybgdvqlsd IS NULL OR (ngaybgdvqlsd IS NOT NULL AND trunc(ngaybgdvqlsd) > TO_DATE('{0}', 'YYYY-MM-DD')))
                    AND UPPER(chinhanh) = :ChiNhanh
                    AND UPPER(dvqlsd1) = :DVQLSD
                    AND UPPER(loaitb) = :LoaiTB"
                , request.EndDate.ToString("yyyy-MM-dd"));
            var query = dbContext.Database.SqlQuery<DeviceDetailsReportSummary>(sql,
                new OracleParameter("ChiNhanh", request.ChiNhanh.ToUpper()),
                new OracleParameter("DVQLSD", request.DVQLSD.ToUpper()),
                new OracleParameter("LoaiTB", request.LoaiTB.ToUpper())).AsQueryable();
            return new PagedResult<DeviceDetailsReportSummary>(query, request.Page, request.RecordsPerPage);
        }

        public IEnumerable<InventoryDeviceStatisticBPCByLHD> GetInventoryDeviceStatisticBPCByLHD(DeviceReportQuery request)
        {
            var orgs = StringHelper.MakeOrgSearch(request.Organizations);
            /*var sql = string.Format(@"SELECT CHINHANH
                          ,LOAIHD
                          ,SLNOTEST SoLuongChuaTest,SLCHOSC SoLuongChoSuaChua,SLNFFCHUAGIAO SoLuongNFFChuaGiao,SLTOTCHUAGIAO SoLuongTotChuaGiao,SLKTSCCHUAGIAO SoLuongKiemTraSSCChuaGiao
                        FROM (
                          SELECT CHINHANH,LOAIHD
                          ,SUM(CASE 
                                WHEN TINHTRANGBANGIAO = 'TN'
                                  AND TRUNC(NGAYDTBG) < TO_DATE(:EndDate1, 'YYYY-MM-DD')
                                  AND (
                                    TRUNC(NGAYHTKTSSC) IS NULL
                                    OR TRUNC(NGAYHTKTSSC) > TO_DATE(:EndDate2, 'YYYY-MM-DD')
                                    )
                                  THEN 1
                                ELSE 0
                                END) SLNOTEST
 
                            ,(SUM(CASE 
                                WHEN (
                                    CODEKQTSC = '1'
                                    AND TRUNC(NGAYHTKTTSC) BETWEEN TO_DATE(:StartDate1, 'YYYY-MM-DD')
                                      AND TO_DATE(:EndDate3, 'YYYY-MM-DD')
                                    )
                                  AND (
                                    NGAYBGDT IS NULL
                                    OR TRUNC(NGAYBGDT) > TO_DATE(:EndDate4, 'YYYY-MM-DD')
                                    )
                                  THEN 1
                                ELSE 0
                                END) + 
                            SUM(CASE WHEN (TRUNC(NGAYCNNTK) BETWEEN TO_DATE(:StartDate2, 'YYYY-MM-DD')
                                      AND TO_DATE(:EndDate5, 'YYYY-MM-DD'))
                                      AND (NGAYHTKTTSC IS NULL  OR TRUNC(NGAYHTKTTSC) > TO_DATE(:EndDate6, 'YYYY-MM-DD'))
                                  THEN 1
                                  ELSE 0
    
                             END)) SLCHOSC
  
                           ,SUM(CASE 
                                WHEN CODEKQTSC = '2'
                                  AND (
                                    NGAYBGDVQLSD IS NULL
                                    OR TRUNC(NGAYBGDVQLSD) NOT BETWEEN TO_DATE(:StartDate3, 'YYYY-MM-DD')
                                      AND TO_DATE(:EndDate7, 'YYYY-MM-DD')
                                    )
                                  THEN 1
                                ELSE 0
                                END) SLNFFCHUAGIAO
    
                            ,SUM(CASE 
                                WHEN CODEKQSSC = '3'
                                  AND TRUNC(NGAYHTKTSSC) BETWEEN TO_DATE(:StartDate4, 'YYYY-MM-DD')
                                    AND TO_DATE(:EndDate8, 'YYYY-MM-DD')
                                  AND (
                                    NGAYBGDVQLSD IS NULL
                                    OR TRUNC(NGAYBGDVQLSD) NOT BETWEEN TO_DATE(:StartDate5, 'YYYY-MM-DD')
                                      AND TO_DATE(:EndDate9, 'YYYY-MM-DD')
                                    )
                                  THEN 1
                                ELSE 0
                                END) SLTOTCHUAGIAO
    
                            ,SUM(CASE 
                                WHEN (
                                    (
                                      CODEKQTSC = '5'
                                      AND NGAYBGDT IS NULL
                                      )
                                    OR CODEKQSSC = '5'
                                    )
                                  AND 
                              (
                                    NGAYBGDVQLSD IS NULL
                                    OR TRUNC(NGAYBGDVQLSD) NOT BETWEEN TO_DATE(:StartDate6, 'YYYY-MM-DD')
                                      AND TO_DATE(:EndDate10, 'YYYY-MM-DD')
                                    )
                                  THEN 1
                                ELSE 0
                                END) SLKTSCCHUAGIAO

                          FROM (
                            SELECT chinhanh
                            ,CASE 
                                WHEN CODEDTTB IN (
                                    '3'
                                    ,'5'
                                    )
                                  THEN 'NONSLA'
                                WHEN codedttb = '1'
                                  THEN 'SLA'
                                ELSE ''
                                END LOAIHD
                              ,NGAYCNNTK
                              ,CODEKQTSC
                              ,NGAYBGDT
                              ,CODEKQSSC
                              ,NGAYBGDVQLSD
                              ,TINHTRANGBANGIAO
                              ,NGAYDTBG
                              ,NGAYHTKTSSC
                              ,NGAYHTKTTSC
                            FROM MV_BCCHITIET WHERE chinhanh IN ({0})
                            )
                          GROUP BY chinhanh,LOAIHD
                          )", orgs);
            return dbContext.Database.SqlQuery<InventoryDeviceStatisticBPCByLHD>(sql,
                new OracleParameter("EndDate1", request.EndDate.ToString("yyyy-MM-dd")),
                new OracleParameter("EndDate2", request.EndDate.ToString("yyyy-MM-dd")),
                new OracleParameter("StartDate1", request.StartDate.ToString("yyyy-MM-dd")),
                new OracleParameter("EndDate3", request.EndDate.ToString("yyyy-MM-dd")),
                new OracleParameter("EndDate4", request.EndDate.ToString("yyyy-MM-dd")),
                new OracleParameter("StartDate2", request.StartDate.ToString("yyyy-MM-dd")),
                new OracleParameter("EndDate5", request.EndDate.ToString("yyyy-MM-dd")),
                new OracleParameter("EndDate6", request.EndDate.ToString("yyyy-MM-dd")),
                new OracleParameter("StartDate3", request.StartDate.ToString("yyyy-MM-dd")),
                new OracleParameter("EndDate7", request.EndDate.ToString("yyyy-MM-dd")),
                new OracleParameter("StartDate4", request.StartDate.ToString("yyyy-MM-dd")),
                new OracleParameter("EndDate8", request.EndDate.ToString("yyyy-MM-dd")),
                new OracleParameter("StartDate5", request.StartDate.ToString("yyyy-MM-dd")),
                new OracleParameter("EndDate9", request.EndDate.ToString("yyyy-MM-dd")),
                new OracleParameter("StartDate6", request.StartDate.ToString("yyyy-MM-dd")),
                new OracleParameter("EndDate10", request.EndDate.ToString("yyyy-MM-dd"))).ToArray();*/
            var sql = string.Format(@"
                SELECT CHINHANH
	                ,LOAIHD
                    ,SLNOTEST SoLuongChuaTest
                    ,SLCHOSC SoLuongChoSuaChua
                    ,SLNFFCHUAGIAO SoLuongNFFChuaGiao
                    ,SLTOTCHUAGIAO SoLuongTotChuaGiao
                    ,SLKTSCCHUAGIAO SoLuongKiemTraSSCChuaGiao
                FROM (
	                SELECT CHINHANH
		                ,LOAIHD
		                ,SUM(CASE 
				                WHEN NGAYCNNTK IS NOT NULL
					                AND NGAYCNNTK <= TO_DATE('{1}', 'YYYY-MM-DD')
					                AND (
						                CODEKQTSC IS NULL
						                OR CODEKQTSC = '1'
						                )
					                AND (
						                NGAYKTGPXSC IS NULL
						                OR (
							                NGAYKTGPXSC IS NOT NULL
							                AND NGAYKTGPXSC > TO_DATE('{1}', 'YYYY-MM-DD')
							                )
						                )
					                THEN 1
				                ELSE 0
				                END) SLNOTEST -- Ton tren truoc sua chua: SL đang test, chưa sửa:
		                ,SUM(CASE 
				                WHEN NGAYKTGPXSC IS NOT NULL
					                AND NGAYKTGPXSC <= TO_DATE('{1}', 'YYYY-MM-DD')
					                AND (
						                CODEKQSSC = '4'
						                OR CODEKQSSC IS NULL
						                )
					                AND (
						                NGAYBGDVQLSD IS NULL
						                OR (
							                NGAYBGDVQLSD IS NOT NULL
							                AND TRUNC(NGAYBGDVQLSD) > TO_DATE('{1}', 'YYYY-MM-DD')
							                )
						                )
					                THEN 1
				                ELSE 0
				                END) SLCHOSC -- SL đang sửa:
		                ,SUM(CASE 
				                WHEN CODEKQTSC = '2'
					                AND TRUNC(NGAYHTKTTSC) <= TO_DATE('{1}', 'YYYY-MM-DD')
					                AND (
						                NGAYBGDVQLSD IS NULL
						                OR (
							                NGAYBGDVQLSD IS NOT NULL
							                AND TRUNC(NGAYBGDVQLSD) > TO_DATE('{1}', 'YYYY-MM-DD')
							                )
						                )
					                THEN 1
				                ELSE 0
				                END) SLNFFCHUAGIAO
		                ,SUM(CASE 
				                WHEN CODEKQSSC = '3'
					                AND TRUNC(NGAYHTKTSSC) <= TO_DATE('{1}', 'YYYY-MM-DD')
					                AND (
						                NGAYBGDVQLSD IS NULL
						                OR (
							                NGAYBGDVQLSD IS NOT NULL
							                AND TRUNC(NGAYBGDVQLSD) > TO_DATE('{1}', 'YYYY-MM-DD')
							                )
						                )
					                THEN 1
				                ELSE 0
				                END) SLTOTCHUAGIAO
		                ,SUM(CASE 
				                WHEN (
						                (
							                CODEKQTSC = '5'
							                AND TRUNC(NGAYHTKTTSC) <= TO_DATE('{1}', 'YYYY-MM-DD')
							                AND NGAYKTGPXSC IS NULL
							                )
						                OR (
							                CODEKQSSC = '5'
							                AND TRUNC(NGAYHTKTTSC) <= TO_DATE('{1}', 'YYYY-MM-DD')
							                )
						                )
					                AND (
						                NGAYBGDVQLSD IS NULL
						                OR (
							                NGAYBGDVQLSD IS NOT NULL
							                AND TRUNC(NGAYBGDVQLSD) > TO_DATE('{1}', 'YYYY-MM-DD')
							                )
						                )
					                THEN 1
				                ELSE 0
				                END) SLKTSCCHUAGIAO
	                FROM (
		                SELECT chinhanh
			                ,CASE 
				                WHEN CODEDTTB IN (
						                '3'
						                ,'4'
						                ,'5'
						                ,'6'
						                ,'7'
						                ,'8'
						                )
					                THEN 'NONSLA'
				                WHEN codedttb IN (
						                '1'
						                ,'2'
						                )
					                THEN 'SLA'
				                END LOAIHD
			                ,NGAYCNNTK
			                ,CODEKQTSC
			                ,NGAYKTGPXSC
			                ,CODEKQSSC
			                ,NGAYBGDVQLSD
			                ,TINHTRANGBANGIAO
			                ,NGAYDTBG
			                ,NGAYHTKTSSC
			                ,NGAYHTKTTSC
		                FROM MV_BCCHITIET
		                WHERE CODEDTTB IN (
				                '1'
				                ,'2'
				                ,'3'
				                ,'4'
				                ,'5'
				                ,'6'
				                ,'7'
				                ,'8'
				                )
                             AND chinhanh IN ({0})
		                )
	                GROUP BY chinhanh
		                ,LOAIHD
	                ORDER BY chinhanh
		                ,LOAIHD
	                )
                ", orgs, request.EndDate.ToString("yyyy-MM-dd"));
            return dbContext.Database.SqlQuery<InventoryDeviceStatisticBPCByLHD>(sql).ToArray();
        }

        public IEnumerable<InventoryDeviceStatisticBPCByDVQLSD> GetInventoryDeviceStatisticBPCByDVQLSD(DeviceReportQuery request)
        {
            /*var orgs = StringHelper.MakeOrgSearch(request.Organizations);
            var sql = string.Format(@"SELECT CHINHANH,DVQLSD,SLNOTEST SoLuongChuaTest,SLCHOSC SoLuongChoSuaChua,SLNFFCHUAGIAO SoLuongNFFChuaGiao,SLTOTCHUAGIAO SoLuongTotChuaGiao,SLKTSCCHUAGIAO SoLuongKiemTraSSCChuaGiao
                        FROM (
                          SELECT CHINHANH,DVQLSD
                          ,SUM(CASE 
                                WHEN TINHTRANGBANGIAO = 'TN'
                                  AND TRUNC(NGAYDTBG) < TO_DATE(:EndDate1, 'YYYY-MM-DD')
                                  AND (
                                    TRUNC(NGAYHTKTSSC) IS NULL
                                    OR TRUNC(NGAYHTKTSSC) > TO_DATE(:EndDate2, 'YYYY-MM-DD')
                                    )
                                  THEN 1
                                ELSE 0
                                END) SLNOTEST
                            ,(SUM(CASE 
                                WHEN (
                                    CODEKQTSC = '1'
                                    AND TRUNC(NGAYHTKTTSC) BETWEEN TO_DATE(:StartDate1, 'YYYY-MM-DD')
                                      AND TO_DATE(:EndDate3, 'YYYY-MM-DD')
                                    )
                                  AND (
                                    NGAYBGDT IS NULL
                                    OR TRUNC(NGAYBGDT) > TO_DATE(:EndDate4, 'YYYY-MM-DD')
                                    )
                                  THEN 1
                                ELSE 0
                                END) + 
                            SUM(CASE WHEN (TRUNC(NGAYCNNTK) BETWEEN TO_DATE(:StartDate2, 'YYYY-MM-DD')
                                      AND TO_DATE(:EndDate5, 'YYYY-MM-DD'))
                                      AND (NGAYHTKTTSC IS NULL  OR TRUNC(NGAYHTKTTSC) > TO_DATE(:EndDate6, 'YYYY-MM-DD'))
                                  THEN 1
                                  ELSE 0
                             END)) SLCHOSC
                           ,SUM(CASE 
                                WHEN CODEKQTSC = '2'
                                  AND (
                                    NGAYBGDVQLSD IS NULL
                                    OR TRUNC(NGAYBGDVQLSD) NOT BETWEEN TO_DATE(:StartDate3, 'YYYY-MM-DD')
                                      AND TO_DATE(:EndDate7, 'YYYY-MM-DD')
                                    )
                                  THEN 1
                                ELSE 0
                                END) SLNFFCHUAGIAO
                            ,SUM(CASE 
                                WHEN CODEKQSSC = '3'
                                  AND TRUNC(NGAYHTKTSSC) BETWEEN TO_DATE(:StartDate4, 'YYYY-MM-DD')
                                    AND TO_DATE(:EndDate8, 'YYYY-MM-DD')
                                  AND (
                                    NGAYBGDVQLSD IS NULL
                                    OR TRUNC(NGAYBGDVQLSD) NOT BETWEEN TO_DATE(:StartDate5, 'YYYY-MM-DD')
                                      AND TO_DATE(:EndDate9, 'YYYY-MM-DD')
                                    )
                                  THEN 1
                                ELSE 0
                                END) SLTOTCHUAGIAO
                            ,SUM(CASE 
                                WHEN (
                                    (
                                      CODEKQTSC = '5'
                                      AND NGAYBGDT IS NULL
                                      )
                                    OR CODEKQSSC = '5'
                                    )
                                  AND 
                              (
                                    NGAYBGDVQLSD IS NULL
                                    OR TRUNC(NGAYBGDVQLSD) NOT BETWEEN TO_DATE(:StartDate6, 'YYYY-MM-DD')
                                      AND TO_DATE(:EndDate10, 'YYYY-MM-DD')
                                    )
                                  THEN 1
                                ELSE 0
                                END) SLKTSCCHUAGIAO
                          FROM (
                            SELECT chinhanh
                              ,NGAYCNNTK
                              ,CODEKQTSC
                              ,NGAYBGDT
                              ,CODEKQSSC
                              ,NGAYBGDVQLSD
                              ,TINHTRANGBANGIAO
                              ,NGAYDTBG
                              ,NGAYHTKTSSC
                              ,NGAYHTKTTSC
                              ,DVQLSD1 DVQLSD
                            FROM MV_BCCHITIET WHERE chinhanh IN ({0})
                            )
                          GROUP BY chinhanh,DVQLSD
                          )
                        ", orgs);
            return dbContext.Database.SqlQuery<InventoryDeviceStatisticBPCByDVQLSD>(sql,
                new OracleParameter("EndDate1", request.EndDate.ToString("yyyy-MM-dd")),
                new OracleParameter("EndDate2", request.EndDate.ToString("yyyy-MM-dd")),
                new OracleParameter("StartDate1", request.StartDate.ToString("yyyy-MM-dd")),
                new OracleParameter("EndDate3", request.EndDate.ToString("yyyy-MM-dd")),
                new OracleParameter("EndDate4", request.EndDate.ToString("yyyy-MM-dd")),
                new OracleParameter("StartDate2", request.StartDate.ToString("yyyy-MM-dd")),
                new OracleParameter("EndDate5", request.EndDate.ToString("yyyy-MM-dd")),
                new OracleParameter("EndDate6", request.EndDate.ToString("yyyy-MM-dd")),
                new OracleParameter("StartDate3", request.StartDate.ToString("yyyy-MM-dd")),
                new OracleParameter("EndDate7", request.EndDate.ToString("yyyy-MM-dd")),
                new OracleParameter("StartDate4", request.StartDate.ToString("yyyy-MM-dd")),
                new OracleParameter("EndDate8", request.EndDate.ToString("yyyy-MM-dd")),
                new OracleParameter("StartDate5", request.StartDate.ToString("yyyy-MM-dd")),
                new OracleParameter("EndDate9", request.EndDate.ToString("yyyy-MM-dd")),
                new OracleParameter("StartDate6", request.StartDate.ToString("yyyy-MM-dd")),
                new OracleParameter("EndDate10", request.EndDate.ToString("yyyy-MM-dd"))).ToArray();*/
            var orgs = StringHelper.MakeOrgSearch(request.Organizations);
            var sql = string.Format(@"
                SELECT CHINHANH
	                ,DVQLSD
	                ,SLNOTEST SoLuongChuaTest
	                ,SLCHOSC SoLuongChoSuaChua
	                ,SLNFFCHUAGIAO SoLuongNFFChuaGiao
	                ,SLTOTCHUAGIAO SoLuongTotChuaGiao
	                ,SLKTSCCHUAGIAO SoLuongKiemTraSSCChuaGiao
                FROM (
	                SELECT CHINHANH
		                ,DVQLSD
		                ,SUM(CASE 
				                WHEN NGAYCNNTK IS NOT NULL
					                AND NGAYCNNTK <= TO_DATE('{1}', 'YYYY-MM-DD')
					                AND (
						                CODEKQTSC IS NULL
						                OR CODEKQTSC = '1'
						                )
					                AND (
						                NGAYKTGPXSC IS NULL
						                OR (
							                NGAYKTGPXSC IS NOT NULL
							                AND NGAYKTGPXSC > TO_DATE('{1}', 'YYYY-MM-DD')
							                )
						                )
					                THEN 1
				                ELSE 0
				                END) SLNOTEST
		                ,SUM(CASE 
				                WHEN NGAYKTGPXSC IS NOT NULL
					                AND NGAYKTGPXSC <= TO_DATE('{1}', 'YYYY-MM-DD')
					                AND (
						                CODEKQSSC = '4'
						                OR CODEKQSSC IS NULL
						                )
					                AND (
						                NGAYBGDVQLSD IS NULL
						                OR (
							                NGAYBGDVQLSD IS NOT NULL
							                AND TRUNC(NGAYBGDVQLSD) > TO_DATE('{1}', 'YYYY-MM-DD')
							                )
						                )
					                THEN 1
				                ELSE 0
				                END) SLCHOSC
		                ,SUM(CASE 
				                WHEN CODEKQTSC = '2'
					                AND TRUNC(NGAYHTKTTSC) <= TO_DATE('{1}', 'YYYY-MM-DD')
					                AND (
						                NGAYBGDVQLSD IS NULL
						                OR (
							                NGAYBGDVQLSD IS NOT NULL
							                AND TRUNC(NGAYBGDVQLSD) > TO_DATE('{1}', 'YYYY-MM-DD')
							                )
						                )
					                THEN 1
				                ELSE 0
				                END) SLNFFCHUAGIAO
		                ,SUM(CASE 
				                WHEN CODEKQSSC = '3'
					                AND TRUNC(NGAYHTKTSSC) <= TO_DATE('{1}', 'YYYY-MM-DD')
					                AND (
						                NGAYBGDVQLSD IS NULL
						                OR (
							                NGAYBGDVQLSD IS NOT NULL
							                AND TRUNC(NGAYBGDVQLSD) > TO_DATE('{1}', 'YYYY-MM-DD')
							                )
						                )
					                THEN 1
				                ELSE 0
				                END) SLTOTCHUAGIAO
		                ,SUM(CASE 
				                WHEN (
						                (
							                CODEKQTSC = '5'
							                AND TRUNC(NGAYHTKTTSC) <= TO_DATE('{1}', 'YYYY-MM-DD')
							                AND NGAYKTGPXSC IS NULL
							                )
						                OR (
							                CODEKQSSC = '5'
							                AND TRUNC(NGAYHTKTTSC) <= TO_DATE('{1}', 'YYYY-MM-DD')
							                )
						                )
					                AND (
						                NGAYBGDVQLSD IS NULL
						                OR (
							                NGAYBGDVQLSD IS NOT NULL
							                AND TRUNC(NGAYBGDVQLSD) > TO_DATE('{1}', 'YYYY-MM-DD')
							                )
						                )
					                THEN 1
				                ELSE 0
				                END) SLKTSCCHUAGIAO
	                FROM (
		                SELECT chinhanh
			                ,NGAYCNNTK
			                ,CODEKQTSC
			                ,NGAYKTGPXSC
			                ,CODEKQSSC
			                ,NGAYBGDVQLSD
			                ,TINHTRANGBANGIAO
			                ,NGAYDTBG
			                ,NGAYHTKTSSC
			                ,NGAYHTKTTSC
			                ,DVQLSD1 DVQLSD
		                FROM MV_BCCHITIET
		                WHERE CODEDTTB IN (
				                '1'
				                ,'2'
				                ,'3'
				                ,'4'
				                ,'5'
				                ,'6'
				                ,'7'
				                ,'8'
				                )
                             AND chinhanh IN ({0})
		                )
	                GROUP BY chinhanh
		                ,DVQLSD
	                ORDER BY chinhanh
		                ,DVQLSD
	                )"
                , orgs, request.EndDate.ToString("yyyy-MM-dd"));
            return dbContext.Database.SqlQuery<InventoryDeviceStatisticBPCByDVQLSD>(sql).ToArray();
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryAllLevel(DeviceReportAllLevelQuery request)
        {
            var query = GetDbEntity<DeviceReportView>()
                .Where(d => d.ChiNhanh.ToUpper() == request.ChiNhanh.ToUpper()
                    && ((d.NgayCNNTK >= request.StartDate && d.NgayCNNTK <= request.EndDate && d.NgayBGDT == null)
                    || (d.NgayDTBG >= request.StartDate && d.NgayDTBG <= request.EndDate && d.NgayBanGiaoDVQLSD == null)));

            if (!string.IsNullOrWhiteSpace(request.DVQLSD))
            {
                query = query.Where(d => d.DVQLSD.ToUpper() == request.DVQLSD.ToUpper());
            }

            if (!string.IsNullOrWhiteSpace(request.LoaiTB))
            {
                query = query.Where(d => d.LoaiTB.ToUpper() == request.LoaiTB.ToUpper());
            }

            query = query.OrderByDescending(d => d.ReqCode)
                .ThenBy(d => d.Serial);

            var resultQuery = query.Select(d => new DeviceDetailsReportSummary
            {
                Serial = d.Serial,
                TenThietBi = d.TenThietBi,
                TinhTrang = d.TinhTrang
            });

            return new PagedResult<DeviceDetailsReportSummary>(resultQuery, request.Page, request.RecordsPerPage);
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVQLSD(InventoryDeviceReportByDVQLSDQuery request)
        {
            return GetDeviceReportSummaryByDVQLSDHandler[request.LoaiBaoCao.ToUpper()](request);
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByLoaiHD(InventoryDeviceReportByLoaiHDQuery request)
        {
            return GetDeviceReportSummaryByLoaiHDHandler[request.LoaiBaoCao.ToUpper()](request);
        }

        private IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVQLSD_SLNOTEST(InventoryDeviceReportByDVQLSDQuery request)
        {
            var dvqlsdSearch = "DVQLSD1 =:DVQLSD";

            if (string.IsNullOrWhiteSpace(request.DVQLSD))
            {
                dvqlsdSearch = "DVQLSD1 like '%'||:DVQLSD||'%'";
            }

            /*var sql = string.Format(@"SELECT SERIAL, TENTB TenThietBi,TINHTRANG
                          FROM (
                            SELECT chinhanh, SERIAL, TENTB,TINHTRANG,NGAYCNNTK,CODEKQTSC
                              ,NGAYBGDT,CODEKQSSC,NGAYBGDVQLSD,TINHTRANGBANGIAO
                              ,NGAYDTBG,NGAYHTKTSSC,NGAYHTKTTSC,DVQLSD1
                            FROM MV_BCCHITIET)
                            WHERE CHINHANH=:ChiNhanh AND {0}
                              AND TINHTRANGBANGIAO = 'TN'
                              AND TRUNC(NGAYDTBG) < TO_DATE(:EndDate1, 'YYYY-MM-DD')
                              AND (
                                    TRUNC(NGAYHTKTSSC) IS NULL
                                    OR TRUNC(NGAYHTKTSSC) > TO_DATE(:EndDate2, 'YYYY-MM-DD')
                                    )", dvqlsdSearch);*/

            var sql = string.Format(@"
                SELECT
                    serial,
                    tentb tenthietbi,
                    tinhtrang
                FROM
                    (
                        SELECT
                            chinhanh,
                            serial,
                            tentb,
                            tinhtrang,
                            ngaycnntk,
                            codekqtsc,
                            ngaybgdt,
                            codekqssc,
                            ngaybgdvqlsd,
                            tinhtrangbangiao,
                            ngaydtbg,
                            ngayhtktssc,
                            ngayhtkttsc,
                            dvqlsd1,
                            CASE
                                WHEN ngaycnntk IS NOT NULL
                                     AND ngaycnntk <= TO_DATE('{1}', 'YYYY-MM-DD')
                                     AND ( codekqtsc IS NULL
                                           OR codekqtsc = '1' )
                                     AND ( ngayktgpxsc IS NULL
                                           OR ( ngayktgpxsc IS NOT NULL
                                                AND ngayktgpxsc > TO_DATE('{1}', 'YYYY-MM-DD') ) ) THEN
                                    1
                                ELSE
                                    0
                            END slnotest -- Ton tren truoc sua chua: SL đang test, chưa sửa:
                        FROM
                            mv_bcchitiet
                    )
                WHERE
                        chinhanh = :ChiNhanh
                    AND {0}
                    AND slnotest = 1
                ", dvqlsdSearch, request.EndDate.ToString("yyyy-MM-dd"));

            return new SqlPagedResult<DeviceDetailsReportSummary>(dbContext,
                sql,
                request.Page,
                request.RecordsPerPage,
                new OracleParameter("ChiNhanh", request.ChiNhanh.ToUpper()),
                new OracleParameter("DVQLSD", request.DVQLSD.ToUpper()));
        }

        private IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVQLSD_SLCHOSC(InventoryDeviceReportByDVQLSDQuery request)
        {
            var dvqlsdSearch = "DVQLSD1 =:DVQLSD";

            if (string.IsNullOrWhiteSpace(request.DVQLSD))
            {
                dvqlsdSearch = "DVQLSD1 like '%'||:DVQLSD||'%'";
            }

            /*var sql = string.Format(@"SELECT SERIAL, TENTB TenThietBi,TINHTRANG
                          FROM (
                            SELECT chinhanh, SERIAL, TENTB,TINHTRANG,NGAYCNNTK,CODEKQTSC
                              ,NGAYBGDT,CODEKQSSC,NGAYBGDVQLSD,TINHTRANGBANGIAO
                              ,NGAYDTBG,NGAYHTKTSSC,NGAYHTKTTSC,DVQLSD1
                            FROM MV_BCCHITIET)
                            WHERE CHINHANH=:ChiNhanh AND {0}
                              AND (((
                                    CODEKQTSC = '1'
                                    AND TRUNC(NGAYHTKTTSC) BETWEEN TO_DATE(:StartDate1, 'YYYY-MM-DD')
                                      AND TO_DATE(:EndDate1, 'YYYY-MM-DD')
                                    )
                                  AND (
                                    NGAYBGDT IS NULL
                                    OR TRUNC(NGAYBGDT) > TO_DATE(:EndDate2, 'YYYY-MM-DD')
                                    ))
                              OR (
                              (TRUNC(NGAYCNNTK) BETWEEN TO_DATE(:StartDate2, 'YYYY-MM-DD')
                                      AND TO_DATE(:EndDate3, 'YYYY-MM-DD'))
                                      AND (NGAYHTKTTSC IS NULL  OR TRUNC(NGAYHTKTTSC) > TO_DATE(:EndDate4, 'YYYY-MM-DD'))
                              ))", dvqlsdSearch);*/

            var sql = string.Format(@"
                SELECT
                    serial,
                    tentb tenthietbi,
                    tinhtrang
                FROM
                    (
                        SELECT
                            chinhanh,
                            serial,
                            tentb,
                            tinhtrang,
                            ngaycnntk,
                            codekqtsc,
                            ngaybgdt,
                            codekqssc,
                            ngaybgdvqlsd,
                            tinhtrangbangiao,
                            ngaydtbg,
                            ngayhtktssc,
                            ngayhtkttsc,
                            dvqlsd1,
                            CASE 
				                WHEN NGAYKTGPXSC IS NOT NULL
					                AND NGAYKTGPXSC <= TO_DATE('{1}', 'YYYY-MM-DD')
					                AND (
						                CODEKQSSC = '4'
						                OR CODEKQSSC IS NULL
						                )
					                AND (
						                NGAYBGDVQLSD IS NULL
						                OR (
							                NGAYBGDVQLSD IS NOT NULL
							                AND TRUNC(NGAYBGDVQLSD) > TO_DATE('{1}', 'YYYY-MM-DD')
							                )
						                )
					                THEN 1
				                ELSE 0
				            END SLCHOSC
                        FROM
                            mv_bcchitiet
                    )
                WHERE
                        chinhanh = :ChiNhanh
                    AND {0}
                    AND SLCHOSC = 1
                ", dvqlsdSearch, request.EndDate.ToString("yyyy-MM-dd"));

            return new SqlPagedResult<DeviceDetailsReportSummary>(dbContext,
                sql,
                request.Page,
                request.RecordsPerPage,
                new OracleParameter("ChiNhanh", request.ChiNhanh.ToUpper()),
                new OracleParameter("DVQLSD", request.DVQLSD.ToUpper()));
        }

        private IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVQLSD_SLNFFCHUAGIAO(InventoryDeviceReportByDVQLSDQuery request)
        {
            var dvqlsdSearch = "DVQLSD1 =:DVQLSD";

            if (string.IsNullOrWhiteSpace(request.DVQLSD))
            {
                dvqlsdSearch = "DVQLSD1 like '%'||:DVQLSD||'%'";
            }

            /*var sql = string.Format(@"SELECT SERIAL, TENTB TenThietBi,TINHTRANG
                          FROM (
                            SELECT chinhanh, SERIAL, TENTB,TINHTRANG,NGAYCNNTK,CODEKQTSC
                              ,NGAYBGDT,CODEKQSSC,NGAYBGDVQLSD,TINHTRANGBANGIAO
                              ,NGAYDTBG,NGAYHTKTSSC,NGAYHTKTTSC,DVQLSD1
                            FROM MV_BCCHITIET)
                            WHERE CHINHANH=:ChiNhanh AND {0}
                              AND (CODEKQTSC = '2'
                                  AND (
                                    NGAYBGDVQLSD IS NULL
                                    OR TRUNC(NGAYBGDVQLSD) NOT BETWEEN TO_DATE(:StartDate, 'YYYY-MM-DD')
                                      AND TO_DATE(:EndDate, 'YYYY-MM-DD')
                                    ))", dvqlsdSearch);*/

            var sql = string.Format(@"
                SELECT
                    serial,
                    tentb tenthietbi,
                    tinhtrang
                FROM
                    (
                        SELECT
                            chinhanh,
                            serial,
                            tentb,
                            tinhtrang,
                            ngaycnntk,
                            codekqtsc,
                            ngaybgdt,
                            codekqssc,
                            ngaybgdvqlsd,
                            tinhtrangbangiao,
                            ngaydtbg,
                            ngayhtktssc,
                            ngayhtkttsc,
                            dvqlsd1,
                            CASE 
				                WHEN CODEKQTSC = '2'
					                AND TRUNC(NGAYHTKTTSC) <= TO_DATE('{1}', 'YYYY-MM-DD')
					                AND (
						                NGAYBGDVQLSD IS NULL
						                OR (
							                NGAYBGDVQLSD IS NOT NULL
							                AND TRUNC(NGAYBGDVQLSD) > TO_DATE('{1}', 'YYYY-MM-DD')
							                )
						                )
					                THEN 1
				                ELSE 0
				                END SLNFFCHUAGIAO
                        FROM
                            mv_bcchitiet
                    )
                WHERE
                        chinhanh = :ChiNhanh
                    AND {0}
                    AND SLNFFCHUAGIAO = 1
                ", dvqlsdSearch, request.EndDate.ToString("yyyy-MM-dd"));

            return new SqlPagedResult<DeviceDetailsReportSummary>(dbContext,
                sql,
                request.Page,
                request.RecordsPerPage,
                new OracleParameter("ChiNhanh", request.ChiNhanh.ToUpper()),
                new OracleParameter("DVQLSD", request.DVQLSD.ToUpper()));
        }

        private IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVQLSD_SLTOTCHUAGIAO(InventoryDeviceReportByDVQLSDQuery request)
        {
            var dvqlsdSearch = "DVQLSD1 =:DVQLSD";

            if (string.IsNullOrWhiteSpace(request.DVQLSD))
            {
                dvqlsdSearch = "DVQLSD1 like '%'||:DVQLSD||'%'";
            }

            /*var sql = string.Format(@"SELECT SERIAL, TENTB TenThietBi,TINHTRANG
                          FROM (
                            SELECT chinhanh, SERIAL, TENTB,TINHTRANG,NGAYCNNTK,CODEKQTSC
                              ,NGAYBGDT,CODEKQSSC,NGAYBGDVQLSD,TINHTRANGBANGIAO
                              ,NGAYDTBG,NGAYHTKTSSC,NGAYHTKTTSC,DVQLSD1
                            FROM MV_BCCHITIET)
                            WHERE CHINHANH=:ChiNhanh AND {0}
                              AND (CODEKQSSC = '3'
                                  AND TRUNC(NGAYHTKTSSC) BETWEEN TO_DATE(:StartDate1, 'YYYY-MM-DD')
                                    AND TO_DATE(:EndDate1, 'YYYY-MM-DD')
                                  AND (
                                    NGAYBGDVQLSD IS NULL
                                    OR TRUNC(NGAYBGDVQLSD) NOT BETWEEN TO_DATE(:StartDate2, 'YYYY-MM-DD')
                                      AND TO_DATE(:EndDate2, 'YYYY-MM-DD')
                                    ))", dvqlsdSearch);*/

            var sql = string.Format(@"
                SELECT
                    serial,
                    tentb tenthietbi,
                    tinhtrang
                FROM
                    (
                        SELECT
                            chinhanh,
                            serial,
                            tentb,
                            tinhtrang,
                            ngaycnntk,
                            codekqtsc,
                            ngaybgdt,
                            codekqssc,
                            ngaybgdvqlsd,
                            tinhtrangbangiao,
                            ngaydtbg,
                            ngayhtktssc,
                            ngayhtkttsc,
                            dvqlsd1,
		                    CASE 
				                WHEN CODEKQSSC = '3'
					                AND TRUNC(NGAYHTKTSSC) <= TO_DATE('{1}', 'YYYY-MM-DD')
					                AND (
						                NGAYBGDVQLSD IS NULL
						                OR (
							                NGAYBGDVQLSD IS NOT NULL
							                AND TRUNC(NGAYBGDVQLSD) > TO_DATE('{1}', 'YYYY-MM-DD')
							                )
						                )
					                THEN 1
				                ELSE 0
				                END SLTOTCHUAGIAO
                        FROM
                            mv_bcchitiet
                    )
                WHERE
                        chinhanh = :ChiNhanh
                    AND {0}
                    AND SLTOTCHUAGIAO = 1
                ", dvqlsdSearch, request.EndDate.ToString("yyyy-MM-dd"));

            return new SqlPagedResult<DeviceDetailsReportSummary>(dbContext,
                sql,
                request.Page,
                request.RecordsPerPage,
                new OracleParameter("ChiNhanh", request.ChiNhanh.ToUpper()),
                new OracleParameter("DVQLSD", request.DVQLSD.ToUpper()));
        }

        private IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVQLSD_SLKTSCCHUAGIAO(InventoryDeviceReportByDVQLSDQuery request)
        {
            var dvqlsdSearch = "DVQLSD1 =:DVQLSD";

            if (string.IsNullOrWhiteSpace(request.DVQLSD))
            {
                dvqlsdSearch = "DVQLSD1 like '%'||:DVQLSD||'%'";
            }

            /*var sql = string.Format(@"SELECT SERIAL, TENTB TenThietBi,TINHTRANG
                          FROM (
                            SELECT chinhanh, SERIAL, TENTB,TINHTRANG,NGAYCNNTK,CODEKQTSC
                              ,NGAYBGDT,CODEKQSSC,NGAYBGDVQLSD,TINHTRANGBANGIAO
                              ,NGAYDTBG,NGAYHTKTSSC,NGAYHTKTTSC,DVQLSD1
                            FROM MV_BCCHITIET)
                            WHERE CHINHANH=:ChiNhanh AND {0}
                              AND ((
                                    (
                                      CODEKQTSC = '5'
                                      AND NGAYBGDT IS NULL
                                      )
                                    OR CODEKQSSC = '5'
                                    )
                                  AND 
                              (
                                    NGAYBGDVQLSD IS NULL
                                    OR TRUNC(NGAYBGDVQLSD) NOT BETWEEN TO_DATE(:StartDate, 'YYYY-MM-DD')
                                      AND TO_DATE(:EndDate, 'YYYY-MM-DD')
                                    ))", dvqlsdSearch);*/

            var sql = string.Format(@"
                SELECT
                    serial,
                    tentb tenthietbi,
                    tinhtrang
                FROM
                    (
                        SELECT
                            chinhanh,
                            serial,
                            tentb,
                            tinhtrang,
                            ngaycnntk,
                            codekqtsc,
                            ngaybgdt,
                            codekqssc,
                            ngaybgdvqlsd,
                            tinhtrangbangiao,
                            ngaydtbg,
                            ngayhtktssc,
                            ngayhtkttsc,
                            dvqlsd1,
		                    CASE 
				                WHEN (
						                (
							                CODEKQTSC = '5'
							                AND TRUNC(NGAYHTKTTSC) <= TO_DATE('{1}', 'YYYY-MM-DD')
							                AND NGAYKTGPXSC IS NULL
							                )
						                OR (
							                CODEKQSSC = '5'
							                AND TRUNC(NGAYHTKTTSC) <= TO_DATE('{1}', 'YYYY-MM-DD')
							                )
						                )
					                AND (
						                NGAYBGDVQLSD IS NULL
						                OR (
							                NGAYBGDVQLSD IS NOT NULL
							                AND TRUNC(NGAYBGDVQLSD) > TO_DATE('{1}', 'YYYY-MM-DD')
							                )
						                )
					                THEN 1
				                ELSE 0
				                END SLKTSCCHUAGIAO
                        FROM
                            mv_bcchitiet
                    )
                WHERE
                        chinhanh = :ChiNhanh
                    AND {0}
                    AND SLKTSCCHUAGIAO = 1
                ", dvqlsdSearch, request.EndDate.ToString("yyyy-MM-dd"));

            return new SqlPagedResult<DeviceDetailsReportSummary>(dbContext,
                sql,
                request.Page,
                request.RecordsPerPage,
                new OracleParameter("ChiNhanh", request.ChiNhanh.ToUpper()),
                new OracleParameter("DVQLSD", request.DVQLSD.ToUpper()));
        }

        private IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByLoaiHD_SLNOTEST(InventoryDeviceReportByLoaiHDQuery request)
        {
            var loaiHDSearch = "LoaiHD =:LoaiHD";

            if (string.IsNullOrWhiteSpace(request.LoaiHD))
            {
                loaiHDSearch = "LoaiHD like '%'||:LoaiHD||'%'";
            }

            /*var sql = string.Format(@"SELECT SERIAL, TENTB TenThietBi,TINHTRANG
                          FROM (
                            SELECT chinhanh, SERIAL, TENTB,TINHTRANG,NGAYCNNTK,CODEKQTSC
                              ,NGAYBGDT,CODEKQSSC,NGAYBGDVQLSD,TINHTRANGBANGIAO
                              ,NGAYDTBG,NGAYHTKTSSC,NGAYHTKTTSC
                            ,CASE 
                                WHEN CODEDTTB IN (
                                    '3'
                                    ,'5'
                                    )
                                  THEN 'NONSLA'
                                WHEN codedttb = '1'
                                  THEN 'SLA'
                                ELSE ''
                                END LOAIHD
                              
                            FROM MV_BCCHITIET)
                            WHERE CHINHANH=:ChiNhanh AND {0}
                              AND TINHTRANGBANGIAO = 'TN'
                              AND TRUNC(NGAYDTBG) < TO_DATE(:EndDate1, 'YYYY-MM-DD')
                              AND (
                                    TRUNC(NGAYHTKTSSC) IS NULL
                                    OR TRUNC(NGAYHTKTSSC) > TO_DATE(:EndDate2, 'YYYY-MM-DD')
                                    )", loaiHDSearch);*/

            var sql = string.Format(@"
                SELECT
                    serial,
                    tentb tenthietbi,
                    tinhtrang
                FROM
                    (
                        SELECT
                            chinhanh,
                            serial,
                            tentb,
                            tinhtrang,
                            ngaycnntk,
                            codekqtsc,
                            ngaybgdt,
                            codekqssc,
                            ngaybgdvqlsd,
                            tinhtrangbangiao,
                            ngaydtbg,
                            ngayhtktssc,
                            ngayhtkttsc,
                            CASE
                                WHEN codedttb IN (
                                    '3',
                                    '4',
                                    '5',
                                    '6',
                                    '7',
                                    '8'
                                ) THEN
                                    'NONSLA'
                                WHEN codedttb IN (
                                    '1',
                                    '2'
                                ) THEN
                                    'SLA'
                                ELSE
                                    ''
                            END loaihd,
                            CASE
                                WHEN ngaycnntk IS NOT NULL
                                     AND ngaycnntk <= TO_DATE('{1}', 'YYYY-MM-DD')
                                     AND ( codekqtsc IS NULL
                                           OR codekqtsc = '1' )
                                     AND ( ngayktgpxsc IS NULL
                                           OR ( ngayktgpxsc IS NOT NULL
                                                AND ngayktgpxsc > TO_DATE('{1}', 'YYYY-MM-DD') ) ) THEN
                                    1
                                ELSE
                                    0
                            END slnotest -- Ton tren truoc sua chua: SL đang test, chưa sửa:
                        FROM
                            mv_bcchitiet
                    )
                WHERE
                        chinhanh = :ChiNhanh
                    AND {0}
                    AND slnotest = 1
                ", loaiHDSearch, request.EndDate.ToString("yyyy-MM-dd"));

            return new SqlPagedResult<DeviceDetailsReportSummary>(dbContext,
                sql,
                request.Page,
                request.RecordsPerPage,
                new OracleParameter("ChiNhanh", request.ChiNhanh.ToUpper()),
                new OracleParameter("LoaiHD", request.LoaiHD.ToUpper()));
        }

        private IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByLoaiHD_SLCHOSC(InventoryDeviceReportByLoaiHDQuery request)
        {
            var loaiHDSearch = "LoaiHD =:LoaiHD";

            if (string.IsNullOrWhiteSpace(request.LoaiHD))
            {
                loaiHDSearch = "LoaiHD like '%'||:LoaiHD||'%'";
            }

            /*var sql = string.Format(@"SELECT SERIAL, TENTB TenThietBi,TINHTRANG
                          FROM (
                            SELECT chinhanh, SERIAL, TENTB,TINHTRANG,NGAYCNNTK,CODEKQTSC
                              ,NGAYBGDT,CODEKQSSC,NGAYBGDVQLSD,TINHTRANGBANGIAO
                              ,NGAYDTBG,NGAYHTKTSSC,NGAYHTKTTSC
                            ,CASE 
                                WHEN CODEDTTB IN (
                                    '3'
                                    ,'5'
                                    )
                                  THEN 'NONSLA'
                                WHEN codedttb = '1'
                                  THEN 'SLA'
                                ELSE ''
                                END LOAIHD
                              
                            FROM MV_BCCHITIET)
                            WHERE CHINHANH=:ChiNhanh AND {0}
                              AND (((
                                    CODEKQTSC = '1'
                                    AND TRUNC(NGAYHTKTTSC) BETWEEN TO_DATE(:StartDate1, 'YYYY-MM-DD')
                                      AND TO_DATE(:EndDate1, 'YYYY-MM-DD')
                                    )
                                  AND (
                                    NGAYBGDT IS NULL
                                    OR TRUNC(NGAYBGDT) > TO_DATE(:EndDate2, 'YYYY-MM-DD')
                                    ))
                              OR (
                              (TRUNC(NGAYCNNTK) BETWEEN TO_DATE(:StartDate2, 'YYYY-MM-DD')
                                      AND TO_DATE(:EndDate3, 'YYYY-MM-DD'))
                                      AND (NGAYHTKTTSC IS NULL  OR TRUNC(NGAYHTKTTSC) > TO_DATE(:EndDate4, 'YYYY-MM-DD'))
                              ))", loaiHDSearch);*/
            var sql = string.Format(@"
                SELECT
                    serial,
                    tentb tenthietbi,
                    tinhtrang
                FROM
                    (
                        SELECT
                            chinhanh,
                            serial,
                            tentb,
                            tinhtrang,
                            ngaycnntk,
                            codekqtsc,
                            ngaybgdt,
                            codekqssc,
                            ngaybgdvqlsd,
                            tinhtrangbangiao,
                            ngaydtbg,
                            ngayhtktssc,
                            ngayhtkttsc,
                            CASE
                                WHEN codedttb IN (
                                    '3',
                                    '4',
                                    '5',
                                    '6',
                                    '7',
                                    '8'
                                ) THEN
                                    'NONSLA'
                                WHEN codedttb IN (
                                    '1',
                                    '2'
                                ) THEN
                                    'SLA'
                                ELSE
                                    ''
                            END loaihd,
                            CASE 
				                WHEN NGAYKTGPXSC IS NOT NULL
					                AND NGAYKTGPXSC <= TO_DATE('{1}', 'YYYY-MM-DD')
					                AND (
						                CODEKQSSC = '4'
						                OR CODEKQSSC IS NULL
						                )
					                AND (
						                NGAYBGDVQLSD IS NULL
						                OR (
							                NGAYBGDVQLSD IS NOT NULL
							                AND TRUNC(NGAYBGDVQLSD) > TO_DATE('{1}', 'YYYY-MM-DD')
							                )
						                )
					                THEN 1
				                ELSE 0
				            END SLCHOSC
                        FROM
                            mv_bcchitiet
                    )
                WHERE
                        chinhanh = :ChiNhanh
                    AND {0}
                    AND SLCHOSC = 1
                ", loaiHDSearch, request.EndDate.ToString("yyyy-MM-dd"));

            return new SqlPagedResult<DeviceDetailsReportSummary>(dbContext,
                sql,
                request.Page,
                request.RecordsPerPage,
                new OracleParameter("ChiNhanh", request.ChiNhanh.ToUpper()),
                new OracleParameter("LoaiHD", request.LoaiHD.ToUpper()));
        }

        private IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByLoaiHD_SLNFFCHUAGIAO(InventoryDeviceReportByLoaiHDQuery request)
        {
            var loaiHDSearch = "LoaiHD =:LoaiHD";

            if (string.IsNullOrWhiteSpace(request.LoaiHD))
            {
                loaiHDSearch = "LoaiHD like '%'||:LoaiHD||'%'";
            }

            /*var sql = string.Format(@"SELECT SERIAL, TENTB TenThietBi,TINHTRANG
                          FROM (
                            SELECT chinhanh, SERIAL, TENTB,TINHTRANG,NGAYCNNTK,CODEKQTSC
                              ,NGAYBGDT,CODEKQSSC,NGAYBGDVQLSD,TINHTRANGBANGIAO
                              ,NGAYDTBG,NGAYHTKTSSC,NGAYHTKTTSC
                            ,CASE 
                                WHEN CODEDTTB IN (
                                    '3'
                                    ,'5'
                                    )
                                  THEN 'NONSLA'
                                WHEN codedttb = '1'
                                  THEN 'SLA'
                                ELSE ''
                                END LOAIHD
                              
                            FROM MV_BCCHITIET)
                            WHERE CHINHANH=:ChiNhanh AND {0}
                              AND (CODEKQTSC = '2'
                                  AND (
                                    NGAYBGDVQLSD IS NULL
                                    OR TRUNC(NGAYBGDVQLSD) NOT BETWEEN TO_DATE(:StartDate, 'YYYY-MM-DD')
                                      AND TO_DATE(:EndDate, 'YYYY-MM-DD')
                                    ))", loaiHDSearch);*/

            var sql = string.Format(@"
                SELECT
                    serial,
                    tentb tenthietbi,
                    tinhtrang
                FROM
                    (
                        SELECT
                            chinhanh,
                            serial,
                            tentb,
                            tinhtrang,
                            ngaycnntk,
                            codekqtsc,
                            ngaybgdt,
                            codekqssc,
                            ngaybgdvqlsd,
                            tinhtrangbangiao,
                            ngaydtbg,
                            ngayhtktssc,
                            ngayhtkttsc,
                            CASE
                                WHEN codedttb IN (
                                    '3',
                                    '4',
                                    '5',
                                    '6',
                                    '7',
                                    '8'
                                ) THEN
                                    'NONSLA'
                                WHEN codedttb IN (
                                    '1',
                                    '2'
                                ) THEN
                                    'SLA'
                                ELSE
                                    ''
                            END loaihd,
                            CASE 
				                WHEN CODEKQTSC = '2'
					                AND TRUNC(NGAYHTKTTSC) <= TO_DATE('{1}', 'YYYY-MM-DD')
					                AND (
						                NGAYBGDVQLSD IS NULL
						                OR (
							                NGAYBGDVQLSD IS NOT NULL
							                AND TRUNC(NGAYBGDVQLSD) > TO_DATE('{1}', 'YYYY-MM-DD')
							                )
						                )
					                THEN 1
				                ELSE 0
				                END SLNFFCHUAGIAO
                        FROM
                            mv_bcchitiet
                    )
                WHERE
                        chinhanh = :ChiNhanh
                    AND {0}
                    AND SLNFFCHUAGIAO = 1
                ", loaiHDSearch, request.EndDate.ToString("yyyy-MM-dd"));

            return new SqlPagedResult<DeviceDetailsReportSummary>(dbContext,
                sql,
                request.Page,
                request.RecordsPerPage,
                new OracleParameter("ChiNhanh", request.ChiNhanh.ToUpper()),
                new OracleParameter("LoaiHD", request.LoaiHD.ToUpper()));
        }

        private IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByLoaiHD_SLTOTCHUAGIAO(InventoryDeviceReportByLoaiHDQuery request)
        {
            var loaiHDSearch = "LoaiHD =:LoaiHD";

            if (string.IsNullOrWhiteSpace(request.LoaiHD))
            {
                loaiHDSearch = "LoaiHD like '%'||:LoaiHD||'%'";
            }

            /*var sql = string.Format(@"SELECT SERIAL, TENTB TenThietBi,TINHTRANG
                          FROM (
                            SELECT chinhanh, SERIAL, TENTB,TINHTRANG,NGAYCNNTK,CODEKQTSC
                              ,NGAYBGDT,CODEKQSSC,NGAYBGDVQLSD,TINHTRANGBANGIAO
                              ,NGAYDTBG,NGAYHTKTSSC,NGAYHTKTTSC
                            ,CASE 
                                WHEN CODEDTTB IN (
                                    '3'
                                    ,'5'
                                    )
                                  THEN 'NONSLA'
                                WHEN codedttb = '1'
                                  THEN 'SLA'
                                ELSE ''
                                END LOAIHD
                              
                            FROM MV_BCCHITIET)
                            WHERE CHINHANH=:ChiNhanh AND {0}
                              AND (CODEKQSSC = '3'
                                  AND TRUNC(NGAYHTKTSSC) BETWEEN TO_DATE(:StartDate1, 'YYYY-MM-DD')
                                    AND TO_DATE(:EndDate1, 'YYYY-MM-DD')
                                  AND (
                                    NGAYBGDVQLSD IS NULL
                                    OR TRUNC(NGAYBGDVQLSD) NOT BETWEEN TO_DATE(:StartDate2, 'YYYY-MM-DD')
                                      AND TO_DATE(:EndDate2, 'YYYY-MM-DD')
                                    ))", loaiHDSearch);*/

            var sql = string.Format(@"
                SELECT
                    serial,
                    tentb tenthietbi,
                    tinhtrang
                FROM
                    (
                        SELECT
                            chinhanh,
                            serial,
                            tentb,
                            tinhtrang,
                            ngaycnntk,
                            codekqtsc,
                            ngaybgdt,
                            codekqssc,
                            ngaybgdvqlsd,
                            tinhtrangbangiao,
                            ngaydtbg,
                            ngayhtktssc,
                            ngayhtkttsc,
                            CASE
                                WHEN codedttb IN (
                                    '3',
                                    '4',
                                    '5',
                                    '6',
                                    '7',
                                    '8'
                                ) THEN
                                    'NONSLA'
                                WHEN codedttb IN (
                                    '1',
                                    '2'
                                ) THEN
                                    'SLA'
                                ELSE
                                    ''
                            END loaihd,
		                    CASE 
				                WHEN CODEKQSSC = '3'
					                AND TRUNC(NGAYHTKTSSC) <= TO_DATE('{1}', 'YYYY-MM-DD')
					                AND (
						                NGAYBGDVQLSD IS NULL
						                OR (
							                NGAYBGDVQLSD IS NOT NULL
							                AND TRUNC(NGAYBGDVQLSD) > TO_DATE('{1}', 'YYYY-MM-DD')
							                )
						                )
					                THEN 1
				                ELSE 0
				                END SLTOTCHUAGIAO
                        FROM
                            mv_bcchitiet
                    )
                WHERE
                        chinhanh = :ChiNhanh
                    AND {0}
                    AND SLTOTCHUAGIAO = 1
                ", loaiHDSearch, request.EndDate.ToString("yyyy-MM-dd"));

            return new SqlPagedResult<DeviceDetailsReportSummary>(dbContext,
                sql,
                request.Page,
                request.RecordsPerPage,
                new OracleParameter("ChiNhanh", request.ChiNhanh.ToUpper()),
                new OracleParameter("LoaiHD", request.LoaiHD.ToUpper()));
        }

        private IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByLoaiHD_SLKTSCCHUAGIAO(InventoryDeviceReportByLoaiHDQuery request)
        {
            var loaiHDSearch = "LoaiHD =:LoaiHD";

            if (string.IsNullOrWhiteSpace(request.LoaiHD))
            {
                loaiHDSearch = "LoaiHD like '%'||:LoaiHD||'%'";
            }

            /*var sql = string.Format(@"SELECT SERIAL, TENTB TenThietBi,TINHTRANG
                          FROM (
                            SELECT chinhanh, SERIAL, TENTB,TINHTRANG,NGAYCNNTK,CODEKQTSC
                              ,NGAYBGDT,CODEKQSSC,NGAYBGDVQLSD,TINHTRANGBANGIAO
                              ,NGAYDTBG,NGAYHTKTSSC,NGAYHTKTTSC
                            ,CASE 
                                WHEN CODEDTTB IN (
                                    '3'
                                    ,'5'
                                    )
                                  THEN 'NONSLA'
                                WHEN codedttb = '1'
                                  THEN 'SLA'
                                ELSE ''
                                END LOAIHD
                              
                            FROM MV_BCCHITIET)
                            WHERE CHINHANH=:ChiNhanh AND {0}
                              AND ((
                                    (
                                      CODEKQTSC = '5'
                                      AND NGAYBGDT IS NULL
                                      )
                                    OR CODEKQSSC = '5'
                                    )
                                  AND 
                              (
                                    NGAYBGDVQLSD IS NULL
                                    OR TRUNC(NGAYBGDVQLSD) NOT BETWEEN TO_DATE(:StartDate, 'YYYY-MM-DD')
                                      AND TO_DATE(:EndDate, 'YYYY-MM-DD')
                                    ))", loaiHDSearch);*/

            var sql = string.Format(@"
                SELECT
                    serial,
                    tentb tenthietbi,
                    tinhtrang
                FROM
                    (
                        SELECT
                            chinhanh,
                            serial,
                            tentb,
                            tinhtrang,
                            ngaycnntk,
                            codekqtsc,
                            ngaybgdt,
                            codekqssc,
                            ngaybgdvqlsd,
                            tinhtrangbangiao,
                            ngaydtbg,
                            ngayhtktssc,
                            ngayhtkttsc,
                            CASE
                                WHEN codedttb IN (
                                    '3',
                                    '4',
                                    '5',
                                    '6',
                                    '7',
                                    '8'
                                ) THEN
                                    'NONSLA'
                                WHEN codedttb IN (
                                    '1',
                                    '2'
                                ) THEN
                                    'SLA'
                                ELSE
                                    ''
                            END loaihd,
		                    CASE 
				                WHEN (
						                (
							                CODEKQTSC = '5'
							                AND TRUNC(NGAYHTKTTSC) <= TO_DATE('{1}', 'YYYY-MM-DD')
							                AND NGAYKTGPXSC IS NULL
							                )
						                OR (
							                CODEKQSSC = '5'
							                AND TRUNC(NGAYHTKTTSC) <= TO_DATE('{1}', 'YYYY-MM-DD')
							                )
						                )
					                AND (
						                NGAYBGDVQLSD IS NULL
						                OR (
							                NGAYBGDVQLSD IS NOT NULL
							                AND TRUNC(NGAYBGDVQLSD) > TO_DATE('{1}', 'YYYY-MM-DD')
							                )
						                )
					                THEN 1
				                ELSE 0
				                END SLKTSCCHUAGIAO
                        FROM
                            mv_bcchitiet
                    )
                WHERE
                        chinhanh = :ChiNhanh
                    AND {0}
                    AND SLKTSCCHUAGIAO = 1
                ", loaiHDSearch, request.EndDate.ToString("yyyy-MM-dd"));

            return new SqlPagedResult<DeviceDetailsReportSummary>(dbContext,
                sql,
                request.Page,
                request.RecordsPerPage,
                new OracleParameter("ChiNhanh", request.ChiNhanh.ToUpper()),
                new OracleParameter("LoaiHD", request.LoaiHD.ToUpper()));
        }


    }
}
