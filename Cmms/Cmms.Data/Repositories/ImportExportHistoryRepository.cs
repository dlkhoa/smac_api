﻿using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Entities.Views;
using Cmms.Domain.Queries;
using System.Linq;

namespace Cmms.Data.Repositories
{
    public class ImportExportHistoryRepository : BaseRepository, IImportExportHistoryRepository
    {
        public ImportExportHistoryRepository(CmmsDbContext dbContext) : base(dbContext)
        {

        }

        public IPagedResult<ImportExportHistorySummary> GetHistories(ImportExportHistoryQuery historyQuery)
        {
            var query = GetDbEntity<ImportExportHistoryView>()
                .Where(v => v.Serial.ToUpper() == historyQuery.Serial.ToUpper()
                    && v.Org.ToUpper() == historyQuery.Branch.ToUpper())
                .OrderByDescending(v => v.Ngay)
                .ThenByDescending(v => v.DeNghi)
                .Select(v => new ImportExportHistorySummary
                {
                    DeNghi = v.DeNghi,
                    LoaiPhieu = v.LoaiPhieu,
                    Ngay = v.Ngay,
                    SoGiaoDich = v.SoGiaoDich,
                    Supplier = v.Supplier,
                    TinhTrangPhieu = v.TinhTrangPhieu
                });

            return new PagedResult<ImportExportHistorySummary>(query, historyQuery.Page, historyQuery.RecordsPerPage);
        }
    }
}
