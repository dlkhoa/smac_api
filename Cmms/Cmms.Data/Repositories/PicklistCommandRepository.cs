﻿using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Commands;
using Oracle.ManagedDataAccess.Client;
using System;

namespace Cmms.Data.Repositories
{
    public class PicklistCommandRepository : BaseRepository, IPicklistCommandRepository
    {
        public PicklistCommandRepository(CmmsDbContext dbContext) : base(dbContext)
        {

        }

        //public void ApprovePicklist(ApprovePicklistCommand command)
        //{
        //    var sqlCommand = @" UPDATE r5picklists
        //                        SET pic_status=:NewStatus
        //                        WHERE pic_code=:PicklistCode";

        //    var sqlParameters = new OracleParameter[2];
        //    sqlParameters[0] = new OracleParameter("NewStatus", command.NewStatus.ToUpper());
        //    sqlParameters[1] = new OracleParameter("PicklistCode", command.PicklistCode);

        //    dbContext.Database.ExecuteSqlCommand(sqlCommand, sqlParameters);
        //}

        public void ApprovePicklist(string username, string picklistCode)
        {
            var sql = @"UPDATE r5picklists p
                        SET pic_status=(SELECT NVL(aut_statnew,pic_status) 
				                        FROM r5permissions, r5userorganization,r5auth,r5stores
				                        WHERE uog_user =:Username 
					                        AND uog_org = str_org 
					                        AND pic_store=str_code
					                        AND uog_group = prm_group 
					                        AND prm_update = '*' 
					                        AND prm_function IN ('SSPICH','SSPICK','SSPICR')
					                        AND aut_group=uog_group 
					                        AND aut_entity='PICK'
					                        AND aut_status=pic_status
                                            AND rownum = 1 )
                        WHERE pic_code =:PickCode";

            var sqlParameters = new OracleParameter[2];
            sqlParameters[0] = new OracleParameter("Username", username.ToUpper());
            sqlParameters[1] = new OracleParameter("PickCode", picklistCode);
            dbContext.Database.ExecuteSqlCommand(sql, sqlParameters);
        }

        public void UpdatePicklistStatus(ApprovePicklistCommand command)
        {
            var sql = @"UPDATE r5picklists p
                        SET pic_status =:NewStatus,
                            pic_auth=:AuthUser,
                            pic_approv=:ApproveDate
                        WHERE pic_code =:PickCode";

            var sqlParameters = new OracleParameter[4];
            sqlParameters[0] = new OracleParameter("NewStatus", command.NewStatus.ToUpper());
            sqlParameters[1] = new OracleParameter("AuthUser", command.Username.ToUpper());
            sqlParameters[2] = new OracleParameter("ApproveDate", DateTime.Now);
            sqlParameters[3] = new OracleParameter("PickCode", command.PicklistCode);
            dbContext.Database.ExecuteSqlCommand(sql, sqlParameters);
        }
    }
}
