﻿using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using Oracle.ManagedDataAccess.Client;

namespace Cmms.Data.Repositories
{
    public class DeviceHistoryRepository : BaseRepository, IDeviceHistoryRepository
    {
        public DeviceHistoryRepository(CmmsDbContext dbContext) : base(dbContext)
        {

        }

        public IPagedResult<DeviceHistorySummary> GetDeviceHistories(DeviceHistoryQuery request)
        {
            var sql = @"SELECT EVT_CODE MaSophieuKiemTra,EVT_ORG Chinhanh, eo.eob_object Serial,
                                r5o7.o7get_desc('VI','UCOD', e.evt_jobtype,'JBTP', '') Loaikiemtra,
                                r5o7.o7get_desc('VI','UCOD', e.evt_status,'EVST', '') Tinhtrangphieu,
                                r5o7.o7get_desc('VI','UCOD', e.evt_priority,'JBPR', '') TinhTrangSauKiemTra,
                                evt_udfchar02 KetQuaKiemTra,EVT_TARGET NgayBatDau, EVT_COMPLETED NgayHoanThanh 
                        FROM r5events e, r5eventobjects eo, r5activities a, r5tabpermissions, r5userorganization, r5ucodes u
                        WHERE eo.eob_object = :Serial 
	                        AND eo.eob_object_org = :Branch
	                        AND e.evt_code = eo.eob_event 
	                        AND e.evt_code = a.act_event (+)
	                        AND NOT EXISTS (SELECT b.act_act 
					                        FROM r5activities b 
					                        where b.act_event = COALESCE(a.act_event,'X') 
					                        AND b.act_act < COALESCE(a.act_act,0))
					                        AND e.evt_org = uog_org 
					                        AND uog_user = :Username 
					                        AND uog_group = trp_group
					                        AND trp_select = '?' 
					                        AND trp_tab = 'EVT' 
					                        AND trp_function = 'OSOBJA'
					                        AND u.uco_rentity (+) = 'JBTP' 
					                        AND u.uco_code (+) = e.evt_jobtype 
					                        AND (evt_jobtype IN ('BFTEST','AFTEST')) 
                        ORDER BY EVT_COMPLETED DESC";

            var sqlParameters = new OracleParameter[3];
            sqlParameters[0] = new OracleParameter("Serial", request.Serial.ToUpper());
            sqlParameters[1] = new OracleParameter("Branch", request.Branch.ToUpper());
            sqlParameters[2] = new OracleParameter("Username", request.Username.ToUpper());

            return new SqlPagedResult<DeviceHistorySummary>(dbContext, sql, request.Page, request.RecordsPerPage, sqlParameters);
        }
    }
}
