﻿using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Oracle.ManagedDataAccess.Client;
using System.Collections.Generic;
using System.Linq;

namespace Cmms.Data.Repositories
{
    public class RequisitionLineRepository : BaseRepository, IRequisitionLineRepository
    {
        public RequisitionLineRepository(CmmsDbContext dbContext) : base(dbContext)
        {
        }

        public IEnumerable<RequisitionLineSummary> GetDevicesForRequisition(string username, string requisitionCode)
        {
            var sql = @"SELECT  PAR_CLASS LoaiThietBi,
                                RQL_PART Serial,
                                r5o7.o7get_desc('VI','PART', rql_part||'#'||rql_part_org,'', '') Tenthietbi,
                                RQL_QTY Soluong,
                                RQL_UOM DonViTinh
                        FROM r5requislines
                            LEFT OUTER JOIN r5parts ON ( rql_part = par_code  AND rql_part_org = par_org )
                        WHERE rql_req = :RequisitionCode 
                            AND rql_rtype IN ( 'PS',  'PD', 'RE' )
                            AND exists (SELECT 1 
                                        FROM r5tabpermissions, r5userorganization
                                        WHERE uog_user = :Username AND uog_org = rql_part_org
                                            AND uog_group = trp_group AND trp_select = '?')
                        ORDER BY RQL_REQLINE ASC";

            return dbContext.Database.SqlQuery<RequisitionLineSummary>(sql,
                new OracleParameter("RequisitionCode", requisitionCode),
                new OracleParameter("Username", username.ToUpper())).ToArray();    
        }
    }
}
