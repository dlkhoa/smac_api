﻿using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using Oracle.ManagedDataAccess.Client;
using System.Linq;

namespace Cmms.Data.Repositories
{
    public class RequisitionRepository : BaseRepository, IRequisitionRepository
    {

        public RequisitionRepository(CmmsDbContext dbContext) : base(dbContext)
        {
        }

        public IPagedResult<RequisitionSummary> GetRequisitions(RequisitionQuery requisitionQuery)
        {
            /*var sql = string.Format(@"SELECT  req_desc Mota,
                                REQ_ORG Chinhanh,
                                r5o7.o7get_desc('VI','UCOD', req_status,'RQST', '') Tinhtrangphieu,
                                REQ_CODE MaSophieu, 
                                req_date as NgayTao,
                                req_org,req_class Loai,req_rtype
                        FROM r5requisitions r
                        WHERE   req_rtype = 'RECV' 
                            AND req_class IN ({0})
                            AND req_org like '%'||:Branch||'%'
                            AND EXISTS (    SELECT 1    
                                            FROM r5permissions, r5userorganization
                                            WHERE   uog_user = :Username 
                                                AND uog_org = req_org
                                                AND uog_group = prm_group 
                                                AND prm_select = '?' 
                                                AND prm_function IN ({1})
                                        )
                            AND r.REQ_TOCODE like '%SC-%'
                    ORDER BY req_date desc, req_code desc", requisitionQuery.RequisitionClass, requisitionQuery.PermissionFunction);*/
            var sql = string.Format(@"SELECT  req_desc Mota,
                                REQ_ORG Chinhanh,
                                r5o7.o7get_desc('VI','UCOD', req_status,'RQST', '') Tinhtrangphieu,
                                REQ_CODE MaSophieu, 
                                req_date as NgayTao,
                                req_org,req_class Loai,req_rtype
                        FROM r5requisitions r
                        WHERE req_rtype = 'RECV'
                          AND req_org like '%'||:Branch||'%'
                          AND EXISTS (
                            SELECT 1
                            FROM r5permissions, r5userorganization
                            WHERE UPPER(uog_user) = :Username
                              AND uog_org = req_org
                              AND uog_group = prm_group
                              AND prm_select = '?'
                              AND req_class IN ({0})
                              AND prm_function IN ({1})
                              AND req_fromcode IN (
                                  SELECT com_code
                                  FROM r5companies
                                  WHERE nvl(com_parent, com_code) IN (
                                      SELECT dse_mrc
                                      FROM r5departmentsecurity
                                      WHERE UPPER(dse_user) = :Username1
                                  )
                              )
                          )
                    ORDER BY req_date desc, req_code desc", requisitionQuery.RequisitionClass, requisitionQuery.PermissionFunction);
            var sqlParameters = new OracleParameter[3];
            sqlParameters[0] = new OracleParameter("Branch", requisitionQuery.Branch.ToUpper());
            sqlParameters[1] = new OracleParameter("Username", requisitionQuery.Username.ToUpper());
            sqlParameters[2] = new OracleParameter("Username1", requisitionQuery.Username.ToUpper());
            return new SqlPagedResult<RequisitionSummary>(dbContext, sql, requisitionQuery.Page, requisitionQuery.RecordsPerPage, sqlParameters);
        }

        public IPagedResult<RequisitionSummary> GetAwaitingRequisitions(RequisitionQuery requisitionQuery)
        {
            var sql = string.Format(@"SELECT  req_desc Mota,
                                REQ_ORG Chinhanh,
                                r5o7.o7get_desc('VI','UCOD', req_status,'RQST', '') Tinhtrangphieu,
                                REQ_CODE MaSophieu, 
                                req_date as NgayTao,
                                req_org,req_class,req_rtype
                        FROM r5requisitions r
                        WHERE   req_rtype = 'RECV' 
                            AND req_class IN ({0})
                            AND req_org like '%'||:Branch||'%'
                            AND EXISTS (    SELECT 1    
                                            FROM r5permissions, r5userorganization
                                            WHERE   uog_user = :Username 
                                                AND uog_org = req_org
                                                AND uog_group = prm_group 
                                                AND prm_select = '?' 
                                                AND prm_function IN ({1})
                                        ) 
                            AND r.REQ_TOCODE like '%SC-%'
                        ORDER BY req_date desc, req_code desc", 
                        requisitionQuery.RequisitionClass, 
                        requisitionQuery.PermissionFunction);
            var sqlParameters = new OracleParameter[2];
            sqlParameters[0] = new OracleParameter("Branch", requisitionQuery.Branch.ToUpper());
            sqlParameters[1] = new OracleParameter("Username", requisitionQuery.Username.ToUpper());

            return new SqlPagedResult<RequisitionSummary>(dbContext, sql, requisitionQuery.Page, requisitionQuery.RecordsPerPage, sqlParameters);
        }


        public RequisitionDetails GetRequisition(string requisitionCode)
        {
            var sql = @"SELECT  
                            req_desc Mota,
                            REQ_ORG Chinhanh,
                            r5o7.o7get_desc('VI','UCOD', req_status,'RQST', '') Tinhtrangphieu,
                            req_status MaTinhTrangPhieu,
                            REQ_TOCODE Kho,
                            REQ_FROMCODE Bengiao,
                            req_udfchar02 Nguoigiao,
                            REQ_CLASS Loai,
                            req_udfchar01 Bennhan,
                            req_origin Nguoinhan,
                            req_dateapproved NgayTiepNhan,
                            req_udfchar07 NoiDungBanGiao,
                            REQ_UDFDATE02 NgayGuiYeuCau,
                            REQ_UDFDATE04 NgayHoanThanhPhanLoai,
                            REQ_CODE MaSophieu 
                        FROM r5requisitions r
                        WHERE REQ_CODE=:RequisitionCode AND req_rtype = 'RECV'";

            var query = dbContext.Database
                                    .SqlQuery<RequisitionDetails>(sql, new OracleParameter("RequisitionCode", requisitionCode.ToUpper()));

            return query.FirstOrDefault();
        }

        public string GetCreatedUser(string requisitionCode)
        {
            return GetDbEntity<Requisition>()
                .Where(r => r.RequisitionCode.ToUpper() == requisitionCode.ToUpper())
                .Select(r => r.CreatedUser)
                .FirstOrDefault();
        }
    }
}
