﻿using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using Oracle.ManagedDataAccess.Client;
using System.Linq;

namespace Cmms.Data.Repositories
{
    public class PicklistRepository : BaseRepository, IPicklistRepository
    {
        public PicklistRepository(CmmsDbContext dbContext) : base(dbContext)
        {

        }
        public IPagedResult<PicklistSummary> GetPicklists(PicklistQuery picklistQuery)
        {
            var sql = string.Format(@"SELECT s.str_org ChiNhanh,p.pic_code MaSoPhieu,p.pic_desc MoTa,r5o7.o7get_desc('VI','UCOD', p.pic_status,'PLST', '') TinhTrangPhieu, pic_class Loai
                                    FROM r5picklists p, r5stores s
                                    WHERE pic_code IN
                                        ( SELECT p.pic_code
                                          FROM r5stores s, r5userorganization, r5permissions, r5picklists p 
                                          WHERE p.pic_store = s.str_code
                                            AND s.str_org like '%'||:Branch||'%' 
                                            AND uog_user = :Username
                                            AND uog_org=s.str_org
                                            AND uog_group = prm_group
                                            AND prm_select = '?'
                                            AND prm_function IN ({0}))
                                      AND p.pic_store=s.str_code
                                      AND p.PIC_STORE like '%SC-%'
                                    ORDER BY to_number(pic_code) DESC, pic_required DESC", picklistQuery.PermissionFunction);

            var sqlParameters = new OracleParameter[2];
            sqlParameters[0] = new OracleParameter("Branch", picklistQuery.Branch.ToUpper());
            sqlParameters[1] = new OracleParameter("Username", picklistQuery.Username.ToUpper());

            return new SqlPagedResult<PicklistSummary>(dbContext, sql, picklistQuery.Page, picklistQuery.RecordsPerPage, sqlParameters);
        }

        public PicklistDetails GetPicklist(string picklistCode)
        {
            var sql = @"SELECT  s.str_org ChiNhanh,
                                p.pic_code MaSoPhieu,
                                p.pic_desc MoTa,
                                p.pic_store Kho,
                                pic_class LoaiPhieu,
                                p.pic_status MaTinhTrangPhieu,
                                r5o7.o7get_desc('VI','UCOD', p.pic_status,'PLST', '') TinhTrangPhieu,
                                p.pic_supplier DoitacDVQLSD, 
                                p.pic_required NgayDeNghi, 
                                p.pic_origin NguoiDeNghi,
                                p.pic_auth NguoiPheDuyet,
                                p.pic_approv NgayPheDuyet 
                        FROM r5stores s, r5picklists p
                        WHERE p.pic_store = s.str_code
                            AND p.pic_code=:PicklistCode";

            var query = dbContext.Database
                                    .SqlQuery<PicklistDetails>(sql, new OracleParameter("PicklistCode", picklistCode.ToUpper()));

            return query.FirstOrDefault();
        }

        public IPagedResult<PicklistSummary> GetAwaitingPicklists(PicklistQuery picklistQuery)
        {
            var sql = string.Format(@"SELECT s.str_org ChiNhanh,p.pic_code MaSoPhieu,p.pic_desc MoTa,r5o7.o7get_desc('VI','UCOD', p.pic_status,'PLST', '') TinhTrangPhieu, pic_class Loai
                                    FROM r5picklists p, r5stores s
                                    WHERE pic_code IN
                                        ( SELECT p.pic_code
                                          FROM r5stores s, r5userorganization,r5permissions, r5picklists p,r5auth
                                          WHERE p.pic_store = s.str_code
	                                        AND s.str_org like '%'||:Branch||'%' 
                                            AND uog_user = :Username
	                                        AND uog_org=s.str_org
	                                        AND uog_group = prm_group
	                                        AND prm_select = '?'
	                                        AND aut_group=uog_group 
	                                        AND aut_entity='PICK' 
	                                        AND aut_status=pic_status
	                                        AND prm_function IN ({0}))
                                        AND p.pic_status IN ({1})
                                        AND p.pic_store=s.str_code
                                        AND p.PIC_STORE like '%SC-%'
                                    ORDER BY pic_required DESC, pic_code DESC", picklistQuery.PermissionFunction, picklistQuery.Status);

            var sqlParameters = new OracleParameter[2];
            sqlParameters[0] = new OracleParameter("Branch", picklistQuery.Branch.ToUpper());
            sqlParameters[1] = new OracleParameter("Username", picklistQuery.Username.ToUpper());

            return new SqlPagedResult<PicklistSummary>(dbContext, sql, picklistQuery.Page, picklistQuery.RecordsPerPage, sqlParameters);
        }

        public Picklist GetPicklistForUpdate(string code)
        {
            return GetDbEntity<Picklist>()
                .FirstOrDefault(p => p.Code == code);
        }

        public string GetNewStatus(string username, string picklistStatus, string picklistStore)
        {
            var sql = string.Format(@"SELECT NVL(aut_statnew,'{0}')
				                        FROM r5permissions, r5userorganization,r5auth,r5stores
				                        WHERE uog_user = :Username 
					                        AND uog_org = str_org 
                                            AND str_code = :PicklistStore
					                        AND uog_group = prm_group 
					                        AND prm_update = '*' 
					                        AND prm_function IN ('SSPICH','SSPICK','SSPICR')
					                        AND aut_group=uog_group 
					                        AND aut_entity='PICK'
					                        AND aut_status= '{0}'
                                        ORDER By aut_updated desc", picklistStatus);

            var sqlParameters = new OracleParameter[2];
            sqlParameters[0] = new OracleParameter("Username", username.ToUpper());
            sqlParameters[1] = new OracleParameter("PicklistStore", picklistStore);

            return dbContext.Database.SqlQuery<string>(sql, sqlParameters).FirstOrDefault();
        }

        public string GetCreatedUser(string picklistCode)
        {
            return GetDbEntity<Picklist>()
                .Where(p => p.Code.ToUpper() == picklistCode.ToUpper())
                .Select(p => p.CreatedUser)
                .FirstOrDefault();
        }
    }
}
