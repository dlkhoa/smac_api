﻿using Cmms.Data.Constants;
using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Commands;
using Cmms.Domain.Entities;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries.DeviceRepair;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cmms.Data.Repositories
{
    public class DocumentRepository : BaseRepository, IDocumentRepository
    {
        public DocumentRepository(CmmsDbContext dbContext)
            : base(dbContext)
        {

        }

        /* 
        /* type = 'PORD' => De nghi nhap kho / Dieu dong nhap kho thiet bi hong
        /* type = 'PICK' => De nghi xuat kho
        /*/
        public IEnumerable<DocumentDetails> GetDocumentList(string type, string orderCode)
        {
            var sql = string.Format(@"
                    SELECT DOC_CODE Code, DOC_DESC Description, DOC_FILENAME Filename
                    FROM R5DOCENTITIES
                    INNER JOIN R5DOCUMENTS ON DOC_CODE = DAE_DOCUMENT
                    INNER JOIN R5ORDERS ON ORD_CODE || '#' || ORD_ORG = DAE_CODE
                    WHERE DAE_ENTITY = '{0}' AND UPPER(DOC_FILENAME) LIKE '%.PDF' AND ORD_CODE = '{1}'", type, orderCode);
            if (type == "PICK")
            {
                sql = string.Format(@"
                    SELECT DOC_CODE Code, DOC_DESC Description, DOC_FILENAME Filename
                    FROM R5DOCENTITIES
                    INNER JOIN R5DOCUMENTS ON DOC_CODE = DAE_DOCUMENT
                    INNER JOIN R5PICKLISTS ON PIC_CODE = DAE_CODE
                    WHERE DAE_ENTITY = '{0}' AND UPPER(DOC_FILENAME) LIKE '%.PDF' AND PIC_CODE = '{1}'", type, orderCode);
            }

            return dbContext.Database.SqlQuery<DocumentDetails>(sql).ToList();
        }

    }
}
