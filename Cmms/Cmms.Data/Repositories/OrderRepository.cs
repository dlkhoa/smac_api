﻿using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using Oracle.ManagedDataAccess.Client;
using System.Linq;

namespace Cmms.Data.Repositories
{

    public class OrderRepository : BaseRepository, IOrderRepository
    {
        public OrderRepository(CmmsDbContext dbContext) : base(dbContext)
        {

        }

        public IPagedResult<OrderSummary> GetOrders(OrderQuery orderQuery)
        {
            //var sql = @"SELECT ord_code MaSoPhieu, ord_desc MoTa, ord_org ChiNhanh, r5o7.o7get_desc('VI','UCOD', ord_status,'DOST', '') TinhTrangPhieu
            //            FROM r5orders
            //            WHERE ord_org like '%'||:Branch||'%' 
            //                AND ord_org IN( SELECT uog_org FROM r5userorganization WHERE uog_user = :Username)
            //                AND ord_type = 'P'

            //            ORDER BY ord_date DESC, ord_code DESC";
            /*var sql = string.Format(@"SELECT ord_code MaSoPhieu, ord_desc MoTa, ord_org ChiNhanh, r5o7.o7get_desc('VI','UCOD', ord_status,'DOST', '') TinhTrangPhieu, 
                                                (select re.req_class 
                                                from r5requisitions re 
                                                    inner join r5orderlines orl on re.req_code = orl.orl_req 
                                                where orl.orl_order = r5orders.ORD_CODE and rownum=1) as Loai
                                    FROM r5orders
                                    WHERE ord_code IN (SELECT o.ord_code
                                            FROM r5orders o, r5companies c,r5permissions p, r5userorganization u
                                            WHERE ord_org like '%'||:Branch||'%' 
                                              AND o.ord_supplier = c.com_code
                                              AND o.ord_supplier_org = c.com_org
                                              AND u.uog_org=o.ord_org
                                              AND u.uog_user = :Username
                                              AND u.uog_group = p.prm_group
                                              AND p.prm_select = '?'
                                              AND p.prm_function IN ({0})
                                              AND o.ORD_STORE like '%SC-%'
                                              AND o.ord_supplier in (select com_code 
                                                                     from r5companies 
                                                                     where nvl(com_parent,com_code) in ( SELECT dse_mrc
                                                                                                         FROM r5departmentsecurity 
                                                                                                         WHERE dse_user = :Username2
                                                                                                       )
                                                                    )
                                            )
                                   ORDER BY to_number(ord_code) DESC, ord_date DESC", orderQuery.PermissionFunction);*/
            var sql = string.Format(@"SELECT ord_code MaSoPhieu, ord_desc MoTa, ord_org ChiNhanh, r5o7.o7get_desc('VI','UCOD', ord_status,'DOST', '') TinhTrangPhieu, 
                                                (select re.req_class 
                                                from r5requisitions re 
                                                    inner join r5orderlines orl on re.req_code = orl.orl_req 
                                                where orl.orl_order = r5orders.ORD_CODE and rownum=1) as Loai
                                    FROM r5orders
                                    WHERE ord_code IN (SELECT o.ord_code
                                            FROM r5orders o, r5companies c,r5permissions p, r5userorganization u
                                            WHERE ord_org like '%'||:Branch||'%' 
                                              AND o.ord_supplier = c.com_code
                                              AND o.ord_supplier_org = c.com_org
                                              AND u.uog_org=o.ord_org
                                              AND u.uog_user = :Username
                                              AND u.uog_group = p.prm_group
                                              AND p.prm_select = '?'
                                              AND p.prm_function IN ({0})
                                              AND o.ORD_STORE like '%SC-%'
                                            )
                                   ORDER BY to_number(ord_code) DESC, ord_date DESC", orderQuery.PermissionFunction);
            var sqlParameters = new OracleParameter[2];
            sqlParameters[0] = new OracleParameter("Branch", orderQuery.Branch.ToUpper());
            sqlParameters[1] = new OracleParameter("Username", orderQuery.Username.ToUpper());
            //sqlParameters[2] = new OracleParameter("Username2", orderQuery.Username.ToUpper());

            return new SqlPagedResult<OrderSummary>(dbContext, sql, orderQuery.Page, orderQuery.RecordsPerPage, sqlParameters);
        }

        public IPagedResult<OrderSummary> GetAwatingOrders(AwatingOrderQuery orderQuery)
        {
            /*var sql = string.Format(@"SELECT ord_code MaSoPhieu, ord_desc MoTa, ord_org ChiNhanh, r5o7.o7get_desc('VI','UCOD', ord_status,'DOST', '') TinhTrangPhieu, 
                                                (select re.req_class 
                                                from r5requisitions re 
                                                    inner join r5orderlines orl on re.req_code = orl.orl_req 
                                                where orl.orl_order = r5orders.ORD_CODE and rownum=1) as Loai
                                    FROM r5orders
                                    WHERE ord_code IN (  SELECT o.ord_code 
                                        FROM r5orders o, r5companies c,r5permissions p, r5userorganization u,r5auth
                                        WHERE ord_org like '%'||:Branch||'%'
                                          AND o.ord_supplier = c.com_code
                                          AND o.ord_supplier_org = c.com_org
                                          AND u.uog_org=o.ord_org
                                          AND u.uog_user = :Username
                                          AND u.uog_group = p.prm_group
                                          AND p.prm_select = '?'
                                          AND aut_group=uog_group 
                                          AND aut_entity='PORD' 
                                          AND aut_status=ord_status
                                          AND p.prm_function IN ({0}) 
                                          AND ord_status IN ({1})
                                          AND o.ORD_STORE like '%SC-%'
                                          AND o.ord_supplier in (select com_code 
                                                                     from r5companies 
                                                                     where nvl(com_parent,com_code) in ( SELECT dse_mrc
                                                                                                         FROM r5departmentsecurity 
                                                                                                         WHERE dse_user = :Username2
                                                                                                       )
                                                                    )
                                            )
                                   ORDER BY ord_date DESC, ord_code DESC", orderQuery.PermissionFunction, orderQuery.Type);*/
            var sql = string.Format(@"SELECT ord_code MaSoPhieu, ord_desc MoTa, ord_org ChiNhanh, r5o7.o7get_desc('VI','UCOD', ord_status,'DOST', '') TinhTrangPhieu, 
                                                (select re.req_class 
                                                from r5requisitions re 
                                                    inner join r5orderlines orl on re.req_code = orl.orl_req 
                                                where orl.orl_order = r5orders.ORD_CODE and rownum=1) as Loai
                                    FROM r5orders
                                    WHERE ord_code IN (  SELECT o.ord_code 
                                        FROM r5orders o, r5companies c,r5permissions p, r5userorganization u,r5auth
                                        WHERE ord_org like '%'||:Branch||'%'
                                          AND o.ord_supplier = c.com_code
                                          AND o.ord_supplier_org = c.com_org
                                          AND u.uog_org=o.ord_org
                                          AND u.uog_user = :Username
                                          AND u.uog_group = p.prm_group
                                          AND p.prm_select = '?'
                                          AND aut_group=uog_group 
                                          AND aut_entity='PORD' 
                                          AND aut_status=ord_status
                                          AND p.prm_function IN ({0}) 
                                          AND ord_status IN ({1})
                                          AND o.ORD_STORE like '%SC-%'
                                   )
                                   ORDER BY ord_date DESC, ord_code DESC", orderQuery.PermissionFunction, orderQuery.Type);

            var sqlParameters = new OracleParameter[2];
            sqlParameters[0] = new OracleParameter("Branch", orderQuery.Branch.ToUpper());
            sqlParameters[1] = new OracleParameter("Username", orderQuery.Username.ToUpper());
            //sqlParameters[2] = new OracleParameter("Username2", orderQuery.Username.ToUpper());

            return new SqlPagedResult<OrderSummary>(dbContext, sql, orderQuery.Page, orderQuery.RecordsPerPage, sqlParameters);
        }

        public OrderDetails GetOrder(string orderCode)
        {
            var sql = @"SELECT  ord_code MaSoPhieu, 
                                ord_desc MoTa, 
                                ord_org ChiNhanh, 
                                ord_status MaTinhTrangPhieu,
                                r5o7.o7get_desc('VI','UCOD', ord_status,'DOST', '') TinhTrangPhieu,
                                ORD_STORE Kho, 
                                ORD_ORIGIN NguoiDeNghi, 
                                ORD_DUE NgayDeNghi, 
                                ORD_AUTH NguoiPheDuyet, 
                                ORD_APPROV NgayPheDuyet, 
                                ORD_UDFCHAR03 LyDoNhapKho,
                                ORD_SUPPLIER NhanVeTu,
                                (select re.req_class 
                                                from r5requisitions re 
                                                    inner join r5orderlines orl on re.req_code = orl.orl_req 
                                                where orl.orl_order = r5orders.ORD_CODE and rownum=1) as Loai
                        FROM r5orders 
                        WHERE ord_code=:OrderCode";

            var query = dbContext.Database
                                    .SqlQuery<OrderDetails>(sql, new OracleParameter("OrderCode", orderCode.ToUpper()));

            return query.FirstOrDefault();
        }

        public string GetCreatedUser(string orderCode)
        {
            return GetDbEntity<Order>()
                .Where(o => o.OrderCode.ToUpper() == orderCode.ToUpper())
                .Select(o => o.CreatedUser)
                .FirstOrDefault();
        }
    }
}
