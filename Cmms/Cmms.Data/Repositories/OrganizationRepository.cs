﻿using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Cmms.Data.Repositories
{
    public class OrganizationRepository : BaseRepository, IOrganizationRepository
    {
        public OrganizationRepository(CmmsDbContext dbContext) : base(dbContext)
        {

        }

        public IEnumerable<Organization> GetBranches()
        {
            return GetDbEntity<Organization>().Where(o => o.OrgCode != "*").ToArray();
        }
    }
}
