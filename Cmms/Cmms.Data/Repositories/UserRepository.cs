﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities;

namespace Cmms.Data.Repositories
{
    public class UserRepository : BaseRepository, IUserRepository
    {
        public UserRepository(CmmsDbContext dbContext) : base(dbContext)
        {

        }

        public User GetUser(string username)
        {

            return GetDbEntity<User>()
                .Where(u => u.UserCode.ToUpper() == username.ToUpper())
                .Include(u => u.Group)
                .FirstOrDefault();
        }

        public string GetUserGroupCode(string username)
        {
            var groupCode = string.Empty;
            var user = GetDbEntity<User>()
                .Where(u => u.UserCode.ToUpper() == username.ToUpper())
                .FirstOrDefault();
            if (user != null)
            {
                groupCode = user.GroupCode;
            }
            return groupCode;
        }

        public List<string> GetUserManagers(string organization)
        {
            var managerGroup = new List<string> { "CN-LD-PT", "CN-SC-PT" };
            return GetDbEntity<User>()
                .Where(u => u.DefaultOrg == organization && managerGroup.Contains(u.GroupCode))
                .Select(u => u.UserCode)
                .ToList();
        }

        public List<string> Get_CN_SC_PT_Users(string organization)
        {
            return GetDbEntity<User>()
                .Where(u => u.DefaultOrg == organization && u.GroupCode == "CN-SC-PT")
                .Select(u => u.UserCode)
                .ToList();
        }

        public List<string> Get_LD_TP_Users(string organization)
        {
            return GetDbEntity<User>()
                .Where(u => u.DefaultOrg == organization && u.GroupCode == "LD-TP")
                .Select(u => u.UserCode)
                .ToList();
        }

        public List<string> GetAccountingUsers(string organization)
        {
            return GetDbEntity<User>()
                .Where(u => u.DefaultOrg == organization && (u.GroupCode == "KT-TP" || u.GroupCode == "KT-NV"))
                .Select(u => u.UserCode)
                .ToList();
        }

        public List<string> Get_NV_Users(string organization)
        {
            var NVGroup = new List<string> { "CN-SC-NV", "CN-LD-NV", "CN-SC-NV-HN*", "CN-SC-NV-HCM", "CN-SC-NV-DN" };
            return GetDbEntity<User>()
                .Where(u => u.DefaultOrg == organization && NVGroup.Contains(u.GroupCode))
                .Select(u => u.UserCode)
                .ToList();
        }


        public List<string> GetOranizations(string username)
        {
            if (string.IsNullOrWhiteSpace(username))
            {
                return new List<string>() { "CNHN", "CNHCM", "CNDN" };
            }

            var superuserGroup = new List<string> { "BLD-CN", "BLD-TT", "LD-TP", "LD-NV", "KT-TP", "KT-NV", "ADMIN" };
            var groupCode = string.Empty;
            var defaultOrg = string.Empty;
            var canSeeAllBranches = false;
            var user = GetDbEntity<User>()
                .Where(u => u.UserCode.ToUpper() == username.ToUpper())
                .FirstOrDefault();
            if (user != null)
            {
                groupCode = user.GroupCode;
                defaultOrg = user.DefaultOrg;
                if (superuserGroup.Contains(groupCode))
                {
                    canSeeAllBranches = true;
                }
            }
            if (canSeeAllBranches)
            {
                return new List<string>() { "CNHN", "CNHCM", "CNDN" };
            }
            else
            {
                return new List<string>() { defaultOrg };

            }
        }
        private string GetBranch(string username)
        {
            return GetDbEntity<User>()
                .Where(u => u.UserCode.ToUpper() == username.ToUpper() && !string.IsNullOrEmpty(u.DefaultOrg))
                .Select(u => u.DefaultOrg)
                .FirstOrDefault();

        }

    }
}
