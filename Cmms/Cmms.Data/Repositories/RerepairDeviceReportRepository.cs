﻿using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Entities.Views;
using Cmms.Domain.Queries;
using System.Collections.Generic;
using System.Linq;
using System;
using MBoL.Utils;
using Oracle.ManagedDataAccess.Client;

namespace Cmms.Data.Repositories
{
    public class RerepairDeviceReportRepository : BaseRepository, IRerepairDeviceReportRepository
    {
        public RerepairDeviceReportRepository(CmmsDbContext dbContext) : base(dbContext)
        {

        }

        public IEnumerable<RerepairDeliveryDeviceStatisticByDTSC> GetStatisticByDTSC(DeviceReportQuery request)
        {
            /*var query = GetDbEntity<DeviceReportView>()
                .Where(d => d.SLXH > 1 && d.NgayBGDT >= request.StartDate && d.NgayBGDT <= request.EndDate && request.Organizations.Contains(d.ChiNhanh))
                .GroupBy(d => new { d.ChiNhanh, d.DoiTuongThietBi, d.NhaCungCap, d.DoiTac })
                .Select(d => new RerepairDeliveryDeviceStatisticByDTSC
                {
                    ChiNhanh = d.Key.ChiNhanh,
                    NhaCungCap = d.Key.NhaCungCap,
                    DoiTuongTB = d.Key.DoiTuongThietBi,
                    DoiTac = d.Key.DoiTac,
                    Tong = d.Count()
                });

            return query.ToArray();*/
            var orgs = StringHelper.MakeOrgSearch(request.Organizations);
            var sql = string.Format(@"
                SELECT m1.chinhanh
	                ,m1.DoituongTB
	                ,m1.Nhacc NhaCungCap
	                ,m1.Doitac
	                ,count(1) tong
                FROM (
	                SELECT REQ_CODE
		                ,serial
		                ,CHINHANH
		                ,CODELOAITB
		                ,DoituongTB
		                ,DOITAC
		                ,Nhacc
		                ,DAIVT
		                ,NGAYCNNTK
	                FROM MV_BCCHITIET
	                WHERE NGAYCNNTK BETWEEN to_date('{1}', 'YYYY-MM-DD')
			                AND to_date('{2}', 'YYYY-MM-DD')
		                AND codekqtsc = '1'
	                ) m1
                LEFT OUTER JOIN MV_BCCHITIET m2 ON m1.REQ_CODE <> m2.REQ_CODE
	                AND m1.chinhanh = m2.chinhanh
	                AND m1.serial = m2.serial
	                AND m2.codekqtsc = '1'
	                AND TRUNC(m1.NGAYCNNTK) > TRUNC(m2.NGAYBGDVQLSD)
                WHERE m2.NGAYBGDVQLSD IS NOT NULL
	                AND TRUNC(m1.NGAYCNNTK) - TRUNC(m2.NGAYBGDVQLSD) BETWEEN 1 AND 90
                    AND m1.chinhanh IN ({0})
                GROUP BY m1.chinhanh
	                ,m1.DoituongTB
	                ,m1.Nhacc
	                ,m1.Doitac
                ORDER BY m1.chinhanh
	                ,m1.DoituongTB
	                ,m1.Nhacc
	                ,m1.Doitac"
                , orgs, request.StartDate.ToString("yyyy-MM-dd"), request.EndDate.ToString("yyyy-MM-dd"));
            return dbContext.Database.SqlQuery<RerepairDeliveryDeviceStatisticByDTSC>(sql).ToArray();
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDTSC(RerepairDeviceReportByDTSCQuery request)
        {
            /*var query = GetDbEntity<DeviceReportView>()
               .Where(d => d.ChiNhanh.ToUpper() == request.ChiNhanh.ToUpper()
                   && d.DoiTuongThietBi.ToUpper() == request.DoiTuongTB.ToUpper()
                   && d.NhaCungCap.ToUpper() == request.NhaCungCap.ToUpper()
                   && d.DoiTac == request.DoiTac.ToUpper()
                   && d.SLXH > 1 && d.NgayBGDT >= request.StartDate && d.NgayBGDT <= request.EndDate)
               .OrderByDescending(d => d.ReqCode)
               .ThenBy(d => d.Serial)
               .Select(d => new DeviceDetailsReportSummary
               {
                   Serial = d.Serial,
                   TenThietBi = d.TenThietBi,
                   TinhTrang = d.TinhTrang
               });

            return new PagedResult<DeviceDetailsReportSummary>(query, request.Page, request.RecordsPerPage);*/
            var sql = string.Format(@"
                SELECT
                    m1.serial,
                    m1.tentb TenThietBi,
                    m1.tinhtrang
                FROM
                    (
                        SELECT
                            serial,
                            tentb,
                            tinhtrang,
                            partnumber,
                            loaitb,
                            nhasx,
                            doituongtb,
                            dvqlsd1,
                            daivt,
                            sltn,
                            slxh,
                            dvqlsd2,
                            chinhanh,
                            doitac,
                            nhacc,
                            ngaycnntk,
                            req_code
                        FROM
                            mv_bcchitiet
                        WHERE
                            ngaycnntk BETWEEN TO_DATE('{0}', 'YYYY-MM-DD') AND TO_DATE('{1}', 'YYYY-MM-DD')
                            AND codekqtsc = '1'
                    ) m1
                    LEFT OUTER JOIN mv_bcchitiet m2 ON m1.req_code <> m2.req_code
                                                       AND m1.chinhanh = m2.chinhanh
                                                       AND m1.serial = m2.serial
                                                       AND m2.codekqtsc = '1'
                                                       AND trunc(m1.ngaycnntk) > trunc(m2.ngaybgdvqlsd)
                WHERE
                    m2.ngaybgdvqlsd IS NOT NULL
                    AND trunc(m1.ngaycnntk) - trunc(m2.ngaybgdvqlsd) BETWEEN 1 AND 90
                    AND upper(m1.chinhanh) = :ChiNhanh
                    AND upper(m1.doituongtb) = :DoiTuongTB
                    AND upper(m1.nhacc) = :NhaCungCap
                    AND upper(m1.doitac) = :DoiTac"
                , request.StartDate.ToString("yyyy-MM-dd"), request.EndDate.ToString("yyyy-MM-dd"));
            var query = dbContext.Database.SqlQuery<DeviceDetailsReportSummary>(sql,
                new OracleParameter("ChiNhanh", request.ChiNhanh.ToUpper()),
                new OracleParameter("DoiTuongTB", request.DoiTuongTB.ToUpper()),
                new OracleParameter("NhaCungCap", request.NhaCungCap.ToUpper()),
                new OracleParameter("DoiTac", request.DoiTac.ToUpper())).AsQueryable();
            return new PagedResult<DeviceDetailsReportSummary>(query, request.Page, request.RecordsPerPage);
        }

        public IEnumerable<RerepairDeviceStatisticByDVT> GetDeviceStatisticByDVT(DeviceReportQuery request)
        {
            /*var query = GetDbEntity<DeviceReportView>()
                .Where(d => d.SLXH > 1 && d.NgayBGDT >= request.StartDate && d.NgayBGDT <= request.EndDate && request.Organizations.Contains(d.ChiNhanh))
                .GroupBy(d => new { d.ChiNhanh, d.DVQLSD, d.DaiVienThong })
                .Select(d => new RerepairDeviceStatisticByDVT
                {
                    ChiNhanh = d.Key.ChiNhanh,
                    DaiVT = d.Key.DaiVienThong,
                    DVQLSD = d.Key.DVQLSD,
                    Tong = d.Count()
                });

            return query.ToArray();*/
            var orgs = StringHelper.MakeOrgSearch(request.Organizations);
            var sql = string.Format(@"
                SELECT
                    m1.chinhanh,
                    m1.dvqlsd1 DVQLSD,
                    m1.daivt,
                    COUNT(1) tong
                FROM
                    (
                        SELECT
                            req_code,
                            serial,
                            chinhanh,
                            codeloaitb,
                            doituongtb,
                            doitac,
                            nhacc,
                            dvqlsd1,
                            daivt,
                            ngaycnntk
                        FROM
                            mv_bcchitiet
                        WHERE
                            ngaycnntk BETWEEN TO_DATE('{1}', 'YYYY-MM-DD') AND TO_DATE('{2}', 'YYYY-MM-DD')
                            AND codekqtsc = '1'
                    ) m1
                    LEFT OUTER JOIN mv_bcchitiet m2 ON m1.req_code <> m2.req_code
                                                       AND m1.chinhanh = m2.chinhanh
                                                       AND m1.serial = m2.serial
                                                       AND m2.codekqtsc = '1'
                                                       AND trunc(m1.ngaycnntk) > trunc(m2.ngaybgdvqlsd)
                WHERE
                    m2.ngaybgdvqlsd IS NOT NULL
                    AND trunc(m1.ngaycnntk) - trunc(m2.ngaybgdvqlsd) BETWEEN 1 AND 90
                    AND m1.chinhanh IN ({0})
                GROUP BY
                    m1.chinhanh,
                    m1.dvqlsd1,
                    m1.daivt
                ORDER BY
                    m1.chinhanh,
                    m1.dvqlsd1,
                    m1.daivt"
                , orgs, request.StartDate.ToString("yyyy-MM-dd"), request.EndDate.ToString("yyyy-MM-dd"));
            return dbContext.Database.SqlQuery<RerepairDeviceStatisticByDVT>(sql).ToArray();
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVT(RerepairDeviceReportByDVTQuery request)
        {
            /*var query = GetDbEntity<DeviceReportView>()
               .Where(d => d.ChiNhanh.ToUpper() == request.ChiNhanh.ToUpper()
                   && d.DVQLSD.ToUpper() == request.DVQLSD.ToUpper()
                   && d.DaiVienThong.ToUpper() == request.DaiVT.ToUpper()
                   //&& d.DoiTac != null
                   && d.SLXH > 1 && d.NgayBGDT >= request.StartDate && d.NgayBGDT <= request.EndDate)
               .OrderByDescending(d => d.ReqCode)
               .ThenBy(d => d.Serial)
               .Select(d => new DeviceDetailsReportSummary
               {
                   Serial = d.Serial,
                   TenThietBi = d.TenThietBi,
                   TinhTrang = d.TinhTrang
               });

            return new PagedResult<DeviceDetailsReportSummary>(query, request.Page, request.RecordsPerPage);*/
            var sql = string.Format(@"
                SELECT
                    m1.serial,
                    m1.tentb TenThietBi,
                    m1.TinhTrang
                FROM
                    (
                        SELECT
                            serial,
                            tentb,
                            tinhtrang,
                            partnumber,
                            loaitb,
                            nhasx,
                            doituongtb,
                            dvqlsd1,
                            daivt,
                            sltn,
                            slxh,
                            dvqlsd2,
                            chinhanh,
                            doitac,
                            nhacc,
                            ngaycnntk,
                            req_code
                        FROM
                            mv_bcchitiet
                        WHERE
                            ngaycnntk BETWEEN TO_DATE('{0}', 'YYYY-MM-DD') AND TO_DATE('{1}', 'YYYY-MM-DD')
                            AND codekqtsc = '1'
                    ) m1
                    LEFT OUTER JOIN mv_bcchitiet m2 ON m1.req_code <> m2.req_code
                                                       AND m1.chinhanh = m2.chinhanh
                                                       AND m1.serial = m2.serial
                                                       AND m2.codekqtsc = '1'
                                                       AND trunc(m1.ngaycnntk) > trunc(m2.ngaybgdvqlsd)
                WHERE
                    m2.ngaybgdvqlsd IS NOT NULL
                    AND trunc(m1.ngaycnntk) - trunc(m2.ngaybgdvqlsd) BETWEEN 1 AND 90
                    AND upper(m1.chinhanh) = :ChiNhanh
                    AND upper(m1.dvqlsd1) = :DVQLSD
                    AND upper(m1.daivt) = :DaiVT"
                , request.StartDate.ToString("yyyy-MM-dd"), request.EndDate.ToString("yyyy-MM-dd"));
            var query = dbContext.Database.SqlQuery<DeviceDetailsReportSummary>(sql,
                new OracleParameter("ChiNhanh", request.ChiNhanh.ToUpper()),
                new OracleParameter("DVQLSD", request.DVQLSD.ToUpper()),
                new OracleParameter("DaiVT", request.DaiVT.ToUpper())).AsQueryable();
            return new PagedResult<DeviceDetailsReportSummary>(query, request.Page, request.RecordsPerPage);
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryAllLevel(DeviceReportAllLevelQuery request)
        {
            var query = GetDbEntity<DeviceReportView>()
               .Where(d => d.ChiNhanh.ToUpper() == request.ChiNhanh.ToUpper()
                   //&& d.DoiTac != null
                   && d.SLXH > 1 && d.NgayBGDT >= request.StartDate && d.NgayBGDT <= request.EndDate);

            if (!string.IsNullOrWhiteSpace(request.DVQLSD))
            {
                query = query.Where(d => d.DVQLSD.ToUpper() == request.DVQLSD.ToUpper());
            }

            if (!string.IsNullOrWhiteSpace(request.DaiVT))
            {
                query = query.Where(d => d.DaiVienThong.ToUpper() == request.DaiVT.ToUpper());
            }

            if (!string.IsNullOrWhiteSpace(request.NhaCungCap))
            {
                query = query.Where(d => d.NhaCungCap.ToUpper() == request.NhaCungCap.ToUpper());
            }

            if (!string.IsNullOrWhiteSpace(request.DoiTuongTB))
            {
                query = query.Where(d => d.DoiTuongThietBi.ToUpper() == request.DoiTuongTB.ToUpper());
            }


            if (!string.IsNullOrWhiteSpace(request.DoiTac))
            {
                query = query.Where(d => d.DoiTac.ToUpper() == request.DoiTac.ToUpper());
            }
            else
            {
                query = query.Where(d => d.DoiTac != null);
            }

            query = query.OrderByDescending(d => d.ReqCode)
               .ThenBy(d => d.Serial);

            var result = query
               .Select(d => new DeviceDetailsReportSummary
               {
                   Serial = d.Serial,
                   TenThietBi = d.TenThietBi,
                   TinhTrang = d.TinhTrang
               });

            return new PagedResult<DeviceDetailsReportSummary>(result, request.Page, request.RecordsPerPage);
        }
    }
}
