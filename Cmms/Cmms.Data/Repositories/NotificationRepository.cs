﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cmms.Data.Constants;
using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities;
using Oracle.ManagedDataAccess.Client;

namespace Cmms.Data.Repositories
{
    public class NotificationRepository : BaseRepository, INotificationRepository
    {
        public NotificationRepository(CmmsDbContext dbContext) : base(dbContext)
        {
        }

        public MobileDevice GetDevice(string deviceId)
        {
            return GetDbEntity<MobileDevice>()
                .FirstOrDefault(d => d.DeviceId.Equals(deviceId));
        }

        public int AddNotificationDevice(MobileDevice mobileDevice)
        {
            //GetDbEntity<MobileDevice>().Add(mobileDevice);
            var sql = "INSERT INTO r5devicesInfor (DI_USERNAME,DI_DEVICEID,DI_DEVICEOS) VALUES (:Username,:DeviceID,:OS)";
            return dbContext.Database.ExecuteSqlCommand(sql, 
                new OracleParameter("Username", mobileDevice.Username.ToUpper()), 
                new OracleParameter("DeviceID", mobileDevice.DeviceId), 
                new OracleParameter("OS", mobileDevice.DeviceOS));
        }

        public bool IsExisted(string deviceId, string username)
        {
            return GetDbEntity<MobileDevice>()
                .Any(d => d.DeviceId.Equals(deviceId) && d.Username.Equals(username, StringComparison.OrdinalIgnoreCase));
        }

        public List<string> GetDeviceIDs(List<string> usernames)
        {
            return GetDbEntity<MobileDevice>()
                .Where(d => usernames.Contains(d.Username))
                .Select(d => d.DeviceId)
                .ToList();
        }

        public int DeleteMobileDevice(MobileDevice mobileDevice)
        {
            var sql = "DELETE FROM r5DevicesInfor WHERE DI_DEVICEID = :DeviceID";
            return dbContext.Database.ExecuteSqlCommand(sql, new OracleParameter("DeviceID", mobileDevice.DeviceId));
        }

        public IEnumerable<NotificationItem> GetUnsendNotifications()
        {
            //return GetDbEntity<NotificationItem>()
            //    .Where(n => (n.IsNotified == null || n.IsNotified != "1")
            //            && ((n.EntityType == EntityTypeConstants.Order 
            //                    && ((n.FromStatus == null && n.ToStatus == OrderStatusConstant.ChoLanhDaoChiNhanhPheDuyet) 
            //                     || (n.FromStatus == OrderStatusConstant.ChoLanhDaoChiNhanhPheDuyet && n.ToStatus == OrderStatusConstant.DaPheDuyetTaiChiNhanh)))
            //                || (n.EntityType == EntityTypeConstants.Picklist 
            //                    && ((n.FromStatus == null && n.ToStatus == PicklistStatusConstants.ChoPheDuyetTaiChiNhanh) 
            //                        || (n.FromStatus==PicklistStatusConstants.ChoPheDuyetTaiChiNhanh && n.ToStatus == PicklistStatusConstants.DaPheDuyetTaiChiNhanh)))))
            //    .ToArray();
            var sql = @"SELECT AVA_PRIMARYID as PrimaryId, AVA_TABLE as EntityType, AVA_FROM as FromStatus, AVA_TO as ToStatus, NOTIFICATION as IsNotified 
                        FROM r5sendemail 
                        WHERE (notification IS NULL OR notification <> '1')
                          AND ((ava_table ='R5ORDERS' AND ((ava_from is null AND (ava_to='U' OR ava_to='U-CN')) OR (ava_from='U' AND ava_to='A-CN') OR (ava_from='U-CN' AND ava_to='W-CN') OR (ava_from='W-CN' AND ava_to='A-CN')) AND AVA_PRIMARYID IN (SELECT DISTINCT ORD_CODE FROM R5ORDERS WHERE ORD_STORE LIKE '%SC-%'))
                            OR (ava_table ='R5PICKLISTS' AND ((ava_from is null AND ava_to='U') OR (ava_from='U' AND ava_to='R')  OR (ava_from='R' AND ava_to='A')) AND AVA_PRIMARYID IN (SELECT DISTINCT PIC_CODE FROM R5PICKLISTS WHERE PIC_STORE LIKE '%SC-%'))
                            OR (ava_table ='R5REQUISITIONS' AND ((ava_from = 'U-CN' AND ava_to='U-SC') OR (ava_from='U-SC' AND ava_to='A-SC')) AND AVA_PRIMARYID IN (SELECT DISTINCT REQ_CODE FROM R5REQUISITIONS WHERE REQ_TOCODE LIKE '%SC-%')))";

            return dbContext.Database.SqlQuery<NotificationItem>(sql).ToArray();
        }

        public void AddNotificationItem(NotificationItem notificationItem)
        {
            GetDbEntity<NotificationItem>().Add(notificationItem);
        }

        public void UpdateNotificationStatus(string primaryId, string entityType, string toStatus)
        {
            var sql = @"UPDATE r5sendemail
                        SET notification = '1'
                        WHERE ava_primaryid=:primaryId AND ava_table=:entityType AND ava_to=:toStatus";
            var sqlParameters = new OracleParameter[3];
            sqlParameters[0] = new OracleParameter("primaryId", primaryId);
            sqlParameters[1] = new OracleParameter("entityType", entityType);
            sqlParameters[2] = new OracleParameter("toStatus", toStatus);

            dbContext.Database.ExecuteSqlCommand(sql, sqlParameters);
        }

        public int DeleteAll()
        {
            var sql = @"DELETE FROM R5DEVICESINFOR";
            return dbContext.Database.ExecuteSqlCommand(sql);
        }
    }
}
