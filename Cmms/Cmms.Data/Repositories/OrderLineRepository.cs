﻿using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Oracle.ManagedDataAccess.Client;
using System.Collections.Generic;
using System.Linq;

namespace Cmms.Data.Repositories
{
    public class OrderLineRepository : BaseRepository, IOrderLineRepository
    {
        public OrderLineRepository(CmmsDbContext dbContext) : base(dbContext)
        {

        }

        public IEnumerable<OrderLineSummary> GetOrderLines(string orderCode)
        {
            var sql = @"SELECT orl_ordline SoThuTu,orl_part Serial,r5o7.o7get_desc('VI','PART',orl_part||'#'||orl_part_org,'', '') TenThietbi,
                            round(nvl(orl_ordqty,0)/nvl(orl_multiply,1), 6) SoLuong,p.par_uom DonViTinh,orl_req MaSoPhieuTiepNhan 
                        FROM r5orderlines o 
                          LEFT OUTER JOIN r5parts p ON o.orl_part = p.par_code AND o.orl_part_org = p.par_org
                          LEFT OUTER JOIN r5catalogue c ON o.orl_part = c.cat_part 
                            AND o.orl_part_org = c.cat_part_org 
                            AND o.orl_supplier = c.cat_supplier 
                            AND o.orl_supplier_org = c.cat_supplier_org
                          LEFT OUTER JOIN r5requislines r ON o.orl_req = r.rql_req AND o.orl_reqline = r.rql_reqline
                        WHERE o.orl_order= :OrderCode
                          AND o.orl_rtype IN ('PD','PS','RE' )
                        ORDER BY orl_ordline ASC";
            return dbContext.Database.SqlQuery<OrderLineSummary>(sql,
                new OracleParameter("OrderCode", orderCode)).ToArray();
        }
    }
}
