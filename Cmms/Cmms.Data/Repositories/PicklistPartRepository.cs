﻿using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Oracle.ManagedDataAccess.Client;
using System.Collections.Generic;
using System.Linq;

namespace Cmms.Data.Repositories
{
    public class PicklistPartRepository : BaseRepository, IPicklistPartRepository
    {
        public PicklistPartRepository(CmmsDbContext dbContext):base(dbContext)
        {

        }

        public IEnumerable<PicklistPartSummary> GetPicklistPartsForPicklist(string picklistCode)
        {
            var sql = @"SELECT pip_part Serial,par_Desc TenThietBi,pip_qty SoLuong,par_uom DonViTinh 
                        FROM r5picklistparts inner join r5parts on par_code=pip_part and par_org=pip_part_org
                        WHERE pip_picklist=:PicklistCode";

            return dbContext.Database.SqlQuery<PicklistPartSummary>(sql,
                new OracleParameter("PicklistCode", picklistCode)).ToArray();
        }
    }
}
