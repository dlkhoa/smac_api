﻿using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using Oracle.ManagedDataAccess.Client;

namespace Cmms.Data.Repositories
{
    public class BranchRepository : BaseRepository, IBranchRepository
    {
        public BranchRepository(CmmsDbContext dbContext) : base(dbContext)
        {

        }

        public IPagedResult<Branch> GetBranches(BranchQuery branchQuery)
        {
            var sql = @"SELECT ORG_CODE Machinhanh,ORG_DESC Tenchinhanh,ORG_CURR Tiente ,loc_desc Vitri
                        FROM r5organization a, r5locale l,r5descriptions e
                        WHERE org_locale = l.loc_code
	                        AND e.des_rentity = 'UCOD'
	                        AND e.des_lang='VI'
	                        AND e.des_code=org_depmethod
	                        AND e.des_rtype = 'DPTP'
	                        AND org_code<>'*'
                            AND org_code like '%'||:Branch||'%'
                        ORDER BY ORG_CODE ASC";

            return new SqlPagedResult<Branch>(dbContext,
                sql,
                branchQuery.Page,
                branchQuery.RecordsPerPage,
                new OracleParameter("Branch", branchQuery.Branch.ToUpper()));

        }
    }
}
