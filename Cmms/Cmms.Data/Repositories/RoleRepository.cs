﻿using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Cmms.Data.Repositories
{
    public class RoleRepository : BaseRepository, IRoleRepository
    {
        public RoleRepository(CmmsDbContext dbContext) : base(dbContext)
        {

        }

        public IEnumerable<Role> GetRoleByGroupCode(string groupCode)
        {
            return GetDbEntity<Role>()
                .Where(r => r.GroupCode.ToUpper() == groupCode.ToUpper())
                .ToArray();
        }
    }
}
