﻿using Cmms.Data.Constants;
using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Commands;
using Cmms.Domain.Entities;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries.DeviceRepair;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Cmms.Data.Repositories
{
    public class DeviceRepairRepository : BaseRepository, IDeviceRepairRepository
    {
        public DeviceRepairRepository(CmmsDbContext dbContext)
            : base(dbContext)
        {

        }

        public RepairDeviceDetails GetRepairDevice(RepairDeviceQuery request)
        {
            RepairDeviceDetails result = null;
            var sql = @"select SC_CODE ID, SC_BARCODE Barcode, SC_TENTB DeviceName, SC_TRANGTHAI Status, SC_UUTIEN PriorityStr, SC_KETQUA_SSC RepairedSuccessStr,
                            SC_TVT TVT, SC_DVT DVT, SC_GHICHU Notes,
                            SC_NGAYHONG BROKENDATE, SC_NGAYDVT_NHAN BrokenReceivedDate
                        from U5SCTB 
                        where (upper(SC_BARCODE)=:Barcode or upper(SC_SERIAL_TSC)=:Barcode or upper(SC_SERIAL_SSC)=:Barcode) and rownum=1
                        order by SC_NGAYCAPNHAT desc";
            var sqlParameters = new OracleParameter[1];
            sqlParameters[0] = new OracleParameter("Barcode", request.Barcode.ToUpper());

            result = dbContext.Database.SqlQuery<RepairDeviceDetails>(sql, sqlParameters).FirstOrDefault();

            if (result == null)
            {
                sql = @"SELECT UPPER(OBJ_CODE) Barcode, OBJ_DESC DeviceName, '" + StatusConstants.RepairedDelivered + @"' Status
                        FROM R5OBJECTS
                        WHERE OBJ_ORG IN 
                            (SELECT UOG_ORG FROM R5USERORGANIZATION WHERE UPPER(UOG_USER)=:Username AND OBJ_OBTYPE = 'A' AND UPPER(OBJ_CODE) = :Barcode)";
                sqlParameters = new OracleParameter[2];
                sqlParameters[0] = new OracleParameter("Username", request.Username.ToUpper());
                sqlParameters[1] = new OracleParameter("Barcode", request.Barcode.ToUpper());

                result = dbContext.Database.SqlQuery<RepairDeviceDetails>(sql, sqlParameters).FirstOrDefault();
            }

            if (result == null)
            {
                result = new RepairDeviceDetails
                {
                    Barcode = request.Barcode,
                    Status = StatusConstants.RepairedDelivered
                };
            }

            if (!string.IsNullOrEmpty(result.Id))
            {
                string sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/" + CommonConstant.UploadedImagesDiectory + "/" + result.Id + "/");
                if (Directory.Exists(sPath))
                {
                    string[] fileArray = Directory.GetFiles(sPath);
                    if (fileArray != null && fileArray.Count() > 0)
                    {
                        result.Attachments = fileArray.Select(f => CommonConstant.UploadedImagesDiectory + "/" + result.Id + "/" + Path.GetFileName(f)).ToArray();
                    }
                }
            }

            return result;
        }

        public RepairDeviceDetails GetRepairDeviceById(RepairDeviceQuery request)
        {
            RepairDeviceDetails result = null;
            var sql = @"select SC_CODE ID, SC_BARCODE Barcode, SC_TENTB DeviceName, SC_TRANGTHAI Status, SC_UUTIEN PriorityStr, SC_KETQUA_SSC RepairedSuccessStr,
                            SC_TVT TVT, SC_DVT DVT, SC_GHICHU Notes,
                            SC_NGAYHONG BROKENDATE, SC_NGAYDVT_NHAN BrokenReceivedDate
                        from U5SCTB 
                        where upper(SC_CODE)=:Id";
            var sqlParameters = new OracleParameter[1];
            sqlParameters[0] = new OracleParameter("Id", request.Id.ToUpper());

            result = dbContext.Database.SqlQuery<RepairDeviceDetails>(sql, sqlParameters).FirstOrDefault();

            if (result != null && !string.IsNullOrEmpty(result.Id))
            {
                string sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/" + CommonConstant.UploadedImagesDiectory + "/" + result.Id + "/");
                if (Directory.Exists(sPath))
                {
                    string[] fileArray = Directory.GetFiles(sPath);
                    if (fileArray != null && fileArray.Count() > 0)
                    {
                        result.Attachments = fileArray.Select(f => CommonConstant.UploadedImagesDiectory + "/" + result.Id + "/" + Path.GetFileName(f)).ToArray();
                    }
                }
            }

            return result;
        }

        public RepairDeviceDetails GetRepairedDevice(RepairDeviceQuery request)
        {
            RepairDeviceDetails result = null;

            var sql = @"SELECT OBJ_UDFCHAR45 BARCODE, OBJ_SERIALNO SERIALBEFORE
FROM R5OBJECTS
WHERE OBJ_OBTYPE = 'A' AND(NVL(OBJ_UDFCHAR13, '') = :SerialAfter OR OBJ_SERIALNO = :SerialBefore) AND OBJ_ORG = :Branch";
            var sqlParameters = new OracleParameter[3];
            sqlParameters[0] = new OracleParameter("SerialAfter", request.Serial.ToUpper());
            sqlParameters[1] = new OracleParameter("SerialBefore", request.Serial.ToUpper());
            sqlParameters[2] = new OracleParameter("Branch", request.Branch.ToUpper());

            var repairedDeviceStatus = dbContext.Database.SqlQuery<RepairedDeviceStatus>(sql, sqlParameters).FirstOrDefault();

            if (repairedDeviceStatus != null)
            {

                sql = string.Format(@"select SC_CODE ID, SC_BARCODE Barcode, SC_TENTB DeviceName, SC_TRANGTHAI Status, SC_UUTIEN PriorityStr, SC_KETQUA_SSC RepairedSuccessStr,
                            SC_TVT TVT, SC_DVT DVT, 
                            SC_NGAYHONG BROKENDATE, SC_NGAYDVT_NHAN BrokenReceivedDate
                        from U5SCTB 
                        where (upper(SC_BARCODE)=:Barcode and upper(SC_SERIAL_TSC)=:SerialBefore) and SC_TRANGTHAI='{0}' and rownum=1
                        order by SC_NGAYCAPNHAT desc", StatusConstants.BrokenDelivered);
                sqlParameters = new OracleParameter[2];
                sqlParameters[0] = new OracleParameter("Barcode", repairedDeviceStatus.Barcode.ToUpper());
                sqlParameters[1] = new OracleParameter("SerialBefore", repairedDeviceStatus.SerialBefore.ToUpper());

                result = dbContext.Database.SqlQuery<RepairDeviceDetails>(sql, sqlParameters).FirstOrDefault();

                if (result != null)
                {
                    result.SerialAfter = request.Serial.ToUpper();
                    sql = @"SELECT CASE WHEN OBJ_UDFCHAR14 = 'OK' THEN '+'
            WHEN OBJ_UDFCHAR14 = 'FAULT' THEN '-'
            WHEN OBJ_UDFCHAR08 = 'OK' THEN '+'
            WHEN OBJ_UDFCHAR08 = 'FAULT' THEN '-'
            ELSE '-' END TTTB_SSC
FROM R5OBJECTS WHERE (NVL(OBJ_UDFCHAR13,'') = :SerialBefore OR OBJ_CODE = :SerialAfter) AND OBJ_ORG IN(SELECT UOG_ORG FROM R5USERORGANIZATION WHERE UOG_USER = :Username)";
                    sqlParameters = new OracleParameter[3];
                    sqlParameters[0] = new OracleParameter("SerialAfter", request.Serial.ToUpper());
                    sqlParameters[1] = new OracleParameter("SerialAfter", request.Serial.ToUpper());
                    sqlParameters[2] = new OracleParameter("Username", request.Username.ToUpper());
                    result.RepairedSuccessStr = dbContext.Database.SqlQuery<string>(sql, sqlParameters).FirstOrDefault();
                }
            }

            return result;
        }

        public IEnumerable<string> GetDeviceNames()
        {
            var sql = @"SELECT DISTINCT TEN FROM U5R5DESCOBJECT ORDER BY TEN";

            var result = dbContext.Database.SqlQuery<string>(sql).ToList();

            return result;
        }

        public UserUnit GetUserUnit(string username)
        {
            var userUnit = new UserUnit
            {
                UserCode = username
            };
            var userTVT = GetDbEntity<UserTVT>()
                .Where(u => u.UserCode.ToUpper() == username.ToUpper())
                .FirstOrDefault();

            if (userTVT != null)
            {
                userUnit.TVT = userTVT.TVT;
                userUnit.DVT = userTVT.DVT;
            }
            else
            {
                var userDVT = GetDbEntity<UserDVT>()
                    .Where(u => u.UserCode.ToUpper() == username.ToUpper())
                    .FirstOrDefault();
                if (userDVT != null)
                {
                    userUnit.DVT = userDVT.DVT;
                }
            }

            return userUnit;
        }

        public string AddRepairDevice(AddRepairDeviceCommand command)
        {
            var id = DateTime.Now.Ticks.ToString();

            var organization = string.Empty;
            if (!string.IsNullOrEmpty(command.TVT))
            {
                var TVT = GetDbEntity<TVT>()
                    .Where(u => u.Code.ToUpper() == command.TVT.ToUpper())
                    .FirstOrDefault();
                if (TVT != null)
                {
                    organization = TVT.Organization;
                }
            }

            var sql = string.Empty;
            OracleParameter[] sqlParameters;
            var count = 0;

            if (!string.IsNullOrEmpty(command.Barcode))
            {
                sql = @"select count(*)
                        from U5SCTB 
                        where (upper(SC_BARCODE)=:Barcode or upper(SC_SERIAL_TSC)=:Barcode or upper(SC_SERIAL_SSC)=:Barcode)";
                sqlParameters = new OracleParameter[1];
                sqlParameters[0] = new OracleParameter("Barcode", command.Barcode.ToUpper());
                count = dbContext.Database.SqlQuery<int>(sql, sqlParameters).FirstOrDefault();
            }

            var barcode = command.Barcode;
            if (command.SystemBarcode && command.Status == StatusConstants.BrokenReceived)
            {
                sql = @"select count(*)
                        from U5SCTB 
                        where SC_MOBARCODE = '+' and upper(SC_DVT) = :DVT";
                sqlParameters = new OracleParameter[1];
                sqlParameters[0] = new OracleParameter("DVT", command.DVT.ToUpper());
                var countSystemBarcode = dbContext.Database.SqlQuery<int>(sql, sqlParameters).FirstOrDefault();
                barcode = command.DVT.ToUpper() + "-" + (countSystemBarcode + 1);
            }

            var sqlCommand = @"insert into u5sctb
                                (SC_CODE, SC_BARCODE, SC_TRANGTHAI, SC_TENTB, SC_UUTIEN, SC_NGAYHONG, SC_NGAYDVT_NHAN, SC_MOBARCODE, SC_LANXH,
                                SC_TVT, SC_DVT, SC_CHINHANH,
                                SC_NGUOITAO, SC_NGAYTAO, SC_GHICHU)
                                values
                                (:Id, :Barcode, :Status, :DeviceName, :Priority, :BrokenDate, :BrokenReceivedDate, :SystemBarcode, :NbOccurrences,
                                :TVT, :DVT, :Organization,
                                :CreatedBy, sysdate, :Notes)";
            sqlParameters = new OracleParameter[14];
            sqlParameters[0] = new OracleParameter("Id", id);
            sqlParameters[1] = new OracleParameter("Barcode", barcode);
            sqlParameters[2] = new OracleParameter("Status", command.Status);
            sqlParameters[3] = new OracleParameter("DeviceName", command.DeviceName);
            sqlParameters[4] = new OracleParameter("Priority", command.Priority ? "+" : "-");
            sqlParameters[5] = new OracleParameter("BrokenDate", command.BrokenDate);
            sqlParameters[6] = new OracleParameter("BrokenReceivedDate", command.BrokenReceivedDate);
            sqlParameters[7] = new OracleParameter("SystemBarcode", command.SystemBarcode ? "+" : "-");
            sqlParameters[8] = new OracleParameter("NbOccurrences", count + 1);
            sqlParameters[9] = new OracleParameter("TVT", command.TVT);
            sqlParameters[10] = new OracleParameter("DVT", command.DVT);
            sqlParameters[11] = new OracleParameter("Organization", organization);
            sqlParameters[12] = new OracleParameter("CreatedBy", command.Username);
            sqlParameters[13] = new OracleParameter("Notes", command.Notes);

            var result = dbContext.Database.ExecuteSqlCommand(sqlCommand, sqlParameters);

            return id;
        }

        public IEnumerable<string> GetCenterList(string userCode)
        {
            var sql = @"SELECT DISTINCT DSE_MRC
                        FROM R5DEPARTMENTSECURITY
                        WHERE UPPER(DSE_USER) = :UserCode AND DSE_MRC NOT LIKE '*' AND DSE_MRC LIKE '%ML%'";
            var sqlParameters = new OracleParameter[1];
            sqlParameters[0] = new OracleParameter("UserCode", userCode.ToUpper());

            var result = dbContext.Database.SqlQuery<string>(sql, sqlParameters).ToList();

            return result;
        }

        public IPagedResult<AvailableDeviceDetails> GetAvailableDeviceList(AvailableDeviceListQuery request)
        {
            var sql = @"
                    SELECT OBJ_CODE SERIALNUMBER, OBJ_DESC DEVICENAME, OBJ_UDFCHAR15 STATUS, OBJ_UDFCHAR28 STATIONNAME 
                    FROM R5OBJECTS
                    WHERE ((OBJ_STATUS = 'I11' AND OBJ_UDFCHAR14 = 'OK') OR (OBJ_STATUS = 'I9' AND OBJ_UDFCHAR08 = 'OK'))";

            if (request == null) request = new AvailableDeviceListQuery();

            if (!string.IsNullOrEmpty(request.CodepartNumber))
            {
                sql += " AND UPPER(OBJ_UDFCHAR01) LIKE '%" + request.CodepartNumber.ToUpper() + "%'";
            }
            if (!string.IsNullOrEmpty(request.DeviceName))
            {
                sql += " AND OBJ_DESC LIKE '%" + request.DeviceName + "%'";
            }
            sql += " AND OBJ_CODE NOT IN (SELECT NVL(SC_SERIAL_TSC,'  ')  FROM U5SCTB WHERE SC_UUTIEN = '+')";
            if (!string.IsNullOrEmpty(request.Username))
            {
                
                sql += " AND OBJ_ORG IN(SELECT UOG_ORG FROM R5USERORGANIZATION WHERE UPPER(UOG_USER) = '" + request.Username.ToUpper() + "')";
            }
            var query = dbContext.Database.SqlQuery<AvailableDeviceDetails>(sql).AsQueryable();
            return new PagedResult<AvailableDeviceDetails>(query, request.Page, request.RecordsPerPage);
        }

        public IPagedResult<PriorityRepairDeviceDetails> GetPriorityRepairDeviceList(PriorityRepairDeviceListQuery request)
        {
            var sql = @"SELECT SC_CODE ID, SC_BARCODE BARCODE, SC_TENTB DEVICENAME, SC_NGAYHONG BROKENDATE, SC_TRANGTHAI STATUS, trunc(sysdate)-trunc(SC_NGAYHONG)+1 NbBrokenDays
                        FROM U5SCTB
                        WHERE SC_TRANGTHAI <> '5' and SC_UUTIEN = '+' ";
            var where = string.Empty;
            if (!string.IsNullOrEmpty(request.TextToSearch))
            {
                where += string.Format(" and (upper(SC_BARCODE) LIKE '%{0}%' or upper(SC_TENTB) LIKE '%{0}%')", request.TextToSearch.ToUpper());
            }
            if (request.Organizations != null && request.Organizations.Count > 0)
            {
                where += " and SC_CHINHANH in (";
                foreach (var o in request.Organizations)
                {
                    where += "'" + o + "', ";
                }
                where += "'*')";
            }
            if (request.UserGroup == GroupCodeConstants.TVT)
            {
                where += " and upper(SC_TVT) = '" + request.TVT.ToUpper() + "' ";
            }
            if (request.UserGroup == GroupCodeConstants.DVT)
            {
                where += " and upper(SC_DVT) = '" + request.DVT.ToUpper() + "' ";
            }
            var query = dbContext.Database.SqlQuery<PriorityRepairDeviceDetails>(sql + where).AsQueryable();
            return new PagedResult<PriorityRepairDeviceDetails>(query, request.Page, request.RecordsPerPage);
        }

        public IPagedResult<RepairDeviceDetails> GetRepairDeviceList(RepairDeviceListQuery request)
        {
            var sql = @"SELECT SC_CODE ID, SC_BARCODE BARCODE, SC_TENTB DEVICENAME, SC_TRANGTHAI STATUS, SC_UUTIEN PriorityStr, 
                            SC_NGAYHONG BROKENDATE,
                            SC_TVT TVT, SC_DVT DVT
                        FROM U5SCTB
                        WHERE 1=1 ";
            if (request.UserGroup == GroupCodeConstants.TVT)
            {
                sql += " AND SC_TVT = '" + request.TVT + "'";
            }
            if (request.UserGroup == GroupCodeConstants.DVT)
            {
                sql += " AND SC_DVT = '" + request.DVT + "'";
            }
            if (request.Statuses != null && request.Statuses.Count > 0)
            {
                sql += " AND SC_TRANGTHAI IN (";
                foreach (var s in request.Statuses)
                {
                    sql += "'" + s + "', ";
                }
                sql += "'0')";
            }
            if (!string.IsNullOrEmpty(request.TextToSearch))
            {
                sql += string.Format(" and (upper(SC_BARCODE) LIKE '%{0}%' or upper(SC_SERIAL_TSC) LIKE '%{0}%' or upper(SC_SERIAL_SSC) LIKE '%{0}%' or upper(SC_TENTB) LIKE '%{0}%')", request.TextToSearch.ToUpper());
            }
            var query = dbContext.Database.SqlQuery<RepairDeviceDetails>(sql).AsQueryable();
            return new PagedResult<RepairDeviceDetails>(query, request.Page, request.RecordsPerPage);
        }

        public string ReceiveDeviceFromTVT(ReceiveDeviceFromTVTCommand command)
        {
            var sqlCommand = string.Format(@"UPDATE u5sctb
                                SET SC_TRANGTHAI = '{0}',
                                    SC_NGAYDVT_NHAN = :BrokenReceivedDate,
                                    SC_NGUOICAPNHAT = :UpdatedBy,
                                    SC_NGAYCAPNHAT = sysdate
                                WHERE SC_CODE = :Id", StatusConstants.BrokenReceived);
            var sqlParameters = new OracleParameter[3];
            sqlParameters[0] = new OracleParameter("BrokenReceivedDate", command.BrokenReceivedDate);
            sqlParameters[1] = new OracleParameter("UpdatedBy", command.Username);
            sqlParameters[2] = new OracleParameter("Id", command.Id);

            var result = dbContext.Database.ExecuteSqlCommand(sqlCommand, sqlParameters);

            return command.Id;
        }

        public string ReceiveDeviceFromTTDKSC(ReceiveDeviceFromTTDKSCCommand command)
        {
            var sqlCommand = string.Format(@"UPDATE u5sctb
                                SET SC_TRANGTHAI = '{0}',
                                    SC_NGAYTTDKSC_TRA = :RepairedReceivedDate,
                                    SC_SERIAL_SSC = :SerialAfter,
                                    SC_KETQUA_SSC = :RepairedSuccess,
                                    SC_LUUKHO = :ToStock,
                                    SC_NGUOICAPNHAT = :UpdatedBy,
                                    SC_NGAYCAPNHAT = sysdate
                                WHERE SC_CODE = :Id", StatusConstants.RepairedReceived);
            var sqlParameters = new OracleParameter[6];
            sqlParameters[0] = new OracleParameter("RepairedReceivedDate", command.RepairedReceivedDate);
            sqlParameters[1] = new OracleParameter("SerialAfter", command.SerialAfter);
            sqlParameters[2] = new OracleParameter("RepairedSuccess", command.RepairedSuccess ? "+" : "-");
            sqlParameters[3] = new OracleParameter("ToStock", command.ToStock ? "+" : "-");
            sqlParameters[4] = new OracleParameter("UpdatedBy", command.Username);
            sqlParameters[5] = new OracleParameter("Id", command.Id);

            var result = dbContext.Database.ExecuteSqlCommand(sqlCommand, sqlParameters);

            return command.Id;
        }

        public string DeliveryDevicesToTVT(DeliveryDevicesToTVTCommand command)
        {
            if (command.Ids == null || command.Ids.Count <= 0) return string.Empty;

            var id = DateTime.Now.Ticks.ToString();

            var sqlCommand = @"insert into u5bgtbssc
                                (BG_CODE, BG_NGAY, BG_MOTA, BG_NGUOINHAN, BG_NGUOINHAN_CD, BG_NGUOIGIAO, BG_NGUOIGIAO_CD)
                                values
                                (:Id, :RepairedDeliveredDate, :Description, :Receiver, :ReceiverTitle, :Deliverer, :DelivererTitle)";

            var sqlParameters = new OracleParameter[7];
            sqlParameters[0] = new OracleParameter("Id", id);
            sqlParameters[1] = new OracleParameter("RepairedDeliveredDate", command.RepairedDeliveredDate);
            sqlParameters[2] = new OracleParameter("Description", command.Description);
            sqlParameters[3] = new OracleParameter("Receiver", command.Receiver);
            sqlParameters[4] = new OracleParameter("ReceiverTitle", command.ReceiverTitle);
            sqlParameters[5] = new OracleParameter("Deliverer", command.Deliverer);
            sqlParameters[6] = new OracleParameter("DelivererTitle", command.DelivererTitle);

            var result = dbContext.Database.ExecuteSqlCommand(sqlCommand, sqlParameters);

            foreach (var repairId in command.Ids)
            {
                sqlCommand = string.Format(@"UPDATE u5sctb
                                SET SC_TRANGTHAI = '{0}',
                                    SC_LO_BANGIAO = :DeliveryId,
                                    SC_NGUOICAPNHAT = :UpdatedBy,
                                    SC_NGAYCAPNHAT = sysdate
                                WHERE SC_CODE = :RepairId", StatusConstants.RepairedDelivered);

                sqlParameters = new OracleParameter[3];
                sqlParameters[0] = new OracleParameter("DeliveryId", id);
                sqlParameters[1] = new OracleParameter("UpdatedBy", command.Username);
                sqlParameters[2] = new OracleParameter("RepairId", repairId);
                result = dbContext.Database.ExecuteSqlCommand(sqlCommand, sqlParameters);
            }

            return id;
        }

        public IEnumerable<TVT> GetTVTList(string dvt)
        {
            var sql = @"SELECT DISTINCT TVT_CODE Code, TVT_DESC Name, TVT_DVT DVT, TVT_ORG Organization, TVT_DVT_ORG DVTOrganization FROM U5TOVT 
                        WHERE UPPER(TVT_DVT) = :DVT";
            if (string.IsNullOrEmpty(dvt))
            {
                sql += " OR 1=1";
            }
            var sqlParameters = new OracleParameter[1];
            sqlParameters[0] = new OracleParameter("DVT", dvt.ToUpper());

            var result = dbContext.Database.SqlQuery<TVT>(sql, sqlParameters).ToList();

            return result;
        }

        public RepairDeviceSummarize SummarizeReport(DeviceRepairSummarizeReportQuery request)
        {
            var result = new RepairDeviceSummarize
            {
                UserCode = request.Username
            };

            var sql = @"select count(*)
                        from U5SCTB 
                        where SC_TRANGTHAI = '1' and trunc(SC_NGAYHONG) >= :FromDate and trunc(SC_NGAYHONG) <= :ToDate ";
            var where = string.Empty;
            switch (request.UserGroup)
            {
                case GroupCodeConstants.TVT:
                    where += " and UPPER(SC_TVT) = '" + request.TVT.ToUpper() + "' ";
                    break;
                case GroupCodeConstants.DVT:
                    if (!string.IsNullOrEmpty(request.TVT))
                    {
                        where += " and UPPER(SC_TVT) = '" + request.TVT.ToUpper() + "' ";
                    }
                    else
                    {
                        where += " and UPPER(SC_DVT) = '" + request.DVT.ToUpper() + "' ";
                    }
                    break;
                default:
                    if (!string.IsNullOrEmpty(request.TVT))
                    {
                        where += " and UPPER(SC_TVT) = '" + request.TVT.ToUpper() + "' ";
                    }
                    else
                    {
                        if (request.Organizations != null && request.Organizations.Count > 0)
                        {
                            where += " and SC_CHINHANH in (";
                            foreach (var o in request.Organizations)
                            {
                                where += "'" + o + "', ";
                            }
                            where += "'*')";
                        }
                    }
                    break;
            }
            if (!string.IsNullOrEmpty(request.DeviceName))
            {
                where += " and upper(SC_TENTB) LIKE '%" + request.DeviceName.ToUpper() + "%' ";
            }
            if (request.UserGroup == GroupCodeConstants.TVT)
            {
                where += " and upper(SC_TVT) = '" + request.TVT.ToUpper() + "' ";
            }
            if (request.UserGroup == GroupCodeConstants.DVT)
            {
                where += " and upper(SC_DVT) = '" + request.DVT.ToUpper() + "' ";
            }

            var sqlParameters = new OracleParameter[2];
            sqlParameters[0] = new OracleParameter("FromDate", request.FromDate.Date);
            sqlParameters[1] = new OracleParameter("ToDate", request.ToDate.Date);
            var count = dbContext.Database.SqlQuery<int>(sql + where, sqlParameters).FirstOrDefault();

            var item = new RepairDeviceSummarizeItem
            {
                Name = "Số lượng thiết bị xác nhận hỏng chưa chuyển về kho",
                NbDevices = count,
                SummarizeType = SummarizeTypeConstants.Broken
            };
            result.Items.Add(item);

            sql = @"select count(*)
                        from U5SCTB 
                        where SC_TRANGTHAI = '2' and trunc(SC_NGAYDVT_NHAN) >= :FromDate and trunc(SC_NGAYDVT_NHAN) <= :ToDate ";
            count = dbContext.Database.SqlQuery<int>(sql + where, sqlParameters).FirstOrDefault();
            item = new RepairDeviceSummarizeItem
            {
                Name = "Số lượng thiết bị đang chờ gửi đi sửa chữa",
                NbDevices = count,
                SummarizeType = SummarizeTypeConstants.BrokenReceived
            };
            result.Items.Add(item);

            sql = @"select count(*)
                        from U5SCTB 
                        where SC_TRANGTHAI = '3' and trunc(SC_NGAYTTDKSC_NHAN) >= :FromDate and trunc(SC_NGAYTTDKSC_NHAN) <= :ToDate ";
            count = dbContext.Database.SqlQuery<int>(sql + where, sqlParameters).FirstOrDefault();
            item = new RepairDeviceSummarizeItem
            {
                Name = "Số lượng thiết bị đang gửi đi sửa chữa",
                NbDevices = count,
                SummarizeType = SummarizeTypeConstants.BrokenDelivered
            };
            result.Items.Add(item);

            sql = @"select count(*)
                        from U5SCTB 
                        where SC_TRANGTHAI = '4' and trunc(SC_NGAYTTDKSC_TRA) >= :FromDate and trunc(SC_NGAYTTDKSC_TRA) <= :ToDate and SC_KETQUA_SSC = '+' ";
            count = dbContext.Database.SqlQuery<int>(sql + where, sqlParameters).FirstOrDefault();
            item = new RepairDeviceSummarizeItem
            {
                Name = "Số lượng thiết bị tốt sau sửa chữa chưa bàn giao TVT",
                NbDevices = count,
                SummarizeType = SummarizeTypeConstants.RepairedReceived
            };
            result.Items.Add(item);

            sql = @"select count(*)
                        from U5SCTB 
                        where SC_TRANGTHAI = '5' and trunc(SC_NGAYCAPNHAT) >= :FromDate and trunc(SC_NGAYCAPNHAT) <= :ToDate ";
            count = dbContext.Database.SqlQuery<int>(sql + where, sqlParameters).FirstOrDefault();
            item = new RepairDeviceSummarizeItem
            {
                Name = "Số lượng thiết bị tốt sau sửa chữa đã bàn giao TVT",
                NbDevices = count,
                SummarizeType = SummarizeTypeConstants.RepairedDelivered
            };
            result.Items.Add(item);

            sql = @"select count(*)
                        from U5SCTB 
                        where SC_TRANGTHAI = '3' and trunc(SC_NGAYTTDKSC_NHAN) >= :FromDate and trunc(SC_NGAYTTDKSC_NHAN) <= :ToDate and SC_UUTIEN = '+' ";
            count = dbContext.Database.SqlQuery<int>(sql + where, sqlParameters).FirstOrDefault();
            item = new RepairDeviceSummarizeItem
            {
                Name = "Số lượng thiết bị ưu tiên sửa chữa đã giao TTĐKSC",
                NbDevices = count,
                SummarizeType = SummarizeTypeConstants.PriorityBrokenDelivered
            };
            result.Items.Add(item);

            return result;
        }

        public IPagedResult<RepairDeviceDetails> SummarizeListReport(DeviceRepairSummarizeReportQuery request)
        {
            var where = string.Empty;
            switch (request.UserGroup)
            {
                case GroupCodeConstants.TVT:
                    where += " and UPPER(SC_TVT) = '" + request.TVT.ToUpper() + "' ";
                    break;
                case GroupCodeConstants.DVT:
                    if (!string.IsNullOrEmpty(request.TVT))
                    {
                        where += " and UPPER(SC_TVT) = '" + request.TVT.ToUpper() + "' ";
                    }
                    else
                    {
                        where += " and UPPER(SC_DVT) = '" + request.DVT.ToUpper() + "' ";
                    }
                    break;
                default:
                    if (!string.IsNullOrEmpty(request.TVT))
                    {
                        where += " and UPPER(SC_TVT) = '" + request.TVT.ToUpper() + "' ";
                    }
                    else
                    {
                        if (request.Organizations != null && request.Organizations.Count > 0)
                        {
                            where += " and SC_CHINHANH in (";
                            foreach (var o in request.Organizations)
                            {
                                where += "'" + o + "', ";
                            }
                            where += "'*')";
                        }
                    }
                    break;
            }
            if (!string.IsNullOrEmpty(request.DeviceName))
            {
                where += " and upper(SC_TENTB) LIKE '%" + request.DeviceName.ToUpper() + "%' ";
            }
            if (request.UserGroup == GroupCodeConstants.TVT)
            {
                where += " and upper(SC_TVT) = '" + request.TVT.ToUpper() + "' ";
            }
            if (request.UserGroup == GroupCodeConstants.DVT)
            {
                where += " and upper(SC_DVT) = '" + request.DVT.ToUpper() + "' ";
            }

            var sql = @"SELECT SC_CODE ID, SC_BARCODE BARCODE, SC_TENTB DEVICENAME, SC_TRANGTHAI STATUS, SC_UUTIEN PriorityStr, 
                            SC_NGAYHONG BROKENDATE,
                            SC_TVT TVT, SC_DVT DVT
                        FROM U5SCTB
                        WHERE 1=1 ";
            switch (request.ListType)
            {
                case SummarizeTypeConstants.Broken:
                    sql += " and SC_TRANGTHAI = '1' and trunc(SC_NGAYHONG) >= :FromDate and trunc(SC_NGAYHONG) <= :ToDate ";
                    break;
                case SummarizeTypeConstants.BrokenReceived:
                    sql += " and SC_TRANGTHAI = '2' and trunc(SC_NGAYDVT_NHAN) >= :FromDate and trunc(SC_NGAYDVT_NHAN) <= :ToDate ";
                    break;
                case SummarizeTypeConstants.BrokenDelivered:
                    sql += " and SC_TRANGTHAI = '3' and trunc(SC_NGAYTTDKSC_NHAN) >= :FromDate and trunc(SC_NGAYTTDKSC_NHAN) <= :ToDate ";
                    break;
                case SummarizeTypeConstants.RepairedReceived:
                    sql += " and SC_TRANGTHAI = '4' and trunc(SC_NGAYTTDKSC_TRA) >= :FromDate and trunc(SC_NGAYTTDKSC_TRA) <= :ToDate and SC_KETQUA_SSC = '+'  ";
                    break;
                case SummarizeTypeConstants.RepairedDelivered:
                    sql += " and SC_TRANGTHAI = '5' and trunc(SC_NGAYCAPNHAT) >= :FromDate and trunc(SC_NGAYCAPNHAT) <= :ToDate ";
                    break;
                case SummarizeTypeConstants.PriorityBrokenDelivered:
                    sql += " and SC_TRANGTHAI = '3' and trunc(SC_NGAYTTDKSC_NHAN) >= :FromDate and trunc(SC_NGAYTTDKSC_NHAN) <= :ToDate and SC_UUTIEN = '+' ";
                    break;
            }
            var sqlParameters = new OracleParameter[2];
            sqlParameters[0] = new OracleParameter("FromDate", request.FromDate.Date);
            sqlParameters[1] = new OracleParameter("ToDate", request.ToDate.Date);
            var query = dbContext.Database.SqlQuery<RepairDeviceDetails>(sql + where, sqlParameters).AsQueryable();
            return new PagedResult<RepairDeviceDetails>(query, request.Page, request.RecordsPerPage);
        }


        public IEnumerable<WorkingDeviceDetails> GetWorkingDevices(string serial)
        {
            var sql = @"SELECT SERIAL, TENTB DeviceName, PICCODE_SSC PickCode, NGAYBGDVQLSD DeliveryDate, DVQLSD1 CenterName, LT_NGAYTREO WorkingDate, VU5LENTRAM.LT_NGAYTAIHONG ReBrokenDate, VU5LENTRAM.LT_MATRAM StationCode, VU5LENTRAM.SERIALSSC SerialScc
                        FROM VU5LENTRAM LEFT OUTER JOIN U5LENTRAM ON LT_SERIAL = SERIAL AND LT_PICCODE = PICCODE_SSC
                        WHERE UPPER(SERIAL) = :Serial";
            var sqlParameters = new OracleParameter[1];
            sqlParameters[0] = new OracleParameter("Serial", serial.ToUpper());
            return dbContext.Database.SqlQuery<WorkingDeviceDetails>(sql, sqlParameters).ToArray();
        }

        public string SetWorkingDate(SetWorkingDateCommand command)
        {
            var sqlCommand = @"merge into U5LENTRAM ult
using (SELECT SERIAL, PICCODE_SSC, 
      case WHEN DVQLSD1 = 'TT MLMT' THEN 'CNDN'
           WHEN DVQLSD1 = 'TT MLMN' THEN 'CNHCM'
           ELSE 'CNHN'  
      end CHINHANH
FROM VU5LENTRAM LEFT OUTER JOIN U5LENTRAM ON LT_SERIAL = SERIAL AND LT_PICCODE = PICCODE_SSC
WHERE SERIAL = :Serial and PICCODE_SSC = :PickCode) vlt
on (ult.LT_SERIAL = vlt.SERIAL AND ult.LT_PICCODE = vlt.PICCODE_SSC)
when matched then update set
     ult.lt_ngaytreo = :WorkingDate,
     ult.lt_matram = :StationCode";
            if (command.ReBrokenDate.HasValue)
            {
                sqlCommand += @",ult.lt_ngaytaihong = :ReBrokengDate ";
            }
            sqlCommand += @"
when not matched then insert
     (LT_SERIAL, LT_PICCODE, LT_NGAYTREO, LT_CHINHANH, LT_MATRAM";
            if (command.ReBrokenDate.HasValue)
            {
                sqlCommand += @",LT_NGAYTAIHONG";
            }
            sqlCommand += @")
     values
     (vlt.serial, vlt.piccode_ssc, :WorkingDate, vlt.chinhanh, :StationCode";
            if (command.ReBrokenDate.HasValue)
            {
                sqlCommand += @",:ReBrokengDate";
            }
            sqlCommand += @")";

            var sqlParameters = new OracleParameter[4];
            if (command.ReBrokenDate.HasValue)
            {
                sqlParameters = new OracleParameter[5];
            }
            sqlParameters[0] = new OracleParameter("Serial", command.Serial);
            sqlParameters[1] = new OracleParameter("PickCode", command.PickCode);
            sqlParameters[2] = new OracleParameter("WorkingDate", command.WorkingDate);
            sqlParameters[3] = new OracleParameter("StationCode", command.StationCode);
            if (command.ReBrokenDate.HasValue)
            {
                sqlParameters[4] = new OracleParameter("StationCode", command.ReBrokenDate.Value);
            }

            var result = dbContext.Database.ExecuteSqlCommand(sqlCommand, sqlParameters);

            return command.Serial;
        }

        IEnumerable<RepairKPIReportItem> IDeviceRepairRepository.RepairKPIReport(RepairKPIReportQuery request)
        {
            var fromDate = request.FromDate.ToString("yyyyMMdd");
            var toDate = request.ToDate.ToString("yyyyMMdd");
            var sql = string.Format(@"
select a.chinhanh Branch,
       a.tylescthanhcong SuccessRatio,
       b.sla,
       b.sla1,
       b.sla2,
       b.sla3,
       c.sltaihong RebrokenQty,
       c.tyletaihong RebrokenRatio
from
(SELECT DECODE(GROUPING(CHINHANH), 1, 'TOAN TT', CHINHANH) CHINHANH, ROUND(100 * NVL(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL  AND (CODEKQTSC = 2 OR CODEKQSSC = 3)) THEN 1 ELSE 0  END) / 
NULLIF(SUM(CASE WHEN (NGAYBGDVQLSD IS NOT NULL) THEN 1 ELSE 0  END), 0), 1), 2) TYLESCTHANHCONG
FROM MV_BCCHITIET WHERE NGAYBGDVQLSD BETWEEN TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd')
AND NVL(DVQLSD1,'*') IN( SELECT DSE_MRC FROM R5DEPARTMENTSECURITY WHERE UPPER(DSE_USER) = :Username)
GROUP BY GROUPING SETS((CHINHANH),())) a
inner join
(SELECT CHINHANH, ROUND((SLA1 + SLA2 + SLA3)/3, 2) SLA, SLA1, SLA2, SLA3 FROM
(SELECT DECODE(GROUPING(CHINHANH), 1, 'TOAN TT', CHINHANH) CHINHANH,
ROUND(100 * NVL(SUM(CASE WHEN (NGAYBGDVQLSD -NGAYCNNTK)<51 THEN 1 ELSE 0 END)/ NULLIF(SUM(1), 0), 1), 2) SLA1,
ROUND(100 * NVL(SUM(CASE WHEN (NGAYBGDVQLSD -NGAYCNNTK)<66 THEN 1 ELSE 0 END)/ NULLIF(SUM(1), 0), 1), 2) SLA2,
ROUND(100 * NVL(SUM(CASE WHEN (NGAYBGDVQLSD -NGAYCNNTK)<81 THEN 1 ELSE 0 END)/ NULLIF(SUM(1), 0), 1), 2) SLA3
FROM MV_BCCHITIET
WHERE NGAYCNNTK BETWEEN TO_DATE('20170913', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd')
AND NGAYBGDVQLSD BETWEEN TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd')
AND CODEDTTB NOT IN ('1','2') AND (CODEKQTSC='2' OR CODEKQSSC='3')
AND NVL(DVQLSD1,'*') IN( SELECT DSE_MRC FROM R5DEPARTMENTSECURITY WHERE UPPER(DSE_USER) = :Username)
GROUP BY GROUPING SETS((CHINHANH),()))
GROUP BY CHINHANH, SLA1, SLA2, SLA3 ORDER BY CHINHANH) b
on a.chinhanh=b.chinhanh
inner join
(SELECT DECODE(GROUPING(M1.CHINHANH), 1, 'TOAN TT', M1.CHINHANH) CHINHANH, SUM(CASE WHEN M2.SERIAL IS NULL THEN 0  ELSE 1  END) AS SLTAIHONG
,ROUND(100 * NVL(SUM(CASE WHEN M2.SERIAL IS NULL  THEN 0  ELSE 1  END) /
NULLIF(SUM(M1.MAUSO), 0), 1), 2) TYLETAIHONG
FROM(SELECT REQ_CODE, CHINHANH, CODELOAITB, LOAITB, 1 MAUSO, DVQLSD1, DOITAC, CODEDAIVT, DAIVT, NGAYBGDVQLSD, SERIAL, FIRSTDAY, LASTDAY
  FROM(SELECT A.*, ADD_MONTHS(TRUNC(TO_DATE('{0}', 'yyyyMMdd')), -3) FIRSTDAY,
  TO_DATE('{1}', 'yyyyMMdd') LASTDAY FROM MV_BCCHITIET A)
  WHERE NGAYBGDVQLSD BETWEEN FIRSTDAY AND LASTDAY AND(CODEKQTSC = '2'  OR CODEKQSSC = '3')
  AND NVL(DVQLSD1, '*') IN(SELECT DSE_MRC FROM R5DEPARTMENTSECURITY WHERE UPPER(DSE_USER) = :Username)
  ) M1
LEFT OUTER JOIN MV_BCCHITIET M2 ON M1.REQ_CODE<> M2.REQ_CODE
 AND M1.CHINHANH = M2.CHINHANH AND M1.SERIAL = M2.SERIAL
  AND(M2.NGAYCNNTK - M1.NGAYBGDVQLSD) BETWEEN 1 AND 90
  AND M2.NGAYCNNTK BETWEEN FIRSTDAY AND LASTDAY AND M2.CODEKQTSC = '1'
 GROUP BY GROUPING SETS((M1.CHINHANH), ())) c
  on a.chinhanh = c.chinhanh", fromDate, toDate);
            var sqlParameters = new OracleParameter[1];
            sqlParameters[0] = new OracleParameter("Username", request.Username.ToUpper());
            return dbContext.Database.SqlQuery<RepairKPIReportItem>(sql, sqlParameters).ToArray();
        }

        IEnumerable<RepairTimeReportItem> IDeviceRepairRepository.RepairTimeReport(RepairTimeReportQuery request)
        {
            var fromDate = request.FromDate.ToString("yyyyMMdd");
            var toDate = request.ToDate.ToString("yyyyMMdd");
            var sql = string.Format(@"
SELECT DECODE(GROUPING(CHINHANH), 1, 'TOAN TT', CHINHANH) Branch,
     case when SUM(PKHUC1NUMBER)=0 then 0 else ROUND(SUM(PKHUC1)/SUM(PKHUC1NUMBER),2) end Receiving,
     case when SUM(PKHUC2NUMBER)=0 then 0 else ROUND(SUM(PKHUC2)/SUM(PKHUC2NUMBER),2) end TestingBefore,
     case when SUM(PKHUC3NUMBER)=0 then 0 else ROUND(SUM(PKHUC3)/SUM(PKHUC3NUMBER),2) end Waiting,
     case when SUM(PKHUC4NUMBER)=0 then 0 else ROUND(SUM(PKHUC4)/SUM(PKHUC4NUMBER),2) end Repairing,
     case when SUM(PKHUC5NUMBER)=0 then 0 else ROUND(SUM(PKHUC5)/SUM(PKHUC5NUMBER),2) end TestingAfter,
     case when SUM(PKHUC6NUMBER)=0 then 0 else ROUND(SUM(PKHUC6)/SUM(PKHUC6NUMBER),2) end Deliverying,
     case when SUM(PKHUC7NUMBER)=0 then 0 else ROUND(SUM(PKHUC7)/SUM(PKHUC7NUMBER),2) end AllTime

FROM (
SELECT CHINHANH,
CASE WHEN (NGAYYCSC IS NOT NULL AND NGAYDVVCBG IS NOT NULL AND 
  NGAYYCSC BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYDVVCBG BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYYCSC <= NGAYDVVCBG)
  THEN (NGAYDVVCBG-NGAYYCSC) ELSE 0 END PKHUC1,
CASE WHEN (NGAYYCSC IS NOT NULL AND NGAYDVVCBG IS NOT NULL AND 
  NGAYYCSC BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYDVVCBG BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYYCSC <= NGAYDVVCBG)
  THEN 1 ELSE 0 END PKHUC1NUMBER,
CASE WHEN (NGAYDVVCBG IS NOT NULL AND NGAYHTKTTSC IS NOT NULL AND 
  NGAYDVVCBG BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYHTKTTSC BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYDVVCBG <= NGAYHTKTTSC)
  THEN (NGAYHTKTTSC-NGAYDVVCBG) ELSE 0 END PKHUC2,
CASE WHEN (NGAYDVVCBG IS NOT NULL AND NGAYHTKTTSC IS NOT NULL AND 
  NGAYDVVCBG BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYHTKTTSC BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYDVVCBG <= NGAYHTKTTSC)
  THEN 1 ELSE 0 END PKHUC2NUMBER,
CASE WHEN (NGAYHTKTTSC IS NOT NULL AND NGAYBGDT IS NOT NULL AND 
  NGAYHTKTTSC BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYBGDT BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYHTKTTSC <= NGAYBGDT)
  THEN (NGAYBGDT-NGAYHTKTTSC) ELSE 0 END PKHUC3,
CASE WHEN (NGAYHTKTTSC IS NOT NULL AND NGAYBGDT IS NOT NULL AND 
  NGAYBGDT BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{0}', 'yyyyMMdd') AND
  NGAYHTKTTSC BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYHTKTTSC <= NGAYBGDT)
  THEN 1 ELSE 0 END PKHUC3NUMBER,
CASE WHEN (NGAYBGDT IS NOT NULL AND NGAYDTBG IS NOT NULL AND 
  NGAYDTBG BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYBGDT BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYBGDT <= NGAYDTBG)
  THEN (NGAYDTBG-NGAYBGDT) ELSE 0 END PKHUC4,
CASE WHEN (NGAYBGDT IS NOT NULL AND NGAYDTBG IS NOT NULL AND 
  NGAYDTBG BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYBGDT BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYBGDT <= NGAYDTBG)
  THEN 1 ELSE 0 END PKHUC4NUMBER,           
CASE WHEN (NGAYBGDT IS NOT NULL AND NGAYHTKTSSC IS NOT NULL AND 
  NGAYDTBG BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYHTKTSSC BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYDTBG <= NGAYHTKTSSC)
  THEN (NGAYHTKTSSC-NGAYDTBG) ELSE 0 END PKHUC5,
CASE WHEN (NGAYHTKTSSC IS NOT NULL AND NGAYDTBG IS NOT NULL AND 
  NGAYDTBG BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYHTKTSSC BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYDTBG <= NGAYHTKTSSC)
  THEN 1 ELSE 0 END PKHUC5NUMBER,
CASE WHEN (NGAYBGDVQLSD IS NOT NULL AND NGAYHTKTSSC IS NOT NULL AND 
  NGAYBGDVQLSD BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYHTKTSSC BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYHTKTSSC <= NGAYBGDVQLSD)
  THEN (NGAYBGDVQLSD-NGAYHTKTSSC) ELSE 0 END PKHUC6,
CASE WHEN (NGAYHTKTSSC IS NOT NULL AND NGAYBGDVQLSD IS NOT NULL AND 
  NGAYBGDVQLSD BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYHTKTSSC BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYHTKTSSC <= NGAYBGDVQLSD)
  THEN 1 ELSE 0 END PKHUC6NUMBER,
CASE WHEN (NGAYBGDVQLSD IS NOT NULL AND NGAYCNNTK IS NOT NULL AND 
  NGAYBGDVQLSD BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYCNNTK BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYCNNTK <= NGAYBGDVQLSD)
  THEN (NGAYBGDVQLSD-NGAYCNNTK) ELSE 0 END PKHUC7,
CASE WHEN (NGAYCNNTK IS NOT NULL AND NGAYBGDVQLSD IS NOT NULL AND 
  NGAYBGDVQLSD BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYCNNTK BETWEEN  TO_DATE('{0}', 'yyyyMMdd') AND TO_DATE('{1}', 'yyyyMMdd') AND
  NGAYCNNTK <= NGAYBGDVQLSD)
  THEN 1 ELSE 0 END PKHUC7NUMBER
FROM MV_BCCHITIET WHERE NVL(DVQLSD1,'*') IN( SELECT DSE_MRC FROM R5DEPARTMENTSECURITY WHERE UPPER(DSE_USER) = :Username))
GROUP BY GROUPING SETS((CHINHANH),())", fromDate, toDate);
            var sqlParameters = new OracleParameter[1];
            sqlParameters[0] = new OracleParameter("Username", request.Username.ToUpper());
            return dbContext.Database.SqlQuery<RepairTimeReportItem>(sql, sqlParameters).ToArray();
        }

        IEnumerable<RepairInventoryReportItem> IDeviceRepairRepository.RepairInventoryReport(RepairInventoryReportQuery request)
        {
            var nbDays = request.NbDays;
            var toDate = request.ToDate.ToString("yyyyMMdd");
            var sql = string.Format(@"
SELECT DECODE(GROUPING(CHINHANH), 1, 'TOAN TT', CHINHANH) Branch,
SUM(CASE WHEN (NGAYCNNTK IS NOT NULL AND (CODEKQTSC IS NULL OR CODEKQTSC='1') AND (NGAYBGDT IS NULL OR (NGAYBGDT IS NOT NULL AND NGAYBGDT > TO_DATE('{1}', 'yyyyMMdd') )) AND (TO_DATE('{1}', 'yyyyMMdd') - NGAYCNNTK) > {0}) THEN 1  ELSE 0  END) AS BeforeRepairing,
SUM(CASE WHEN(NGAYBGDT IS NOT NULL AND(NGAYDTBG IS NULL OR(NGAYDTBG IS NOT NULL AND NGAYDTBG > TO_DATE('{1}', 'yyyyMMdd'))) AND(TO_DATE('{1}', 'yyyyMMdd') - NGAYBGDT) > {0}) THEN 1  ELSE 0  END) AS InRepairing,
SUM(CASE WHEN((CODEKQTSC IN('2', '5') AND NGAYBGDVQLSD IS NULL AND(TO_DATE('{1}', 'yyyyMMdd') - NGAYHTKTTSC) > {0}) OR(NGAYDTBG IS NOT NULL AND NGAYBGDVQLSD IS NULL) AND(TO_DATE('{1}', 'yyyyMMdd') - NGAYDTBG) > {0}) THEN 1  ELSE 0  END) AS AfterRepairing,
SUM(CASE WHEN((NGAYBGDVQLSD IS NULL OR(NGAYBGDVQLSD IS NOT NULL AND NGAYBGDVQLSD > TO_DATE('{1}', 'yyyyMMdd'))) AND NGAYCNNTK IS NOT NULL AND(TO_DATE('{1}', 'yyyyMMdd') - NGAYCNNTK) > {0})  THEN 1  ELSE 0  END) AS InTTDKSCStock,
SUM(CASE WHEN((NGAYBGDVQLSD IS NULL OR(NGAYBGDVQLSD IS NOT NULL AND NGAYBGDVQLSD > TO_DATE('{1}', 'yyyyMMdd'))) AND NGAYCNNTK IS NOT NULL  AND(CODEKQTSC = 2 OR CODEKQSSC = 3))  THEN 1  ELSE 0  END) AS GoodToDelivery
FROM MV_BCCHITIET
WHERE NVL(DVQLSD1,'*') IN(SELECT DSE_MRC FROM R5DEPARTMENTSECURITY WHERE UPPER(DSE_USER) = :Username)
GROUP BY GROUPING SETS((CHINHANH), ())
", nbDays, toDate);
            var sqlParameters = new OracleParameter[1];
            sqlParameters[0] = new OracleParameter("Username", request.Username.ToUpper());
            return dbContext.Database.SqlQuery<RepairInventoryReportItem>(sql, sqlParameters).ToArray();
        }

    }
}
