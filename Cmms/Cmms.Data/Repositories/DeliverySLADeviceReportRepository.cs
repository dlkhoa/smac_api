﻿using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Entities.Views;
using Cmms.Domain.Queries;
using MBoL.Utils;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cmms.Data.Repositories
{
    public class DeliverySLADeviceReportRepository : BaseRepository, IDeliverySLADeviceReportRepository
    {
        public DeliverySLADeviceReportRepository(CmmsDbContext dbContext) : base(dbContext)
        {

        }

        public IEnumerable<DeliverySLADeviceStatisticByMonth> GetStatisticByMonth(DeviceReportQuery request)
        {
            /*var query = GetDbEntity<DeviceReportView>()
                .Where(d => d.NgayBanGiaoDVQLSD >= request.StartDate && d.NgayBanGiaoDVQLSD <= request.EndDate && d.CodeKQSSC == "3" && (d.CodeDTTB == "3" || d.CodeDTTB == "5") && request.Organizations.Contains(d.ChiNhanh))
                .GroupBy(d => new { d.ChiNhanh, d.NgayBanGiaoDVQLSD })
                .Select(d => new DeliverySLADeviceStatisticByMonth
                {
                    ChiNhanh = d.Key.ChiNhanh,
                    NgayBanGiao = d.Key.NgayBanGiaoDVQLSD.Value,
                    Tong = d.Count()
                });

            return query.ToArray();*/
            var orgs = StringHelper.MakeOrgSearch(request.Organizations);
            var sql = string.Format(@"
                SELECT
                    chinhanh,
                    ngaybgdvqlsd NgayBanGiao,
                    COUNT(1) tong
                FROM
                    mv_bcchitiet
                WHERE
                    codedttb IN ('3', '4', '5', '6', '7', '8')
                    AND codekqssc = '3'
                    AND ( ngaybgdvqlsd BETWEEN TO_DATE('{1}', 'YYYY-MM-DD') AND TO_DATE('{2}', 'YYYY-MM-DD') )
                    AND chinhanh IN ({0})
                GROUP BY
                    chinhanh,
                    ngaybgdvqlsd
                ORDER BY
                    chinhanh,
                    ngaybgdvqlsd"
                , orgs, request.StartDate.ToString("yyyy-MM-dd"), request.EndDate.ToString("yyyy-MM-dd"));
            return dbContext.Database.SqlQuery<DeliverySLADeviceStatisticByMonth>(sql).ToArray();
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByMonth(DeliverySLADeviceReportByMonthQuery request)
        {
            /*var query = GetDbEntity<DeviceReportView>()
               .Where(d => d.ChiNhanh.ToUpper() == request.ChiNhanh.ToUpper()
                   && d.NgayBanGiaoDVQLSD >= request.StartDate && d.NgayBanGiaoDVQLSD <= request.EndDate
                   && d.NgayBanGiaoDVQLSD.Value.Year == request.YearValue
                   && d.NgayBanGiaoDVQLSD.Value.Month == request.MonthValue
                   && d.CodeKQSSC == "3" && (d.CodeDTTB == "3" || d.CodeDTTB == "5"))
               .OrderByDescending(d => d.ReqCode)
               .ThenBy(d => d.Serial)
               .Select(d => new DeviceDetailsReportSummary
               {
                   Serial = d.Serial,
                   TenThietBi = d.TenThietBi,
                   TinhTrang = d.TinhTrang
               });

            return new PagedResult<DeviceDetailsReportSummary>(query, request.Page, request.RecordsPerPage);*/
            var sql = string.Format(@"
                    SELECT
                        serial,
                        tentb TenThietBi,
                        TinhTrang
                    FROM
                        mv_bcchitiet
                    WHERE
                        codedttb IN ('3', '4', '5', '6', '7', '8')
                        AND codekqssc = '3'
                        AND ( ngaybgdvqlsd BETWEEN TO_DATE('{0}', 'YYYY-MM-DD') AND TO_DATE('{1}', 'YYYY-MM-DD') )
                        AND upper(chinhanh) = :ChiNhanh
                        AND year(ngaybgdvqlsd) = {2}
                        AND month(ngaybgdvqlsd) = {3}"
                , request.StartDate.ToString("yyyy-MM-dd"), request.EndDate.ToString("yyyy-MM-dd"), request.YearValue, request.MonthValue);
            var query = dbContext.Database.SqlQuery<DeviceDetailsReportSummary>(sql,
                new OracleParameter("ChiNhanh", request.ChiNhanh.ToUpper())).AsQueryable();
            return new PagedResult<DeviceDetailsReportSummary>(query, request.Page, request.RecordsPerPage);
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryAllLevel(DeviceReportAllLevelQuery request)
        {
            var query = GetDbEntity<DeviceReportView>()
               .Where(d => d.ChiNhanh.ToUpper() == request.ChiNhanh.ToUpper()
                   && d.NgayBanGiaoDVQLSD >= request.StartDate && d.NgayBanGiaoDVQLSD <= request.EndDate
                   && d.CodeKQSSC == "3" && (d.CodeDTTB == "3" || d.CodeDTTB == "5"));

            if (!string.IsNullOrWhiteSpace(request.Thang))
            {
                int month;
                int year;
                if (int.TryParse(request.Thang.Split('-')[0], out month) && int.TryParse(request.Thang.Split('-')[1], out year))
                {
                    query = query.Where(d => d.NgayBanGiaoDVQLSD.Value.Year == year
                                            && d.NgayBanGiaoDVQLSD.Value.Month == month);
                }
            }

            query = query.OrderByDescending(d => d.ReqCode)
               .ThenBy(d => d.Serial);

            var result = query.Select(d => new DeviceDetailsReportSummary
            {
                Serial = d.Serial,
                TenThietBi = d.TenThietBi,
                TinhTrang = d.TinhTrang
            });

            return new PagedResult<DeviceDetailsReportSummary>(result, request.Page, request.RecordsPerPage);
        }
    }
}
