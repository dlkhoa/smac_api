﻿using System.Collections.Generic;
using System.Linq;
using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Entities.Views;
using Cmms.Domain.Queries;

namespace Cmms.Data.Repositories
{
    public class RefusedHTKTDeviceReportRepository : BaseRepository, IRefusedHTKTDeviceReportRepository
    {
        public RefusedHTKTDeviceReportRepository(CmmsDbContext dbContext) : base(dbContext)
        {

        }

        public IEnumerable<RefusedHTKTDeviceStatisticByProvider> GetDeliveryDeviceStatisticProvider(DeviceReportQuery request)
        {
            var query = GetDbEntity<DeviceReportView>()
                .Where(d => d.TinhTrangBanGiao == "TCTN" && d.NgayDTBG >= request.StartDate && d.NgayDTBG <= request.EndDate && request.Organizations.Contains(d.ChiNhanh))
                .GroupBy(d => new { d.ChiNhanh, d.NhaCungCap })
                .Select(d => new RefusedHTKTDeviceStatisticByProvider
                {
                    ChiNhanh = d.Key.ChiNhanh,
                    NhaCungCap = d.Key.NhaCungCap,
                    Tong = d.Count()
                });

            return query.ToArray();
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByProvider(RefusedHTKTDeviceReportByProviderQuery request)
        {
            var query = GetDbEntity<DeviceReportView>()
                .Where(d => d.ChiNhanh.ToUpper() == request.ChiNhanh.ToUpper()
                    && d.NhaCungCap.ToUpper() == request.NhaCungCap.ToUpper()
                    && d.TinhTrangBanGiao == "TCTN"
                     && d.NgayDTBG >= request.StartDate && d.NgayDTBG <= request.EndDate)
                .OrderByDescending(d => d.ReqCode)
                .ThenBy(d => d.Serial)
                .Select(d => new DeviceDetailsReportSummary
                {
                    Serial = d.Serial,
                    TenThietBi = d.TenThietBi,
                    TinhTrang = d.TinhTrang
                });

            return new PagedResult<DeviceDetailsReportSummary>(query, request.Page, request.RecordsPerPage);
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryAllLevel(DeviceReportAllLevelQuery request)
        {
            var query = GetDbEntity<DeviceReportView>()
               .Where(d => d.ChiNhanh.ToUpper() == request.ChiNhanh.ToUpper()
                   && d.TinhTrangBanGiao == "TCTN"
                    && d.NgayDTBG >= request.StartDate && d.NgayDTBG <= request.EndDate);

            if (!string.IsNullOrWhiteSpace(request.NhaCungCap))
            {
                query = query.Where(d => d.NhaCungCap.ToUpper() == request.NhaCungCap.ToUpper());
            }

            query = query.OrderByDescending(d => d.ReqCode)
               .ThenBy(d => d.Serial);

            var result = query
               .Select(d => new DeviceDetailsReportSummary
               {
                   Serial = d.Serial,
                   TenThietBi = d.TenThietBi,
                   TinhTrang = d.TinhTrang
               });

            return new PagedResult<DeviceDetailsReportSummary>(result, request.Page, request.RecordsPerPage);
        }

    }
}
