﻿using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Commands;
using Oracle.ManagedDataAccess.Client;

namespace Cmms.Data.Repositories
{
    public class OrderCommandRepository : BaseRepository, IOrderCommandRepository
    {
        public OrderCommandRepository(CmmsDbContext dbContext) : base(dbContext)
        {

        }
        public void ApproveOrder(ApproveOrderCommand command)
        {
            var sqlCommand = @"UPDATE r5orders o
                                SET ord_status=(SELECT NVL(aut_statnew,ord_status) 
				                                FROM r5permissions, r5userorganization, r5auth
				                                WHERE uog_user = :Username 
					                                AND uog_org = ord_org 
					                                AND uog_group = prm_group 
					                                AND prm_update = '*' 
					                                AND prm_function IN ('PSPORF','PSPORD','PSPORG','PSPOBH')
					                                AND aut_group=uog_group 
					                                AND aut_entity='PORD'
					                                AND aut_status=ord_status
                                                    AND rownum = 1)
                                WHERE ord_code=:OrderCode";
            var sqlParameters = new OracleParameter[2];
            sqlParameters[0] = new OracleParameter("Username", command.Username.ToUpper());
            sqlParameters[1] = new OracleParameter("OrderCode", command.OrderCode);

            dbContext.Database.ExecuteSqlCommand(sqlCommand, sqlParameters);
        }

        public void UpdateOrderStatus(ApproveOrderCommand command)
        {
            var sqlCommand = @"UPDATE r5orders o
                                SET ord_status=:NewStatus
                                WHERE ord_code=:OrderCode";
            var sqlParameters = new OracleParameter[2];
            sqlParameters[0] = new OracleParameter("NewStatus", command.NewStatus);
            sqlParameters[1] = new OracleParameter("OrderCode", command.OrderCode);

            dbContext.Database.ExecuteSqlCommand(sqlCommand, sqlParameters);
        }
    }
}
