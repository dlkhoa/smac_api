﻿using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Entities.Views;
using System.Collections.Generic;
using System.Linq;
using System;
using Cmms.Domain.Queries;
using Cmms.Data.Infrastructure.Paging;

namespace Cmms.Data.Repositories
{
    public class DeviceStatisticRepository : BaseRepository, IDeviceStatisticRepository
    {
        public DeviceStatisticRepository(CmmsDbContext dbContext) : base(dbContext)
        {

        }


        public IEnumerable<DeviceStatisticByProvider> GetDeviceStatisticProvider(DeviceReportQuery request)
        {
            //var sql = @"SELECT chinhanh Branch,nhacc Provider, COUNT(1) Total 
            //            FROM MV_BCCHITIET
            //            GROUP BY chinhanh,nhacc";
            var query = GetDbEntity<DeviceReportView>()
                .Where(d => d.NgayCNNTK >= request.StartDate && d.NgayCNNTK <= request.EndDate && request.Organizations.Contains(d.ChiNhanh))
                .GroupBy(d => new
                {
                    d.ChiNhanh,
                    d.NhaCungCap
                })
                .Select(d => new DeviceStatisticByProvider
                {
                    Branch = d.Key.ChiNhanh,
                    Provider = d.Key.NhaCungCap,
                    Total = d.Count()
                });
            return query.ToArray();
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByProvider(DeviceReportByProviderQuery request)
        {
            var query = GetDbEntity<DeviceReportView>()
                .Where(d => d.NhaCungCap.ToUpper() == request.Provider.ToUpper()
                    && d.ChiNhanh.ToUpper() == request.Branch.ToUpper()
                    && d.NgayCNNTK >= request.StartDate
                    && d.NgayCNNTK <= request.EndDate)
                .OrderByDescending(d => d.ReqCode)
                .ThenBy(d => d.Serial)
                .Select(d => new DeviceDetailsReportSummary
                {
                    Serial = d.Serial,
                    TenThietBi = d.TenThietBi,
                    TinhTrang = d.TinhTrang
                });

            return new PagedResult<DeviceDetailsReportSummary>(query, request.Page, request.RecordsPerPage);
        }

        public IEnumerable<DeviceStatisticByDaiVT> GetDeviceStatisticByDVQLSD_Dai(DeviceReportQuery request)
        {
            var query = GetDbEntity<DeviceReportView>()
                .Where(d => d.NgayCNNTK >= request.StartDate && d.NgayCNNTK <= request.EndDate && request.Organizations.Contains(d.ChiNhanh))
                .GroupBy(d => new
                {
                    d.ChiNhanh,
                    d.DVQLSD,
                    d.DaiVienThong
                })
                .Select(d => new DeviceStatisticByDaiVT
                {
                    Branch = d.Key.ChiNhanh,
                    DVQLSD1 = d.Key.DVQLSD,
                    DaiVT = d.Key.DaiVienThong,
                    Total = d.Count()
                });
            return query.ToArray();
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVQLSD_Dai(DeviceReportByDVQLSDDaiQuery request)
        {
            var query = GetDbEntity<DeviceReportView>()
                .Where(d => d.ChiNhanh.ToUpper() == request.Branch.ToUpper()
                    && d.DVQLSD.ToUpper() == request.DVQLSD.ToUpper()
                    && d.DaiVienThong.ToUpper() == request.DaiVT.ToUpper()
                    && d.NgayCNNTK >= request.StartDate
                    && d.NgayCNNTK <= request.EndDate)
                .OrderByDescending(d => d.ReqCode)
                .ThenBy(d => d.Serial)
                .Select(d => new DeviceDetailsReportSummary
                {
                    Serial = d.Serial,
                    TenThietBi = d.TenThietBi,
                    TinhTrang = d.TinhTrang
                });

            return new PagedResult<DeviceDetailsReportSummary>(query, request.Page, request.RecordsPerPage);
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryAllLevel(DeviceReportAllLevelQuery request)
        {
            var query = GetDbEntity<DeviceReportView>()
               .Where(d => d.ChiNhanh.ToUpper() == request.ChiNhanh.ToUpper()
                   && d.NgayCNNTK >= request.StartDate
                   && d.NgayCNNTK <= request.EndDate);

            if (!string.IsNullOrWhiteSpace(request.DVQLSD))
            {
                query = query.Where(d => d.DVQLSD.ToUpper() == request.DVQLSD.ToUpper());
            }

            if (!string.IsNullOrWhiteSpace(request.DaiVT))
            {
                query = query.Where(d => d.DaiVienThong.ToUpper() == request.DaiVT.ToUpper());
            }

            if (!string.IsNullOrWhiteSpace(request.NhaCungCap))
            {
                query = query.Where(d => d.NhaCungCap.ToUpper() == request.NhaCungCap.ToUpper());
            }

            query = query.OrderByDescending(d => d.ReqCode)
                        .ThenBy(d => d.Serial);

            var resultQuery = query.Select(d => new DeviceDetailsReportSummary
            {
                Serial = d.Serial,
                TenThietBi = d.TenThietBi,
                TinhTrang = d.TinhTrang
            });
            return new PagedResult<DeviceDetailsReportSummary>(resultQuery, request.Page, request.RecordsPerPage);
        }
    }
}
