﻿using System.Collections.Generic;
using System.Linq;
using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Entities.Views;
using Cmms.Domain.Queries;
using System;
using MBoL.Utils;
using Oracle.ManagedDataAccess.Client;

namespace Cmms.Data.Repositories
{
    public class NotDeliveryDeviceReportRepository : BaseRepository, INotDeliveryDeviceReportRepository
    {
        public NotDeliveryDeviceReportRepository(CmmsDbContext dbCtx) : base(dbCtx)
        {

        }

        public IEnumerable<NotDeliveryDeviceStatisticByDTSC> GetStatisticByDTSC(DeviceReportQuery request)
        {
            //var query = GetDbEntity<DeviceReportView>()
            //    .Where(d => d.DoiTac != null
            //        && d.NgayDTBG > request.EndDate && request.Organizations.Contains(d.ChiNhanh))
            //    .GroupBy(d => new { d.ChiNhanh, d.DoiTuongThietBi, d.NhaCungCap, d.DoiTac })
            //    .Select(d => new NotDeliveryDeviceStatisticByDTSC
            //    {
            //        ChiNhanh = d.Key.ChiNhanh,
            //        NhaCungCap = d.Key.NhaCungCap,
            //        DoiTuongTB = d.Key.DoiTuongThietBi,
            //        DoiTac = d.Key.DoiTac,
            //        Tong = d.Count()
            //    });

            /*var query = GetDbEntity<DeviceReportView>()
                .Where(d => d.DoiTac != null
                    && d.NgayBGDT >= request.StartDate && d.NgayBGDT <= request.EndDate
                    && (d.NgayDTBG == null || d.NgayDTBG > request.EndDate)
                    && d.TinhTrangBanGiao == "TN" && request.Organizations.Contains(d.ChiNhanh))
                .GroupBy(d => new { d.ChiNhanh, d.DoiTuongThietBi, d.NhaCungCap, d.DoiTac })
                .Select(d => new NotDeliveryDeviceStatisticByDTSC
                {
                    ChiNhanh = d.Key.ChiNhanh,
                    NhaCungCap = d.Key.NhaCungCap,
                    DoiTuongTB = d.Key.DoiTuongThietBi,
                    DoiTac = d.Key.DoiTac,
                    Tong = d.Count()
                });

            return query.ToArray();*/
            var orgs = StringHelper.MakeOrgSearch(request.Organizations);
            var sql = string.Format(@"
                SELECT
                    chinhanh,
                    doituongtb,
                    nhacc NhaCungCap,
                    doitac,
                    COUNT(1) tong
                FROM
                    mv_bcchitiet
                WHERE
                    doitac IS NOT NULL
                    AND ngaybgdt IS NOT NULL
                    AND ( trunc(ngaybgdt) BETWEEN TO_DATE('{1}', 'YYYY-MM-DD') AND TO_DATE('{2}', 'YYYY-MM-DD') )
                    AND ( ngaydtbg IS NULL
                          OR ( ngaydtbg IS NOT NULL
                               AND trunc(ngaydtbg) > TO_DATE('{2}', 'YYYY-MM-DD') ) )
                    AND chinhanh IN ({0})
                GROUP BY
                    chinhanh,
                    doituongtb,
                    nhacc,
                    doitac
                ORDER BY
                    chinhanh,
                    doituongtb,
                    nhacc,
                    doitac"
                , orgs, request.StartDate.ToString("yyyy-MM-dd"), request.EndDate.ToString("yyyy-MM-dd"));
            return dbContext.Database.SqlQuery<NotDeliveryDeviceStatisticByDTSC>(sql).ToArray();
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDTSC(NotDeliveryDeviceReportByDTSCQuery request)
        {
            /*var query = GetDbEntity<DeviceReportView>()
                .Where(d => d.ChiNhanh.ToUpper() == request.ChiNhanh.ToUpper()
                    && d.DoiTuongThietBi.ToUpper() == request.DoiTuongTB.ToUpper()
                    && d.NhaCungCap.ToUpper() == request.NhaCungCap.ToUpper()
                    && d.DoiTac == request.DoiTac.ToUpper()
                    && d.NgayBGDT >= request.StartDate && d.NgayBGDT <= request.EndDate
                    && (d.NgayDTBG == null || d.NgayDTBG > request.EndDate)
                    && d.TinhTrangBanGiao == "TN")
                .OrderByDescending(d => d.ReqCode)
                .ThenBy(d => d.Serial)
                .Select(d => new DeviceDetailsReportSummary
                {
                    Serial = d.Serial,
                    TenThietBi = d.TenThietBi,
                    TinhTrang = d.TinhTrang
                });

            return new PagedResult<DeviceDetailsReportSummary>(query, request.Page, request.RecordsPerPage);*/
            var sql = string.Format(@"
                SELECT
                    serial,
                    tentb tenthietbi,
                    tinhtrang
                FROM
                    mv_bcchitiet
                WHERE
                    doitac IS NOT NULL
                    AND ngaybgdt IS NOT NULL
                    AND ( trunc(ngaybgdt) BETWEEN TO_DATE('{0}', 'YYYY-MM-DD') AND TO_DATE('{1}', 'YYYY-MM-DD') )
                    AND ( ngaydtbg IS NULL
                          OR ( ngaydtbg IS NOT NULL
                               AND trunc(ngaydtbg) > TO_DATE('{1}', 'YYYY-MM-DD') ) )
                    AND upper(chinhanh) = :ChiNhanh
                    AND upper(doituongtb) = :DoiTuongTB
                    AND upper(nhacc) = :NhaCungCap
                    AND upper(doitac) = :DoiTac"
                , request.StartDate.ToString("yyyy-MM-dd"), request.EndDate.ToString("yyyy-MM-dd"));
            var query = dbContext.Database.SqlQuery<DeviceDetailsReportSummary>(sql,
                new OracleParameter("ChiNhanh", request.ChiNhanh.ToUpper()),
                new OracleParameter("DoiTuongTB", request.DoiTuongTB.ToUpper()),
                new OracleParameter("NhaCungCap", request.NhaCungCap.ToUpper()),
                new OracleParameter("DoiTac", request.DoiTac.ToUpper())).AsQueryable();
            return new PagedResult<DeviceDetailsReportSummary>(query, request.Page, request.RecordsPerPage);
        }

        public IEnumerable<NotDeliveryDeviceStatisticByDVT> GetDeviceStatisticByDVT(DeviceReportQuery request)
        {
            /*var query = GetDbEntity<DeviceReportView>()
                .Where(d => d.DoiTac != null
                    && d.NgayBGDT >= request.StartDate && d.NgayBGDT <= request.EndDate
                    && (d.NgayDTBG == null || d.NgayDTBG > request.EndDate)
                    && d.TinhTrangBanGiao == "TN" && request.Organizations.Contains(d.ChiNhanh))
                .GroupBy(d => new { d.ChiNhanh, d.DVQLSD, d.DaiVienThong })
                .Select(d => new NotDeliveryDeviceStatisticByDVT
                {
                    ChiNhanh = d.Key.ChiNhanh,
                    DaiVT = d.Key.DaiVienThong,
                    DVQLSD = d.Key.DVQLSD,
                    Tong = d.Count()
                });

            return query.ToArray();*/
            var orgs = StringHelper.MakeOrgSearch(request.Organizations);
            var sql = string.Format(@"
                SELECT
                    chinhanh,
                    dvqlsd1,
                    daivt,
                    count(1) tong
                FROM
                    mv_bcchitiet
                WHERE
                    doitac IS NOT NULL
                    AND ngaybgdt IS NOT NULL
                        AND ( trunc(ngaybgdt) BETWEEN TO_DATE('{1}', 'YYYY-MM-DD') AND TO_DATE('{2}', 'YYYY-MM-DD') )
                            AND ( ngaydtbg IS NULL
                                  OR ( ngaydtbg IS NOT NULL
                                       AND trunc(ngaydtbg) > TO_DATE('{2}', 'YYYY-MM-DD') ) )
                    AND chinhanh IN ({0})
                GROUP BY
                    chinhanh,
                    dvqlsd1,
                    daivt
                ORDER BY
                    chinhanh,
                    dvqlsd1,
                    daivt"
                , orgs, request.StartDate.ToString("yyyy-MM-dd"), request.EndDate.ToString("yyyy-MM-dd"));
            return dbContext.Database.SqlQuery<NotDeliveryDeviceStatisticByDVT>(sql).ToArray();
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVT(NotDeliveryDeviceReportByDVTQuery request)
        {
            /*var query = GetDbEntity<DeviceReportView>()
                .Where(d => d.ChiNhanh.ToUpper() == request.ChiNhanh.ToUpper()
                    && d.DVQLSD.ToUpper() == request.DVQLSD.ToUpper()
                    && d.DaiVienThong.ToUpper() == request.DaiVT.ToUpper()
                    && d.DoiTac != null
                    && d.NgayBGDT >= request.StartDate && d.NgayBGDT <= request.EndDate
                    && (d.NgayDTBG == null || d.NgayDTBG > request.EndDate)
                    && d.TinhTrangBanGiao == "TN")
                .OrderByDescending(d => d.ReqCode)
                .ThenBy(d => d.Serial)
                .Select(d => new DeviceDetailsReportSummary
                {
                    Serial = d.Serial,
                    TenThietBi = d.TenThietBi,
                    TinhTrang = d.TinhTrang
                });

            return new PagedResult<DeviceDetailsReportSummary>(query, request.Page, request.RecordsPerPage);*/
            var sql = string.Format(@"
                SELECT
                    serial,
                    tentb tenthietbi,
                    tinhtrang
                FROM
                    mv_bcchitiet
                WHERE
                    doitac IS NOT NULL
                    AND ngaybgdt IS NOT NULL
                    AND ( trunc(ngaybgdt) BETWEEN TO_DATE('{0}', 'YYYY-MM-DD') AND TO_DATE('{1}', 'YYYY-MM-DD') )
                    AND ( ngaydtbg IS NULL
                          OR ( ngaydtbg IS NOT NULL
                               AND trunc(ngaydtbg) > TO_DATE('{1}', 'YYYY-MM-DD') ) )
                    AND upper(chinhanh) = :ChiNhanh
                    AND upper(dvqlsd1) = :DVQLSD
                    AND upper(daivt) = :DaiVT"
                , request.StartDate.ToString("yyyy-MM-dd"), request.EndDate.ToString("yyyy-MM-dd"));
            var query = dbContext.Database.SqlQuery<DeviceDetailsReportSummary>(sql,
                new OracleParameter("ChiNhanh", request.ChiNhanh.ToUpper()),
                new OracleParameter("DVQLSD", request.DVQLSD.ToUpper()),
                new OracleParameter("DaiVT", request.DaiVT.ToUpper())).AsQueryable();
            return new PagedResult<DeviceDetailsReportSummary>(query, request.Page, request.RecordsPerPage);
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryAllLevel(DeviceReportAllLevelQuery request)
        {
            var query = GetDbEntity<DeviceReportView>()
                .Where(d => d.ChiNhanh.ToUpper() == request.ChiNhanh.ToUpper()
                    && d.NgayBGDT >= request.StartDate && d.NgayBGDT <= request.EndDate
                    && (d.NgayDTBG == null || d.NgayDTBG > request.EndDate)
                    && d.TinhTrangBanGiao == "TN");

            if (!string.IsNullOrWhiteSpace(request.DVQLSD))
            {
                query = query.Where(d => d.DVQLSD.ToUpper() == request.DVQLSD.ToUpper());
            }

            if (!string.IsNullOrWhiteSpace(request.DaiVT))
            {
                query = query.Where(d => d.DaiVienThong.ToUpper() == request.DaiVT.ToUpper());
            }

            if (!string.IsNullOrWhiteSpace(request.NhaCungCap))
            {
                query = query.Where(d => d.NhaCungCap.ToUpper() == request.NhaCungCap.ToUpper());
            }

            if (!string.IsNullOrWhiteSpace(request.DoiTuongTB))
            {
                query = query.Where(d => d.DoiTuongThietBi.ToUpper() == request.DoiTuongTB.ToUpper());
            }

            if (!string.IsNullOrWhiteSpace(request.DoiTac))
            {
                query = query.Where(d => d.DoiTac.ToUpper() == request.DoiTac.ToUpper());
            }
            else
            {
                query = query.Where(d => d.DoiTac != null);
            }

            query = query.OrderByDescending(d => d.ReqCode)
                .ThenBy(d => d.Serial);

            var result = query
                .Select(d => new DeviceDetailsReportSummary
                {
                    Serial = d.Serial,
                    TenThietBi = d.TenThietBi,
                    TinhTrang = d.TinhTrang
                });

            return new PagedResult<DeviceDetailsReportSummary>(result, request.Page, request.RecordsPerPage);
        }
    }
}
