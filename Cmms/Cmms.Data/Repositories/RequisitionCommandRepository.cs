﻿using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cmms.Domain.Commands;
using Oracle.ManagedDataAccess.Client;

namespace Cmms.Data.Repositories
{
    public class RequisitionCommandRepository : BaseRepository, IRequisitionCommandRepository
    {
        public RequisitionCommandRepository(CmmsDbContext dbContext) : base(dbContext)
        {

        }

        public void ApproveRequisition(ApproveRequisitionCommand command)
        {
            var sqlCommand = @"UPDATE r5requisitions r
                                SET req_status=(SELECT NVL(aut_statnew,req_status) 
                                                FROM r5permissions, r5userorganization, r5auth
                                                WHERE uog_user = :Username 
                                                  AND uog_org = req_org
                                                  AND uog_group = prm_group
                                                  AND prm_update = '*' 
                                                  AND prm_function = 'TNTBH' 
                                                  AND aut_group=uog_group 
                                                  AND aut_entity='REQ' 
                                                  AND aut_status=req_status
                                                  AND rownum=1),
                                    req_auth= :AuthUser
                                WHERE req_class='TNTBHH' 
                                  AND req_code= :RequisitionCode";
            var sqlParameters = new OracleParameter[3];
            sqlParameters[0] = new OracleParameter("Username", command.Username.ToUpper());
            sqlParameters[1] = new OracleParameter("AuthUser", command.AuthUser.ToUpper());
            sqlParameters[2] = new OracleParameter("RequisitionCode", command.RequisitionCode);

            dbContext.Database.ExecuteSqlCommand(sqlCommand, sqlParameters);
        }

        public void UpdateRequisitionStatus(ApproveRequisitionCommand command)
        {
            var sqlCommand = @"UPDATE r5requisitions r
                                SET req_status=:NewStatus,
                                    req_auth= :AuthUser
                                WHERE req_class='TNTBHH' 
                                  AND req_code= :RequisitionCode";
            var sqlParameters = new OracleParameter[3];
            sqlParameters[0] = new OracleParameter("NewStatus", command.NewStatus.ToUpper());
            sqlParameters[1] = new OracleParameter("AuthUser", command.AuthUser.ToUpper());
            sqlParameters[2] = new OracleParameter("RequisitionCode", command.RequisitionCode);

            dbContext.Database.ExecuteSqlCommand(sqlCommand, sqlParameters);
        }
    }
}
