﻿using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Cmms.Data.Repositories
{
    public class EntityStatusRepository : BaseRepository, IEntityStatusRepository
    {
        public EntityStatusRepository(CmmsDbContext dbContext) : base(dbContext)
        {

        }

        public IEnumerable<EntityStatus> GetStatuses(string entityCode)
        {
            return GetDbEntity<EntityStatus>()
                .Where(s => s.Entity.Equals(entityCode) && s.NotUsed == Constants.CommonConstant.FalseValueString)
                .ToArray();
        }
    }
}
