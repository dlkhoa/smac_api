﻿using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Entities.Views;
using Cmms.Domain.Queries;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Cmms.Data.Repositories
{
    public class DeliveryGoodDeviceReportRepository : BaseRepository, IDeliveryGoodDeviceReportRepository
    {
        public DeliveryGoodDeviceReportRepository(CmmsDbContext dbContext) : base(dbContext)
        {

        }

        public IEnumerable<DeliveryGoodDeviceStatisticByProvider> GetDeliveryDeviceStatisticProvider(DeviceReportQuery request)
        {
            var query = GetDbEntity<DeviceReportView>()
                .Where(d => d.NgayBanGiaoDVQLSD >= request.StartDate && d.NgayBanGiaoDVQLSD <= request.EndDate && d.CodeKQSSC == "3" && request.Organizations.Contains(d.ChiNhanh))
                .GroupBy(d => new { d.ChiNhanh, d.DoiTuongThietBi, d.NhaCungCap })
                .Select(d => new DeliveryGoodDeviceStatisticByProvider
                {
                    ChiNhanh = d.Key.ChiNhanh,
                    NhaCungCap = d.Key.NhaCungCap,
                    DoiTuongTB = d.Key.DoiTuongThietBi,
                    Tong = d.Count()
                });

            return query.ToArray();
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByProvider(DeliveryDeviceReportByProviderQuery request)
        {
            var query = GetDbEntity<DeviceReportView>()
                .Where(d => d.ChiNhanh.ToUpper() == request.ChiNhanh.ToUpper()
                    && d.DoiTuongThietBi.ToUpper() == request.DoiTuongTb.ToUpper()
                    && d.NhaCungCap.ToUpper() == request.NhaCungCap.ToUpper()
                    && d.NgayBanGiaoDVQLSD >= request.StartDate && d.NgayBanGiaoDVQLSD <= request.EndDate
                    && d.CodeKQSSC == "3")
                .OrderByDescending(d => d.ReqCode)
                .ThenBy(d => d.Serial)
                .Select(d => new DeviceDetailsReportSummary
                {
                    Serial = d.Serial,
                    TenThietBi = d.TenThietBi,
                    TinhTrang = d.TinhTrang
                });

            return new PagedResult<DeviceDetailsReportSummary>(query, request.Page, request.RecordsPerPage);
        }
        public IEnumerable<DeliveryGoodDeviceStatisticByDaiVT> GetDeviceStatisticByDVQLSD_Dai(DeviceReportQuery request)
        {
            var query = GetDbEntity<DeviceReportView>()
                .Where(d => d.NgayBanGiaoDVQLSD >= request.StartDate && d.NgayBanGiaoDVQLSD <= request.EndDate && d.CodeKQSSC == "3" && request.Organizations.Contains(d.ChiNhanh))
                .GroupBy(d => new { d.ChiNhanh, d.DVQLSD, d.DaiVienThong })
                .Select(d => new DeliveryGoodDeviceStatisticByDaiVT
                {
                    ChiNhanh = d.Key.ChiNhanh,
                    DaiVT = d.Key.DaiVienThong,
                    DVQLSD = d.Key.DVQLSD,
                    Tong = d.Count()
                });

            return query.ToArray();
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryByDVQLSD_Dai(DeliveryDeviceReportByDVQLSDDaiQuery request)
        {
            var query = GetDbEntity<DeviceReportView>()
                .Where(d => d.ChiNhanh.ToUpper() == request.ChiNhanh.ToUpper()
                    && d.DVQLSD.ToUpper() == request.DVQLSD.ToUpper()
                    && d.DaiVienThong.ToUpper() == request.DaiVT.ToUpper()
                    && d.NgayBanGiaoDVQLSD >= request.StartDate && d.NgayBanGiaoDVQLSD <= request.EndDate
                    && d.CodeKQSSC == "3")
                .OrderByDescending(d => d.ReqCode)
                .ThenBy(d => d.Serial)
                .Select(d => new DeviceDetailsReportSummary
                {
                    Serial = d.Serial,
                    TenThietBi = d.TenThietBi,
                    TinhTrang = d.TinhTrang
                });

            return new PagedResult<DeviceDetailsReportSummary>(query, request.Page, request.RecordsPerPage);
        }

        public IPagedResult<DeviceDetailsReportSummary> GetDeviceReportSummaryAllLevel(DeviceReportAllLevelQuery request)
        {
            var query = GetDbEntity<DeviceReportView>()
                .Where(d => d.ChiNhanh.ToUpper() == request.ChiNhanh.ToUpper()
                    && d.NgayBanGiaoDVQLSD >= request.StartDate && d.NgayBanGiaoDVQLSD <= request.EndDate
                    && d.CodeKQSSC == "3");

            if (!string.IsNullOrWhiteSpace(request.DVQLSD))
            {
                query = query.Where(d => d.DVQLSD.ToUpper() == request.DVQLSD.ToUpper());
            }

            if (!string.IsNullOrWhiteSpace(request.DaiVT))
            {
                query = query.Where(d => d.DaiVienThong.ToUpper() == request.DaiVT.ToUpper());
            }

            if (!string.IsNullOrWhiteSpace(request.NhaCungCap))
            {
                query = query.Where(d => d.NhaCungCap.ToUpper() == request.NhaCungCap.ToUpper());
            }

            if (!string.IsNullOrWhiteSpace(request.DoiTuongTB))
            {
                query = query.Where(d => d.DoiTuongThietBi.ToUpper() == request.DoiTuongTB.ToUpper());
            }

            query = query.OrderByDescending(d => d.ReqCode)
                .ThenBy(d => d.Serial);

            var resultQuery = query.Select(d => new DeviceDetailsReportSummary
            {
                Serial = d.Serial,
                TenThietBi = d.TenThietBi,
                TinhTrang = d.TinhTrang
            });

            return new PagedResult<DeviceDetailsReportSummary>(resultQuery, request.Page, request.RecordsPerPage);
        }
    }
}
