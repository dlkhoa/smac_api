﻿using Cmms.Data.Constants;
using Cmms.Data.Infrastructure;
using Cmms.Data.Infrastructure.Paging;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cmms.Data.Repositories
{
    public class CompanyRepository : BaseRepository, ICompanyRepository
    {
        public CompanyRepository(CmmsDbContext dbContext) : base(dbContext)
        {

        }

        public IPagedResult<DanhMucDoiTacSuaChua> LayDanhMucDoiTacSuaChua(CompanyQuery companyQuery)
        {
            var query = GetDbEntity<Company>()
                .Where(c => c.UdfChkBox02 == CommonConstant.TrueValueString);

            if (!string.IsNullOrEmpty(companyQuery.Code))
            {
                query = query.Where(c => c.Code.ToUpper().Contains(companyQuery.Code.ToUpper()));
            }

            var companies = query.OrderBy(c => c.Code).ToList();
            companies.Insert(0, new Company { Code = "", Description = "[TẤT CẢ]", });

            return new PagedResultMapper<Company, DanhMucDoiTacSuaChua>(companies.AsQueryable(), companyQuery.Page, companyQuery.RecordsPerPage);
        }

        public IPagedResult<DanhMucDVQLSDTB> LayDanhMucDVQLSDTB(CompanyQuery companyQuery)
        {
            var query = GetDbEntity<Company>()
                 .Where(c => c.UdfChkBox01 == CommonConstant.TrueValueString);

            if (!string.IsNullOrEmpty(companyQuery.Code))
            {
                query = query.Where(c => c.Code.ToUpper().Contains(companyQuery.Code.ToUpper()));
            }

            return new PagedResultMapper<Company, DanhMucDVQLSDTB>(query.OrderBy(c => c.Code), companyQuery.Page, companyQuery.RecordsPerPage);
        }

        public IPagedResult<CodeName> LayDanhMucLoaiThietbi(CompanyQuery companyQuery)
        {
            // Hardcode for the moment
            var list = new List<CodeName>() {
                new CodeName
                {
                    Code = "",
                    Name = "[TẤT CẢ]"
                },
                new CodeName
                {
                    Code = "BSCRNC",
                    Name = "BSC/RNC"
                },
                new CodeName
                {
                    Code = "BTSNODEB",
                    Name = "BTS/Node B"
                },
                new CodeName
                {
                    Code = "CSCORE",
                    Name = "CS Core"
                },
                new CodeName
                {
                    Code = "Minilink",
                    Name = "Minilink"
                },
                new CodeName
                {
                    Code = "NGUON",
                    Name = "Nguồn"
                },
                new CodeName
                {
                    Code = "PASOLINK",
                    Name = "Pasolink"
                },
                new CodeName
                {
                    Code = "PSCORE",
                    Name = "PS Core"
                },
                new CodeName
                {
                    Code = "KHAC",
                    Name = "Khác"
                },
            };
            return new PagedResult<CodeName>(list.AsQueryable(), companyQuery.Page, companyQuery.RecordsPerPage);
        }

        public IPagedResult<CodeName> LayDanhMucDaiVienthong(CompanyQuery companyQuery)
        {
            // Hardcode for the moment
            var list = new List<CodeName>() {
                new CodeName
                {
                    Code = "",
                    Name = "[TẤT CẢ]"
                },
                new CodeName
                {
                    Code = "DAI HA NOI 1",
                    Name = "Đài viễn thông Hà Nội 1"
                },
                new CodeName
                {
                    Code = "DAI HA NOI 2",
                    Name = "Đài viễn thông Hà Nội 2"
                },
                new CodeName
                {
                    Code = "DAI HAI PHONG",
                    Name = "Đài viễn thông Hải Phòng"
                },
                new CodeName
                {
                    Code = "DAI HOA BINH",
                    Name = "Đài viễn thông Hòa Bình"
                },
                new CodeName
                {
                    Code = "DAI HUNG YEN",
                    Name = "Đài viễn thông Hưng Yên"
                },
                new CodeName
                {
                    Code = "DAI KHOAI CHAU",
                    Name = "Đài viễn thông Khoái Châu"
                },
                new CodeName
                {
                    Code = "DAI NAM DINH",
                    Name = "Đài viễn thông Nam Định"
                },
                new CodeName
                {
                    Code = "DAI NGHE AN",
                    Name = "Đài viễn thông Nghệ An"
                },
                new CodeName
                {
                    Code = "DAI THAI BINH",
                    Name = "Đài viễn thông Thái Bình"
                },
                new CodeName
                {
                    Code = "DAI THAI NGUYEN",
                    Name = "Đài viễn thông Thái Nguyên"
                },
                new CodeName
                {
                    Code = "DAI VHMB",
                    Name = "Đài vận hành miền Bắc"
                },
                new CodeName
                {
                    Code = "DAI VINH PHUC",
                    Name = "Đài viễn thông Vĩnh Phúc"
                },
                new CodeName
                {
                    Code = "P.CSHT TTMLMB",
                    Name = "P.CSHT TTMLMB"
                },
                new CodeName
                {
                    Code = "PHONG KT",
                    Name = "Phòng KT- TT. MLMB"
                },
                new CodeName
                {
                    Code = "CTO",
                    Name = "Tổ OMC Miền Tây - Đài Vận Hành Mạng - TTMLMN"
                },
                new CodeName
                {
                    Code = "DNI",
                    Name = "Tổ OMC Miền Đông - Đài Vận Hành Mạng - TTMLMN"
                },
                new CodeName
                {
                    Code = "HCM",
                    Name = "Tổ OMC HCM - Đài Vận Hành  Mạng - TTMLMN"
                },
                new CodeName
                {
                    Code = "DAI BINH DINH",
                    Name = "Đài viễn thông Bình Định"
                },
                new CodeName
                {
                    Code = "DAI DA NANG",
                    Name = "Đài viễn thông Đà Nẵng"
                },
                new CodeName
                {
                    Code = "DAI DAKLAK",
                    Name = "Đài viễn thông ĐAKLAK"
                },
                new CodeName
                {
                    Code = "DAI DIEU HANH MB",
                    Name = "Đài điều hành Miền Bắc (NOC)"
                },
                new CodeName
                {
                    Code = "DAI DIEU HANH MN",
                    Name = "Đài điều hành Miền Nam (NOC)"
                },
                new CodeName
                {
                    Code = "DAI DIEU HANH MT",
                    Name = "Đài điều hành Miền Trung (NOC)"
                },
                new CodeName
                {
                    Code = "CNHN",
                    Name = "Chi nhánh Hà Nội"
                },
                new CodeName
                {
                    Code = "KHO_SC_NOIBO",
                    Name = "Kho sửa chữa TTĐK&SCTBVT"
                },
                new CodeName
                {
                    Code = "TT MVAS",
                    Name = "TT MVAS"
                },
            };
            return new PagedResult<CodeName>(list.AsQueryable(), companyQuery.Page, companyQuery.RecordsPerPage);
        }

        public IPagedResult<CodeName> LayDanhMucDonviQLSDTB(CompanyQuery companyQuery)
        {
            var superuserGroup = new List<string> { "BLD-CN", "BLD-TT", "LD-TP", "LD-NV", "KT-TP", "KT-NV", "ADMIN" };
            var groupCode = string.Empty;
            var defaultOrg = string.Empty;
            var canSeeAllBranches = false;
            var user = GetDbEntity<User>()
                .Where(u => u.UserCode.ToUpper() == companyQuery.Username.ToUpper())
                .FirstOrDefault();
            if (user != null)
            {
                groupCode = user.GroupCode;
                defaultOrg = user.DefaultOrg;
                if (superuserGroup.Contains(groupCode))
                {
                    canSeeAllBranches = true;
                }
            }
            // Hardcode for the moment
            var list = new List<CodeName>();
            var dvqlsdALL = new CodeName
            {
                Code = "ALL",
                Name = "TẤT CẢ"
            };
            var dvqlsdEmpty = new CodeName
            {
                Code = "",
                Name = "[TẤT CẢ]"
            };
            var dvqlsdMB = new CodeName
            {
                Code = "TT MLMB",
                Name = "Trung tâm mạng lưới Miền Bắc"
            };
            var dvqlsdMT = new CodeName
            {
                Code = "TT MLMT",
                Name = "Trung tâm mạng lưới Miền Trung"
            };
            var dvqlsdMN = new CodeName
            {
                Code = "TT MLMN",
                Name = "Trung tâm mạng lưới Miền Nam"
            };
            if (canSeeAllBranches || defaultOrg.ToUpper() == "" || defaultOrg.ToUpper() == "ALL")
            {
                list.Add(dvqlsdALL);
                list.Add(dvqlsdMB);
                list.Add(dvqlsdMT);
                list.Add(dvqlsdMN);
            }
            else
            {
                list.Add(dvqlsdEmpty);
                switch (defaultOrg.ToUpper())
                {
                    case "CNHN":
                        list.Add(dvqlsdMB);
                        break;
                    case "CNDN":
                        list.Add(dvqlsdMT);
                        break;
                    case "CNHCM":
                        list.Add(dvqlsdMN);
                        break;
                    case "ALL":
                        list.Add(dvqlsdMB);
                        list.Add(dvqlsdMT);
                        list.Add(dvqlsdMN);
                        break;
                }
            }
            list.Add(
                new CodeName
                {
                    Code = "TT MVAS",
                    Name = "Trung tâm MVAS"
                }
            );
            list.Add(
                new CodeName
                {
                    Code = "TT NOC",
                    Name = "Trung tâm NOC"
                }
            );
            list.Add(
                new CodeName
                {
                    Code = "TT TCTK",
                    Name = "TT TCTK"
                }
            );
            list.Add(
                new CodeName
                {
                    Code = "TT ĐKSC",
                    Name = "TT ĐKSC"
                }
            );
            return new PagedResult<CodeName>(list.AsQueryable(), companyQuery.Page, companyQuery.RecordsPerPage);
        }

        public IPagedResult<CodeName> LayDanhMucKetquaKiemtraTruocSC(CompanyQuery companyQuery)
        {
            // Hardcode for the moment
            var list = new List<CodeName>()
            { 
                new CodeName
                {
                    Code = "",
                    Name = "[TẤT CẢ]"
                },
                new CodeName
                {
                    Code = "Hỏng",
                    Name = "Hỏng"
                },
                new CodeName
                {
                    Code = "Không hỏng",
                    Name = "Không hỏng"
                },
                new CodeName
                {
                    Code = "Không thể sửa chữa",
                    Name = "Không thể sửa chữa"
                },
            };
            //BTS/Node B, , Nguồn, PS Core, CS Core, Minilink, Pasolink, Khác
            return new PagedResult<CodeName>(list.AsQueryable(), companyQuery.Page, companyQuery.RecordsPerPage);
        }

        public IPagedResult<CodeName> LayDanhMucKetquaKiemtraSauSC(CompanyQuery companyQuery)
        {
            // Hardcode for the moment
            var list = new List<CodeName>()
            {
                new CodeName
                {
                    Code = "",
                    Name = "[TẤT CẢ]"
                },
                new CodeName
                {
                    Code = "Tốt",
                    Name = "Tốt"
                },
                new CodeName
                {
                    Code = "Không tốt",
                    Name = "Không tốt"
                },
                new CodeName
                {
                    Code = "Không thể sửa chữa",
                    Name = "Không thể sửa chữa"
                },
            };
            return new PagedResult<CodeName>(list.AsQueryable(), companyQuery.Page, companyQuery.RecordsPerPage);
        }

        public IPagedResult<CodeName> LayDanhMucTrangthaiThietbi(CompanyQuery companyQuery)
        {
            // Hardcode for the moment
            var list = new List<CodeName>()
            {
                new CodeName
                {
                    Code = "",
                    Name = "[TẤT CẢ]"
                },
                new CodeName
                {
                    Code = "I",
                    Name = "CHƯA TIẾP NHẬN"
                },
                new CodeName
                {
                    Code = "I1",
                    Name = "CHỜ TIẾP NHẬN"
                },
                new CodeName
                {
                    Code = "I2",
                    Name = "ĐÃ TIẾP NHẬN"
                },
                new CodeName
                {
                    Code = "I3",
                    Name = "CHỜ PHÊ DUYỆT NHẬP KHO"
                },
                new CodeName
                {
                    Code = "I4",
                    Name = "HỦY PHÊ DUYỆT NHẬP KHO"
                },
                new CodeName
                {
                    Code = "I5",
                    Name = "ĐÃ PHÊ DUYỆT NHẬP KHO"
                },
                new CodeName
                {
                    Code = "I6",
                    Name = "CHỜ NHẬP KHO"
                },
                new CodeName
                {
                    Code = "I7",
                    Name = "ĐÃ NHẬP KHO"
                },
                new CodeName
                {
                    Code = "I8",
                    Name = "ĐANG KIỂM TRA TRƯỚC SC/BẢO HÀNH"
                },
                new CodeName
                {
                    Code = "I9",
                    Name = "ĐÃ KIỂM TRA TRƯỚC SC/BẢO HÀNH"
                },
                new CodeName
                {
                    Code = "I10",
                    Name = "ĐANG KIỂM TRA SAU SC/BẢO HÀNH"
                },
                new CodeName
                {
                    Code = "I11",
                    Name = "ĐÃ KIỂM TRA SAU SC/BẢO HÀNH"
                },
                new CodeName
                {
                    Code = "I12",
                    Name = "CHỜ PHÊ DUYỆT XUẤT KHO"
                },
                new CodeName
                {
                    Code = "I13",
                    Name = "ĐÃ PHÊ DUYỆT XUẤT KHO"
                },
                new CodeName
                {
                    Code = "I14",
                    Name = "ĐÃ XUẤT KHO ĐI SC"
                },
                new CodeName
                {
                    Code = "I15",
                    Name = "ĐÃ XUẤT KHO CHO ĐVQLSD"
                },
                new CodeName
                {
                    Code = "I16",
                    Name = "ĐÃ XUẤT KHO ĐI BẢO HÀNH"
                },
                new CodeName
                {
                    Code = "I17",
                    Name = "HỎNG, KHÔNG THỂ SỬA CHỮA ĐƯỢC"
                },
            };
            return new PagedResult<CodeName>(list.AsQueryable(), companyQuery.Page, companyQuery.RecordsPerPage);
        }

        public IPagedResult<CodeName> LayDanhMucDoituongThietbi(CompanyQuery companyQuery)
        {
            // Hardcode for the moment
            var list = new List<CodeName>()
            { 
                new CodeName
                {
                    Code = "",
                    Name = "[TẤT CẢ]"
                },
                new CodeName
                {
                    Code = "1",
                    Name = "HTKT"
                },
                new CodeName
                {
                    Code = "2",
                    Name = "Bảo hành HTKT"
                },
                new CodeName
                {
                    Code = "3",
                    Name = "Ngoài HTKT"
                },
                new CodeName
                {
                    Code = "4",
                    Name = "Bảo hành theo HĐ ngoài HTKT"
                },
                new CodeName
                {
                    Code = "5",
                    Name = "Từ chối HTKT"
                },
                new CodeName
                {
                    Code = "6",
                    Name = "Từ chối tiếp nhận tại ĐVQLSD"
                },
            };
            return new PagedResult<CodeName>(list.AsQueryable(), companyQuery.Page, companyQuery.RecordsPerPage);
        }

        public IPagedResult<CodeName> LayDanhMucChinhanh(CompanyQuery companyQuery)
        {
            var superuserGroup = new List<string> { "BLD-CN", "BLD-TT", "LD-TP", "LD-NV", "KT-TP", "KT-NV", "ADMIN" };
            var groupCode = string.Empty;
            var defaultOrg = string.Empty;
            var canSeeAllBranches = false;
            var user = GetDbEntity<User>()
                .Where(u => u.UserCode.ToUpper() == companyQuery.Username.ToUpper())
                .FirstOrDefault();
            if (user != null)
            {
                groupCode = user.GroupCode;
                defaultOrg = user.DefaultOrg;
                if (superuserGroup.Contains(groupCode))
                {
                    canSeeAllBranches = true;
                }
            }
            List<CodeName> list = null;
            if (canSeeAllBranches)
            {
                // Hardcode for the moment
                list = new List<CodeName>()
                { 
                    new CodeName
                    {
                        Code = "ALL",
                        Name = "TẤT CẢ"
                    },
                    new CodeName
                    {
                        Code = "CNHN",
                        Name = "CNHN"
                    },
                    new CodeName
                    {
                        Code = "CNDN",
                        Name = "CNDN"
                    },
                    new CodeName
                    {
                        Code = "CNHCM",
                        Name = "CNHCM"
                    },
                    new CodeName
                    {
                        Code = "TTDKSC",
                        Name = "TTDKSC"
                    },
                };
            }
            else
            {
                list = new List<CodeName>()
                { 
                    new CodeName
                    {
                        Code = defaultOrg,
                        Name = defaultOrg
                    },
                };
            }
            return new PagedResult<CodeName>(list.AsQueryable(), companyQuery.Page, companyQuery.RecordsPerPage);
        }

        public IPagedResult<CodeName> LayDanhMucNam(CompanyQuery companyQuery)
        {
            // Hardcode for the moment
            var startYear = 2015;
            var list = new List<CodeName>()
            {
                new CodeName
                {
                    Code = "0",
                    Name = "TẤT CẢ"
                },
            };
            for (int y = DateTime.Today.Year; y >= startYear; y--)
            {
                list.Add(
                    new CodeName
                    {
                        Code = y.ToString(),
                        Name = y.ToString()
                    });
            }

            return new PagedResult<CodeName>(list.AsQueryable(), companyQuery.Page, companyQuery.RecordsPerPage);
        }
    }
}
