﻿using Cmms.Data.Infrastructure;
using Cmms.Data.Repositories.Abstracts;
using Cmms.Domain.Entities.DataContracts;
using Cmms.Domain.Queries;
using Oracle.ManagedDataAccess.Client;
using System.Collections.Generic;
using System.Linq;
using System;
using Cmms.Data.Infrastructure.Paging;
using MBoL.Utils;

namespace Cmms.Data.Repositories
{
    public class ImportExportDeviceReportRepository : BaseRepository, IImportExportDeviceReportRepository
    {
        public ImportExportDeviceReportRepository(CmmsDbContext dbContext) : base(dbContext)
        {

        }
        
        public IPagedResult<ImportExportDeviceReport> GetAwaitingRepairDevices(ImportExportDeviceReportQuery request)
        {
            var orgs = StringHelper.MakeOrgSearch(request.Organizations);
            var sql = string.Format(@"SELECT TenThietBi, CHINHANH,TONDAU,NhapTrongKy,XuatTrongKy,TonCuoi, Kho
                        FROM (
                            SELECT  TenThietBi, CHINHANH,(NHAPDK-KTDK-XUATDK) TONDAU,NHAPTK NhapTrongKy,XUATTK XuatTrongKy,
                                    (NHAPDK-KTDK-XUATDK)+NHAPTK -XUATTK TonCuoi,
                                    CASE WHEN CHINHANH='CNHN' THEN 'KHOSC-HN'
                                        WHEN CHINHANH='CNHCM' THEN 'KHOSC-HCM'
                                        WHEN CHINHANH='CNDN' THEN 'KHOSC-DN'
                                        END KHO
                            FROM(
                                SELECT TenThietBi,chinhanh,SUM(NHAPDK) NHAPDK,SUM(KTDK) KTDK,SUM(XUATDK) XUATDK,
                                        SUM(NHAPTK) NHAPTK,SUM(XUATTK) XUATTK 
                                FROM (
                                    SELECT req_code,serial,trim(TENTB) TenThietBi,chinhanh,
                                        CASE WHEN trunc(ngayktlptsc)< TO_DATE(:StartDate1,'YYYY-MM-DD') THEN 1 ELSE 0 END NHAPDK,
                                        CASE WHEN codekqtsc in ('2','5') AND trunc(NGAYHTKTTSC)< TO_DATE(:StartDate2,'YYYY-MM-DD')  THEN 1 ELSE 0 END KTDK ,
                                        CASE WHEN trunc(NGAYKTGPXSC)< TO_DATE(:StartDate3,'YYYY-MM-DD')  THEN 1 ELSE 0 END XUATDK,
                                        (CASE WHEN trunc(NGAYKTLPTSC) 
                                                    between TO_DATE(:StartDate4,'YYYY-MM-DD') 
                                                    and TO_DATE(:EndDate1,'YYYY-MM-DD') 
                                            THEN 1 
                                            ELSE 0 END - 
                                         CASE WHEN codekqtsc IN ('2','5') 
                                                AND trunc(NGAYHTKTTSC) 
                                                between TO_DATE(:StartDate5,'YYYY-MM-DD') 
                                                and TO_DATE(:EndDate2,'YYYY-MM-DD') 
                                            THEN 1 ELSE 0 END) NHAPTK,

                                        CASE WHEN trunc(NGAYKTGPXSC) 
                                                between TO_DATE(:StartDate6,'YYYY-MM-DD') 
                                                and TO_DATE(:EndDate3,'YYYY-MM-DD') 
                                            THEN 1 ELSE 0 END XUATTK
                                    FROM mv_bcchitiet WHERE chinhanh IN ({0})
                                    )
                                GROUP BY TenThietBi,CHINHANH
                                )
                            )
                        WHERE KHO=:Kho
                        ORDER BY TenThietBi ASC", orgs);

            return new SqlPagedResult<ImportExportDeviceReport>(dbContext,
                sql,
                request.Page,
                request.RecordsPerPage,
                new OracleParameter("StartDate1", request.StartDate.ToString("yyyy-MM-dd")),
                new OracleParameter("StartDate2", request.StartDate.ToString("yyyy-MM-dd")),
                new OracleParameter("StartDate3", request.StartDate.ToString("yyyy-MM-dd")),
                new OracleParameter("StartDate4", request.StartDate.ToString("yyyy-MM-dd")),
                new OracleParameter("EndDate1", request.EndDate.ToString("yyyy-MM-dd")),
                new OracleParameter("StartDate5", request.StartDate.ToString("yyyy-MM-dd")),
                new OracleParameter("EndDate2", request.EndDate.ToString("yyyy-MM-dd")),
                new OracleParameter("StartDate6", request.StartDate.ToString("yyyy-MM-dd")),
                new OracleParameter("EndDate3", request.EndDate.ToString("yyyy-MM-dd")),
                new OracleParameter("Kho", request.Kho.ToUpper()));
        }

        public IPagedResult<ImportExportDeviceReport> GetAfterRepairedDevices(ImportExportDeviceReportQuery request)
        {
            var orgs = StringHelper.MakeOrgSearch(request.Organizations);
            var sql = string.Format(@"SELECT TenThietBi, CHINHANH,TONDAU,NhapTrongKy,XuatTrongKy,TonCuoi, Kho
                        FROM (
                            SELECT  TenThietBi,CHINHANH,(NHAPDK+KTTSCDK-KTSSCDK-XUATDK) TONDAU,
                                    NHAPTK NhapTrongKy,XUATTK XuatTrongKy,
                                    (NHAPDK+KTTSCDK-KTSSCDK-XUATDK)+NHAPTK -XUATTK TonCuoi,
                                    CASE WHEN CHINHANH='CNHN' THEN 'KHOSC-HN'
                                        WHEN CHINHANH='CNHCM' THEN 'KHOSC-HCM'
                                        WHEN CHINHANH='CNDN' THEN 'KHOSC-DN'
                                    END KHO FROM(
                                                SELECT TenThietBi,chinhanh,SUM(NHAPDK) NHAPDK,
                                                        SUM(KTTSCDK) KTTSCDK,SUM(KTSSCDK) KTSSCDK , 
                                                        SUM(XUATDK) XUATDK,SUM(NHAPTK) NHAPTK,SUM(XUATTK) XUATTK 
                                                FROM(
                                                    select req_code,serial,trim(TENTB) TenThietBi,chinhanh,
                                                        CASE WHEN trunc(NGAYKTLPSSC)< TO_DATE(:StartDate1,'YYYY-MM-DD') 
                                                             THEN 1 
                                                             ELSE 0 
                                                        END NHAPDK,
                                                        CASE WHEN codekqtsc in ('2','5') 
                                                                AND trunc(NGAYHTKTTSC)< TO_DATE(:StartDate2,'YYYY-MM-DD')  
                                                             THEN 1 
                                                             ELSE 0 
                                                        END KTTSCDK,
                                                        CASE WHEN codekqSSC ='4' 
                                                                AND trunc(NGAYHTKTSSC)< TO_DATE(:StartDate3,'YYYY-MM-DD')  
                                                             THEN 1 
                                                             ELSE 0 
                                                        END KTSSCDK,
                                                        CASE WHEN trunc(NGAYKTGPXDVSD)< TO_DATE(:StartDate4,'YYYY-MM-DD')  
                                                             THEN 1 
                                                             ELSE 0 
                                                        END XUATDK,
                                                        (CASE WHEN trunc(NGAYKTLPSSC) 
                                                                between TO_DATE(:StartDate5,'YYYY-MM-DD') 
                                                                and TO_DATE(:EndDate1,'YYYY-MM-DD') 
                                                              THEN 1 
                                                              ELSE 0 
                                                         END +
                                                        CASE WHEN trunc(NGAYHTKTTSC) 
                                                                between TO_DATE(:StartDate6,'YYYY-MM-DD') 
                                                                and TO_DATE(:EndDate2,'YYYY-MM-DD') 
                                                                AND CODEKQTSC IN ('2','5') 
                                                             THEN 1 
                                                             ELSE 0 
                                                        END -
                                                        CASE WHEN codekqSSC ='4' 
                                                                AND trunc(NGAYHTKTSSC) 
                                                                between TO_DATE(:StartDate7,'YYYY-MM-DD') 
                                                                and TO_DATE(:EndDate3,'YYYY-MM-DD') 
                                                             THEN 1 
                                                             ELSE 0 
                                                        END ) NHAPTK,
                                                        CASE WHEN trunc(NGAYKTGPXDVSD) 
                                                                between TO_DATE(:StartDate8,'YYYY-MM-DD') 
                                                                and TO_DATE(:EndDate4,'YYYY-MM-DD') 
                                                             THEN 1 
                                                             ELSE 0 
                                                        END XUATTK
                                                    from mv_bcchitiet WHERE chinhanh IN ({0}))
                                                GROUP BY CHINHANH,TenThietBi
                                            )
                            )
                        WHERE KHO=:Kho
                        ORDER BY TenThietBi ASC", orgs);

            return new SqlPagedResult<ImportExportDeviceReport>(dbContext,
                sql,
                request.Page,
                request.RecordsPerPage,
               new OracleParameter("StartDate1", request.StartDate.ToString("yyyy-MM-dd")),
               new OracleParameter("StartDate2", request.StartDate.ToString("yyyy-MM-dd")),
               new OracleParameter("StartDate3", request.StartDate.ToString("yyyy-MM-dd")),
               new OracleParameter("StartDate4", request.StartDate.ToString("yyyy-MM-dd")),
               new OracleParameter("StartDate5", request.StartDate.ToString("yyyy-MM-dd")),
               new OracleParameter("EndDate1", request.EndDate.ToString("yyyy-MM-dd")),
               new OracleParameter("StartDate6", request.StartDate.ToString("yyyy-MM-dd")),
               new OracleParameter("EndDate2", request.EndDate.ToString("yyyy-MM-dd")),
               new OracleParameter("StartDate7", request.StartDate.ToString("yyyy-MM-dd")),
               new OracleParameter("EndDate3", request.EndDate.ToString("yyyy-MM-dd")),
               new OracleParameter("StartDate8", request.StartDate.ToString("yyyy-MM-dd")),
               new OracleParameter("EndDate4", request.EndDate.ToString("yyyy-MM-dd")),
               new OracleParameter("Kho", request.Kho.ToUpper()));
        }

        public IPagedResult<ImportExportFixingDeviceReport> GetFixingDevices(ImportExportDeviceReportQuery request)
        {
            var orgs = StringHelper.MakeOrgSearch(request.Organizations);
            var sql = string.Format(@"SELECT KHO,trim(TENTB) TenThietBi, SUM(SL) SoLuong
                        FROM
                        (
                            SELECT CASE 
                                    WHEN CHINHANH = 'CNHN' THEN 'KHOSC-HN'
                                    WHEN CHINHANH = 'CNHCM' THEN 'KHOSC-HCM' 
                                    WHEN CHINHANH = 'CNDN' THEN 'KHOSC-DN' 
                                  END KHO, TENTB,1 SL,SERIAL
                            FROM MV_BCCHITIET
                            WHERE TRUNC(NGAYKTLPSSC) 
                                    between TO_DATE(:StartDate,'YYYY-MM-DD') 
                                    and TO_DATE(:EndDate,'YYYY-MM-DD') 
                                  AND CODEKQSSC ='4' 
                                  AND NGAYBGDVQLSD IS NULL
                                  AND chinhanh IN ({0})
                        ) 
                        WHERE KHO=:Kho
                        GROUP BY KHO,TENTB
                        ORDER BY TenThietBi", orgs);

            return new SqlPagedResult<ImportExportFixingDeviceReport>(dbContext,
                sql,
                request.Page,
                request.RecordsPerPage,
               new OracleParameter("StartDate", request.StartDate.ToString("yyyy-MM-dd")),
               new OracleParameter("EndDate", request.EndDate.ToString("yyyy-MM-dd")),
               new OracleParameter("Kho", request.Kho.ToUpper()));
        }
    }
}
