﻿using Cmms.Data.Infrastructure;
using Microsoft.Practices.Unity;

namespace Cmms.Data
{
    public class Bootstrapper
    {
        public static void RegisterDependencies(IUnityContainer container)
        {
            container.RegisterType<IUnitOfWork, UnitOfWork>(new InjectionConstructor("CmmsDbContext"))
                .RegisterType<CmmsDbContext>(new InjectionConstructor("CmmsDbContext"));
        }
    }
}
