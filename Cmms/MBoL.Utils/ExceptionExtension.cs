﻿using System;

namespace MBoL.Utils
{
    public static class ExceptionExtension
    {
        public static string GetFullMessage(this Exception exception)
        {
            var message = exception.Message;
            var innerEx = exception.InnerException;
            while(null != innerEx)
            {
                message += " -- | -- " +innerEx.Message;
                innerEx = innerEx.InnerException;
            }
            return message;
        }

        public static Exception GetLastInnerException(this Exception exception)
        {
            var inner = exception.InnerException;
            if (inner == null)
            {
                return exception;
            }

            while (inner.InnerException != null)
            {
                inner = inner.InnerException;
            }
            return inner;
        }
    }
}
