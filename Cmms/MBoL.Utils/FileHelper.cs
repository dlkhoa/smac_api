﻿using System;
using System.IO;

namespace MBoL.Utils
{
    public class FileHelper
    {
        public static string GenerateFileName(string originalFileName)
        {
            var fileName = Path.GetFileNameWithoutExtension(originalFileName);
            fileName += "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + Path.GetExtension(originalFileName);

            return fileName;
        }

        public static string GenerateDirectoryName(string directory)
        {
            if (!directory.EndsWith("\\"))
            {
                directory = directory + "\\";
            }

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            return directory;
        }

        public static byte[] GetFileContents(string fileName)
        {
            var filePath = Path.Combine(Config.DocumentDirectory, fileName);
            return File.ReadAllBytes(filePath);
        }

        public static string GetBase64FromFile(string fileName)
        {
            var bytes = GetFileContents(fileName);
            return Convert.ToBase64String(bytes);
        }
    }
}
