﻿namespace MBoL.Utils.Hasher
{
    public interface IHasher
    {
        string HashPassword(string password, string salt);
    }
}
