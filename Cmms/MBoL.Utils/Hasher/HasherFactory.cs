﻿namespace MBoL.Utils.Hasher
{
    public class HasherFactory
    {
        public static IHasher CreateHasherInstance()
        {
            return new Aes256Hasher();
        }
    }
}
