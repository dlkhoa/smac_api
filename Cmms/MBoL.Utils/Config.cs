﻿using System.Web;

namespace MBoL.Utils
{
    public static class Config
    {
        public static bool EnabledNotificationTimer
        {
            get
            {
                return SettingHelper.GetBooleanSetting("EnabledNotificationTimer", false);
            }
        }

        /// <summary>
        /// Timer Period in miliseconds
        /// </summary>
        public static int TimerPeriod
        {
            get
            {
                return SettingHelper.GetIntSetting("TimerPeriod", 60000);
            }
        }

        public static string DateFormat
        {
            get
            {
                return SettingHelper.GetStringSetting("DateFormat", "dd/MM/yyyy");
            }
        }

        public static string DocumentDirectory
        {
            get
            {
                return FileHelper.GenerateDirectoryName(SettingHelper.GetStringSetting("DocumentDirectory", "c:\\temp"));

            }
        }

        public static string BaseUrl
        {
            get
            {
                return string.Format("{0}://{1}/api/",
                    HttpContext.Current.Request.Url.Scheme,
                    HttpContext.Current.Request.Url.Authority);
            }
        }

        public static bool IsWriteRequestLog
        {
            get
            {
                return SettingHelper.GetBooleanSetting("IsWriteRequestLog", false);
            }
        }

        public static string DbDefaultSchema
        {
            get
            {
                return SettingHelper.GetStringSetting("DbDefaultSchema", "");
            }
        }
    }
}
