﻿using System;
using System.Globalization;

namespace MBoL.Utils
{
    public class DateTimeHelper
    {
        public static DateTime? ConvertStringToDateTime(string dateString)
        {
            if(string.IsNullOrEmpty(dateString))
            {
                return null;
            }
            DateTime date;
            if (!DateTime.TryParse(dateString, out date))
            {
                date = DateTime.ParseExact(dateString, Config.DateFormat, CultureInfo.InvariantCulture);
            }

            return date;
        }
    }
}
