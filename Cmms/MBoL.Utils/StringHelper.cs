﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MBoL.Utils
{
    public class StringHelper
    {
        public static string GetFirstLineInString(string input)
        {
            return input.Split(new[] { '\r', '\n' }).FirstOrDefault();
        }

        public static string MakeOrgSearch(List<string> organizations)
        {
            var result = new StringBuilder();

            if (organizations != null && organizations.Count > 0)
            {
                foreach (var org in organizations)
                {
                    result.Append(string.Format("'{0}',", org));
                }
            }

            return result.ToString().TrimEnd(',');
        }
    }
}
