﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace MBoL.Utils
{
    public class FcmResponse
    {
        public int Success { get; set; }
        public int Failure { get; set; }
        public int Canonical_ids { get; set; }
        public IEnumerable<FcmResultResponse> Results { get; set; }
    }

    public class FcmResultResponse
    {
        public string Error { get; set; }
    }

    public class FCMServiceHelper
    {
        public static FcmResponse PushNotification(List<string> deviceIDs, string message, object data = null)
        {
            try
            {
                var serverKey = SettingHelper.GetStringSetting("FCMServerKey"); //"";
                var senderId = SettingHelper.GetStringSetting("FCMSenderID");// "";
                var fcmUrl = SettingHelper.GetStringSetting("FCMUrl", "https://fcm.googleapis.com/fcm/send");
                //string deviceId = "9d311ef7 49caaa33 36d1b7bb ffc4c099 38e6dbd8 48c1c86c 1abeb693 2478bbb8";
                WebRequest tRequest = WebRequest.Create(fcmUrl);
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";

                if (data == null)
                {
                    data = new { code = "ABC-XYZ", user = "ADMIN" };
                }
                var responseData = new
                {
                    registration_ids = deviceIDs,
                    notification = new
                    {
                        body = message
                    },
                    data = data,
                    priority = "high"

                };
                /*var responseData = new
                {
                    to = deviceId,
                    notification = new
                    {
                        body = message,
                    },
                    data = data,
                    priority = "high"

                };*/
                var formatting = new JsonSerializerSettings();
                formatting.ContractResolver = new CamelCasePropertyNamesContractResolver();
                var json = JsonConvert.SerializeObject(responseData, formatting);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                tRequest.ContentLength = byteArray.Length;

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                //Response.Write(sResponseFromServer);
                                var setting = new JsonSerializerSettings();

                                return JsonConvert.DeserializeObject<FcmResponse>(sResponseFromServer);
                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {

            }

            return null;
        }

    }
}
