﻿using System.Configuration;

namespace MBoL.Utils
{
    public static class SettingHelper
    {
        public static int GetIntSetting(string key, int defaultValue)
        {
            int result;
            if (int.TryParse(ConfigurationManager.AppSettings[key], out result))
            {
                return result;
            }
            return defaultValue;
        }

        public static double GetDoubleSetting(string key, double defaultValue)
        {
            double result;
            if (double.TryParse(ConfigurationManager.AppSettings[key], out result))
            {
                return result;
            }
            return defaultValue;
        }

        public static string GetStringSetting(string key, string defaultValue = "")
        {
            var result = ConfigurationManager.AppSettings[key];

            if(string.IsNullOrEmpty(result))
            {
                return defaultValue;
            }

            return result;
        }

        internal static bool GetBooleanSetting(string key, bool defaultValue = false)
        {
            bool result;
            if (bool.TryParse(ConfigurationManager.AppSettings[key], out result))
            {
                return result;
            }
            return defaultValue;
        }
    }
}
