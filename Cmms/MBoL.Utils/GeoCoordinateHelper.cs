﻿using System;

namespace MBoL.Utils
{
    public class GeoCoordinateHelper
    {
        public static double GetDistance(double fromLatitude,
            double fromLongitude, 
            double toLatitude, 
            double toLongitude)
        {
            if ((double.IsNaN(fromLatitude) || double.IsNaN(fromLongitude)) 
                || (double.IsNaN(toLatitude) || double.IsNaN(toLongitude)))
            {
                throw new ArgumentException("Argument_LatitudeOrLongitudeIsNotANumber");
            }
            double d = fromLatitude * 0.017453292519943295;
            double num3 = fromLongitude * 0.017453292519943295;
            double num4 = toLatitude * 0.017453292519943295;
            double num5 = toLongitude * 0.017453292519943295;
            double num6 = num5 - num3;
            double num7 = num4 - d;
            double num8 = Math.Pow(Math.Sin(num7 / 2.0), 2.0) + ((Math.Cos(d) * Math.Cos(num4)) * Math.Pow(Math.Sin(num6 / 2.0), 2.0));
            double num9 = 2.0 * Math.Atan2(Math.Sqrt(num8), Math.Sqrt(1.0 - num8));
            return (6376500.0 * num9); //m
        }
    }
}
