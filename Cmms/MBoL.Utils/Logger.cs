﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace MBoL.Utils
{
    public class Logger
    {
        public static void WriteRequestLogAsync<T>(T entity) where T : class
        {
            if (Config.IsWriteRequestLog)
            {
                Task.Factory.StartNew(() =>
                {
                    WriteEntityLog(entity);
                });
            }
        }

        public static void WriteEntityLog<T>(T entity) where T : class
        {
            try
            {
                var type = entity.GetType();
                var properties = type.GetProperties();
                var logMessage = new StringBuilder();
                logMessage.AppendLine();
                foreach (var property in properties)
                {
                    logMessage.AppendLine(property.Name + " = " + property.GetValue(entity));
                }
                WriteLog(logMessage.ToString());
            }
            catch { }
        }

        public static void WriteLog(string logMessage, string logName = "", bool alwaysLog = false)
        {
            try
            {
                if (!alwaysLog && !Config.IsWriteRequestLog)
                {
                    return;
                }

                var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Temp");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                var filePath = Path.Combine(path, "log_" + logName + "_" + DateTime.Now.ToString("dd_MM_yy_HH_mm_ss_fff") + ".txt");

                using (StreamWriter writer = new StreamWriter(filePath))
                {
                    writer.WriteLine(logMessage);
                }
            }
            catch
            { }
        }
    }
}
