﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MBoL.Utils
{
    public class Validator<T>
    {
        public static void Require(T entity)
        {
            var type = entity.GetType();
            var exceptionMessage = string.Empty;

            foreach (var propertyInfo in type.GetProperties())
            {
                var value = propertyInfo.GetValue(entity);
                if (propertyInfo.GetCustomAttributes(false).Any(t => t is RequiredAttribute))
                {
                    if (propertyInfo.PropertyType == typeof(String))
                    {
                        var strValue = value as string;
                        if (string.IsNullOrEmpty(strValue))
                        {
                            exceptionMessage += string.Format("{0} is required /n/n", propertyInfo.Name);
                        }
                    }
                }
            }

            if(exceptionMessage != string.Empty)
            {
                throw new Exception(exceptionMessage);
            }
        }
    }
}
