﻿namespace MBoL.Utils
{
    public class UrlHelper
    {
        public static string GetDownloadDocumentUrl(string username, string stationCode, string documentCode)
        {
            return string.Format(Config.BaseUrl + "Users/{0}/Stations/{1}/Documents/{2}/Download", username, stationCode, documentCode);
        }
    }
}
