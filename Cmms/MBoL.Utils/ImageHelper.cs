﻿using System;
using System.Drawing;
using System.IO;

namespace MBoL.Utils
{
    public class ImageHelper
    {
        public static void SaveImageFromBase64(string filePath, string base64String)
        {
            byte[] bytes = Convert.FromBase64String(base64String);

            using (MemoryStream ms = new MemoryStream(bytes))
            {
                using (var image = Bitmap.FromStream(ms))
                {
                    image.Save(filePath);
                }
            }
        }

        public static string GetBase64FromImage(string fileName)
        {
            var filePath = Path.Combine(Config.DocumentDirectory,fileName);
            if(File.Exists(filePath))
            {
                using (var image = Image.FromFile(filePath))
                {
                    using (MemoryStream m = new MemoryStream())
                    {
                        image.Save(m, image.RawFormat);
                        byte[] imageBytes = m.ToArray();

                        // Convert byte[] to Base64 String
                        return Convert.ToBase64String(imageBytes);
                    }
                }
            }
            return string.Empty;
        }
    }
}
